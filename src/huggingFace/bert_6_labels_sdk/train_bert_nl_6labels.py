# -*- coding: utf-8 -*-
"""
Created on Tue Dec  5 16:31:36 2023

@author: gcara
"""
import logging, os
import numpy as np
import pandas as pd
from sklearn import metrics
import transformers
import torch
from torch.utils.data import Dataset, DataLoader, RandomSampler, SequentialSampler
from transformers import BertTokenizer, BertModel, BertConfig
from sklearn.metrics import accuracy_score, label_ranking_average_precision_score, recall_score
import pickle
from utilities import PlotDistribution, PlotConfusionMatrix, dataAugmentation


from torch import cuda
device = 'cuda' if cuda.is_available() else 'cpu'
import sys
sys.path.append("..") # Adds higher directory to python modules path.
from utilities import PlotDistribution, PlotConfusionMatrix, dataAugmentation
NUM_LABELS=6
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.ERROR)
logging.getLogger().setLevel(logging.ERROR)
model_selection='bert-base-uncased'
#gc='../../data/cozmo/huggingFaceDataSets/sdk_calls.p'
#dataset_path ='../../data/cozmo/huggingFaceDataSets/natural_language.p'
dataset_path ='../../data/cozmo/huggingFaceDataSets/sdk_calls.p'


logging.info('Selected model = {}'.format(model_selection))
logging.info('Selected dataset = {}'.format(dataset_path))

df = None
logging.info(dataset_path[:-1])
print(dataset_path[:-1])

#with open(dataset_path, 'rb') as file:
#with open(dataset_path[:-1], 'rb') as file:
with open(dataset_path, 'rb') as file:
    df = pickle.load(file)
logging.info('Dataset loaded')

labels = {
    'interest': 0,
    'alarm': 1,
    'confusion': 2,
    'understanding': 3,
    'frustration': 4,
    'relief': 5,
    'sorrow': 6,
    'joy': 7,
    'anger': 8,
    'gratitude': 9,
    'fear': 10,
    'hope': 11,
    'boredom': 12,
    'surprise': 13,
    'disgust': 14,
    'desire': 15
}
labels = {
    0: 'interest',
    1: 'alarm',
    2: 'confusion',
    3: 'understanding',
    4: 'frustration',
    5: 'relief',
    6: 'sorrow',
    7: 'joy',
    8: 'anger',
    9: 'gratitude',
    10: 'fear',
    11: 'hope',
    12: 'boredom',
    13: 'surprise',
    14: 'disgust',
    15: 'desire'
}


def get_array_from_label(label):
    ret_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ret_data[labels[label]] = 1
    return ret_data


def get_label_from_array(array):
    retLabel = array.index(1)
    return retLabel


# Labels Consolidation
Lbl1 = ['joy', 'hope']
Lbl2 = ['disgust', 'surprise', 'alarm', 'fear']
Lbl3 = ['interest', 'desire']
Lbl4 = ['understanding', 'gratitude', 'relief']
Lbl5 = ['anger', 'frustration']
Lbl6 = ['confusion', 'sorrow', 'boredom']

Arr1 = [1, 0, 0, 0, 0, 0]
Arr2 = [0, 1, 0, 0, 0, 0]
Arr3 = [0, 0, 1, 0, 0, 0]
Arr4 = [0, 0, 0, 1, 0, 0]
Arr5 = [0, 0, 0, 0, 1, 0]
Arr6 = [0, 0, 0, 0, 0, 1]

all_consolidations = [[Lbl1, Arr1],
                      [Lbl2, Arr2],
                      [Lbl3, Arr3],
                      [Lbl4, Arr4],
                      [Lbl5, Arr5],
                      [Lbl6, Arr6]]

def consolidate_label(arr):
    lb=labels[get_label_from_array(arr)]
    for l in all_consolidations:
        if lb in l[0]:
            return l[1]

def process_data(x):
    x['new_list']=consolidate_label(x['list'])
    return x
new_df=df.apply(process_data,axis=1)

# Defining some key variables that will be used later on in the training
MAX_LEN = 128
TRAIN_BATCH_SIZE = 32
VALID_BATCH_SIZE = 86
EPOCHS = 1000
LEARNING_RATE = 1.0000e-05

tokenizer = BertTokenizer.from_pretrained(model_selection)


class CustomDataset(Dataset):

    def __init__(self, dataframe, tokenizer, max_len):
        self.tokenizer = tokenizer
        self.data = dataframe
        self.comment_text = dataframe.comment_text
        self.targets = self.data.new_list
        self.max_len = max_len

    def __len__(self):
        return len(self.comment_text)

    def __getitem__(self, index):
        comment_text = str(self.comment_text[index])
        comment_text = " ".join(comment_text.split())

        inputs = self.tokenizer.encode_plus(
            comment_text,
            None,
            add_special_tokens=True,
            max_length=self.max_len,
            pad_to_max_length=True,
            return_token_type_ids=True
        )
        ids = inputs['input_ids']
        mask = inputs['attention_mask']
        token_type_ids = inputs["token_type_ids"]

        return {
            'ids': torch.tensor(ids, dtype=torch.long),
            'mask': torch.tensor(mask, dtype=torch.long),
            'token_type_ids': torch.tensor(token_type_ids, dtype=torch.long),
            'targets': torch.tensor(self.targets[index], dtype=torch.float)
        }


# Creating the dataset and dataloader for the neural network

train_size = 0.90
train_dataset = new_df.sample(frac=train_size, random_state=250)
test_dataset = new_df.drop(train_dataset.index).reset_index(drop=True)

dist = PlotDistribution()
max_label_train, max_label_test = dist.plot_distribution(
    df_train=train_dataset,
    df_test=test_dataset,
    fn='initial_distribution.png')

print(test_dataset.head())
train_dataset = train_dataset.reset_index(drop=True)

print("FULL Dataset: {}".format(new_df.shape))
print("TRAIN Dataset: {}".format(train_dataset.shape))
print("TEST Dataset: {}".format(test_dataset.shape))

training_set = CustomDataset(train_dataset, tokenizer, MAX_LEN)
testing_set = CustomDataset(test_dataset, tokenizer, MAX_LEN)

train_params = {'batch_size': TRAIN_BATCH_SIZE,
                'shuffle': True,
                'num_workers': 0
                }

test_params = {'batch_size': VALID_BATCH_SIZE,
               'shuffle': True,
               'num_workers': 0
               }

training_loader = DataLoader(training_set, **train_params)
testing_loader = DataLoader(testing_set, **test_params)


class BERTClass(torch.nn.Module):

    def __init__(self):
        super(BERTClass, self).__init__()
        self.l1 = transformers.BertModel.from_pretrained(model_selection)
        self.l2 = torch.nn.Dropout(0.3)
        self.l3 = torch.nn.Linear(768, NUM_LABELS)
        self.softmax = torch.nn.Softmax(dim=1)

    def forward(self, ids, mask, token_type_ids):
        _, output_1 = self.l1(ids, attention_mask=mask, token_type_ids=token_type_ids, return_dict=False)
        output_2 = self.l2(output_1)
        output = self.l3(output_2)
        return output


model = BERTClass()
model.to(device)


def loss_fn(outputs, targets):
    #return torch.nn.CrossEntropyLoss(weight=class_weights)(outputs, targets)
    return torch.nn.CrossEntropyLoss()(outputs, targets)
    #return torch.nn.BCEWithLogitsLoss()(outputs, targets)


optimizer = torch.optim.Adam(params=model.parameters(), lr=LEARNING_RATE)


def train(epoch):
    model.train()
    for _, data in enumerate(training_loader, 0):
        ids = data['ids'].to(device, dtype=torch.long)
        mask = data['mask'].to(device, dtype=torch.long)
        token_type_ids = data['token_type_ids'].to(device, dtype=torch.long)
        targets = data['targets'].to(device, dtype=torch.float)

        outputs = model(ids, mask, token_type_ids)

        loss = loss_fn(outputs, targets)
        # if _%10==0:
        print(f'Epoch: {epoch}, Loss:  {loss.item()}')
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # scheduler.step(loss)


# scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',
#    factor=0.01, threshold=0.0001, patience=3, verbose=True)


cm = PlotConfusionMatrix()


def validation(epoch):
    model.eval()
    fin_targets = []
    fin_outputs = []
    with torch.no_grad():
        for _, data in enumerate(testing_loader, 0):
            ids = data['ids'].to(device, dtype=torch.long)
            mask = data['mask'].to(device, dtype=torch.long)
            token_type_ids = data['token_type_ids'].to(device, dtype=torch.long)
            targets = data['targets'].to(device, dtype=torch.float)
            outputs = model(ids, mask, token_type_ids)
            fin_targets.extend(targets.cpu().detach().numpy().tolist())
            fin_outputs.extend(torch.sigmoid(outputs).cpu().detach().numpy().tolist())

            outval = [apply_max(a) for a in fin_outputs]
            accuracy = metrics.accuracy_score(fin_targets, outval)
            print(f"Accuracy Score = {accuracy}")
            # cm.plot_matrix(outputs=outval, targets=fin_targets, fn='cm.png')
    return fin_outputs, fin_targets


out = None
tar = None


def apply_max(data):
    ret_data = [0.0] * NUM_LABELS
    index = max(enumerate(data), key=lambda x: x[1])[0]
    ret_data[index] = 1.0
    return ret_data


cm = PlotConfusionMatrix()
for epoch in range(EPOCHS):
    train(epoch)
    outputs_t, targets = validation(epoch)
    outputs = [apply_max(a) for a in outputs_t]
    tar = targets
    out = outputs

    accuracy = metrics.accuracy_score(targets, outputs)

    # f1_score_micro = metrics.f1_score(targets, outputs, average='micro')
    # f1_score_macro = metrics.f1_score(targets, outputs, average='macro')

    print('plotting cm')
    cm.plot_matrix(outputs=outputs, targets=targets, fn='cmt.png')
    print(f"Accuracy Score = {accuracy}")
    # print(f"F1 Score (Micro) = {f1_score_micro}")
    # print(f"F1 Score (Macro) = {f1_score_macro}")

# open a file, where you ant to store the data
file = open('eval_data.p', 'wb')

# dump information to that file
pickle.dump([targets, outputs], file)

# close the file
file.close()
print('plotting cm')
cm.plot_matrix(outputs=outputs, targets=targets, fn='cm.png')





