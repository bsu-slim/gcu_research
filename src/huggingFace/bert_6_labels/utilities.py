#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 18:07:26 2023

@author: gerardocaracas
"""

import pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import random

labels_list=[
    'interest',
    'alarm',
    'confusion',
    'understanding',
    'frustration',
    'relief',
    'sorrow',
    'joy',
    'anger',
    'gratitude',
    'fear',
    'hope',
    'boredom',
    'surprise',
    'disgust',
    'desire'
    ]

labels={
        'interest':0,
        'alarm':1,
        'confusion':2,
        'understanding':3,
        'frustration':4,
        'relief':5,
        'sorrow':6,
        'joy':7,
        'anger':8,
        'gratitude':9,
        'fear':10,
        'hope':11,
        'boredom':12,
        'surprise':13,
        'disgust':14,
        'desire':15
        }
index_map={
    0:'interest',
    1:'alarm',
    2:'confusion',
    3:'understanding',
    4:'frustration',
    5:'relief',
    6:'sorrow',
    7:'joy',
    8:'anger',
    9:'gratitude',
    10:'fear',
    11:'hope',
    12:'boredom',
    13:'surprise',
    14:'disgust',
    15:'desire'
}

def get_label_array(label):
    ret_data=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    ret_data[labels[label]]=1
    return ret_data

def get_label_from_array(arr):
    return index_map[arr.index(1)]

class PlotDistribution:
    def __init__(self):
        pass

    
    def plot_distribution(self, df_train='', df_test='', fn=''):
        distribution_train=[]
        distribution_test=[]
        all_values_train=df_train['list'].value_counts()
        all_values_test=df_test['list'].value_counts()
    
        for index, value in all_values_train.items():
            distribution_train.append([get_label_from_array(index), value])
            
        for index, value in all_values_test.items():
            distribution_test.append([get_label_from_array(index), value])
            
        lbls_train=[a[0] for a in distribution_train]
        counts_train=[a[1] for a in distribution_train]
        
        lbls_test=[a[0] for a in distribution_test]
        counts_test=[a[1] for a in distribution_test]
            
        values=[counts_train, counts_test]
        print(len(values))
    
        n = len(values)                # Number of bars to plot
        w = .35                        # With of each column
        x = np.arange(0, len(lbls_train))   # Center position of group on x axis
        label_bar=['train','test']
        for i, value in enumerate(values):
            position = x + (w*(1-n)/2) + i*w
            plt.bar(position, value, width=w, label=label_bar[i])
    
        plt.xticks(x, lbls_train);
    
        plt.ylabel('Samples')
        plt.xticks(rotation=90)
        plt.legend()
        plt.savefig(fn)
        
        return [max(all_values_train.tolist()), max(all_values_test.tolist())]
        
class PlotConfusionMatrix:
    
    
    def __init__(self):
        pass
        
    def plot_matrix(self, outputs='', targets='', fn=''):
        targets_labels=[]
        outputs_labels=[]
        for t in targets:
            targets_labels.append(get_label_from_array(t))
        for t in outputs:
            outputs_labels.append(get_label_from_array(t))
    
        cm = confusion_matrix(targets_labels, outputs_labels)
        print(cm)
        return
        #cmd_obj = ConfusionMatrixDisplay(cm, display_labels=labels_list)
        #cmd_obj.plot()
        '''cmd_obj.ax_.set(
                        title='Sklearn Confusion Matrix with labels!!', 
                        xlabel='Predicted emotion', 
                        ylabel='Target emotion')
        plt.xticks(rotation=90)
        '''
            
        #cmd_obj.savefig(fn)
        
class dataAugmentation:
    labels={
        'interest':0,
        'alarm':1,
        'confusion':2,
        'understanding':3,
        'frustration':4,
        'relief':5,
        'sorrow':6,
        'joy':7,
        'anger':8,
        'gratitude':9,
        'fear':10,
        'hope':11,
        'boredom':12,
        'surprise':13,
        'disgust':14,
        'desire':15
        }
    
    index_map={
        0:'interest',
        1:'alarm',
        2:'confusion',
        3:'understanding',
        4:'frustration',
        5:'relief',
        6:'sorrow',
        7:'joy',
        8:'anger',
        9:'gratitude',
        10:'fear',
        11:'hope',
        12:'boredom',
        13:'surprise',
        14:'disgust',
        15:'desire'
    }
    
    labels_list=[
    'interest',
    'alarm',
    'confusion',
    'understanding',
    'frustration',
    'relief',
    'sorrow',
    'joy',
    'anger',
    'gratitude',
    'fear',
    'hope',
    'boredom',
    'surprise',
    'disgust',
    'desire'
    ]

    
    def __init__(self):
        pass
    
    def get_label_array(self, label):
        ret_data=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        ret_data[self.labels[label]]=1
        return ret_data

    def get_label_from_array(self, arr):
        return self.index_map[arr.index(1)]
    
    def remove_element(self, data_in):
        d=data_in.split('.')[0:-1]
        ret_data=d.copy()
        len_row=len(ret_data)
        # Select element to remove
        sel_word=random.randrange(len_row)
        del ret_data[sel_word]
        # Convert into single string
        final_string=''
        for x in ret_data:
            final_string = final_string + x + '. '
        return final_string
    
    def data_augmentation(self, data_samples='', target_samples=0):
        aug_data=[]
        num_samples=len(data_samples)
        needed_samples=0
        if num_samples >= target_samples:
            print('No need to augment data')
        else:
            needed_samples=target_samples-num_samples
            print('Augmenting {} samples'.format(needed_samples))

        # Selecting random rows to augment
        for i in range(needed_samples):
            sel_row=random.randrange(num_samples)
            aug_data.append(self.remove_element(data_samples[sel_row]))

        return aug_data
    def augment(self, x):
            l=self.get_label_from_array(x[1])
            return l

    def perform_augmentation(self, new_df=None, target_samples=0):
        
        # Remove rows with empty text
        print("before ",new_df.shape)
        new_df['comment_text'].replace('', np.nan, inplace=True)
        new_df.dropna(subset=['comment_text'], inplace=True)
        print("after ",new_df.shape)
        
        
        new_df['label']=new_df.apply(self.augment, axis=1, raw=True)

        all_dataframes=[]
        for emotion in self.labels_list:
            # Filter all dataframes
            filter_df = new_df[new_df['label']==emotion].copy()
            # Get all the texts
            text_list=filter_df['comment_text'].tolist()
            # Perform augmentation
            a_text=self.data_augmentation(data_samples=text_list, 
                                          target_samples=target_samples)
            # Create a dataframe array now
            text_all=[]
            for t in a_text:
                text_all.append([t, self.get_label_array(emotion), str(emotion)])

            # Create dataframe
            df = pd.DataFrame(text_all,
                       columns =['comment_text','list','label'])

            # Append to list of dataframes
            all_dataframes.append(df.copy())

        # Add augmented data into main dataset

        all_dataframes.append(new_df.copy())
        final_ds = pd.concat(all_dataframes)
        return final_ds
    
    
