# -*- coding: utf-8 -*-
import os


from data_creation_base import JsonProcessor

jp = JsonProcessor()

jp.read_dataset(path='../../data/cozmo/labeled_dataset_from_slim/scaled_curated_full_dataset.p')

# Process all text files with sdk calls.
all_files = os.listdir('../../data/cozmo/json_files/')
for file in all_files:
    jp.read_json(path='../../data/cozmo/json_files/', filename=file)
    jp.convert_json_to_progPrompt()
