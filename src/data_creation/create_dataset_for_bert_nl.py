# -*- coding: utf-8 -*-
import os, json, sys
import numpy as np
import pandas as pd
import copy
import pickle
import logging
from pathlib import Path
import pandas as pd


logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

original_dataset=None
with open('../../data/cozmo/labeled_dataset_from_slim/scaled_curated_full_dataset.p', 'rb') as f:
    original_dataset = pickle.load(f) 
    
def get_label(filename):
    if ((original_dataset['file_name'] == filename)).any():
        data=original_dataset.loc[original_dataset['file_name'] == filename, 'label'].values[0]
        return data
    else:
        return None
    
labels={
        'interest':0,
        'alarm':1,
        'confusion':2,
        'understanding':3,
        'frustration':4,
        'relief':5,
        'sorrow':6,
        'joy':7,
        'anger':8,
        'gratitude':9,
        'fear':10,
        'hope':11,        
        'boredom':12,
        'surprise':13,
        'disgust':14, 
        'desire':15
        }

def get_label_array(label):
    ret_data=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    ret_data[labels[label]]=1
    return ret_data
    

    
path_converted_sdk='../../data/cozmo/natural_language_converted/'

def generate_full_sentence(filename=''):
    lines=''
    with open(path_converted_sdk+filename) as sdk_file:
        lines = sdk_file.readlines()
        
    # Extract animation name
    anim_name = lines[1].split(' ')[1]
    
    # Now get label from our dataset
    label = get_label(anim_name)
    if label == None:
        return None, None, None
    
    list_label = get_label_array(label)
    
    # Extract data from animation and get only sdk calls and timestamps
    # but still unsorted
    all_lines=[]
    # Read next line in sdk file
    for line in lines:
        # If that line is not a comment but rather a function call
        if "Trigger time ms" in line:
            # Separate timestamp from sdk call
            x = line.split(" --> ")
            # time is the numeric value from the timestamp
            time=int(x[0][18::])
            # Sentence is the secod part of the split list
            sentence_t=x[1]
            # Remove the \n at the end of the sentence and
            # adding a dot at the end to see if this helps the classifier, letting it know
            # it's the end of the sentence.
            sentence = sentence_t[:-1] + '.'
            # Create a new structure of [ts, sentence]
            line=[time,sentence] 
            # append to all lines
            all_lines.append(line)
            
    # Now we need to sort all the calls based on the timestamp
    # and save it in a dataframe so we can sort it
    df = pd.DataFrame(all_lines, columns=['time', 'sentence'])
    df_lst = df.sort_values(by=['time'])
    
    # Convert now into list
    lst = df_lst['sentence'].tolist()
    
    # Here is where we flat the entire list so we can
    # generate a single sentence.
    full_sentence=''
    for call in lst:
        full_sentence = full_sentence+' '+call
    
    return 0, list_label, full_sentence #error, label, sentence
    
all_files = os.listdir('../../data/cozmo/sdk_function_calls_converted/')

list_dataset=[]
for sdk_file in all_files:
    if sdk_file == '.ipynb_checkpoints':
        continue
    err, label, full_sentence = generate_full_sentence(filename=sdk_file)
    if err == 0:
        list_dataset.append([full_sentence, label])  
        
new_df = pd.DataFrame(list_dataset, columns=['comment_text', 'list'])

pickle.dump( new_df, open( "../../data/cozmo/huggingFaceDataSets/natural_language.p", "wb" ) )

logging.info("Dataset created. Exiting")

