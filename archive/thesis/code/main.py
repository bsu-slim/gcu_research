import sys
sys.path.append("pkg/")
import logging
import numpy as np
import pandas as pd

from data import data
from feature_extraction import feature_extraction
from social_agent import social_agent

logging.basicConfig(level=logging.INFO, format='%(levelname)s: file[%(name)s] line number[%(lineno)d]: %(message)s')

logging.info('Init program')
my_data=data(log=logging)
fe = feature_extraction(log=logging)
sa = social_agent(log=logging)

x1 = []
y1 = []
datasets = []
my_data.read_main_data()
for v in my_data.valence_values:
    logging.info('Processing {}'.format(v))
    d, y = my_data.get_subset_w_y(affect=v, mutuallyExclusive=False)
    x2 = []
    y2 = []
    for datagrams in d:
        if isinstance(datagrams, pd.Series):
            continue
        x2.append(sa.generate_vector(l1=fe.extend_1(df=datagrams),
                                     l3=fe.extend_3(df=datagrams),
                                     l5=fe.extend_5(df=datagrams),
                                     l6=fe.extend_6(df=datagrams),
                                     l8=fe.extend_8(df=datagrams),
                                     l9=fe.extend_9(df=datagrams),
                                     l11=fe.high_strenght_11(df=datagrams),
                                     l12=fe.low_strenght_12(df=datagrams),
                                     l18=fe.extend_18(df=datagrams)))
        y2.append(y)

    x1.append(x2)
    y1.append(y)

# Now convert labels
logging.info('Converting labels')
y=[]
X=[]
for xt,yt in zip(x1,y1):
    # Valences
    for xxt,yyt in zip(xt,yt):
        # samples
        X.append(xxt)
        y.append(my_data.convert_binary(yt))

y=np.asarray(y)
X=np.asarray(X)

n_classes = y.shape[1]
num_features = X.shape[1]

classifier,  X_test, y_test=sa.classify_rnd_forest(X, y, '2,3,5,6,8,11,12,18', n_classes, num_features)

sa.Print_Metrics(classifier, X_test, y_test)

sa.Plot_predictor_importance(classifier, ['1','3','5','6','8','9','11','12','18'])