'''
Main parameters
Approach
 5.- Extend or expand it's body.- Hability to expand
     itself.
    It will be measured by the height of lift, which
    will be the percentage of lifting arms above 40 degrees.



Avoidance

Energy
  11 - High Strenght.- Motor's speed at high speed.
       It will be measured by the percentage of a full
       animation at least at speed:10 mm/s
  12 - Low Strenght.- Motor's speed at low level
       It will be measured by the percentage of a full
       animation less than:10mm/s

Flow

'''
import sys
sys.path.append("pkg/")
import logging
import matplotlib.pyplot as plt
import logging
import numpy as np
from itertools import cycle

from sklearn.metrics import roc_curve, auc
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.multiclass import OneVsRestClassifier
from sklearn import svm
from scipy import interp

import sklearn
from sklearn.preprocessing import OneHotEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn import metrics
from sklearn.neighbors import KNeighborsClassifier

from sklearn.metrics import roc_auc_score, roc_curve, auc, classification_report
from sklearn.metrics import f1_score, precision_score, recall_score
from sklearn.metrics import mean_squared_error, cohen_kappa_score, make_scorer
from sklearn.metrics import confusion_matrix, accuracy_score, average_precision_score
from sklearn.metrics import precision_recall_curve, SCORERS
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit
from sklearn.externals import joblib

from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel


from operator import itemgetter

from tabulate import tabulate




from data import data




class social_agent():
    def __init__(self,log=''):
        self.logging = log
        self.logging.info('social agent new instance')
        self.my_data = data(log=log)

        

    def classify_lin_reg(self, X_train, y_train, X_validation, y_validation, X_test, y_test, label):
        n_classes = 1
        X_tr = np.array(X_train)
        X_tr.shape
        X_te = np.array(X_test)
        logit1 = LogisticRegression()
        y_score = logit1.fit(X_tr, y_train).decision_function(X_te)
        y_score = (y_score).reshape(-1, 1)
        print("Score for is {}".format(str(logit1.score(X_te, y_test))))
        # Compute ROC curve and ROC area for each class
        fpr = dict()  # False positive rate
        tpr = dict()  # True positive rate
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

        # Compute micro-average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        lw = 2
        plt.plot(fpr[0], tpr[0], color='darkorange',
                 lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[0])
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title(label)
        plt.legend(loc="lower right")
        plt.show()

    def generate_vector(self, l1='',l3='', l5='', l6='',l8='', l9='',l11='', l12='', l18=''):
        return [l1, l3, l5, l6, l8, l9, l11, l12, l18]
    
    def classify_rnd_forest(self, X,y , label, n_classes, num_features):
        # shuffle and split training and test sets
        random_state = np.random.RandomState(0)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.2,
                                                            random_state=0)
        classifier = RandomForestClassifier(class_weight = 'balanced',
                                         n_estimators=100,
                                         max_depth=46,
                                         min_samples_leaf=32,
                                         max_features='auto',
                                         min_samples_split=4,
                                         criterion='gini',
                                         oob_score=True,
                                         verbose=1
                                        )
        classifier.fit(X_train, y_train)
        estimator=SelectFromModel(classifier)
        y_score = classifier.predict(X_test)

        # Compute ROC curve and ROC area for each class
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

        # Compute micro-average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        print(cross_val_score(classifier,X_test,y_test, scoring= 'accuracy'))
        
        lw=2
        all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

        # Then interpolate all ROC curves at this points
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr[i], tpr[i])

        # Finally average it and compute AUC
        mean_tpr /= n_classes

        fpr["macro"] = all_fpr
        tpr["macro"] = mean_tpr
        roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

        # Plot all ROC curves
        plt.figure(figsize=(15,15))
        plt.plot(fpr["micro"], tpr["micro"],
                 label='micro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["micro"]),
                 color='deeppink', linestyle=':', linewidth=4)

        plt.plot(fpr["macro"], tpr["macro"],
                 label='macro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["macro"]),
                 color='navy', linestyle=':', linewidth=4)

        colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], lw=lw,
                     label='ROC curve of class {0} (area = {1:0.2f})'
                     ''.format(self.my_data.valence_values[i], roc_auc[i]))

        plt.plot([0, 1], [0, 1], 'k--', lw=lw)
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title(label)
        plt.legend(loc="best")
        plt.show()
        return classifier,  X_test, y_test

    def classify_knn(self, X, y):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.2,
                                                            random_state=0,stratify=y)
        classifier = KNeighborsClassifier(3)
        classifier.fit(X_train, y_train)
        acc = classifier.score(X_test, y_test)
        return classifier, acc, X_test, y_test

        
    def Print_Metrics(self, model, X_test, y_test):
        print('\nModel performance on the test data set:')

        # print('Train Accuracy.......', accuracy_score(y_train, best_model.predict(X_train)))
        # print('Validate Accuracy....', accuracy_score(y_valid, best_model.predict(X_valid)))

        y_predict_test  = model.predict(X_test)
        mse             = metrics.mean_squared_error(y_test, y_predict_test)
        logloss_test    = metrics.log_loss(y_test, y_predict_test)
        accuracy_test   = metrics.accuracy_score(y_test, y_predict_test)
        accuracy_test2  = model.score(X_test, y_test)
        F1_test         = metrics.f1_score(y_test, y_predict_test, average='samples')
        precision_test  = precision_score(y_test, y_predict_test, average='samples')
        precision_test2 = metrics.precision_score(y_test, y_predict_test, average='samples')
        recall_test     = recall_score(y_test, y_predict_test, average='samples')
        #auc_test        = metrics.roc_auc_score(y_test, y_predict_test)
        r2_test         = metrics.r2_score(y_test, y_predict_test)

        #test_auc       = h2o.get_model("best_rf").model_performance(test_data=test).auc()
        #print('Best model performance based on auc: ', test_auc)

        header = ["Metric", "Test"]
        table  = [
                   ["logloss",   logloss_test],
                   ["accuracy",  accuracy_test],
                   ["precision", precision_test],
                   ["F1",        F1_test],
                   ["r2",        r2_test],
                   ["Recall",    recall_test],
                   ["MSE",       mse]
                 ]

        print(tabulate(table, header, tablefmt="fancy_grid"))
        
    def Plot_predictor_importance(self,classifier, feature_columns):
        feature_importances = classifier.feature_importances_
        sorted_idx = np.argsort(feature_importances)
        y_pos  = np.arange(sorted_idx.shape[0]) + .5
        fig, ax = plt.subplots()
        ax.barh(y_pos, 
                feature_importances[sorted_idx], 
                align='center', 
                color='green', 
                ecolor='black', 
                height=0.5)
        ax.set_yticks(y_pos)
        ax.set_yticklabels(feature_columns)
        ax.invert_yaxis()
        ax.set_xlabel('Relative Importance')
        ax.set_title('Predictor Importance')
        plt.show()