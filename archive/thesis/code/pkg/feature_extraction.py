import logging
import numpy as np
import sys
sys.path.append("pkg/")
'''
Main parameters
Approach
1
  3.- Move bodyforward
  5.- Extend or expand it's body.- Hability to expand
     itself.
    It will be measured by the height of lift, which
    will be the percentage of lifting arms above 40 degrees.
  

Avoidance
6
   8 - Move Backwards

Energy
  11 - High Strenght.- Motor's speed at high speed.
       It will be measured by the percentage of a full
       animation at least at speed:10 mm/s
  12 - Low Strenght.- Motor's speed at low level
       It will be measured by the percentage of a full
       animation less than:10mm/s

Flow
  18 .- High change in tempo

'''
from social_agent import social_agent
import pandas as pd

class feature_extraction():
    def __init__(self,log=''):
        self.logging = log
        self.logging.info('feature extraction new instance')
        self.speed_threshold = 10 #10 mm/s
        self.lift_threshold = 60 #degrees
        self.lift_threshold_low = 40 #degrees
        self.change_speed = 10 # Each change in 10 mm/s we will record it as change in speed
        self.valence_values = ['interest', 'alarm', 'confusion', 'understanding', 'frustration', 'relief',
                          'sorrow', 'joy', 'anger', 'gratitude', 'fear', 'hope', 'boredom',
                          'surprise', 'disgust', 'desire']
        #self.sa = social_agent(log=log)

    def extend_1(self, df=''):
        data = list(df.head_angle_degs)
        cnt = 0.00001;
        val=0
        for a in data:
            if a > 0:
                val = val + a
                cnt = cnt + 1
        return val / cnt

    def get_1(self, df_list=''):
        retVal = []
        for d in df_list:
            retVal.append(self.extend_1(df=d))
        return retVal
    # returns percentage of moving forward
    def extend_3(self, df=''):
        data = list(df.pose_0)
        moving=list(df.are_wheels_moving)
        count = 0;
        for a,move in zip(data,moving):
            if (a > 0) and (move == 1):
                count = count + 1
        return count / len(data)

    def get_3(self, df_list=''):
        retVal = []
        for d in df_list:
            retVal.append(self.extend_3(df=d))
        return retVal

    # returns percentage of lift above 40 degrees
    def extend_5(self, df=''):
        data = list(abs(df.lift_position_height))
        count = 0;
        for a in data:
            if a >= self.lift_threshold:
                count = count + 1
        return count / len(data)

    def get_5(self, df_list=''):
        height = []
        for d in df_list:
            height.append(self.extend_5(df=d))
        return height

    def extend_6(self, df=''):
        data = list(df.head_angle_degs)
        cnt = 0.00001;
        val=0
        for a in data:
            if a < 0:
                val = val + a
                cnt = cnt + 1
        return val /cnt

    def get_6(self, df_list=''):
        retVal = []
        for d in df_list:
            retVal.append(self.extend_6(df=d))
        return retVal

    # returns percentage of moving backwards
    def extend_8(self, df=''):
        data = list(df.pose_0)
        moving = list(df.are_wheels_moving)
        count = 0;
        for a,move in zip(data,moving):
            if (a < 0) and (move == 1):
                count = count + 1
        return count / len(data)

    def get_8(self, df_list=''):
        retVal = []
        for d in df_list:
            retVal.append(self.extend_8(df=d))
        return retVal

    # returns percentage of lift below 40 degrees
    def extend_9(self, df=''):
        data = list(abs(df.lift_position_height))
        count = 0;
        for a in data:
            if a <= self.lift_threshold_low:
                count = count + 1
        return count / len(data)

    def get_9(self, df_list=''):
        height = []
        for d in df_list:
            height.append(self.extend_9(df=d))
        return height
    # returns percentage of high speed over all animation
    def high_strenght_11(self, df=''):
        # We just need one speed, so let's just select
        # left_wheel_speed. We don't care the difference
        # of both speeds for this metric. Also it will be
        # the absolute value of speed, we don't care the
        # direction.
        data = list(abs(df.left_wheel_speed))
        moving = list(df.are_wheels_moving)
        count=0;
        for a,move in zip(data,moving):
            if (a >= self.speed_threshold) and (move == 1):
                count=count+1
        return count/len(data)

    def get_11(self, df_list=''):
        speeds = []
        for d in df_list:
            speeds.append(self.high_strenght_11(df=d))
        return speeds

        # returns percentage of low speed over all animation
    def low_strenght_12(self, df=''):
        # We just need one speed, so let's just select
        # left_wheel_speed. We don't care the difference
        # of both speeds for this metric. Also it will be
        # the absolute value of speed, we don't care the
        # direction.
        data = list(abs(df.left_wheel_speed))
        moving = list(df.are_wheels_moving)
        count = 0;
        for a,move in zip(data,moving):
            if (a <self.speed_threshold) and (move == 1):
                count = count + 1
        return count / len(data)

    def get_12(self, df_list=''):
        speeds = []
        for d in df_list:
            speeds.append(self.low_strenght_12(df=d))
        return speeds




    # returns number of times speed changed
    def extend_18(self, df=''):
        data = list(df.left_wheel_speed)
        past_value=data[0]
        count = 0;
        moving = list(df.are_wheels_moving)
        for a, move in zip(data, moving):
            if (abs(a - past_value) > self.change_speed) and (move == 1):
                count = count + 1
            past_value=a
        return count

    def get_18(self, df_list=''):
        retVal = []
        for d in df_list:
            retVal.append(self.extend_18(df=d))
        return retVal

    def get_labels(self, df_list=''):
        labels=[]
        for a in df_list:
            if isinstance(a, pd.Series):
                continue
            labels.append(list(a.Y)[0])
        #labels=[list(a.Y)[0] for a in df_list]
        #return np.asarray(labels)
        return labels
    def get_vectors(self, table):
        val=[]
        y=[]
        for d in table:
            if isinstance(d, pd.Series):
                val.append([0]*9)
                y.append([])
                continue
            val.append(self.sa.generate_vector(l1=self.extend_1(df=d),
                                               l3=self.extend_3(df=d),
                                               l5=self.extend_5(df=d),
                                               l6=self.extend_6(df=d),
                                               l8=self.extend_8(df=d),
                                               l9=self.extend_9(df=d),
                                               l11=self.high_strenght_11(df=d),
                                               l12=self.low_strenght_12(df=d),
                                               l18=self.extend_18(df=d)))
            #y.append(self.get_labels(df_list=d))
            y.append(d.Y.tolist()[0])
        return val, y
    def get_vectors_x (self, table):
        val=[]
        for d in table:
            if isinstance(d, pd.Series):
                val.append([0]*9)
                continue
            val.append(self.sa.generate_vector(l1=self.extend_1(df=d),
                                               l3=self.extend_3(df=d),
                                               l5=self.extend_5(df=d),
                                               l6=self.extend_6(df=d),
                                               l8=self.extend_8(df=d),
                                               l9=self.extend_9(df=d),
                                               l11=self.high_strenght_11(df=d),
                                               l12=self.low_strenght_12(df=d),
                                               l18=self.extend_18(df=d)))
        return val

    # This method gets the time series based data
    # and converts it into the 9 feature based static data
    def feature_transformation(self, table=[], y_value=[1,0]):
        val=[]
        for d in table:
            if isinstance(d, pd.Series):
                continue
            val.append(self.sa.generate_vector(l1=self.extend_1(df=d),
                                               l3=self.extend_3(df=d),
                                               l5=self.extend_5(df=d),
                                               l6=self.extend_6(df=d),
                                               l8=self.extend_8(df=d),
                                               l9=self.extend_9(df=d),
                                               l11=self.high_strenght_11(df=d),
                                               l12=self.low_strenght_12(df=d),
                                               l18=self.extend_18(df=d)))

        y_len = len(val)
        y = [y_value]*y_len
        return val, y


            
    

