import logging
import pickle
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split



class data():
    def __init__(self,log=''):
        self.logging = log
        self.main_data=''
        self.valence_values = ['interest', 'alarm', 'confusion', 'understanding', 'frustration', 'relief',
                          'sorrow', 'joy', 'anger', 'gratitude', 'fear', 'hope', 'boredom',
                          'surprise', 'disgust', 'desire']
        self.vd={
            'interest':0,
            'alarm':1, 
            'confusion':2, 
            'understanding':3, 
            'frustration':4, 
            'relief':5,
            'sorrow':6, 
            'joy':7, 
            'anger':8, 
            'gratitude':9, 
            'fear':10, 
            'hope':11, 
            'boredom':12,
            'surprise':13, 
            'disgust':14, 
            'desire':15
        }
        self.seq_len = 60
        logging.info('Init Data')

    def read_main_data(self):
        self.main_data = pickle.load(open("data/all_affect_dataframe.p", "rb"))

    def get_subset(self, affect='', mutuallyExclusive=True):
        self.read_main_data()
        retVal = []

        for a in range(0, len(self.main_data), 60):
            tmp = (self.main_data[a:a + 60])
            if ((mutuallyExclusive == True) and ((np.sum(list(tmp.Y)[0])) > 1)):
                continue
            # remove Y column
            #tmp2 = tmp.drop(['Y'], axis=1)
            tmp3 = tmp.loc[(tmp != 0).any(1)]
            feat = (list(tmp.Y))[0]
            features = []
            for i in range(len(feat)):
                if 1 in feat:
                    features.append(self.valence_values[feat.index(1)])
                    feat[feat.index(1)] = 0
                else:
                    break

            for f in features:
                if f == affect:
                    retVal.append(tmp3)

        return retVal

    def remove_empty(self, x):
        if np.sum(x[16]) == 0:
            return None
        else:
            return x

    def get_labels(self, animation):
        animation = list(animation)
        retVal = []
        sample = animation[0]  # all animation has the same
                               # vector, we just take the
                               # first one
        for feat, affect in zip(sample, self.valence_values):
            if feat == 1:
                retVal.append(affect)
        return retVal

    def get_subset_w_y(self, affect='', mutuallyExclusive=True):
        self.read_main_data()
        retVal = []
        retY=[]
        for a in range(0, len(self.main_data), 60):
            tmp = (self.main_data[a:a + 60])
            if ((mutuallyExclusive == True) and ((np.sum(list(tmp.Y)[0])) > 1)):
                continue
            lb = self.get_labels(tmp['Y'])
            if len(lb) > 0:
                if affect in lb:
                    df1 = tmp.apply(lambda x: self.remove_empty(x), axis=1)
                    df2 = df1.dropna()
                    retVal.append(df2)
                    retY.append(lb)
        return retVal, retY

    def plot_histogram(self, data='', bins=''):
        _, bins, _ = plt.hist(data, bins=bins)
        plt.show()

    def compare_histograms(self, data1='', data2='' , label1='', label2='', bins=10):
        _, bins, _ = plt.hist(data1, bins=bins, alpha=0.5,label=label1)
        _ = plt.hist(data2, bins=bins, alpha=0.5,
                     label=label2)
        plt.legend(loc='best')

        plt.show()

    # Basic split of train, test
    def splitdf(self, df1='', df2=''):
        y1 = [1] * len(df1)
        y2 = [0] * len(df2)
        X = df1 + df2
        Y = y1 + y2
        X_train, X_test, y_train, y_test = train_test_split(X, Y,
                                                            test_size=0.33,
                                                            random_state=0)
        return X_train, X_test, y_train, y_test


    # Setup data for classification, it will have only one class,
    # Thus this is for binary classification.
    # It will return 1 for val1, and 0 for val2.
    # Also it will return train, validation, test data sets.
    # vals are actually tables of 1 dimension values
    def get_data_for_classification_2_vals(self, val1,val2):
        X_train_t, X_test, y_train_t, y_test = self.splitdf(df1=val1, df2=val2)

        X_train, X_validation, y_train, y_validation = train_test_split(X_train_t, y_train_t,
                                                                        test_size=0.33,
                                                                        random_state=0)
        return X_train, y_train, X_validation, y_validation, X_test, y_test
    
    def add_multiclass(self, array_lists):
        retVal=[]
        for lists in array_lists:
            for list_t in lists:
                retVal.append(list_t)
        return retVal
    
    def convert_binary(self, a):
        ret=[0]*16
        for i in a:
            ret[self.vd[i]]=1
        return ret
