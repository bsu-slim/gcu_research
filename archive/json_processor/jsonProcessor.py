#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  3 15:00:09 2023

@author: gerardocaracas
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os, json, sys
import numpy as np
import pandas as pd
import copy
import pickle
import logging
import cozmo
from pathlib import Path



logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

'''
Developing flags
'''
SAVE_RESULTS_TO_FILE=True

'''
Global constants
'''
MIN_LIFT_HEIGHT_MM = 32.0
MAX_HEAD_ANGLE = cozmo.util.degrees(44.5)
MIN_HEAD_ANGLE = cozmo.util.degrees(-25)
MAX_LIFT_HEIGHT_MM = 92.0

# track widht is needed for the math to map from
# raidus to left/right wheel speed
TRACK_WIDTH = 45.0 # mm
    
    
class JsonProcessor:
    
    
    
    def __init__(self, destination='data/converted/'):
        self.destination=destination
        self.dataset=pd.DataFrame()
        self.json_dicts=dict()
        self.no_process_files=[
            '.ipynb_checkpoints'
        ]
        self.json_filename=''
        
    def get_number_clips(self):
        # Validate we have data to process
        if len(self.json_dicts) == 0:
            logging.error("Call read_json() first")
            return -1
        return len(self.json_dicts['clips'])
            
    
    def read_dataset(self, path=''):
        file = open(path, 'rb')
        self.dataset = pickle.load(file)
        file.close()
        
    def _validate_files(self, data:list):
        # Remove common directories that are not in fact files
        tmp_list=[]
        for f in data:
            if f not in self.no_process_files:
                tmp_list.append(f)
        return tmp_list
        
    def read_json(self, path='', filename=''):
        self.json_filename=filename
        with open(path+filename) as user_file:
                self.json_dicts= json.loads(user_file.read())
    
    def convert_ms_to_sec(self, data):
        # This layer will allow us to fine tune any conversioin
        result=data/1000
        logging.debug("Converted {} ms into {} seconds".format(data, result))
        return result
    
    def convert_height_mm(self, data):
        # to tune height in mm
        return data
    
    def convert_angle_degree(self, data):
        # to tune degrees
        return data
    
    def convert_radius_mm(self, data):
        # to tune radius mm
        return data
    
    def tune_speed_mmps(self, data):
        # to tune speed
        return data
    
    def tune_turn_in_place(self, speed):
        # Need to convert this metric. From duration/speed to degrees
        # and define degrees or radians
        # This is just temporarily.
        return speed
    
    def tune_wheels_speed(self, radius_mm, speed):
        # Found how pycozmo do this mapping, 
        # https://github.com/zayfod/pycozmo/blob/1b6dcd9b869a3784f1d8b02e820bb033f95fd13a/pycozmo/anim.py#L85
        # this is their implementaiton
        
        # we need to tune this, the values are getting out of range. max speed is 200mmps
        vl = float(speed) * (float(radius_mm) - TRACK_WIDTH / 2.0)
        vr = float(speed) * (float(radius_mm) + TRACK_WIDTH / 2.0)
        return vl, vr
    
    def process_body_motion(self, data):
        '''
         async def drive_wheels(self, l_wheel_speed, r_wheel_speed,
                                 l_wheel_acc=None, r_wheel_acc=None, duration=None):
        'BodyMotionKeyFrame'
        {'triggerTime_ms': 693,
        'durationTime_ms': 495,
        'radius_mm': 'STRAIGHT',
        'speed': -38
        }
        '''
        triggerTime_ms=data['triggerTime_ms']
        durationTime_sec=self.convert_ms_to_sec(data['durationTime_ms'])
        radius_mm=self.convert_radius_mm(data['radius_mm'])
        speed=self.tune_speed_mmps(data['speed'])
        if radius_mm == 'STRAIGHT':
            sdk_conversion="Trigger time ms = "+str(triggerTime_ms)+" --> "+"drive_wheels(l_wheel_speed="+str(speed)+", r_wheel_speed="+str(speed)+", duration="+str(durationTime_sec)+")"
        elif radius_mm == 'TURN_IN_PLACE':
            degrees=self.tune_turn_in_place(speed)
            sdk_conversion="Trigger time ms = "+str(triggerTime_ms)+" --> "+"turn_in_place(degrees("+str(degrees)+"))"
        else:
            left_sp, right_sp = self.tune_wheels_speed(radius_mm, speed)
            sdk_conversion="Trigger time ms = "+str(triggerTime_ms)+" --> "+"drive_wheels(l_wheel_speed="+str(left_sp)+", r_wheel_speed="+str(right_sp)+", duration="+str(durationTime_sec)+")"
            logging.debug(sdk_conversion)
        return sdk_conversion
    
    def process_height(self, data):
        '''
        sdk format: 
        set_lift_height(self, height, accel=10.0, max_speed=10.0, duration=0.0,
                        in_parallel=False, num_retries=0)
                        
        Note: Duration is in seconds in the sdk, whereas in pycozmo is in ms
        '''
        triggerTime_ms=data['triggerTime_ms']
        durationTime_sec=self.convert_ms_to_sec(data['durationTime_ms'])
        height_mm=self.convert_height_mm(data['height_mm'])
        heightVariability_mm=data['heightVariability_mm']
        sdk_conversion="Trigger time ms = "+str(triggerTime_ms)+" --> "+"set_lift_height("+str(height_mm)+", duration="+str(durationTime_sec)+")"
        return sdk_conversion
    
    def process_head_angle(self, data):
        '''
        def set_head_angle(self, angle, accel=10.0, max_speed=10.0, duration=0.0,
                       warn_on_clamp=True, in_parallel=False, num_retries=0):
        {'triggerTime_ms': 264,
         'durationTime_ms': 198,
         'angle_deg': -3,
         'angleVariability_deg': 0},
        '''
        
        triggerTime_ms=data['triggerTime_ms']
        durationTime_sec=self.convert_ms_to_sec(data['durationTime_ms'])
        head_degree=self.convert_angle_degree(data['angle_deg'])
        angleVariability_deg=data['angleVariability_deg']
        sdk_conversion="Trigger time ms = "+str(triggerTime_ms)+" --> "+"set_head_angle("+str(head_degree)+", duration="+str(durationTime_sec)+")"
        return sdk_conversion
        
        
    
    def process_dictionary(self, path='', keyframe=''):
        total_clips=self.get_number_clips()
        clip_counter=1;
        # Metadata of all the files for this animation
        for clip in self.json_dicts['clips']:
            content=[]
            # Get the sub clip name
            clip_name=clip['Name']
            
            # Define the final filename for this sub clip
            fn=path+'_'+clip_name+'.txt'
            
            # What clip of all the clips is this
            fraction=str(clip_counter)+'/'+str(total_clips)
            
            # Define info of this sub clip to be saved into the file
            clip_name=clip['Name']
            if keyframe == 'LiftHeightKeyFrame':
                content.append("Original file = "+ path)
                content.append("Animation "+clip_name+" clip "+str(clip_counter)+"/"+str(total_clips))
                content.append('---------------------------------------')
                content.append('Process lift height')
                content.append('---------------------------------------')
            elif keyframe =='HeadAngleKeyFrame':
                content.append('---------------------------------------')
                content.append('Process Head angle animations')
                content.append('---------------------------------------')
            elif keyframe =='BodyMotionKeyFrame':
                content.append('---------------------------------------')
                content.append('Process body motion animations')
                content.append('---------------------------------------')
            # Now process all entries for this clip
            
            for data in clip['keyframes'][keyframe]:
                if keyframe == 'LiftHeightKeyFrame':
                    logging.debug("Processing LiftHeightKeyFrame, clip {} of {}".format(
                        str(clip_counter), str(total_clips)))
                    result=self.process_height(data)
                    content.append(result)
                    
                elif keyframe == 'HeadAngleKeyFrame':
                    logging.debug("Processing HeadAngleKeyFrame, clip {} of {}".format(
                        str(clip_counter), str(total_clips)))
                    result=self.process_head_angle(data)
                    content.append(result)
                    
                elif keyframe == 'BodyMotionKeyFrame':
                    logging.debug("Processing BodyMotionKeyFrame, clip {} of {}".format(
                        str(clip_counter), str(total_clips)))
                    result=self.process_body_motion(data)
                    content.append(result)
                    
            clip_counter=clip_counter+1
            if SAVE_RESULTS_TO_FILE:
                self.write_file(data=content, filename=fn)


    
    def write_file(self, data='', filename=''):
        with open(filename, 'a') as f:
            for line in data:
                f.write(line)
                f.write('\n')
        
    
    def process_data(self):
        json_filename=self.destination+self.json_filename[:-5]
        self.process_dictionary(path=json_filename,
                                          keyframe='LiftHeightKeyFrame')
        self.process_dictionary(path=json_filename,
                                          keyframe='HeadAngleKeyFrame')
        self.process_dictionary(path=json_filename,
                                          keyframe='BodyMotionKeyFrame')
        
        
    
    def convert_json_to_sdk(self):
        if len(self.json_dicts) == 0:
            logging.error("Call read_json() first")
            return -1
        self.process_data()
        
