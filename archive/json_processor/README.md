**JSON to SDK converter.**

This project reads a .json file from:

`anims/
`

directory and places the restuls in:

`data/converted/`

**Instructions**

*Setup*

I provide a Makefile to automate tasks.

Makefile targets:

prepare-dev-mac: For mac, Install python, and virtual environment

venv: This will only generate the virtual environment "venv" for this project.

*Run this converter*

* Open the file demo.py (or do your own) change the filename to the .json file you want to process
* Get into the virutal environment and execute the file.

`(base)# source venv/bin/activate`

`(venv)# python demo.py`

Note: I processed one file for you to see the result, but delete the files between runs.

**How the filenames are created?**

One .json (.bin) animations can have more than 1 sub animation. The file names are gerenrated like this

<main json file>_<sub animation name>.txt

Inside of each file, I created a header with the information of what is the main .json file, and the order of this file.
E.g., 

Original file = data/converted/anim_meetcozmo
Animation anim_meetcozmo_sayname_02 clip 5/30

The last part is important, the "clip 5/30", this is how someone knows the order of these file in the right sequence.




