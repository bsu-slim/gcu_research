Original file = data/sdk_converted/anim_hiking_rtnewarea_01
Animation anim_hiking_rtnewarea_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 891 --> set_lift_height(44, duration=0.099)
Trigger time ms = 990 --> set_lift_height(8, duration=0.099)
Trigger time ms = 1089 --> set_lift_height(40, duration=0.066)
Trigger time ms = 1155 --> set_lift_height(4, duration=0.099)
Trigger time ms = 1254 --> set_lift_height(30, duration=0.099)
Trigger time ms = 1353 --> set_lift_height(3, duration=0.165)
Trigger time ms = 1518 --> set_lift_height(0, duration=0.231)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 99 --> set_head_angle(38, duration=0.132)
Trigger time ms = 231 --> set_head_angle(43, duration=0.132)
Trigger time ms = 792 --> set_head_angle(45, duration=0.066)
Trigger time ms = 858 --> set_head_angle(-20, duration=0.099)
Trigger time ms = 957 --> set_head_angle(16, duration=0.099)
Trigger time ms = 1056 --> set_head_angle(9, duration=0.132)
Trigger time ms = 1188 --> set_head_angle(16, duration=0.132)
Trigger time ms = 1320 --> set_head_angle(14, duration=0.165)
Trigger time ms = 1485 --> set_head_angle(17, duration=0.099)
Trigger time ms = 1584 --> set_head_angle(7, duration=0.165)
Trigger time ms = 1749 --> set_head_angle(1, duration=0.132)
Trigger time ms = 1881 --> set_head_angle(0, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-53, r_wheel_speed=-53, duration=0.066)
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.726)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.099)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.033)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=78, r_wheel_speed=78, duration=0.132)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=60, r_wheel_speed=60, duration=0.066)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.066)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=-14, r_wheel_speed=-14, duration=0.033)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=-42, r_wheel_speed=-42, duration=0.099)
Trigger time ms = 1320 --> drive_wheels(l_wheel_speed=38, r_wheel_speed=38, duration=0.099)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=25, r_wheel_speed=25, duration=0.066)
Trigger time ms = 1485 --> drive_wheels(l_wheel_speed=-4, r_wheel_speed=-4, duration=0.099)
Trigger time ms = 1584 --> drive_wheels(l_wheel_speed=-28, r_wheel_speed=-28, duration=0.165)
