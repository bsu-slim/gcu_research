Original file = data/sdk_converted/anim_reacttoblock_success_01
Animation anim_reacttoblock_success_01_head_angle_40 clip 3/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> set_lift_height(44, duration=0.099)
Trigger time ms = 231 --> set_lift_height(8, duration=0.099)
Trigger time ms = 330 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(36, duration=0.099)
Trigger time ms = 99 --> set_head_angle(-9, duration=0.099)
Trigger time ms = 198 --> set_head_angle(44, duration=0.099)
Trigger time ms = 297 --> set_head_angle(38, duration=0.132)
Trigger time ms = 429 --> set_head_angle(44, duration=0.132)
Trigger time ms = 561 --> set_head_angle(42, duration=0.165)
Trigger time ms = 726 --> set_head_angle(44, duration=0.099)
Trigger time ms = 825 --> set_head_angle(9, duration=0.165)
Trigger time ms = 990 --> set_head_angle(6, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-133, r_wheel_speed=-133, duration=0.132)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.033)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=95, r_wheel_speed=95, duration=0.132)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=73, r_wheel_speed=73, duration=0.066)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=11, r_wheel_speed=11, duration=0.099)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-37, r_wheel_speed=-37, duration=0.198)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.363)
