Original file = data/sdk_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getin_short_01 clip 1/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 660 --> set_lift_height(56, duration=0.165)
Trigger time ms = 825 --> set_lift_height(58, duration=0.066)
Trigger time ms = 891 --> set_lift_height(54, duration=0.132)
Trigger time ms = 1023 --> set_lift_height(38, duration=0.066)
Trigger time ms = 1089 --> set_lift_height(10, duration=0.099)
Trigger time ms = 1188 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-16, duration=0.099)
Trigger time ms = 99 --> set_head_angle(0, duration=0.099)
Trigger time ms = 198 --> set_head_angle(3, duration=0.099)
Trigger time ms = 297 --> set_head_angle(4, duration=0.066)
Trigger time ms = 363 --> set_head_angle(4, duration=0.066)
Trigger time ms = 429 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 528 --> set_head_angle(-13, duration=0.099)
Trigger time ms = 627 --> set_head_angle(12, duration=0.198)
Trigger time ms = 924 --> set_head_angle(14, duration=0.066)
Trigger time ms = 990 --> set_head_angle(12, duration=0.066)
Trigger time ms = 1056 --> set_head_angle(-12, duration=0.165)
Trigger time ms = 1221 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=-43, r_wheel_speed=-43, duration=0.099)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=94, r_wheel_speed=94, duration=0.198)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=10, r_wheel_speed=10, duration=0.099)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=8, r_wheel_speed=8, duration=0.099)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=-48, r_wheel_speed=-48, duration=0.066)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=-21, r_wheel_speed=-21, duration=0.198)
