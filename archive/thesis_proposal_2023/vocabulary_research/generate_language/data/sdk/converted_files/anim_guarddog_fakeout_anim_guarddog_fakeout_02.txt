Original file = data/sdk_converted/anim_guarddog_fakeout
Animation anim_guarddog_fakeout_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.165)
Trigger time ms = 363 --> set_lift_height(49, duration=0.099)
Trigger time ms = 462 --> set_lift_height(32, duration=0.132)
Trigger time ms = 1056 --> set_lift_height(42, duration=0.264)
Trigger time ms = 1320 --> set_lift_height(54, duration=0.297)
Trigger time ms = 1617 --> set_lift_height(41, duration=0.363)
Trigger time ms = 1980 --> set_lift_height(52, duration=0.231)
Trigger time ms = 2211 --> set_lift_height(42, duration=0.33)
Trigger time ms = 3102 --> set_lift_height(32, duration=0.297)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(3, duration=0.231)
Trigger time ms = 231 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 363 --> set_head_angle(17, duration=0.099)
Trigger time ms = 462 --> set_head_angle(-5, duration=0.198)
Trigger time ms = 660 --> set_head_angle(5, duration=0.132)
Trigger time ms = 792 --> set_head_angle(8, duration=0.264)
Trigger time ms = 1386 --> set_head_angle(17, duration=0.297)
Trigger time ms = 1683 --> set_head_angle(6, duration=0.363)
Trigger time ms = 2046 --> set_head_angle(17, duration=0.231)
Trigger time ms = 2277 --> set_head_angle(8, duration=0.231)
Trigger time ms = 2508 --> set_head_angle(6, duration=0.825)
Trigger time ms = 3333 --> set_head_angle(-8, duration=0.396)
Trigger time ms = 3729 --> set_head_angle(1, duration=0.363)
Trigger time ms = 4092 --> set_head_angle(3, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> turn_in_place(degrees(-36))
Trigger time ms = 495 --> turn_in_place(degrees(36))
Trigger time ms = 1353 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.297)
Trigger time ms = 1650 --> drive_wheels(l_wheel_speed=-19, r_wheel_speed=-19, duration=0.363)
Trigger time ms = 2013 --> drive_wheels(l_wheel_speed=26, r_wheel_speed=26, duration=0.231)
Trigger time ms = 2244 --> drive_wheels(l_wheel_speed=-26, r_wheel_speed=-26, duration=0.231)
