Original file = data/sdk_converted/anim_hiccup_02
Animation anim_hiccup_selfcure_getout clip 5/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.297)
Trigger time ms = 3168 --> set_lift_height(40, duration=0.132)
Trigger time ms = 3300 --> set_lift_height(8, duration=0.198)
Trigger time ms = 3498 --> set_lift_height(0, duration=0.495)
Trigger time ms = 5082 --> set_lift_height(0, duration=1.155)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-3, duration=0.231)
Trigger time ms = 231 --> set_head_angle(-2, duration=0.132)
Trigger time ms = 363 --> set_head_angle(29, duration=0.132)
Trigger time ms = 495 --> set_head_angle(-6, duration=0.165)
Trigger time ms = 660 --> set_head_angle(4, duration=0.132)
Trigger time ms = 1287 --> set_head_angle(5, duration=0.099)
Trigger time ms = 1386 --> set_head_angle(5, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(7, duration=0.099)
Trigger time ms = 1551 --> set_head_angle(9, duration=0.066)
Trigger time ms = 1617 --> set_head_angle(13, duration=0.099)
Trigger time ms = 1716 --> set_head_angle(18, duration=0.066)
Trigger time ms = 1782 --> set_head_angle(24, duration=0.066)
Trigger time ms = 1848 --> set_head_angle(29, duration=0.066)
Trigger time ms = 1914 --> set_head_angle(33, duration=0.066)
Trigger time ms = 1980 --> set_head_angle(38, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(40, duration=0.099)
Trigger time ms = 2178 --> set_head_angle(42, duration=0.099)
Trigger time ms = 2277 --> set_head_angle(44, duration=0.132)
Trigger time ms = 2409 --> set_head_angle(45, duration=0.198)
Trigger time ms = 2607 --> set_head_angle(35, duration=0.132)
Trigger time ms = 2739 --> set_head_angle(-25, duration=0.198)
Trigger time ms = 2937 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 3003 --> set_head_angle(-17, duration=0.132)
Trigger time ms = 6435 --> set_head_angle(-25, duration=0.132)
Trigger time ms = 6567 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=-82, r_wheel_speed=-82, duration=0.066)
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=67, r_wheel_speed=67, duration=0.066)
Trigger time ms = 561 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.033)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=7, r_wheel_speed=7, duration=0.231)
Trigger time ms = 1617 --> drive_wheels(l_wheel_speed=21, r_wheel_speed=21, duration=0.627)
Trigger time ms = 2244 --> drive_wheels(l_wheel_speed=2, r_wheel_speed=2, duration=0.396)
Trigger time ms = 2640 --> drive_wheels(l_wheel_speed=-15, r_wheel_speed=-15, duration=0.033)
Trigger time ms = 2673 --> drive_wheels(l_wheel_speed=-202, r_wheel_speed=-202, duration=0.033)
Trigger time ms = 2706 --> drive_wheels(l_wheel_speed=-88, r_wheel_speed=-88, duration=0.033)
Trigger time ms = 2739 --> drive_wheels(l_wheel_speed=-52, r_wheel_speed=-52, duration=0.033)
Trigger time ms = 2772 --> drive_wheels(l_wheel_speed=-42, r_wheel_speed=-42, duration=0.033)
Trigger time ms = 2805 --> drive_wheels(l_wheel_speed=-55, r_wheel_speed=-55, duration=0.066)
Trigger time ms = 4653 --> turn_in_place(degrees(11))
Trigger time ms = 6402 --> turn_in_place(degrees(-61))
Trigger time ms = 6534 --> turn_in_place(degrees(61))
