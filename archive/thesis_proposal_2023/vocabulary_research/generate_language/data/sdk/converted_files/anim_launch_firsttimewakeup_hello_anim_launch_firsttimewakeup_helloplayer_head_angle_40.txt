Original file = data/sdk_converted/anim_launch_firsttimewakeup_hello
Animation anim_launch_firsttimewakeup_helloplayer_head_angle_40 clip 3/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 264 --> set_lift_height(41, duration=0.099)
Trigger time ms = 363 --> set_lift_height(0, duration=0.099)
Trigger time ms = 2310 --> set_lift_height(42, duration=0.132)
Trigger time ms = 2442 --> set_lift_height(0, duration=0.165)
Trigger time ms = 6996 --> set_lift_height(43, duration=0.165)
Trigger time ms = 7161 --> set_lift_height(40, duration=0.594)
Trigger time ms = 7755 --> set_lift_height(0, duration=0.198)
Trigger time ms = 15543 --> set_lift_height(38, duration=0.099)
Trigger time ms = 15642 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 198 --> set_head_angle(33, duration=0.099)
Trigger time ms = 297 --> set_head_angle(36, duration=0.198)
Trigger time ms = 495 --> set_head_angle(35, duration=0.33)
Trigger time ms = 1353 --> set_head_angle(39, duration=0.099)
Trigger time ms = 1914 --> set_head_angle(36, duration=0.165)
Trigger time ms = 2079 --> set_head_angle(35, duration=0.231)
Trigger time ms = 2310 --> set_head_angle(42, duration=1.848)
Trigger time ms = 4158 --> set_head_angle(40, duration=0.231)
Trigger time ms = 4389 --> set_head_angle(37, duration=0.066)
Trigger time ms = 4455 --> set_head_angle(39, duration=0.165)
Trigger time ms = 4620 --> set_head_angle(41, duration=0.792)
Trigger time ms = 5412 --> set_head_angle(39, duration=0.066)
Trigger time ms = 5478 --> set_head_angle(41, duration=0.132)
Trigger time ms = 5610 --> set_head_angle(42, duration=1.452)
Trigger time ms = 7062 --> set_head_angle(45, duration=0.165)
Trigger time ms = 7227 --> set_head_angle(45, duration=0.165)
Trigger time ms = 15510 --> set_head_angle(33, duration=0.165)
Trigger time ms = 15774 --> set_head_angle(33, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-41, r_wheel_speed=-41, duration=0.132)
Trigger time ms = 1353 --> turn_in_place(degrees(20))
Trigger time ms = 2013 --> turn_in_place(degrees(-51))
Trigger time ms = 2310 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=1.848)
Trigger time ms = 4158 --> drive_wheels(l_wheel_speed=-6, r_wheel_speed=-6, duration=0.297)
Trigger time ms = 6270 --> drive_wheels(l_wheel_speed=19, r_wheel_speed=19, duration=1.485)
Trigger time ms = 7755 --> drive_wheels(l_wheel_speed=-5, r_wheel_speed=-5, duration=0.825)
Trigger time ms = 10593 --> drive_wheels(l_wheel_speed=16, r_wheel_speed=16, duration=0.198)
Trigger time ms = 10857 --> drive_wheels(l_wheel_speed=-2, r_wheel_speed=-2, duration=0.264)
Trigger time ms = 15543 --> drive_wheels(l_wheel_speed=-13, r_wheel_speed=-13, duration=0.165)
