Original file = data/sdk_converted/anim_greeting_02
Animation anim_greeting_happy_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 396 --> set_lift_height(42, duration=0.066)
Trigger time ms = 462 --> set_lift_height(0, duration=0.033)
Trigger time ms = 726 --> set_lift_height(42, duration=0.066)
Trigger time ms = 792 --> set_lift_height(0, duration=0.066)
Trigger time ms = 858 --> set_lift_height(45, duration=0.066)
Trigger time ms = 924 --> set_lift_height(0, duration=0.066)
Trigger time ms = 990 --> set_lift_height(45, duration=0.066)
Trigger time ms = 1056 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1122 --> set_lift_height(45, duration=0.099)
Trigger time ms = 1221 --> set_lift_height(0, duration=0.099)
Trigger time ms = 1320 --> set_lift_height(43, duration=0.066)
Trigger time ms = 1386 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1452 --> set_lift_height(41, duration=0.066)
Trigger time ms = 1518 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1683 --> set_lift_height(45, duration=0.099)
Trigger time ms = 1782 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1848 --> set_lift_height(43, duration=0.066)
Trigger time ms = 1914 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1980 --> set_lift_height(41, duration=0.066)
Trigger time ms = 2046 --> set_lift_height(0, duration=0.066)
Trigger time ms = 2145 --> set_lift_height(45, duration=0.099)
Trigger time ms = 2244 --> set_lift_height(0, duration=0.099)
Trigger time ms = 2343 --> set_lift_height(43, duration=0.066)
Trigger time ms = 2409 --> set_lift_height(0, duration=0.066)
Trigger time ms = 2475 --> set_lift_height(41, duration=0.066)
Trigger time ms = 2541 --> set_lift_height(0, duration=0.132)
Trigger time ms = 4191 --> set_lift_height(52, duration=0.099)
Trigger time ms = 4290 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 99 --> set_head_angle(2, duration=0.099)
Trigger time ms = 198 --> set_head_angle(7, duration=0.132)
Trigger time ms = 330 --> set_head_angle(7, duration=0.12)
Trigger time ms = 450 --> set_head_angle(2, duration=0.066)
Trigger time ms = 516 --> set_head_angle(9, duration=0.066)
Trigger time ms = 582 --> set_head_angle(2, duration=0.066)
Trigger time ms = 648 --> set_head_angle(9, duration=0.066)
Trigger time ms = 714 --> set_head_angle(2, duration=0.066)
Trigger time ms = 780 --> set_head_angle(9, duration=0.066)
Trigger time ms = 846 --> set_head_angle(2, duration=0.066)
Trigger time ms = 912 --> set_head_angle(9, duration=0.066)
Trigger time ms = 978 --> set_head_angle(2, duration=0.066)
Trigger time ms = 1044 --> set_head_angle(9, duration=0.066)
Trigger time ms = 1110 --> set_head_angle(2, duration=0.066)
Trigger time ms = 1176 --> set_head_angle(9, duration=0.066)
Trigger time ms = 1242 --> set_head_angle(2, duration=0.066)
Trigger time ms = 1308 --> set_head_angle(5, duration=0.111)
Trigger time ms = 1518 --> set_head_angle(3, duration=0.297)
Trigger time ms = 1815 --> set_head_angle(2, duration=0.858)
Trigger time ms = 2673 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 2739 --> set_head_angle(2, duration=0.198)
Trigger time ms = 3003 --> set_head_angle(12, duration=0.066)
Trigger time ms = 3069 --> set_head_angle(-11, duration=0.066)
Trigger time ms = 3135 --> set_head_angle(13, duration=0.099)
Trigger time ms = 3234 --> set_head_angle(-7, duration=0.099)
Trigger time ms = 3333 --> set_head_angle(4, duration=0.099)
Trigger time ms = 3432 --> set_head_angle(1, duration=0.33)
Trigger time ms = 3762 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 3828 --> set_head_angle(2, duration=0.066)
Trigger time ms = 4224 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 4323 --> set_head_angle(2, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-84, r_wheel_speed=-84, duration=0.132)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=26, r_wheel_speed=26, duration=0.132)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=-105.0, r_wheel_speed=345.0, duration=0.066)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-518.5, r_wheel_speed=246.5, duration=0.066)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-132.5, r_wheel_speed=2252.5, duration=0.066)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-1963.5, r_wheel_speed=331.5, duration=0.066)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=311.5, r_wheel_speed=4316.5, duration=0.066)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=-4350.0, r_wheel_speed=150.0, duration=0.066)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=1221.0, r_wheel_speed=11211.0, duration=0.066)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=-11211.0, r_wheel_speed=-1221.0, duration=0.066)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=3001.5, r_wheel_speed=14746.5, duration=0.066)
Trigger time ms = 990 --> drive_wheels(l_wheel_speed=-12916.5, r_wheel_speed=-2251.5, duration=0.066)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=1132.5, r_wheel_speed=7927.5, duration=0.066)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=-3527.5, r_wheel_speed=297.5, duration=0.066)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=-364.5, r_wheel_speed=3280.5, duration=0.066)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=-2100.0, r_wheel_speed=420.0, duration=0.066)
Trigger time ms = 1320 --> drive_wheels(l_wheel_speed=-525.0, r_wheel_speed=1365.0, duration=0.033)
Trigger time ms = 1386 --> turn_in_place(degrees(-295))
Trigger time ms = 3102 --> drive_wheels(l_wheel_speed=23, r_wheel_speed=23, duration=0.066)
Trigger time ms = 3168 --> drive_wheels(l_wheel_speed=-44, r_wheel_speed=-44, duration=0.066)
Trigger time ms = 3234 --> drive_wheels(l_wheel_speed=44, r_wheel_speed=44, duration=0.066)
Trigger time ms = 4224 --> turn_in_place(degrees(-958))
