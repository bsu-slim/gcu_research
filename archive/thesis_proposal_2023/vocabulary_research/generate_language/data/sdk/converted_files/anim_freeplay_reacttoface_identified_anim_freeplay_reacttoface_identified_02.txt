Original file = data/sdk_converted/anim_freeplay_reacttoface_identified
Animation anim_freeplay_reacttoface_identified_02 clip 2/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(3, duration=0.165)
Trigger time ms = 165 --> set_head_angle(33, duration=0.132)
Trigger time ms = 297 --> set_head_angle(31, duration=0.132)
Trigger time ms = 429 --> set_head_angle(22, duration=0.198)
Trigger time ms = 627 --> set_head_angle(5, duration=0.099)
Trigger time ms = 726 --> set_head_angle(39, duration=0.132)
Trigger time ms = 1056 --> set_head_angle(21, duration=0.099)
Trigger time ms = 1155 --> set_head_angle(2, duration=0.066)
Trigger time ms = 1221 --> set_head_angle(1, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=-19, r_wheel_speed=-19, duration=0.099)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-5, r_wheel_speed=-5, duration=0.165)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=59, r_wheel_speed=59, duration=0.198)
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.066)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=-5, r_wheel_speed=-5, duration=0.165)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=-45, r_wheel_speed=-45, duration=0.132)
