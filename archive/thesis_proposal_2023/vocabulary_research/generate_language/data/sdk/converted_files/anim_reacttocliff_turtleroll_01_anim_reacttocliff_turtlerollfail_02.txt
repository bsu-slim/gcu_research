Original file = data/sdk_converted/anim_reacttocliff_turtleroll_01
Animation anim_reacttocliff_turtlerollfail_02 clip 4/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1221 --> set_lift_height(92, duration=0.289)
Trigger time ms = 1510 --> set_lift_height(0, duration=0.14)
Trigger time ms = 3366 --> set_lift_height(50, duration=0.33)
Trigger time ms = 3696 --> set_lift_height(0, duration=0.429)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.066)
Trigger time ms = 66 --> set_head_angle(45, duration=0.198)
Trigger time ms = 627 --> set_head_angle(-12, duration=0.198)
Trigger time ms = 825 --> set_head_angle(-15, duration=0.033)
Trigger time ms = 858 --> set_head_angle(-12, duration=0.363)
Trigger time ms = 1221 --> set_head_angle(43, duration=0.198)
Trigger time ms = 1419 --> set_head_angle(-1, duration=0.231)
Trigger time ms = 1650 --> set_head_angle(-1, duration=0.33)
Trigger time ms = 1980 --> set_head_angle(1, duration=0.066)
Trigger time ms = 2046 --> set_head_angle(11, duration=0.165)
Trigger time ms = 2211 --> set_head_angle(0, duration=0.132)
Trigger time ms = 2343 --> set_head_angle(1, duration=0.231)
Trigger time ms = 2574 --> set_head_angle(0, duration=0.363)
Trigger time ms = 2937 --> set_head_angle(-1, duration=0.396)
Trigger time ms = 3333 --> set_head_angle(2, duration=0.132)
Trigger time ms = 3465 --> set_head_angle(17, duration=0.165)
Trigger time ms = 3630 --> set_head_angle(26, duration=0.099)
Trigger time ms = 3729 --> set_head_angle(44, duration=0.231)
Trigger time ms = 3960 --> set_head_angle(43, duration=0.132)
Trigger time ms = 4092 --> set_head_angle(42, duration=0.066)
Trigger time ms = 4158 --> set_head_angle(39, duration=0.033)
Trigger time ms = 4191 --> set_head_angle(-1, duration=0.198)
Trigger time ms = 4389 --> set_head_angle(5, duration=0.132)
Trigger time ms = 4521 --> set_head_angle(-4, duration=0.231)
Trigger time ms = 4752 --> set_head_angle(-3, duration=0.759)
Trigger time ms = 5511 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 5577 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 5676 --> set_head_angle(6, duration=0.198)
Trigger time ms = 5874 --> set_head_angle(4, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 2112 --> drive_wheels(l_wheel_speed=-74, r_wheel_speed=-74, duration=0.165)
Trigger time ms = 2574 --> turn_in_place(degrees(2))
Trigger time ms = 3036 --> turn_in_place(degrees(-61))
Trigger time ms = 3135 --> turn_in_place(degrees(51))
Trigger time ms = 3234 --> turn_in_place(degrees(-30))
Trigger time ms = 3366 --> turn_in_place(degrees(30))
Trigger time ms = 3432 --> turn_in_place(degrees(-15))
Trigger time ms = 3630 --> turn_in_place(degrees(-15))
Trigger time ms = 3696 --> turn_in_place(degrees(-15))
Trigger time ms = 3828 --> turn_in_place(degrees(15))
Trigger time ms = 4092 --> turn_in_place(degrees(15))
Trigger time ms = 4290 --> turn_in_place(degrees(10))
Trigger time ms = 4389 --> turn_in_place(degrees(-2))
