Original file = data/sdk_converted/anim_meetcozmo
Animation anim_meetcozmo_lookface clip 8/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.033)
Trigger time ms = 1353 --> set_lift_height(39, duration=0.099)
Trigger time ms = 1452 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(10, duration=0.033)
Trigger time ms = 264 --> set_head_angle(11, duration=0.396)
Trigger time ms = 660 --> set_head_angle(13, duration=0.759)
Trigger time ms = 1419 --> set_head_angle(16, duration=0.561)
Trigger time ms = 1980 --> set_head_angle(16, duration=0.363)
Trigger time ms = 2343 --> set_head_angle(11, duration=0.396)
Trigger time ms = 2739 --> set_head_angle(10, duration=0.561)
Trigger time ms = 3300 --> set_head_angle(10, duration=0.495)
---------------------------------------
Process body motion animations
---------------------------------------
