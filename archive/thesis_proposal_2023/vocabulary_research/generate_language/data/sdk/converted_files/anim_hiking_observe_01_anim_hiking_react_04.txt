Original file = data/sdk_converted/anim_hiking_observe_01
Animation anim_hiking_react_04 clip 4/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(2, duration=0.099)
Trigger time ms = 99 --> set_lift_height(37, duration=0.066)
Trigger time ms = 165 --> set_lift_height(2, duration=0.066)
Trigger time ms = 231 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(9, duration=0.066)
Trigger time ms = 99 --> set_head_angle(-9, duration=0.099)
Trigger time ms = 198 --> set_head_angle(0, duration=0.132)
Trigger time ms = 330 --> set_head_angle(0, duration=0.561)
Trigger time ms = 891 --> set_head_angle(-11, duration=0.099)
Trigger time ms = 990 --> set_head_angle(-1, duration=0.231)
Trigger time ms = 1221 --> set_head_angle(0, duration=0.033)
Trigger time ms = 1254 --> set_head_angle(0, duration=0.033)
Trigger time ms = 1287 --> set_head_angle(0, duration=0.033)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> drive_wheels(l_wheel_speed=58, r_wheel_speed=58, duration=0.066)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-87, r_wheel_speed=-87, duration=0.099)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=-13, r_wheel_speed=-13, duration=0.198)
