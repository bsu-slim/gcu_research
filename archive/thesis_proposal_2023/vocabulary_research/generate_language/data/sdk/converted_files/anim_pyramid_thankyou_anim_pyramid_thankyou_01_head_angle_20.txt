Original file = data/sdk_converted/anim_pyramid_thankyou
Animation anim_pyramid_thankyou_01_head_angle_20 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> set_lift_height(0, duration=0.132)
Trigger time ms = 429 --> set_lift_height(41, duration=0.066)
Trigger time ms = 495 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(20, duration=0.099)
Trigger time ms = 231 --> set_head_angle(10, duration=0.099)
Trigger time ms = 330 --> set_head_angle(9, duration=0.033)
Trigger time ms = 363 --> set_head_angle(26, duration=0.099)
Trigger time ms = 462 --> set_head_angle(18, duration=0.165)
Trigger time ms = 627 --> set_head_angle(20, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=29, r_wheel_speed=29, duration=0.132)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.132)
