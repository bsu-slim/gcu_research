Original file = data/sdk_converted/anim_memorymatch_point_fast
Animation anim_memorymatch_pointcenter_fast_02 clip 2/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> set_lift_height(43, duration=0.099)
Trigger time ms = 231 --> set_lift_height(0, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-25, duration=0.099)
Trigger time ms = 99 --> set_head_angle(3, duration=0.066)
Trigger time ms = 165 --> set_head_angle(-12, duration=0.066)
Trigger time ms = 231 --> set_head_angle(-9, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-40, r_wheel_speed=-40, duration=0.066)
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=41, r_wheel_speed=41, duration=0.099)
