Original file = data/sdk_converted/anim_vc_reaction_alreadyhere_01
Animation anim_vc_reaction_alreadyhere_01_head_angle_20 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.033)
Trigger time ms = 396 --> set_lift_height(42, duration=0.099)
Trigger time ms = 495 --> set_lift_height(32, duration=0.099)
Trigger time ms = 1815 --> set_lift_height(38, duration=0.033)
Trigger time ms = 1848 --> set_lift_height(32, duration=0.033)
Trigger time ms = 1947 --> set_lift_height(40, duration=0.066)
Trigger time ms = 2013 --> set_lift_height(32, duration=0.033)
Trigger time ms = 2046 --> set_lift_height(41, duration=0.066)
Trigger time ms = 2112 --> set_lift_height(32, duration=0.066)
Trigger time ms = 2178 --> set_lift_height(39, duration=0.033)
Trigger time ms = 2211 --> set_lift_height(32, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(20, duration=0.033)
Trigger time ms = 198 --> set_head_angle(20, duration=0.099)
Trigger time ms = 297 --> set_head_angle(16, duration=0.066)
Trigger time ms = 363 --> set_head_angle(11, duration=0.066)
Trigger time ms = 429 --> set_head_angle(19, duration=0.066)
Trigger time ms = 495 --> set_head_angle(30, duration=0.066)
Trigger time ms = 561 --> set_head_angle(31, duration=0.066)
Trigger time ms = 924 --> set_head_angle(-1, duration=0.132)
Trigger time ms = 1056 --> set_head_angle(3, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(26, duration=0.165)
Trigger time ms = 1815 --> set_head_angle(28, duration=0.033)
Trigger time ms = 1947 --> set_head_angle(31, duration=0.066)
Trigger time ms = 2013 --> set_head_angle(10, duration=0.165)
Trigger time ms = 2178 --> set_head_angle(-1, duration=0.231)
Trigger time ms = 2409 --> set_head_angle(27, duration=0.198)
Trigger time ms = 3234 --> set_head_angle(16, duration=0.066)
Trigger time ms = 3300 --> set_head_angle(20, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.099)
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=-23, r_wheel_speed=-23, duration=0.132)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=-38, r_wheel_speed=-38, duration=0.132)
Trigger time ms = 1485 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.066)
Trigger time ms = 1683 --> drive_wheels(l_wheel_speed=-78.52207293664, r_wheel_speed=101.47792706336, duration=0.033)
Trigger time ms = 1716 --> turn_in_place(degrees(18))
Trigger time ms = 1782 --> drive_wheels(l_wheel_speed=-242.176362628, r_wheel_speed=657.823637372, duration=0.066)
Trigger time ms = 1848 --> turn_in_place(degrees(22))
Trigger time ms = 1947 --> drive_wheels(l_wheel_speed=-239.08103408309998, r_wheel_speed=1245.9189659169, duration=0.066)
Trigger time ms = 2013 --> turn_in_place(degrees(18))
Trigger time ms = 2112 --> drive_wheels(l_wheel_speed=-186.95522931300002, r_wheel_speed=848.044770687, duration=0.099)
Trigger time ms = 2211 --> turn_in_place(degrees(29))
Trigger time ms = 2277 --> drive_wheels(l_wheel_speed=-141.41111367360003, r_wheel_speed=938.5888863263999, duration=0.099)
Trigger time ms = 2376 --> turn_in_place(degrees(38))
Trigger time ms = 2442 --> turn_in_place(degrees(-49))
Trigger time ms = 2508 --> drive_wheels(l_wheel_speed=-14.5, r_wheel_speed=-1319.5, duration=0.099)
Trigger time ms = 2607 --> turn_in_place(degrees(-10))
