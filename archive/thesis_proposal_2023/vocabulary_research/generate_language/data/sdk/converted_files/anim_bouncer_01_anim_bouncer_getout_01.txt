Original file = data/sdk_converted/anim_bouncer_01
Animation anim_bouncer_getout_01 clip 9/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(28, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-2, duration=0.165)
Trigger time ms = 198 --> set_head_angle(0, duration=0.231)
Trigger time ms = 726 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 825 --> set_head_angle(0, duration=0.099)
Trigger time ms = 1650 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 1782 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=91, r_wheel_speed=91, duration=0.033)
Trigger time ms = 33 --> drive_wheels(l_wheel_speed=182, r_wheel_speed=182, duration=0.066)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 1683 --> drive_wheels(l_wheel_speed=-341.0, r_wheel_speed=649.0, duration=0.033)
Trigger time ms = 1716 --> drive_wheels(l_wheel_speed=-325.5, r_wheel_speed=619.5, duration=0.165)
