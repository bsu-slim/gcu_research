Original file = data/sdk_converted/anim_vc_reaction_yesfaceheardyou_01
Animation anim_vc_reaction_yesfaceheardyou_01_head_angle_40 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.132)
Trigger time ms = 429 --> set_lift_height(41, duration=0.132)
Trigger time ms = 561 --> set_lift_height(32, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(40, duration=0.165)
Trigger time ms = 165 --> set_head_angle(41, duration=0.099)
Trigger time ms = 264 --> set_head_angle(45, duration=0.066)
Trigger time ms = 330 --> set_head_angle(23, duration=0.132)
Trigger time ms = 462 --> set_head_angle(39, duration=0.165)
Trigger time ms = 1584 --> set_head_angle(37, duration=0.099)
Trigger time ms = 1881 --> set_head_angle(44, duration=0.099)
Trigger time ms = 1980 --> set_head_angle(42, duration=0.231)
Trigger time ms = 2409 --> set_head_angle(15, duration=0.132)
Trigger time ms = 3861 --> set_head_angle(44, duration=0.198)
Trigger time ms = 4059 --> set_head_angle(40, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.066)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=111, r_wheel_speed=111, duration=0.099)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 2442 --> drive_wheels(l_wheel_speed=-106, r_wheel_speed=-106, duration=0.132)
Trigger time ms = 2607 --> turn_in_place(degrees(-61))
Trigger time ms = 3762 --> drive_wheels(l_wheel_speed=1957.0, r_wheel_speed=247.0, duration=0.066)
Trigger time ms = 3828 --> drive_wheels(l_wheel_speed=-241.5, r_wheel_speed=-3346.5, duration=0.066)
Trigger time ms = 3894 --> drive_wheels(l_wheel_speed=10823.0, r_wheel_speed=3713.0, duration=0.066)
Trigger time ms = 3960 --> drive_wheels(l_wheel_speed=333.5, r_wheel_speed=-971.5, duration=0.066)
Trigger time ms = 4026 --> drive_wheels(l_wheel_speed=300.0, r_wheel_speed=-60.0, duration=0.132)
