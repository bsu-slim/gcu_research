Original file = data/sdk_converted/anim_cozmosays_app_inout
Animation anim_cozmosays_app_getout_02 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(38, duration=0.099)
Trigger time ms = 99 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 99 --> set_head_angle(2, duration=0.165)
Trigger time ms = 264 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> turn_in_place(degrees(265))
