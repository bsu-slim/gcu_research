Original file = data/sdk_converted/anim_gotosleep_sleeping_01
Animation anim_gotosleep_sleeping_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 2805 --> set_head_angle(-9, duration=1.32)
Trigger time ms = 4125 --> set_head_angle(-15, duration=0.825)
Trigger time ms = 4950 --> set_head_angle(-16, duration=0.363)
Trigger time ms = 6237 --> set_head_angle(-13, duration=0.462)
Trigger time ms = 6699 --> set_head_angle(-10, duration=0.132)
Trigger time ms = 6831 --> set_head_angle(-16, duration=0.561)
Trigger time ms = 9339 --> set_head_angle(-9, duration=0.462)
Trigger time ms = 9801 --> set_head_angle(-6, duration=0.561)
Trigger time ms = 10362 --> set_head_angle(-14, duration=0.792)
Trigger time ms = 11154 --> set_head_angle(-16, duration=0.858)
---------------------------------------
Process body motion animations
---------------------------------------
