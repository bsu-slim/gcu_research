Original file = data/sdk_converted/anim_upgrade_reaction
Animation anim_upgrade_reaction_tracks_01 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.462)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.462)
Trigger time ms = 462 --> set_head_angle(1, duration=0.033)
Trigger time ms = 495 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 594 --> set_head_angle(-2, duration=0.109)
Trigger time ms = 703 --> set_head_angle(0, duration=0.188)
Trigger time ms = 891 --> set_head_angle(-19, duration=0.099)
Trigger time ms = 2607 --> set_head_angle(-23, duration=0.033)
Trigger time ms = 2640 --> set_head_angle(22, duration=0.099)
Trigger time ms = 2739 --> set_head_angle(-11, duration=0.132)
Trigger time ms = 2871 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=92, r_wheel_speed=92, duration=0.066)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-92, r_wheel_speed=-92, duration=0.066)
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=217.5, r_wheel_speed=-3697.5, duration=0.165)
Trigger time ms = 2772 --> drive_wheels(l_wheel_speed=-55, r_wheel_speed=-55, duration=0.106)
Trigger time ms = 2878 --> drive_wheels(l_wheel_speed=100, r_wheel_speed=100, duration=0.066)
Trigger time ms = 2944 --> drive_wheels(l_wheel_speed=-100, r_wheel_speed=-100, duration=0.066)
Trigger time ms = 3010 --> drive_wheels(l_wheel_speed=100, r_wheel_speed=100, duration=0.066)
Trigger time ms = 3076 --> drive_wheels(l_wheel_speed=-100, r_wheel_speed=-100, duration=0.066)
Trigger time ms = 3142 --> drive_wheels(l_wheel_speed=100, r_wheel_speed=100, duration=0.066)
Trigger time ms = 3208 --> drive_wheels(l_wheel_speed=-100, r_wheel_speed=-100, duration=0.066)
Trigger time ms = 3274 --> drive_wheels(l_wheel_speed=39, r_wheel_speed=39, duration=0.026)
