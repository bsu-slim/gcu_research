Original file = data/sdk_converted/vs_speedtap_game
Animation anim_speedtap_fakeout_01 clip 5/8
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> set_lift_height(85, duration=0.066)
Trigger time ms = 132 --> set_lift_height(92, duration=0.231)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-11, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-19, duration=0.099)
Trigger time ms = 132 --> set_head_angle(-20, duration=0.132)
Trigger time ms = 264 --> set_head_angle(-14, duration=0.198)
Trigger time ms = 462 --> set_head_angle(12, duration=0.033)
Trigger time ms = 759 --> set_head_angle(-12, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
