Original file = data/sdk_converted/anim_pounce_success_fail_variations
Animation anim_pounce_success_04 clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(69, duration=0.231)
Trigger time ms = 231 --> set_lift_height(57, duration=0.066)
Trigger time ms = 297 --> set_lift_height(69, duration=0.066)
Trigger time ms = 363 --> set_lift_height(57, duration=0.066)
Trigger time ms = 429 --> set_lift_height(69, duration=0.066)
Trigger time ms = 495 --> set_lift_height(57, duration=0.066)
Trigger time ms = 561 --> set_lift_height(70, duration=0.066)
Trigger time ms = 693 --> set_lift_height(83, duration=0.066)
Trigger time ms = 759 --> set_lift_height(55, duration=0.099)
Trigger time ms = 858 --> set_lift_height(66, duration=0.132)
Trigger time ms = 990 --> set_lift_height(71, duration=0.132)
Trigger time ms = 1122 --> set_lift_height(51, duration=0.066)
Trigger time ms = 1188 --> set_lift_height(70, duration=0.132)
Trigger time ms = 1320 --> set_lift_height(35, duration=0.099)
Trigger time ms = 1419 --> set_lift_height(32, duration=0.066)
Trigger time ms = 1815 --> set_lift_height(36, duration=0.066)
Trigger time ms = 1881 --> set_lift_height(32, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-16, duration=0.099)
Trigger time ms = 99 --> set_head_angle(30, duration=0.099)
Trigger time ms = 198 --> set_head_angle(17, duration=0.099)
Trigger time ms = 297 --> set_head_angle(25, duration=0.099)
Trigger time ms = 396 --> set_head_angle(10, duration=0.099)
Trigger time ms = 495 --> set_head_angle(27, duration=0.099)
Trigger time ms = 594 --> set_head_angle(17, duration=0.099)
Trigger time ms = 693 --> set_head_angle(30, duration=0.099)
Trigger time ms = 792 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 924 --> set_head_angle(21, duration=0.198)
Trigger time ms = 1122 --> set_head_angle(4, duration=0.165)
Trigger time ms = 1287 --> set_head_angle(22, duration=0.165)
Trigger time ms = 1452 --> set_head_angle(24, duration=0.099)
Trigger time ms = 1551 --> set_head_angle(24, duration=0.198)
Trigger time ms = 1749 --> set_head_angle(17, duration=0.066)
Trigger time ms = 1815 --> set_head_angle(24, duration=0.066)
Trigger time ms = 2244 --> set_head_angle(24, duration=0.099)
Trigger time ms = 2343 --> set_head_angle(-14, duration=0.099)
Trigger time ms = 2442 --> set_head_angle(-9, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-38, r_wheel_speed=-38, duration=0.066)
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=164, r_wheel_speed=164, duration=0.165)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-79, r_wheel_speed=-79, duration=0.099)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=-15.5, r_wheel_speed=-1410.5, duration=0.033)
Trigger time ms = 759 --> turn_in_place(degrees(-254))
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=74692.5, r_wheel_speed=71587.5, duration=0.033)
Trigger time ms = 858 --> turn_in_place(degrees(221))
Trigger time ms = 990 --> drive_wheels(l_wheel_speed=-37999.0, r_wheel_speed=-34669.0, duration=0.033)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=-164.5, r_wheel_speed=150.5, duration=0.066)
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=-367.5, r_wheel_speed=307.5, duration=0.033)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=408.0, r_wheel_speed=-312.0, duration=0.099)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=87.0, r_wheel_speed=-183.0, duration=0.099)
