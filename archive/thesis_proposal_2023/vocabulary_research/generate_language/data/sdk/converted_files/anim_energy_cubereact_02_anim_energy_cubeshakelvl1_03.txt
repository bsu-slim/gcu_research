Original file = data/sdk_converted/anim_energy_cubereact_02
Animation anim_energy_cubeshakelvl1_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 165 --> set_head_angle(20, duration=0.099)
Trigger time ms = 264 --> set_head_angle(28, duration=0.066)
Trigger time ms = 330 --> set_head_angle(31, duration=0.066)
Trigger time ms = 396 --> set_head_angle(32, duration=0.066)
Trigger time ms = 1023 --> set_head_angle(36, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(31, duration=0.066)
Trigger time ms = 2442 --> set_head_angle(29, duration=0.099)
Trigger time ms = 2607 --> set_head_angle(35, duration=0.165)
Trigger time ms = 2772 --> set_head_angle(36, duration=0.264)
Trigger time ms = 3993 --> set_head_angle(39, duration=0.132)
Trigger time ms = 4125 --> set_head_angle(30, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-49, r_wheel_speed=-49, duration=0.132)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=26, r_wheel_speed=26, duration=0.165)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.099)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.165)
Trigger time ms = 759 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.132)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.132)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.132)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.132)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.132)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.132)
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.132)
Trigger time ms = 1683 --> drive_wheels(l_wheel_speed=10, r_wheel_speed=10, duration=0.132)
Trigger time ms = 1815 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 1947 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 2079 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 2211 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 2343 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 2475 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 2607 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 2739 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 2871 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 3003 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 3135 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 3267 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 3399 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 3531 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 3663 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.132)
Trigger time ms = 3795 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.066)
Trigger time ms = 3960 --> drive_wheels(l_wheel_speed=-21, r_wheel_speed=-21, duration=0.198)
Trigger time ms = 4158 --> drive_wheels(l_wheel_speed=-4, r_wheel_speed=-4, duration=0.132)
