Original file = data/sdk_converted/anim_freeplay_reacttoface_identified
Animation anim_freeplay_reacttoface_identified_03_head_angle_20 clip 11/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 165 --> set_lift_height(48, duration=0.132)
Trigger time ms = 297 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> set_head_angle(25, duration=0.132)
Trigger time ms = 363 --> set_head_angle(18, duration=0.099)
Trigger time ms = 462 --> set_head_angle(45, duration=0.099)
Trigger time ms = 561 --> set_head_angle(41, duration=0.231)
Trigger time ms = 792 --> set_head_angle(19, duration=0.165)
Trigger time ms = 957 --> set_head_angle(21, duration=0.264)
Trigger time ms = 1221 --> set_head_angle(45, duration=0.561)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.132)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-118, r_wheel_speed=-118, duration=0.198)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=33, r_wheel_speed=33, duration=0.264)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=71, r_wheel_speed=71, duration=0.561)
