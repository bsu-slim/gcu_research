Original file = data/sdk_converted/anim_explorer_driving_01
Animation anim_explorer_driving01_turbo_01 clip 7/49
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(5, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 165 --> set_head_angle(15, duration=0.099)
Trigger time ms = 264 --> set_head_angle(20, duration=0.462)
Trigger time ms = 726 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 825 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
