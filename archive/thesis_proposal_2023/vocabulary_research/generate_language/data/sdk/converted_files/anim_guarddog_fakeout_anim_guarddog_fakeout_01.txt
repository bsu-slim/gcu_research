Original file = data/sdk_converted/anim_guarddog_fakeout
Animation anim_guarddog_fakeout_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.165)
Trigger time ms = 1881 --> set_lift_height(38, duration=0.099)
Trigger time ms = 1980 --> set_lift_height(33, duration=0.396)
Trigger time ms = 2376 --> set_lift_height(32, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-18, duration=0.594)
Trigger time ms = 594 --> set_head_angle(-3, duration=1.32)
Trigger time ms = 1914 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 1980 --> set_head_angle(45, duration=0.066)
Trigger time ms = 2046 --> set_head_angle(-11, duration=0.165)
Trigger time ms = 2211 --> set_head_angle(21, duration=0.165)
Trigger time ms = 2376 --> set_head_angle(-6, duration=0.198)
Trigger time ms = 2574 --> set_head_angle(15, duration=0.198)
Trigger time ms = 2772 --> set_head_angle(-6, duration=0.165)
Trigger time ms = 2937 --> set_head_angle(12, duration=0.231)
Trigger time ms = 3168 --> set_head_angle(-3, duration=0.231)
Trigger time ms = 3399 --> set_head_angle(4, duration=0.396)
Trigger time ms = 3795 --> set_head_angle(1, duration=0.495)
Trigger time ms = 4290 --> set_head_angle(2, duration=0.693)
Trigger time ms = 4983 --> set_head_angle(1, duration=0.528)
Trigger time ms = 5511 --> set_head_angle(-5, duration=0.231)
Trigger time ms = 5742 --> set_head_angle(2, duration=0.429)
Trigger time ms = 6171 --> set_head_angle(3, duration=0.33)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-24, r_wheel_speed=-24, duration=0.165)
Trigger time ms = 1881 --> drive_wheels(l_wheel_speed=-136, r_wheel_speed=-136, duration=0.132)
Trigger time ms = 2013 --> drive_wheels(l_wheel_speed=44, r_wheel_speed=44, duration=0.363)
