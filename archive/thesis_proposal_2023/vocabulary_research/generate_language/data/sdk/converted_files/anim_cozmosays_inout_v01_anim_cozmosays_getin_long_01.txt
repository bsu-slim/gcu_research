Original file = data/sdk_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getin_long_01 clip 3/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 990 --> set_lift_height(43, duration=0.066)
Trigger time ms = 1056 --> set_lift_height(0, duration=0.066)
Trigger time ms = 3333 --> set_lift_height(41, duration=0.132)
Trigger time ms = 3465 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-19, duration=0.198)
Trigger time ms = 198 --> set_head_angle(23, duration=0.132)
Trigger time ms = 330 --> set_head_angle(12, duration=0.066)
Trigger time ms = 396 --> set_head_angle(18, duration=0.066)
Trigger time ms = 858 --> set_head_angle(13, duration=0.066)
Trigger time ms = 924 --> set_head_angle(2, duration=0.033)
Trigger time ms = 957 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 1023 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 1122 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 1188 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 1881 --> set_head_angle(-11, duration=0.066)
Trigger time ms = 3201 --> set_head_angle(-25, duration=0.198)
Trigger time ms = 3399 --> set_head_angle(12, duration=0.132)
Trigger time ms = 3531 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=40, r_wheel_speed=40, duration=0.066)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-90, r_wheel_speed=-90, duration=0.231)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=-75, r_wheel_speed=-75, duration=0.066)
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=75, r_wheel_speed=75, duration=0.066)
Trigger time ms = 3366 --> drive_wheels(l_wheel_speed=107, r_wheel_speed=107, duration=0.132)
