Original file = data/sdk_converted/anim_pounce_fail_02
Animation anim_pounce_fail_02 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2013 --> set_lift_height(49, duration=0.066)
Trigger time ms = 2079 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 132 --> set_head_angle(-13, duration=0.066)
Trigger time ms = 627 --> set_head_angle(-14, duration=0.033)
Trigger time ms = 660 --> set_head_angle(38, duration=0.066)
Trigger time ms = 726 --> set_head_angle(38, duration=0.198)
Trigger time ms = 924 --> set_head_angle(37, duration=0.033)
Trigger time ms = 957 --> set_head_angle(38, duration=0.066)
Trigger time ms = 1023 --> set_head_angle(37, duration=0.066)
Trigger time ms = 1089 --> set_head_angle(26, duration=0.066)
Trigger time ms = 1155 --> set_head_angle(37, duration=0.066)
Trigger time ms = 1221 --> set_head_angle(33, duration=0.099)
Trigger time ms = 1320 --> set_head_angle(36, duration=0.099)
Trigger time ms = 1419 --> set_head_angle(37, duration=0.198)
Trigger time ms = 1617 --> set_head_angle(39, duration=0.099)
Trigger time ms = 1716 --> set_head_angle(37, duration=0.231)
Trigger time ms = 1947 --> set_head_angle(-20, duration=0.231)
Trigger time ms = 2178 --> set_head_angle(-14, duration=0.221)
Trigger time ms = 2399 --> set_head_angle(-10, duration=0.195)
Trigger time ms = 2593 --> set_head_angle(-17, duration=0.154)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-299, r_wheel_speed=-299, duration=0.099)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-34, r_wheel_speed=-34, duration=0.198)
