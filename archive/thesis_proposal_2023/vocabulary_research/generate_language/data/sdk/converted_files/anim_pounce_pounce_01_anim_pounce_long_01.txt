Original file = data/sdk_converted/anim_pounce_pounce_01
Animation anim_pounce_long_01 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 33 --> set_lift_height(57, duration=0.099)
Trigger time ms = 132 --> set_lift_height(68, duration=0.066)
Trigger time ms = 198 --> set_lift_height(68, duration=0.066)
Trigger time ms = 264 --> set_lift_height(2, duration=0.066)
Trigger time ms = 330 --> set_lift_height(0, duration=0.033)
Trigger time ms = 396 --> set_lift_height(41, duration=0.099)
Trigger time ms = 495 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(45, duration=0.297)
Trigger time ms = 297 --> set_head_angle(-25, duration=0.198)
Trigger time ms = 495 --> set_head_angle(-13, duration=0.132)
Trigger time ms = 627 --> set_head_angle(-11, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=13007, r_wheel_speed=13007, duration=0.231)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-45, r_wheel_speed=-45, duration=0.264)
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.198)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=-2, r_wheel_speed=-2, duration=0.099)
