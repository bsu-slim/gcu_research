Original file = data/sdk_converted/anim_memorymatch_idle_01
Animation anim_memorymatch_idle_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> set_head_angle(-1, duration=0.165)
Trigger time ms = 297 --> set_head_angle(5, duration=0.132)
Trigger time ms = 429 --> set_head_angle(6, duration=0.099)
Trigger time ms = 2145 --> set_head_angle(3, duration=0.231)
Trigger time ms = 2607 --> set_head_angle(2, duration=0.132)
Trigger time ms = 2739 --> set_head_angle(7, duration=0.231)
Trigger time ms = 2970 --> set_head_angle(8, duration=0.264)
Trigger time ms = 3234 --> set_head_angle(8, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> turn_in_place(degrees(30))
Trigger time ms = 594 --> turn_in_place(degrees(5))
Trigger time ms = 2013 --> turn_in_place(degrees(-23))
Trigger time ms = 2673 --> drive_wheels(l_wheel_speed=-11, r_wheel_speed=-11, duration=0.363)
Trigger time ms = 3036 --> drive_wheels(l_wheel_speed=7, r_wheel_speed=7, duration=0.132)
