Original file = data/sdk_converted/anim_gotosleep_getout_01
Animation anim_gotosleep_getout_04 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 858 --> set_lift_height(38, duration=0.066)
Trigger time ms = 924 --> set_lift_height(55, duration=0.099)
Trigger time ms = 1023 --> set_lift_height(0, duration=0.099)
Trigger time ms = 1716 --> set_lift_height(44, duration=0.066)
Trigger time ms = 1782 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> set_head_angle(-9, duration=0.462)
Trigger time ms = 594 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 726 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 792 --> set_head_angle(39, duration=0.132)
Trigger time ms = 924 --> set_head_angle(-17, duration=0.099)
Trigger time ms = 1023 --> set_head_angle(3, duration=0.198)
Trigger time ms = 1221 --> set_head_angle(5, duration=0.165)
Trigger time ms = 1386 --> set_head_angle(-2, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(5, duration=0.099)
Trigger time ms = 1551 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 1617 --> set_head_angle(5, duration=0.132)
Trigger time ms = 1749 --> set_head_angle(-7, duration=0.099)
Trigger time ms = 2112 --> set_head_angle(5, duration=0.066)
Trigger time ms = 2178 --> set_head_angle(-7, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=-296, r_wheel_speed=-296, duration=0.132)
Trigger time ms = 990 --> drive_wheels(l_wheel_speed=221, r_wheel_speed=221, duration=0.066)
Trigger time ms = 1650 --> turn_in_place(degrees(465))
Trigger time ms = 2145 --> turn_in_place(degrees(-657))
Trigger time ms = 2244 --> turn_in_place(degrees(500))
Trigger time ms = 2376 --> turn_in_place(degrees(-15))
Trigger time ms = 2607 --> turn_in_place(degrees(-30))
Trigger time ms = 2640 --> turn_in_place(degrees(-8))
