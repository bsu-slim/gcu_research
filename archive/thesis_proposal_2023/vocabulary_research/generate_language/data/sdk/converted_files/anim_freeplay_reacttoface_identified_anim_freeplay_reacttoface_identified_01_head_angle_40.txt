Original file = data/sdk_converted/anim_freeplay_reacttoface_identified
Animation anim_freeplay_reacttoface_identified_01_head_angle_40 clip 4/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 495 --> set_lift_height(51, duration=0.066)
Trigger time ms = 561 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(39, duration=0.132)
Trigger time ms = 132 --> set_head_angle(45, duration=0.132)
Trigger time ms = 726 --> set_head_angle(45, duration=0.099)
Trigger time ms = 825 --> set_head_angle(39, duration=0.165)
Trigger time ms = 990 --> set_head_angle(40, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-63, r_wheel_speed=-63, duration=0.033)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=63, r_wheel_speed=63, duration=0.033)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=-63, r_wheel_speed=-63, duration=0.033)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=63, r_wheel_speed=63, duration=0.066)
