Original file = data/sdk_converted/anim_hiccup_02
Animation anim_hiccup_faceplant_02 clip 8/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.429)
Trigger time ms = 759 --> set_lift_height(91, duration=0.231)
Trigger time ms = 990 --> set_lift_height(92, duration=0.264)
Trigger time ms = 1254 --> set_lift_height(0, duration=0.132)
Trigger time ms = 1386 --> set_lift_height(44, duration=0.099)
Trigger time ms = 1485 --> set_lift_height(3, duration=0.099)
Trigger time ms = 1584 --> set_lift_height(0, duration=0.693)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 495 --> set_head_angle(16, duration=0.099)
Trigger time ms = 660 --> set_head_angle(15, duration=0.264)
Trigger time ms = 924 --> set_head_angle(6, duration=0.198)
Trigger time ms = 1122 --> set_head_angle(24, duration=0.198)
Trigger time ms = 1320 --> set_head_angle(27, duration=0.132)
Trigger time ms = 1452 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 1518 --> set_head_angle(2, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(10, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 1749 --> set_head_angle(7, duration=0.132)
Trigger time ms = 1881 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 1947 --> set_head_angle(3, duration=0.099)
Trigger time ms = 2046 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
