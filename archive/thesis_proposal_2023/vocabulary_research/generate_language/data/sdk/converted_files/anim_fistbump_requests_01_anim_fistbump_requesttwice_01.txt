Original file = data/sdk_converted/anim_fistbump_requests_01
Animation anim_fistbump_requesttwice_01 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(80, duration=0.231)
Trigger time ms = 231 --> set_lift_height(78, duration=0.231)
Trigger time ms = 462 --> set_lift_height(53, duration=0.198)
Trigger time ms = 1254 --> set_lift_height(44, duration=0.099)
Trigger time ms = 1353 --> set_lift_height(80, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(30, duration=0.231)
Trigger time ms = 396 --> set_head_angle(12, duration=0.132)
Trigger time ms = 528 --> set_head_angle(8, duration=0.132)
Trigger time ms = 1287 --> set_head_angle(30, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.099)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=-26, r_wheel_speed=-26, duration=0.066)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=80, r_wheel_speed=80, duration=0.132)
Trigger time ms = 1353 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.066)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.198)
