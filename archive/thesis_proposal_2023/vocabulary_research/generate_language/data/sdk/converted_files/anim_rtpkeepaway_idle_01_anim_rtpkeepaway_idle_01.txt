Original file = data/sdk_converted/anim_rtpkeepaway_idle_01
Animation anim_rtpkeepaway_idle_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(10, duration=0.198)
Trigger time ms = 198 --> set_head_angle(6, duration=0.099)
Trigger time ms = 1815 --> set_head_angle(13, duration=0.198)
Trigger time ms = 3696 --> set_head_angle(-5, duration=0.165)
Trigger time ms = 3861 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 495 --> turn_in_place(degrees(40))
Trigger time ms = 693 --> turn_in_place(degrees(-15))
Trigger time ms = 759 --> turn_in_place(degrees(-15))
Trigger time ms = 4026 --> drive_wheels(l_wheel_speed=-13, r_wheel_speed=-13, duration=0.462)
Trigger time ms = 4521 --> turn_in_place(degrees(-17))
