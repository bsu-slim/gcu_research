Original file = data/sdk_converted/anim_hiccup_02
Animation anim_hiccup_withreaction_01 clip 4/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> set_lift_height(0, duration=0.033)
Trigger time ms = 528 --> set_lift_height(9, duration=0.066)
Trigger time ms = 594 --> set_lift_height(49, duration=0.066)
Trigger time ms = 660 --> set_lift_height(0, duration=0.165)
Trigger time ms = 2376 --> set_lift_height(44, duration=0.066)
Trigger time ms = 2442 --> set_lift_height(1, duration=0.099)
Trigger time ms = 2541 --> set_lift_height(0, duration=0.165)
Trigger time ms = 2706 --> set_lift_height(0, duration=0.033)
Trigger time ms = 2739 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 198 --> set_head_angle(4, duration=0.099)
Trigger time ms = 297 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 396 --> set_head_angle(8, duration=0.099)
Trigger time ms = 495 --> set_head_angle(45, duration=0.099)
Trigger time ms = 594 --> set_head_angle(15, duration=0.099)
Trigger time ms = 693 --> set_head_angle(3, duration=0.099)
Trigger time ms = 792 --> set_head_angle(7, duration=0.165)
Trigger time ms = 1287 --> set_head_angle(7, duration=0.099)
Trigger time ms = 1386 --> set_head_angle(7, duration=0.099)
Trigger time ms = 1485 --> set_head_angle(6, duration=0.099)
Trigger time ms = 1584 --> set_head_angle(5, duration=0.099)
Trigger time ms = 1683 --> set_head_angle(2, duration=0.066)
Trigger time ms = 1749 --> set_head_angle(-1, duration=0.099)
Trigger time ms = 1848 --> set_head_angle(-4, duration=0.066)
Trigger time ms = 1914 --> set_head_angle(-7, duration=0.099)
Trigger time ms = 2013 --> set_head_angle(-9, duration=0.297)
Trigger time ms = 2310 --> set_head_angle(-12, duration=0.132)
Trigger time ms = 2442 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 2508 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=47, r_wheel_speed=47, duration=0.066)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-164, r_wheel_speed=-164, duration=0.099)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=79, r_wheel_speed=79, duration=0.099)
Trigger time ms = 1353 --> turn_in_place(degrees(-15))
Trigger time ms = 1419 --> turn_in_place(degrees(61))
Trigger time ms = 1485 --> turn_in_place(degrees(-121))
Trigger time ms = 1551 --> turn_in_place(degrees(91))
Trigger time ms = 1617 --> turn_in_place(degrees(-45))
Trigger time ms = 1749 --> turn_in_place(degrees(15))
Trigger time ms = 1881 --> turn_in_place(degrees(-18))
Trigger time ms = 2046 --> turn_in_place(degrees(24))
Trigger time ms = 2211 --> turn_in_place(degrees(-30))
Trigger time ms = 2376 --> turn_in_place(degrees(71))
