Original file = data/sdk_converted/anim_repair_severe_fix_01
Animation anim_repair_severe_fix_getready_01 clip 12/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1320 --> set_lift_height(57, duration=0.363)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> set_head_angle(9, duration=0.825)
Trigger time ms = 990 --> set_head_angle(3, duration=0.33)
Trigger time ms = 1320 --> set_head_angle(-14, duration=0.363)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=4, r_wheel_speed=4, duration=0.066)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=14, r_wheel_speed=14, duration=0.066)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=28, r_wheel_speed=28, duration=0.066)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=47, r_wheel_speed=47, duration=0.066)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=55, r_wheel_speed=55, duration=0.231)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=21, r_wheel_speed=21, duration=0.099)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=17, r_wheel_speed=17, duration=0.099)
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=14, r_wheel_speed=14, duration=0.099)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=11, r_wheel_speed=11, duration=0.099)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.099)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=-10, r_wheel_speed=-10, duration=0.099)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=-28, r_wheel_speed=-28, duration=0.099)
Trigger time ms = 1320 --> drive_wheels(l_wheel_speed=-45, r_wheel_speed=-45, duration=0.066)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=-63, r_wheel_speed=-63, duration=0.066)
Trigger time ms = 1452 --> drive_wheels(l_wheel_speed=-51, r_wheel_speed=-51, duration=0.066)
Trigger time ms = 1518 --> drive_wheels(l_wheel_speed=-47, r_wheel_speed=-47, duration=0.066)
Trigger time ms = 1584 --> drive_wheels(l_wheel_speed=-12, r_wheel_speed=-12, duration=0.099)
