Original file = data/sdk_converted/vs_speedtap_handreact
Animation anim_speedtap_winhand_03 clip 3/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(88, duration=0.066)
Trigger time ms = 66 --> set_lift_height(92, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-2, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-12, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
