Original file = data/sdk_converted/anim_energy_idles
Animation anim_energy_idlel2_04 clip 7/15
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(30, duration=0.561)
Trigger time ms = 561 --> set_head_angle(25, duration=0.099)
Trigger time ms = 660 --> set_head_angle(36, duration=0.132)
Trigger time ms = 792 --> set_head_angle(38, duration=0.099)
Trigger time ms = 1584 --> set_head_angle(33, duration=0.099)
Trigger time ms = 1683 --> set_head_angle(38, duration=0.198)
Trigger time ms = 2277 --> set_head_angle(34, duration=0.132)
Trigger time ms = 2805 --> set_head_angle(39, duration=0.066)
Trigger time ms = 2871 --> set_head_angle(27, duration=0.198)
Trigger time ms = 3069 --> set_head_angle(30, duration=0.33)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=-22, r_wheel_speed=-22, duration=0.231)
Trigger time ms = 2904 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.165)
