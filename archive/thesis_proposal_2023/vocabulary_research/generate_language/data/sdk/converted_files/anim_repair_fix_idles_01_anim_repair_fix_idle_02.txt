Original file = data/sdk_converted/anim_repair_fix_idles_01
Animation anim_repair_fix_idle_02 clip 2/6
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.198)
Trigger time ms = 198 --> set_head_angle(-13, duration=0.264)
Trigger time ms = 462 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 1056 --> set_head_angle(-12, duration=0.099)
Trigger time ms = 1155 --> set_head_angle(-10, duration=0.165)
Trigger time ms = 2343 --> set_head_angle(13, duration=0.264)
Trigger time ms = 3102 --> set_head_angle(7, duration=0.099)
Trigger time ms = 4785 --> set_head_angle(-4, duration=0.165)
Trigger time ms = 4950 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=-33.0, r_wheel_speed=-3003.0, duration=0.132)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=1423.5, r_wheel_speed=-331.5, duration=0.132)
Trigger time ms = 4785 --> drive_wheels(l_wheel_speed=152022.5, r_wheel_speed=155847.5, duration=0.165)
