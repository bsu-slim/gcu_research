Original file = data/sdk_converted/anim_codelab_kitchen
Animation anim_codelab_kitchen_yucky_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.066)
Trigger time ms = 66 --> set_lift_height(50, duration=0.099)
Trigger time ms = 165 --> set_lift_height(44, duration=0.099)
Trigger time ms = 1254 --> set_lift_height(49, duration=0.066)
Trigger time ms = 1320 --> set_lift_height(32, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(5, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 99 --> set_head_angle(10, duration=0.099)
Trigger time ms = 198 --> set_head_angle(-2, duration=0.132)
Trigger time ms = 330 --> set_head_angle(3, duration=0.066)
Trigger time ms = 396 --> set_head_angle(4, duration=0.858)
Trigger time ms = 1254 --> set_head_angle(14, duration=0.132)
Trigger time ms = 1386 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 1980 --> set_head_angle(2, duration=0.066)
Trigger time ms = 2046 --> set_head_angle(11, duration=0.066)
Trigger time ms = 2112 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-173, r_wheel_speed=-173, duration=0.066)
Trigger time ms = 66 --> turn_in_place(degrees(-300))
Trigger time ms = 132 --> turn_in_place(degrees(300))
Trigger time ms = 231 --> turn_in_place(degrees(-41))
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=0.0, r_wheel_speed=0.0, duration=0.858)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=-4802.0, r_wheel_speed=-13622.0, duration=0.066)
Trigger time ms = 1320 --> drive_wheels(l_wheel_speed=10827.5, r_wheel_speed=7632.5, duration=0.132)
Trigger time ms = 1452 --> turn_in_place(degrees(219))
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=-94.0, r_wheel_speed=86.0, duration=0.066)
Trigger time ms = 1617 --> drive_wheels(l_wheel_speed=94.0, r_wheel_speed=-86.0, duration=0.066)
Trigger time ms = 1683 --> turn_in_place(degrees(-219))
Trigger time ms = 1782 --> drive_wheels(l_wheel_speed=-313.5, r_wheel_speed=541.5, duration=0.099)
Trigger time ms = 1881 --> drive_wheels(l_wheel_speed=486.0, r_wheel_speed=-1134.0, duration=0.132)
Trigger time ms = 2013 --> drive_wheels(l_wheel_speed=87450.0, r_wheel_speed=97350.0, duration=0.066)
