Original file = data/sdk_converted/anim_reacttoblock_placeblock_01
Animation anim_reacttoblock_placeblock_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> set_lift_height(0, duration=0.858)
Trigger time ms = 2112 --> set_lift_height(42, duration=0.165)
Trigger time ms = 2277 --> set_lift_height(0, duration=0.099)
Trigger time ms = 4224 --> set_lift_height(51, duration=0.297)
Trigger time ms = 4521 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 825 --> set_head_angle(-20, duration=0.627)
Trigger time ms = 1452 --> set_head_angle(-10, duration=0.132)
Trigger time ms = 2145 --> set_head_angle(-4, duration=0.165)
Trigger time ms = 2574 --> set_head_angle(-20, duration=0.396)
Trigger time ms = 3069 --> set_head_angle(1, duration=0.198)
Trigger time ms = 3267 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 3828 --> set_head_angle(2, duration=0.066)
Trigger time ms = 4290 --> set_head_angle(-20, duration=0.297)
Trigger time ms = 4587 --> set_head_angle(3, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> turn_in_place(degrees(116))
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=5, r_wheel_speed=5, duration=0.066)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=27, r_wheel_speed=27, duration=0.198)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=-93, r_wheel_speed=-93, duration=0.264)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=35, r_wheel_speed=35, duration=0.165)
Trigger time ms = 2211 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.132)
Trigger time ms = 2343 --> drive_wheels(l_wheel_speed=71, r_wheel_speed=71, duration=0.528)
Trigger time ms = 3036 --> drive_wheels(l_wheel_speed=-127, r_wheel_speed=-127, duration=0.165)
Trigger time ms = 3201 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.132)
Trigger time ms = 4191 --> drive_wheels(l_wheel_speed=-72, r_wheel_speed=-72, duration=0.264)
Trigger time ms = 4455 --> drive_wheels(l_wheel_speed=57, r_wheel_speed=57, duration=0.165)
Trigger time ms = 4620 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.099)
