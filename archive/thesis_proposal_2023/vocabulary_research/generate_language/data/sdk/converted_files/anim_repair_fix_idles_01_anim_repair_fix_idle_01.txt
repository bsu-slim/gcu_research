Original file = data/sdk_converted/anim_repair_fix_idles_01
Animation anim_repair_fix_idle_01 clip 1/6
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 726 --> set_head_angle(-6, duration=0.132)
Trigger time ms = 858 --> set_head_angle(1, duration=0.165)
Trigger time ms = 1683 --> set_head_angle(6, duration=0.264)
Trigger time ms = 3102 --> set_head_angle(0, duration=0.132)
Trigger time ms = 4851 --> set_head_angle(-4, duration=0.165)
Trigger time ms = 5016 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=456.0, r_wheel_speed=-264.0, duration=0.231)
Trigger time ms = 3069 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.264)
Trigger time ms = 3333 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.066)
Trigger time ms = 3399 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.066)
Trigger time ms = 3465 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.066)
Trigger time ms = 3531 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.066)
Trigger time ms = 3597 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.066)
Trigger time ms = 3663 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.066)
Trigger time ms = 3729 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=0.0, duration=0.066)
Trigger time ms = 3795 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=0.0, duration=0.066)
Trigger time ms = 3861 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=0.0, duration=0.066)
Trigger time ms = 4917 --> drive_wheels(l_wheel_speed=-8834.0, r_wheel_speed=-10094.0, duration=0.198)
Trigger time ms = 5115 --> drive_wheels(l_wheel_speed=-12, r_wheel_speed=-12, duration=0.033)
Trigger time ms = 5148 --> drive_wheels(l_wheel_speed=-11, r_wheel_speed=-11, duration=0.033)
Trigger time ms = 5181 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.033)
Trigger time ms = 5214 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.033)
Trigger time ms = 5247 --> drive_wheels(l_wheel_speed=-7, r_wheel_speed=-7, duration=0.033)
Trigger time ms = 5280 --> drive_wheels(l_wheel_speed=-6, r_wheel_speed=-6, duration=0.033)
Trigger time ms = 5313 --> drive_wheels(l_wheel_speed=137287.5, r_wheel_speed=137062.5, duration=0.033)
Trigger time ms = 5346 --> drive_wheels(l_wheel_speed=-87758.0, r_wheel_speed=-87938.0, duration=0.033)
Trigger time ms = 5379 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.033)
Trigger time ms = 5412 --> drive_wheels(l_wheel_speed=-2, r_wheel_speed=-2, duration=0.033)
Trigger time ms = 5445 --> drive_wheels(l_wheel_speed=-2, r_wheel_speed=-2, duration=0.033)
Trigger time ms = 5478 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.033)
Trigger time ms = 5511 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.033)
Trigger time ms = 5544 --> drive_wheels(l_wheel_speed=0, r_wheel_speed=0, duration=0.033)
Trigger time ms = 5577 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=-0.0, duration=0.033)
Trigger time ms = 5610 --> drive_wheels(l_wheel_speed=0.0, r_wheel_speed=0.0, duration=0.033)
