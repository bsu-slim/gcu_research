Original file = data/sdk_converted/anim_repair_severe_interruptions
Animation anim_repair_severe_interruption_edge_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(45, duration=0.198)
Trigger time ms = 198 --> set_lift_height(32, duration=0.132)
Trigger time ms = 330 --> set_lift_height(54, duration=0.132)
Trigger time ms = 462 --> set_lift_height(51, duration=0.495)
Trigger time ms = 957 --> set_lift_height(54, duration=0.132)
Trigger time ms = 1782 --> set_lift_height(55, duration=0.099)
Trigger time ms = 1881 --> set_lift_height(41, duration=0.132)
Trigger time ms = 2013 --> set_lift_height(41, duration=0.594)
Trigger time ms = 2607 --> set_lift_height(47, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-14, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-25, duration=0.165)
Trigger time ms = 231 --> set_head_angle(22, duration=0.099)
Trigger time ms = 330 --> set_head_angle(-25, duration=0.264)
Trigger time ms = 594 --> set_head_angle(-16, duration=0.165)
Trigger time ms = 759 --> set_head_angle(-18, duration=0.099)
Trigger time ms = 957 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 1056 --> set_head_angle(-25, duration=0.132)
Trigger time ms = 1188 --> set_head_angle(-23, duration=0.066)
Trigger time ms = 1254 --> set_head_angle(-22, duration=0.033)
Trigger time ms = 1287 --> set_head_angle(-21, duration=0.066)
Trigger time ms = 1353 --> set_head_angle(-21, duration=0.066)
Trigger time ms = 1419 --> set_head_angle(-23, duration=0.099)
Trigger time ms = 1518 --> set_head_angle(-22, duration=0.231)
Trigger time ms = 1749 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 1815 --> set_head_angle(1, duration=0.198)
Trigger time ms = 2013 --> set_head_angle(1, duration=0.528)
Trigger time ms = 2541 --> set_head_angle(-17, duration=0.099)
Trigger time ms = 2640 --> set_head_angle(-14, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-27, r_wheel_speed=-27, duration=0.099)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-214, r_wheel_speed=-214, duration=0.099)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-161, r_wheel_speed=-161, duration=0.132)
Trigger time ms = 990 --> turn_in_place(degrees(111))
Trigger time ms = 1419 --> turn_in_place(degrees(59))
Trigger time ms = 1485 --> turn_in_place(degrees(25))
Trigger time ms = 1551 --> turn_in_place(degrees(17))
Trigger time ms = 1584 --> turn_in_place(degrees(13))
Trigger time ms = 1617 --> turn_in_place(degrees(9))
Trigger time ms = 1650 --> turn_in_place(degrees(6))
Trigger time ms = 1683 --> turn_in_place(degrees(4))
Trigger time ms = 1716 --> turn_in_place(degrees(2))
Trigger time ms = 1749 --> turn_in_place(degrees(1))
Trigger time ms = 1782 --> turn_in_place(degrees(0))
Trigger time ms = 1815 --> turn_in_place(degrees(-91))
Trigger time ms = 2013 --> turn_in_place(degrees(0))
Trigger time ms = 2574 --> turn_in_place(degrees(96))
