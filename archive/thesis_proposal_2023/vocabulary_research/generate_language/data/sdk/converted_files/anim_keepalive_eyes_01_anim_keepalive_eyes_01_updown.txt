Original file = data/sdk_converted/anim_keepalive_eyes_01
Animation anim_keepalive_eyes_01_updown clip 3/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> set_head_angle(-3, duration=0.165)
Trigger time ms = 264 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 363 --> set_head_angle(5, duration=0.198)
Trigger time ms = 561 --> set_head_angle(7, duration=0.099)
Trigger time ms = 891 --> set_head_angle(5, duration=0.396)
Trigger time ms = 1782 --> set_head_angle(10, duration=0.198)
Trigger time ms = 2277 --> set_head_angle(9, duration=0.033)
Trigger time ms = 2310 --> set_head_angle(8, duration=0.033)
Trigger time ms = 2343 --> set_head_angle(6, duration=0.033)
Trigger time ms = 2376 --> set_head_angle(5, duration=0.033)
Trigger time ms = 2409 --> set_head_angle(5, duration=0.033)
Trigger time ms = 2442 --> set_head_angle(5, duration=0.033)
Trigger time ms = 2475 --> set_head_angle(5, duration=0.033)
Trigger time ms = 2508 --> set_head_angle(5, duration=0.033)
Trigger time ms = 2541 --> set_head_angle(5, duration=0.165)
Trigger time ms = 3003 --> set_head_angle(3, duration=0.066)
Trigger time ms = 3069 --> set_head_angle(1, duration=0.033)
Trigger time ms = 3102 --> set_head_angle(-1, duration=0.033)
Trigger time ms = 3135 --> set_head_angle(-1, duration=0.033)
Trigger time ms = 3168 --> set_head_angle(-1, duration=0.033)
Trigger time ms = 3201 --> set_head_angle(0, duration=0.033)
Trigger time ms = 3234 --> set_head_angle(0, duration=0.033)
Trigger time ms = 3399 --> set_head_angle(0, duration=0.033)
Trigger time ms = 3432 --> set_head_angle(3, duration=0.066)
Trigger time ms = 3498 --> set_head_angle(3, duration=0.033)
Trigger time ms = 3531 --> set_head_angle(3, duration=0.033)
Trigger time ms = 4026 --> set_head_angle(3, duration=0.033)
Trigger time ms = 4059 --> set_head_angle(2, duration=0.033)
Trigger time ms = 4092 --> set_head_angle(2, duration=0.033)
Trigger time ms = 4125 --> set_head_angle(1, duration=0.033)
Trigger time ms = 4158 --> set_head_angle(0, duration=0.033)
Trigger time ms = 4191 --> set_head_angle(0, duration=0.033)
Trigger time ms = 4818 --> set_head_angle(-1, duration=0.033)
Trigger time ms = 4851 --> set_head_angle(-2, duration=0.033)
Trigger time ms = 4884 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 5346 --> set_head_angle(3, duration=0.231)
Trigger time ms = 5775 --> set_head_angle(1, duration=0.33)
Trigger time ms = 6534 --> set_head_angle(7, duration=0.396)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=3, r_wheel_speed=3, duration=0.132)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-19, r_wheel_speed=-19, duration=0.198)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.165)
Trigger time ms = 2079 --> turn_in_place(degrees(-13))
Trigger time ms = 5544 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.033)
Trigger time ms = 5577 --> drive_wheels(l_wheel_speed=2, r_wheel_speed=2, duration=0.066)
Trigger time ms = 5643 --> drive_wheels(l_wheel_speed=4, r_wheel_speed=4, duration=0.099)
Trigger time ms = 5742 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.033)
Trigger time ms = 5775 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.033)
