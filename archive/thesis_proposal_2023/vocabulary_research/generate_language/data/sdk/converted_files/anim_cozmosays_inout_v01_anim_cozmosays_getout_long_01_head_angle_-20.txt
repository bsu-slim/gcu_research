Original file = data/sdk_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getout_long_01_head_angle_-20 clip 22/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 396 --> set_lift_height(38, duration=0.066)
Trigger time ms = 462 --> set_lift_height(0, duration=0.066)
Trigger time ms = 627 --> set_lift_height(42, duration=0.099)
Trigger time ms = 726 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 99 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 462 --> set_head_angle(-24, duration=0.066)
Trigger time ms = 528 --> set_head_angle(-10, duration=0.066)
Trigger time ms = 594 --> set_head_angle(-22, duration=0.066)
Trigger time ms = 660 --> set_head_angle(-25, duration=0.033)
Trigger time ms = 693 --> set_head_angle(-24, duration=0.033)
Trigger time ms = 726 --> set_head_angle(-14, duration=0.099)
Trigger time ms = 825 --> set_head_angle(-15, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=280.5, r_wheel_speed=-214.5, duration=0.066)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=1210.5, r_wheel_speed=-35104.5, duration=0.099)
Trigger time ms = 561 --> drive_wheels(l_wheel_speed=265.0, r_wheel_speed=5035.0, duration=0.033)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-1102.5, r_wheel_speed=3622.5, duration=0.066)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=-493.0, r_wheel_speed=1037.0, duration=0.033)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=-100.0, r_wheel_speed=-1900.0, duration=0.099)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=-98.0, r_wheel_speed=-1358.0, duration=0.033)
