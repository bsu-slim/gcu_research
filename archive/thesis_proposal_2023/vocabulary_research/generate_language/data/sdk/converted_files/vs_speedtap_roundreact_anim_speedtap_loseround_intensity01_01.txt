Original file = data/sdk_converted/vs_speedtap_roundreact
Animation anim_speedtap_loseround_intensity01_01 clip 4/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> set_lift_height(65, duration=0.297)
Trigger time ms = 858 --> set_lift_height(62, duration=0.132)
Trigger time ms = 1089 --> set_lift_height(65, duration=0.165)
Trigger time ms = 1452 --> set_lift_height(92, duration=0.462)
Trigger time ms = 2178 --> set_lift_height(87, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(20, duration=0.165)
Trigger time ms = 726 --> set_head_angle(36, duration=0.099)
Trigger time ms = 825 --> set_head_angle(17, duration=0.132)
Trigger time ms = 957 --> set_head_angle(19, duration=0.033)
Trigger time ms = 990 --> set_head_angle(16, duration=0.033)
Trigger time ms = 1023 --> set_head_angle(19, duration=0.033)
Trigger time ms = 1056 --> set_head_angle(16, duration=0.066)
Trigger time ms = 1122 --> set_head_angle(19, duration=0.033)
Trigger time ms = 1155 --> set_head_angle(16, duration=0.033)
Trigger time ms = 1188 --> set_head_angle(19, duration=0.033)
Trigger time ms = 1287 --> set_head_angle(-3, duration=0.198)
Trigger time ms = 1980 --> set_head_angle(-12, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> drive_wheels(l_wheel_speed=-77, r_wheel_speed=-77, duration=0.561)
Trigger time ms = 957 --> drive_wheels(l_wheel_speed=-65, r_wheel_speed=-65, duration=0.033)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=65, r_wheel_speed=65, duration=0.033)
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=-65, r_wheel_speed=-65, duration=0.033)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=65, r_wheel_speed=65, duration=0.033)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=-65, r_wheel_speed=-65, duration=0.033)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=66, r_wheel_speed=66, duration=0.033)
