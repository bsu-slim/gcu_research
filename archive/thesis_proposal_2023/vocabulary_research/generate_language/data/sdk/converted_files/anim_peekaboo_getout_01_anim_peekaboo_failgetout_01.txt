Original file = data/sdk_converted/anim_peekaboo_getout_01
Animation anim_peekaboo_failgetout_01 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 5082 --> set_lift_height(0, duration=1.188)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(31, duration=0.363)
Trigger time ms = 363 --> set_head_angle(3, duration=0.099)
Trigger time ms = 462 --> set_head_angle(31, duration=0.132)
Trigger time ms = 1188 --> set_head_angle(23, duration=0.099)
Trigger time ms = 1287 --> set_head_angle(31, duration=0.066)
Trigger time ms = 2211 --> set_head_angle(35, duration=0.099)
Trigger time ms = 2310 --> set_head_angle(9, duration=0.231)
Trigger time ms = 2541 --> set_head_angle(6, duration=0.132)
Trigger time ms = 2673 --> set_head_angle(9, duration=0.165)
Trigger time ms = 3564 --> set_head_angle(15, duration=0.165)
Trigger time ms = 4983 --> set_head_angle(16, duration=0.066)
Trigger time ms = 5049 --> set_head_angle(22, duration=0.165)
Trigger time ms = 5973 --> set_head_angle(-8, duration=0.264)
Trigger time ms = 6402 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=-81, r_wheel_speed=-81, duration=0.297)
Trigger time ms = 2244 --> turn_in_place(degrees(114))
Trigger time ms = 2376 --> turn_in_place(degrees(-333))
Trigger time ms = 2640 --> turn_in_place(degrees(-121))
Trigger time ms = 4851 --> turn_in_place(degrees(-61))
Trigger time ms = 5049 --> turn_in_place(degrees(-30))
