Original file = data/sdk_converted/anim_reacttoblock_react_short_01
Animation anim_reacttoblock_react_short_01_head_angle_-20 clip 5/8
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 264 --> set_lift_height(50, duration=0.099)
Trigger time ms = 363 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 297 --> set_head_angle(-25, duration=0.099)
Trigger time ms = 396 --> set_head_angle(-21, duration=0.099)
Trigger time ms = 495 --> set_head_angle(-18, duration=0.132)
Trigger time ms = 627 --> set_head_angle(-20, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-92, r_wheel_speed=-92, duration=0.099)
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=28, r_wheel_speed=28, duration=0.165)
