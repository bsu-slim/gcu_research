Original file = data/sdk_converted/anim_legacy
Animation anim_reacttppl_surprise clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(31, duration=0.1)
Trigger time ms = 100 --> set_lift_height(31, duration=8.9)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-12, duration=0.1)
Trigger time ms = 100 --> set_head_angle(-12, duration=0.3)
Trigger time ms = 400 --> set_head_angle(-12, duration=0.199)
Trigger time ms = 600 --> set_head_angle(-12, duration=0.4)
Trigger time ms = 1000 --> set_head_angle(12, duration=0.1)
Trigger time ms = 1100 --> set_head_angle(13, duration=4.5)
Trigger time ms = 5600 --> set_head_angle(-14, duration=0.3)
Trigger time ms = 5900 --> set_head_angle(-14, duration=0.899)
Trigger time ms = 6800 --> set_head_angle(3, duration=0.7)
Trigger time ms = 7500 --> set_head_angle(3, duration=1.5)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1600 --> turn_in_place(degrees(-35))
Trigger time ms = 2700 --> turn_in_place(degrees(40))
Trigger time ms = 3600 --> turn_in_place(degrees(40))
Trigger time ms = 4400 --> turn_in_place(degrees(30))
Trigger time ms = 5700 --> turn_in_place(degrees(-100))
