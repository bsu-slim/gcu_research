Original file = data/sdk_converted/anim_hiking_lookaround_01
Animation anim_hiking_lookaround_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-14, duration=0.231)
Trigger time ms = 924 --> set_head_angle(-8, duration=0.165)
Trigger time ms = 1089 --> set_head_angle(-8, duration=0.264)
Trigger time ms = 1353 --> set_head_angle(-8, duration=0.891)
Trigger time ms = 2244 --> set_head_angle(-18, duration=0.066)
Trigger time ms = 2310 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 2970 --> set_head_angle(-12, duration=0.165)
Trigger time ms = 3597 --> set_head_angle(-17, duration=0.132)
Trigger time ms = 3729 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 2409 --> turn_in_place(degrees(56))
Trigger time ms = 3036 --> turn_in_place(degrees(81))
Trigger time ms = 3696 --> turn_in_place(degrees(-127))
