Original file = data/sdk_converted/anim_guarddog_getout_timeout
Animation anim_guarddog_getout_timeout_02 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.165)
Trigger time ms = 2673 --> set_lift_height(47, duration=0.33)
Trigger time ms = 3003 --> set_lift_height(32, duration=0.396)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(4, duration=0.165)
Trigger time ms = 165 --> set_head_angle(3, duration=0.231)
Trigger time ms = 396 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 462 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 528 --> set_head_angle(4, duration=0.099)
Trigger time ms = 627 --> set_head_angle(6, duration=0.396)
Trigger time ms = 1023 --> set_head_angle(4, duration=0.462)
Trigger time ms = 1485 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 1551 --> set_head_angle(4, duration=0.099)
Trigger time ms = 1650 --> set_head_angle(7, duration=0.33)
Trigger time ms = 1980 --> set_head_angle(0, duration=0.066)
Trigger time ms = 2046 --> set_head_angle(7, duration=0.264)
Trigger time ms = 2310 --> set_head_angle(5, duration=0.165)
Trigger time ms = 2475 --> set_head_angle(5, duration=0.165)
Trigger time ms = 2640 --> set_head_angle(-17, duration=0.198)
Trigger time ms = 2838 --> set_head_angle(20, duration=0.297)
Trigger time ms = 3135 --> set_head_angle(-4, duration=0.396)
Trigger time ms = 3531 --> set_head_angle(0, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 2640 --> turn_in_place(degrees(42))
Trigger time ms = 2904 --> turn_in_place(degrees(-41))
Trigger time ms = 3267 --> turn_in_place(degrees(15))
