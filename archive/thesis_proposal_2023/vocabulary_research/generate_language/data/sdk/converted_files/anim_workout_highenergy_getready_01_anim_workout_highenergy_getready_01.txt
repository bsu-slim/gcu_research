Original file = data/sdk_converted/anim_workout_highenergy_getready_01
Animation anim_workout_highenergy_getready_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(92, duration=0.429)
Trigger time ms = 429 --> set_lift_height(53, duration=0.297)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.297)
Trigger time ms = 297 --> set_head_angle(-13, duration=0.132)
Trigger time ms = 429 --> set_head_angle(-10, duration=0.264)
Trigger time ms = 693 --> set_head_angle(1, duration=0.264)
Trigger time ms = 1452 --> set_head_angle(36, duration=0.264)
Trigger time ms = 1716 --> set_head_angle(40, duration=0.099)
Trigger time ms = 1815 --> set_head_angle(6, duration=0.231)
Trigger time ms = 2046 --> set_head_angle(4, duration=0.099)
Trigger time ms = 2145 --> set_head_angle(36, duration=0.264)
Trigger time ms = 2409 --> set_head_angle(40, duration=0.099)
Trigger time ms = 2508 --> set_head_angle(6, duration=0.231)
Trigger time ms = 2739 --> set_head_angle(4, duration=0.099)
Trigger time ms = 2838 --> set_head_angle(36, duration=0.264)
Trigger time ms = 3102 --> set_head_angle(40, duration=0.099)
Trigger time ms = 3201 --> set_head_angle(6, duration=0.231)
Trigger time ms = 3432 --> set_head_angle(4, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=36, r_wheel_speed=36, duration=0.165)
Trigger time ms = 1452 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.198)
Trigger time ms = 1815 --> drive_wheels(l_wheel_speed=-24, r_wheel_speed=-24, duration=0.198)
Trigger time ms = 2112 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.231)
Trigger time ms = 2508 --> drive_wheels(l_wheel_speed=-24, r_wheel_speed=-24, duration=0.198)
Trigger time ms = 2805 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.231)
Trigger time ms = 3201 --> drive_wheels(l_wheel_speed=-24, r_wheel_speed=-24, duration=0.198)
