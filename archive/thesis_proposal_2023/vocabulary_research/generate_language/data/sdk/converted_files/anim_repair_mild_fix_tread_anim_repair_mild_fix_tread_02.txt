Original file = data/sdk_converted/anim_repair_mild_fix_tread
Animation anim_repair_mild_fix_tread_02 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(4, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-12, duration=0.099)
Trigger time ms = 462 --> set_head_angle(-18, duration=0.066)
Trigger time ms = 858 --> set_head_angle(10, duration=0.132)
Trigger time ms = 990 --> set_head_angle(5, duration=0.198)
Trigger time ms = 1518 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(8, duration=0.132)
Trigger time ms = 1716 --> set_head_angle(7, duration=0.594)
Trigger time ms = 2310 --> set_head_angle(-2, duration=0.066)
Trigger time ms = 2376 --> set_head_angle(7, duration=0.099)
Trigger time ms = 2475 --> set_head_angle(5, duration=0.528)
Trigger time ms = 3003 --> set_head_angle(2, duration=0.033)
Trigger time ms = 3036 --> set_head_angle(8, duration=0.066)
Trigger time ms = 3102 --> set_head_angle(-3, duration=0.165)
Trigger time ms = 3267 --> set_head_angle(3, duration=0.066)
Trigger time ms = 3333 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-19, r_wheel_speed=-19, duration=0.231)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=-31, r_wheel_speed=-31, duration=0.099)
Trigger time ms = 957 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.198)
Trigger time ms = 1584 --> drive_wheels(l_wheel_speed=-12.0, r_wheel_speed=-1092.0, duration=0.363)
Trigger time ms = 2343 --> drive_wheels(l_wheel_speed=1001.0, r_wheel_speed=11.0, duration=0.396)
Trigger time ms = 2970 --> drive_wheels(l_wheel_speed=46, r_wheel_speed=46, duration=0.066)
Trigger time ms = 3036 --> drive_wheels(l_wheel_speed=22023.0, r_wheel_speed=21213.0, duration=0.165)
