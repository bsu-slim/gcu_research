Original file = data/sdk_converted/anim_reacttoblock_frustrated
Animation anim_reacttoblock_frustrated_int2_01 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 165 --> set_lift_height(38, duration=0.033)
Trigger time ms = 198 --> set_lift_height(0, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> set_head_angle(4, duration=0.033)
Trigger time ms = 264 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 330 --> set_head_angle(0, duration=0.033)
Trigger time ms = 363 --> set_head_angle(-5, duration=0.033)
Trigger time ms = 396 --> set_head_angle(-3, duration=0.033)
Trigger time ms = 429 --> set_head_angle(-5, duration=0.033)
Trigger time ms = 462 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> turn_in_place(degrees(-45))
Trigger time ms = 165 --> turn_in_place(degrees(212))
Trigger time ms = 198 --> turn_in_place(degrees(-20))
