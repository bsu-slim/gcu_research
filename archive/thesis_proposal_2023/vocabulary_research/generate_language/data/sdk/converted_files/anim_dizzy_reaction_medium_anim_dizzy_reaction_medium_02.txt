Original file = data/sdk_converted/anim_dizzy_reaction_medium
Animation anim_dizzy_reaction_medium_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=1.221)
Trigger time ms = 1221 --> set_lift_height(40, duration=0.066)
Trigger time ms = 1287 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1353 --> set_lift_height(41, duration=0.066)
Trigger time ms = 1419 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1485 --> set_lift_height(44, duration=0.066)
Trigger time ms = 1551 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1617 --> set_lift_height(46, duration=0.066)
Trigger time ms = 1683 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1749 --> set_lift_height(49, duration=0.066)
Trigger time ms = 1815 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1881 --> set_lift_height(50, duration=0.066)
Trigger time ms = 1947 --> set_lift_height(0, duration=0.066)
Trigger time ms = 2013 --> set_lift_height(51, duration=0.066)
Trigger time ms = 2079 --> set_lift_height(0, duration=0.066)
Trigger time ms = 2145 --> set_lift_height(50, duration=0.066)
Trigger time ms = 2211 --> set_lift_height(38, duration=0.033)
Trigger time ms = 2244 --> set_lift_height(46, duration=0.066)
Trigger time ms = 2310 --> set_lift_height(0, duration=0.066)
Trigger time ms = 4488 --> set_lift_height(43, duration=0.099)
Trigger time ms = 4587 --> set_lift_height(0, duration=0.066)
Trigger time ms = 5874 --> set_lift_height(43, duration=0.099)
Trigger time ms = 5973 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(9, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-2, duration=0.066)
Trigger time ms = 132 --> set_head_angle(9, duration=0.066)
Trigger time ms = 198 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 264 --> set_head_angle(9, duration=0.066)
Trigger time ms = 330 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 396 --> set_head_angle(9, duration=0.066)
Trigger time ms = 462 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 528 --> set_head_angle(9, duration=0.066)
Trigger time ms = 594 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 660 --> set_head_angle(9, duration=0.066)
Trigger time ms = 726 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 792 --> set_head_angle(9, duration=0.066)
Trigger time ms = 858 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 924 --> set_head_angle(9, duration=0.066)
Trigger time ms = 990 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 1056 --> set_head_angle(11, duration=0.066)
Trigger time ms = 1122 --> set_head_angle(-2, duration=0.066)
Trigger time ms = 1188 --> set_head_angle(14, duration=0.066)
Trigger time ms = 1254 --> set_head_angle(-4, duration=0.066)
Trigger time ms = 1320 --> set_head_angle(15, duration=0.066)
Trigger time ms = 1386 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(16, duration=0.066)
Trigger time ms = 1518 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(20, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 1716 --> set_head_angle(25, duration=0.066)
Trigger time ms = 1782 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 1848 --> set_head_angle(32, duration=0.066)
Trigger time ms = 1914 --> set_head_angle(8, duration=0.066)
Trigger time ms = 1980 --> set_head_angle(35, duration=0.033)
Trigger time ms = 2013 --> set_head_angle(-25, duration=0.033)
Trigger time ms = 2046 --> set_head_angle(45, duration=0.066)
Trigger time ms = 2145 --> set_head_angle(-25, duration=0.033)
Trigger time ms = 2178 --> set_head_angle(45, duration=0.066)
Trigger time ms = 2244 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 2310 --> set_head_angle(45, duration=0.066)
Trigger time ms = 2376 --> set_head_angle(-11, duration=0.132)
Trigger time ms = 2508 --> set_head_angle(6, duration=0.198)
Trigger time ms = 3465 --> set_head_angle(-20, duration=0.033)
Trigger time ms = 3498 --> set_head_angle(-9, duration=0.066)
Trigger time ms = 3564 --> set_head_angle(-7, duration=0.297)
Trigger time ms = 3861 --> set_head_angle(-14, duration=0.033)
Trigger time ms = 3894 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 3960 --> set_head_angle(-10, duration=0.627)
Trigger time ms = 4587 --> set_head_angle(5, duration=0.099)
Trigger time ms = 4686 --> set_head_angle(-7, duration=0.033)
Trigger time ms = 4719 --> set_head_angle(11, duration=0.099)
Trigger time ms = 4818 --> set_head_angle(3, duration=0.264)
Trigger time ms = 5082 --> set_head_angle(12, duration=0.066)
Trigger time ms = 5148 --> set_head_angle(3, duration=0.165)
Trigger time ms = 5313 --> set_head_angle(3, duration=0.165)
Trigger time ms = 5478 --> set_head_angle(3, duration=0.284)
Trigger time ms = 5762 --> set_head_angle(-20, duration=0.056)
Trigger time ms = 5817 --> set_head_angle(13, duration=0.099)
Trigger time ms = 5916 --> set_head_angle(2, duration=0.132)
Trigger time ms = 6048 --> set_head_angle(-1, duration=0.354)
Trigger time ms = 6402 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-478.5, r_wheel_speed=16.5, duration=2.046)
Trigger time ms = 2079 --> drive_wheels(l_wheel_speed=-1920, r_wheel_speed=-1920, duration=0.066)
Trigger time ms = 2244 --> turn_in_place(degrees(758))
Trigger time ms = 4587 --> turn_in_place(degrees(-758))
Trigger time ms = 4653 --> turn_in_place(degrees(1182))
Trigger time ms = 4719 --> turn_in_place(degrees(-848))
Trigger time ms = 4752 --> turn_in_place(degrees(-818))
Trigger time ms = 4785 --> turn_in_place(degrees(636))
Trigger time ms = 4796 --> turn_in_place(degrees(691))
Trigger time ms = 4851 --> turn_in_place(degrees(-167))
Trigger time ms = 4857 --> turn_in_place(degrees(-215))
Trigger time ms = 4950 --> turn_in_place(degrees(83))
Trigger time ms = 4962 --> turn_in_place(degrees(92))
Trigger time ms = 5049 --> turn_in_place(degrees(67))
Trigger time ms = 5064 --> turn_in_place(degrees(-51))
Trigger time ms = 5191 --> turn_in_place(degrees(26))
Trigger time ms = 5346 --> turn_in_place(degrees(-18))
Trigger time ms = 5511 --> turn_in_place(degrees(18))
Trigger time ms = 5676 --> turn_in_place(degrees(-8))
Trigger time ms = 5808 --> turn_in_place(degrees(258))
