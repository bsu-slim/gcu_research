Original file = data/sdk_converted/anim_peekaboo_idle_01
Animation anim_peekaboo_idle_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.396)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(31, duration=0.429)
Trigger time ms = 429 --> set_head_angle(27, duration=0.099)
Trigger time ms = 528 --> set_head_angle(26, duration=0.066)
Trigger time ms = 594 --> set_head_angle(36, duration=0.231)
Trigger time ms = 2079 --> set_head_angle(40, duration=0.165)
Trigger time ms = 2244 --> set_head_angle(42, duration=0.165)
Trigger time ms = 3399 --> set_head_angle(29, duration=0.099)
Trigger time ms = 3498 --> set_head_angle(31, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
