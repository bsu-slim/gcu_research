Original file = data/sdk_converted/anim_rtpkeepaway_reacttocube_01
Animation anim_rtpkeepaway_reacttocube_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 198 --> set_lift_height(39, duration=0.165)
Trigger time ms = 363 --> set_lift_height(44, duration=0.033)
Trigger time ms = 396 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(5, duration=0.099)
Trigger time ms = 99 --> set_head_angle(-15, duration=0.231)
Trigger time ms = 330 --> set_head_angle(6, duration=0.165)
Trigger time ms = 495 --> set_head_angle(-5, duration=0.099)
Trigger time ms = 594 --> set_head_angle(0, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> drive_wheels(l_wheel_speed=-40, r_wheel_speed=-40, duration=0.165)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=100, r_wheel_speed=100, duration=0.198)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=10, r_wheel_speed=10, duration=0.066)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-46, r_wheel_speed=-46, duration=0.132)
