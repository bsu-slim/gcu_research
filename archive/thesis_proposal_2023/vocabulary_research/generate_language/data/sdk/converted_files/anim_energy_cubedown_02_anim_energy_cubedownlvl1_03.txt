Original file = data/sdk_converted/anim_energy_cubedown_02
Animation anim_energy_cubedownlvl1_03 clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.462)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.198)
Trigger time ms = 198 --> set_head_angle(-13, duration=0.165)
Trigger time ms = 363 --> set_head_angle(-17, duration=0.132)
Trigger time ms = 858 --> set_head_angle(-1, duration=0.198)
Trigger time ms = 1485 --> set_head_angle(-11, duration=0.033)
Trigger time ms = 1518 --> set_head_angle(1, duration=0.165)
Trigger time ms = 2673 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 2772 --> set_head_angle(3, duration=0.132)
Trigger time ms = 2904 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1848 --> drive_wheels(l_wheel_speed=10, r_wheel_speed=10, duration=0.132)
Trigger time ms = 1980 --> drive_wheels(l_wheel_speed=23, r_wheel_speed=23, duration=0.429)
Trigger time ms = 2409 --> drive_wheels(l_wheel_speed=17, r_wheel_speed=17, duration=0.165)
Trigger time ms = 2574 --> drive_wheels(l_wheel_speed=7, r_wheel_speed=7, duration=0.165)
Trigger time ms = 2739 --> drive_wheels(l_wheel_speed=-70, r_wheel_speed=-70, duration=0.132)
Trigger time ms = 2871 --> drive_wheels(l_wheel_speed=25, r_wheel_speed=25, duration=0.165)
