Original file = data/sdk_converted/anim_energy_wakeup
Animation anim_energy_wakeup_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.231)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-16, duration=0.363)
Trigger time ms = 363 --> set_head_angle(-9, duration=0.792)
Trigger time ms = 1155 --> set_head_angle(-8, duration=0.264)
Trigger time ms = 1419 --> set_head_angle(3, duration=0.825)
Trigger time ms = 2244 --> set_head_angle(0, duration=0.132)
Trigger time ms = 2376 --> set_head_angle(-1, duration=0.132)
Trigger time ms = 2508 --> set_head_angle(9, duration=0.528)
Trigger time ms = 3036 --> set_head_angle(11, duration=0.165)
Trigger time ms = 3630 --> set_head_angle(-15, duration=0.165)
Trigger time ms = 3795 --> set_head_angle(-5, duration=0.165)
Trigger time ms = 3960 --> set_head_angle(-9, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
