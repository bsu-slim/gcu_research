Original file = data/sdk_converted/anim_energy_idles
Animation anim_energy_idlel2_02 clip 3/15
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 4125 --> set_lift_height(32, duration=0.825)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-9, duration=0.198)
Trigger time ms = 198 --> set_head_angle(-15, duration=0.066)
Trigger time ms = 297 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 1551 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 1650 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 2574 --> set_head_angle(-14, duration=0.066)
Trigger time ms = 2640 --> set_head_angle(-15, duration=0.033)
Trigger time ms = 2673 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 3894 --> set_head_angle(-14, duration=0.033)
Trigger time ms = 3927 --> set_head_angle(-9, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> turn_in_place(degrees(30))
Trigger time ms = 2607 --> turn_in_place(degrees(-36))
