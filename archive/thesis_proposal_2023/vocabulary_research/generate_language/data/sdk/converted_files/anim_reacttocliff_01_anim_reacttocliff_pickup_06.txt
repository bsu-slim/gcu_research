Original file = data/sdk_converted/anim_reacttocliff_01
Animation anim_reacttocliff_pickup_06 clip 9/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> set_lift_height(44, duration=0.066)
Trigger time ms = 132 --> set_lift_height(3, duration=0.099)
Trigger time ms = 231 --> set_lift_height(0, duration=0.792)
Trigger time ms = 1023 --> set_lift_height(0, duration=0.594)
Trigger time ms = 1617 --> set_lift_height(38, duration=0.099)
Trigger time ms = 1716 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 66 --> set_head_angle(9, duration=0.132)
Trigger time ms = 198 --> set_head_angle(9, duration=0.099)
Trigger time ms = 297 --> set_head_angle(8, duration=0.099)
Trigger time ms = 396 --> set_head_angle(-5, duration=0.099)
Trigger time ms = 495 --> set_head_angle(-9, duration=0.528)
Trigger time ms = 1023 --> set_head_angle(-9, duration=0.231)
Trigger time ms = 1254 --> set_head_angle(3, duration=0.231)
Trigger time ms = 1485 --> set_head_angle(-9, duration=0.198)
Trigger time ms = 1683 --> set_head_angle(-19, duration=0.132)
Trigger time ms = 1815 --> set_head_angle(-12, duration=0.132)
Trigger time ms = 1947 --> set_head_angle(-16, duration=0.099)
Trigger time ms = 2046 --> set_head_angle(-16, duration=0.033)
Trigger time ms = 2079 --> set_head_angle(-16, duration=0.099)
Trigger time ms = 2178 --> set_head_angle(-15, duration=0.066)
Trigger time ms = 2574 --> set_head_angle(3, duration=0.165)
Trigger time ms = 2739 --> set_head_angle(1, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1518 --> drive_wheels(l_wheel_speed=-75, r_wheel_speed=-75, duration=0.264)
