Original file = data/sdk_converted/anim_memorymatch_reacttopattern
Animation anim_memorymatch_reacttopattern_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> set_lift_height(38, duration=0.099)
Trigger time ms = 231 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> set_head_angle(-7, duration=0.132)
Trigger time ms = 231 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
