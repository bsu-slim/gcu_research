Original file = data/sdk_converted/anim_memorymatch_failhand
Animation anim_memorymatch_failhand_player_01 clip 4/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(53, duration=0.099)
Trigger time ms = 99 --> set_lift_height(0, duration=0.099)
Trigger time ms = 792 --> set_lift_height(49, duration=0.132)
Trigger time ms = 1518 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 99 --> set_head_angle(20, duration=0.132)
Trigger time ms = 231 --> set_head_angle(15, duration=0.297)
Trigger time ms = 528 --> set_head_angle(15, duration=0.165)
Trigger time ms = 693 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 792 --> set_head_angle(-3, duration=0.033)
Trigger time ms = 825 --> set_head_angle(-1, duration=0.132)
Trigger time ms = 957 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 1023 --> set_head_angle(1, duration=0.066)
Trigger time ms = 1089 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 1155 --> set_head_angle(5, duration=0.099)
Trigger time ms = 1254 --> set_head_angle(0, duration=0.198)
Trigger time ms = 1452 --> set_head_angle(-9, duration=0.066)
Trigger time ms = 1518 --> set_head_angle(21, duration=0.099)
Trigger time ms = 1617 --> set_head_angle(0, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=81, r_wheel_speed=81, duration=0.198)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-15, r_wheel_speed=-15, duration=0.264)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.099)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=-26, r_wheel_speed=-26, duration=0.066)
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=41, r_wheel_speed=41, duration=0.066)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=-45, r_wheel_speed=-45, duration=0.066)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.066)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=38, r_wheel_speed=38, duration=0.066)
Trigger time ms = 1353 --> drive_wheels(l_wheel_speed=-35, r_wheel_speed=-35, duration=0.099)
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=-53, r_wheel_speed=-53, duration=0.198)
