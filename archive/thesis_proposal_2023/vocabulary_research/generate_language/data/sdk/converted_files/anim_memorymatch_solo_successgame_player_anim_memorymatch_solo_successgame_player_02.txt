Original file = data/sdk_converted/anim_memorymatch_solo_successgame_player
Animation anim_memorymatch_solo_successgame_player_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 264 --> set_lift_height(41, duration=0.099)
Trigger time ms = 363 --> set_lift_height(0, duration=0.198)
Trigger time ms = 561 --> set_lift_height(80, duration=0.231)
Trigger time ms = 792 --> set_lift_height(92, duration=0.198)
Trigger time ms = 990 --> set_lift_height(0, duration=0.198)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 132 --> set_head_angle(4, duration=0.198)
Trigger time ms = 330 --> set_head_angle(-12, duration=0.198)
Trigger time ms = 528 --> set_head_angle(34, duration=0.198)
Trigger time ms = 726 --> set_head_angle(33, duration=0.363)
Trigger time ms = 1089 --> set_head_angle(26, duration=0.066)
Trigger time ms = 1155 --> set_head_angle(3, duration=0.099)
Trigger time ms = 1254 --> set_head_angle(-2, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-111, r_wheel_speed=-111, duration=0.165)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.099)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=60, r_wheel_speed=60, duration=0.264)
Trigger time ms = 660 --> turn_in_place(degrees(61))
Trigger time ms = 726 --> turn_in_place(degrees(-136))
Trigger time ms = 792 --> turn_in_place(degrees(197))
Trigger time ms = 858 --> turn_in_place(degrees(-227))
Trigger time ms = 924 --> turn_in_place(degrees(136))
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=26, r_wheel_speed=26, duration=0.132)
