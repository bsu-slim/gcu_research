Original file = data/sdk_converted/anim_reacttocliff_turtleroll_01
Animation anim_reacttocliff_turtleroll_02 clip 2/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1114 --> set_lift_height(1, duration=0.041)
Trigger time ms = 1287 --> set_lift_height(61, duration=0.094)
Trigger time ms = 1381 --> set_lift_height(0, duration=0.082)
Trigger time ms = 2479 --> set_lift_height(1, duration=0.165)
Trigger time ms = 3185 --> set_lift_height(0, duration=0.124)
Trigger time ms = 4905 --> set_lift_height(92, duration=0.053)
Trigger time ms = 4958 --> set_lift_height(0, duration=0.103)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 479 --> set_head_angle(45, duration=0.082)
Trigger time ms = 798 --> set_head_angle(-17, duration=0.159)
Trigger time ms = 957 --> set_head_angle(-19, duration=0.14)
Trigger time ms = 1097 --> set_head_angle(-21, duration=0.19)
Trigger time ms = 1287 --> set_head_angle(-20, duration=0.338)
Trigger time ms = 1625 --> set_head_angle(-20, duration=0.404)
Trigger time ms = 2030 --> set_head_angle(-16, duration=0.454)
Trigger time ms = 2483 --> set_head_angle(-1, duration=0.082)
Trigger time ms = 2566 --> set_head_angle(0, duration=0.289)
Trigger time ms = 2855 --> set_head_angle(26, duration=0.082)
Trigger time ms = 2937 --> set_head_angle(-10, duration=0.082)
Trigger time ms = 3020 --> set_head_angle(-1, duration=0.083)
Trigger time ms = 3102 --> set_head_angle(0, duration=0.082)
Trigger time ms = 3185 --> set_head_angle(18, duration=0.083)
Trigger time ms = 3267 --> set_head_angle(-18, duration=0.082)
Trigger time ms = 3350 --> set_head_angle(0, duration=0.158)
Trigger time ms = 3507 --> set_head_angle(-1, duration=0.214)
Trigger time ms = 3721 --> set_head_angle(-20, duration=0.082)
Trigger time ms = 3803 --> set_head_angle(-18, duration=0.165)
Trigger time ms = 4752 --> set_head_angle(25, duration=0.12)
Trigger time ms = 4872 --> set_head_angle(17, duration=0.086)
Trigger time ms = 4958 --> set_head_angle(-21, duration=0.087)
Trigger time ms = 5045 --> set_head_angle(16, duration=0.132)
Trigger time ms = 5243 --> set_head_angle(-22, duration=0.066)
Trigger time ms = 5309 --> set_head_angle(33, duration=0.132)
Trigger time ms = 5441 --> set_head_angle(-14, duration=0.099)
Trigger time ms = 5540 --> set_head_angle(13, duration=0.132)
Trigger time ms = 5672 --> set_head_angle(-13, duration=0.066)
Trigger time ms = 5738 --> set_head_angle(3, duration=0.198)
Trigger time ms = 5936 --> set_head_angle(0, duration=0.058)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 2318 --> turn_in_place(degrees(108))
Trigger time ms = 2467 --> turn_in_place(degrees(-493))
Trigger time ms = 2607 --> turn_in_place(degrees(682))
Trigger time ms = 2739 --> turn_in_place(degrees(-417))
Trigger time ms = 2871 --> turn_in_place(degrees(440))
Trigger time ms = 2996 --> turn_in_place(degrees(-439))
Trigger time ms = 3128 --> turn_in_place(degrees(326))
Trigger time ms = 3260 --> turn_in_place(degrees(-177))
Trigger time ms = 5043 --> drive_wheels(l_wheel_speed=144, r_wheel_speed=144, duration=0.066)
Trigger time ms = 5109 --> drive_wheels(l_wheel_speed=-845, r_wheel_speed=-845, duration=0.422)
