Original file = data/sdk_converted/anim_reacttoblock_react_short_01
Animation anim_reacttoblock_react_short_02_head_angle_-20 clip 8/8
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 297 --> set_head_angle(-6, duration=0.099)
Trigger time ms = 396 --> set_head_angle(-19, duration=0.099)
Trigger time ms = 495 --> set_head_angle(-22, duration=0.132)
Trigger time ms = 627 --> set_head_angle(-20, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-44, r_wheel_speed=-44, duration=0.099)
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=14, r_wheel_speed=14, duration=0.165)
