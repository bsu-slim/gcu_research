Original file = data/sdk_converted/anim_gif_01
Animation anim_gif_gleeserious_01 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=2.31)
Trigger time ms = 2310 --> set_lift_height(41, duration=0.066)
Trigger time ms = 2376 --> set_lift_height(32, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.528)
Trigger time ms = 528 --> set_head_angle(-12, duration=0.132)
Trigger time ms = 660 --> set_head_angle(16, duration=0.198)
Trigger time ms = 858 --> set_head_angle(26, duration=0.231)
Trigger time ms = 1122 --> set_head_angle(20, duration=0.099)
Trigger time ms = 1221 --> set_head_angle(26, duration=0.132)
Trigger time ms = 1386 --> set_head_angle(20, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(26, duration=0.132)
Trigger time ms = 1617 --> set_head_angle(20, duration=0.099)
Trigger time ms = 1716 --> set_head_angle(26, duration=0.099)
Trigger time ms = 1848 --> set_head_angle(20, duration=0.099)
Trigger time ms = 1947 --> set_head_angle(26, duration=0.132)
Trigger time ms = 2112 --> set_head_angle(20, duration=0.198)
Trigger time ms = 2310 --> set_head_angle(-20, duration=0.099)
Trigger time ms = 2409 --> set_head_angle(-9, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=-54, r_wheel_speed=-54, duration=0.165)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=80, r_wheel_speed=80, duration=0.231)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.099)
Trigger time ms = 990 --> drive_wheels(l_wheel_speed=-21, r_wheel_speed=-21, duration=0.132)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.033)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=28, r_wheel_speed=28, duration=0.099)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.033)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=-28, r_wheel_speed=-28, duration=0.099)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.033)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=42, r_wheel_speed=42, duration=0.066)
Trigger time ms = 1485 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.033)
Trigger time ms = 1518 --> drive_wheels(l_wheel_speed=-28, r_wheel_speed=-28, duration=0.099)
Trigger time ms = 1617 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.033)
Trigger time ms = 1650 --> drive_wheels(l_wheel_speed=28, r_wheel_speed=28, duration=0.099)
Trigger time ms = 1749 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.033)
Trigger time ms = 1782 --> drive_wheels(l_wheel_speed=-42, r_wheel_speed=-42, duration=0.066)
Trigger time ms = 1848 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.033)
Trigger time ms = 1881 --> drive_wheels(l_wheel_speed=28, r_wheel_speed=28, duration=0.099)
Trigger time ms = 1980 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.033)
Trigger time ms = 2013 --> drive_wheels(l_wheel_speed=-28, r_wheel_speed=-28, duration=0.099)
Trigger time ms = 2112 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.033)
Trigger time ms = 2145 --> drive_wheels(l_wheel_speed=31, r_wheel_speed=31, duration=0.099)
Trigger time ms = 2244 --> turn_in_place(degrees(155))
