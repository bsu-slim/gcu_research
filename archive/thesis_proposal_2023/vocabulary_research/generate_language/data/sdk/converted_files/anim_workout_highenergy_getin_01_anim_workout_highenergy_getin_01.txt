Original file = data/sdk_converted/anim_workout_highenergy_getin_01
Animation anim_workout_highenergy_getin_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.99)
Trigger time ms = 990 --> set_lift_height(43, duration=0.066)
Trigger time ms = 1056 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1122 --> set_lift_height(43, duration=0.066)
Trigger time ms = 1188 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-12, duration=0.132)
Trigger time ms = 264 --> set_head_angle(-13, duration=0.099)
Trigger time ms = 363 --> set_head_angle(-3, duration=0.165)
Trigger time ms = 528 --> set_head_angle(2, duration=0.33)
Trigger time ms = 858 --> set_head_angle(-14, duration=0.099)
Trigger time ms = 957 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 1320 --> set_head_angle(3, duration=0.132)
Trigger time ms = 1452 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=-92, r_wheel_speed=-92, duration=0.132)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.165)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=41, r_wheel_speed=41, duration=0.264)
Trigger time ms = 990 --> drive_wheels(l_wheel_speed=-53, r_wheel_speed=-53, duration=0.033)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=65, r_wheel_speed=65, duration=0.066)
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=-65, r_wheel_speed=-65, duration=0.033)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=53, r_wheel_speed=53, duration=0.066)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=33, r_wheel_speed=33, duration=0.132)
