Original file = data/sdk_converted/anim_codelab_tinyorchestra
Animation anim_codelab_tinyorchestra_conducting clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.165)
Trigger time ms = 165 --> set_lift_height(45, duration=0.165)
Trigger time ms = 396 --> set_lift_height(32, duration=0.165)
Trigger time ms = 759 --> set_lift_height(41, duration=0.198)
Trigger time ms = 957 --> set_lift_height(35, duration=0.099)
Trigger time ms = 1056 --> set_lift_height(32, duration=0.066)
Trigger time ms = 3432 --> set_lift_height(56, duration=0.264)
Trigger time ms = 3696 --> set_lift_height(48, duration=0.231)
Trigger time ms = 3927 --> set_lift_height(55, duration=0.231)
Trigger time ms = 4158 --> set_lift_height(32, duration=0.429)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 165 --> set_head_angle(8, duration=0.231)
Trigger time ms = 396 --> set_head_angle(9, duration=0.231)
Trigger time ms = 627 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 693 --> set_head_angle(13, duration=0.231)
Trigger time ms = 924 --> set_head_angle(14, duration=0.297)
Trigger time ms = 1221 --> set_head_angle(4, duration=0.099)
Trigger time ms = 1320 --> set_head_angle(14, duration=0.198)
Trigger time ms = 1518 --> set_head_angle(15, duration=0.297)
Trigger time ms = 1815 --> set_head_angle(3, duration=0.165)
Trigger time ms = 1980 --> set_head_angle(15, duration=0.297)
Trigger time ms = 2277 --> set_head_angle(6, duration=0.33)
Trigger time ms = 2607 --> set_head_angle(22, duration=0.363)
Trigger time ms = 2970 --> set_head_angle(-6, duration=0.231)
Trigger time ms = 3201 --> set_head_angle(15, duration=0.165)
Trigger time ms = 3366 --> set_head_angle(5, duration=0.231)
Trigger time ms = 3597 --> set_head_angle(15, duration=0.264)
Trigger time ms = 3861 --> set_head_angle(6, duration=0.231)
Trigger time ms = 4092 --> set_head_angle(13, duration=0.33)
Trigger time ms = 4422 --> set_head_angle(3, duration=0.264)
Trigger time ms = 4686 --> set_head_angle(12, duration=0.363)
Trigger time ms = 5049 --> set_head_angle(2, duration=0.264)
Trigger time ms = 5313 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 5445 --> set_head_angle(8, duration=0.231)
Trigger time ms = 5676 --> set_head_angle(-2, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-112.0, r_wheel_speed=-1552.0, duration=0.198)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=122.5, r_wheel_speed=-102.5, duration=0.231)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=0.0, duration=0.033)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=0.0, duration=0.066)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=0.0, r_wheel_speed=0.0, duration=0.132)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=136.5, r_wheel_speed=1.5, duration=0.396)
Trigger time ms = 2244 --> drive_wheels(l_wheel_speed=0.0, r_wheel_speed=0.0, duration=0.429)
Trigger time ms = 3234 --> drive_wheels(l_wheel_speed=27, r_wheel_speed=27, duration=0.33)
Trigger time ms = 4323 --> drive_wheels(l_wheel_speed=-63, r_wheel_speed=-63, duration=0.066)
Trigger time ms = 4389 --> drive_wheels(l_wheel_speed=21, r_wheel_speed=21, duration=0.198)
Trigger time ms = 5247 --> drive_wheels(l_wheel_speed=-781.0, r_wheel_speed=209.0, duration=0.297)
