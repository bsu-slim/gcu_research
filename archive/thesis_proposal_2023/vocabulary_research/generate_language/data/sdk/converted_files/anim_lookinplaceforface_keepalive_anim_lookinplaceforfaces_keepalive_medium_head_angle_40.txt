Original file = data/sdk_converted/anim_lookinplaceforface_keepalive
Animation anim_lookinplaceforfaces_keepalive_medium_head_angle_40 clip 9/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> set_head_angle(36, duration=0.132)
Trigger time ms = 231 --> set_head_angle(45, duration=0.165)
Trigger time ms = 396 --> set_head_angle(45, duration=0.297)
Trigger time ms = 693 --> set_head_angle(37, duration=0.099)
Trigger time ms = 792 --> set_head_angle(39, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> turn_in_place(degrees(-24))
Trigger time ms = 264 --> turn_in_place(degrees(-8))
