Original file = data/sdk_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getout_medium_01_head_angle_20 clip 20/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 231 --> set_lift_height(40, duration=0.099)
Trigger time ms = 330 --> set_lift_height(0, duration=0.132)
Trigger time ms = 1155 --> set_lift_height(49, duration=0.099)
Trigger time ms = 1254 --> set_lift_height(3, duration=0.066)
Trigger time ms = 1320 --> set_lift_height(41, duration=0.066)
Trigger time ms = 1386 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> set_head_angle(32, duration=0.066)
Trigger time ms = 231 --> set_head_angle(40, duration=0.099)
Trigger time ms = 330 --> set_head_angle(24, duration=0.231)
Trigger time ms = 561 --> set_head_angle(5, duration=0.099)
Trigger time ms = 660 --> set_head_angle(34, duration=0.099)
Trigger time ms = 1023 --> set_head_angle(30, duration=0.066)
Trigger time ms = 1287 --> set_head_angle(13, duration=0.066)
Trigger time ms = 1353 --> set_head_angle(31, duration=0.066)
Trigger time ms = 1419 --> set_head_angle(12, duration=0.066)
Trigger time ms = 1485 --> set_head_angle(27, duration=0.066)
Trigger time ms = 1551 --> set_head_angle(20, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> turn_in_place(degrees(127))
Trigger time ms = 429 --> turn_in_place(degrees(71))
Trigger time ms = 528 --> turn_in_place(degrees(20))
Trigger time ms = 627 --> turn_in_place(degrees(-197))
Trigger time ms = 759 --> turn_in_place(degrees(-40))
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=-90, r_wheel_speed=-90, duration=0.066)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=226, r_wheel_speed=226, duration=0.132)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=-12, r_wheel_speed=-12, duration=0.066)
