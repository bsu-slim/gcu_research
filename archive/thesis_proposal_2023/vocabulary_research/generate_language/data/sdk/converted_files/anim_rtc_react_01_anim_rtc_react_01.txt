Original file = data/sdk_converted/anim_rtc_react_01
Animation anim_rtc_react_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.082)
Trigger time ms = 82 --> set_lift_height(41, duration=0.136)
Trigger time ms = 217 --> set_lift_height(0, duration=0.054)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-7, duration=0.054)
Trigger time ms = 54 --> set_head_angle(-8, duration=0.027)
Trigger time ms = 82 --> set_head_angle(7, duration=0.081)
Trigger time ms = 163 --> set_head_angle(8, duration=0.054)
Trigger time ms = 217 --> set_head_angle(-4, duration=0.136)
Trigger time ms = 353 --> set_head_angle(0, duration=0.082)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 109 --> drive_wheels(l_wheel_speed=-18, r_wheel_speed=-18, duration=0.054)
Trigger time ms = 163 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.055)
Trigger time ms = 217 --> drive_wheels(l_wheel_speed=5, r_wheel_speed=5, duration=0.377)
