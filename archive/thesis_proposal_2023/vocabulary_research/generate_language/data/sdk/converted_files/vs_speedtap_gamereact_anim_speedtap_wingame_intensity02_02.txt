Original file = data/sdk_converted/vs_speedtap_gamereact
Animation anim_speedtap_wingame_intensity02_02 clip 3/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(92, duration=0.033)
Trigger time ms = 33 --> set_lift_height(0, duration=0.56)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-12, duration=0.297)
Trigger time ms = 297 --> set_head_angle(18, duration=0.296)
Trigger time ms = 593 --> set_head_angle(17, duration=0.066)
Trigger time ms = 659 --> set_head_angle(-19, duration=0.066)
Trigger time ms = 956 --> set_head_angle(17, duration=0.132)
Trigger time ms = 1088 --> set_head_angle(16, duration=0.396)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> drive_wheels(l_wheel_speed=-133, r_wheel_speed=-133, duration=0.56)
Trigger time ms = 692 --> drive_wheels(l_wheel_speed=160, r_wheel_speed=160, duration=0.066)
Trigger time ms = 791 --> drive_wheels(l_wheel_speed=-308, r_wheel_speed=-308, duration=0.033)
Trigger time ms = 857 --> drive_wheels(l_wheel_speed=300, r_wheel_speed=300, duration=0.033)
Trigger time ms = 923 --> drive_wheels(l_wheel_speed=-240, r_wheel_speed=-240, duration=0.033)
