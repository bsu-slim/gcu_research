Original file = data/sdk_converted/vs_speedtap_ask2play
Animation anim_speedtap_look4block_01 clip 1/7
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> set_lift_height(37, duration=0.099)
Trigger time ms = 198 --> set_lift_height(0, duration=0.099)
Trigger time ms = 693 --> set_lift_height(41, duration=0.066)
Trigger time ms = 759 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 693 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 792 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=1522.5, r_wheel_speed=-52.5, duration=0.264)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=4836.0, r_wheel_speed=3756.0, duration=0.132)
