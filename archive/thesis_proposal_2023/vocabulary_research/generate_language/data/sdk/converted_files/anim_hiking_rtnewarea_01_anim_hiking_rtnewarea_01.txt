Original file = data/sdk_converted/anim_hiking_rtnewarea_01
Animation anim_hiking_rtnewarea_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2178 --> set_lift_height(50, duration=0.066)
Trigger time ms = 2244 --> set_lift_height(0, duration=0.066)
Trigger time ms = 2310 --> set_lift_height(50, duration=0.066)
Trigger time ms = 2376 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-3, duration=0.198)
Trigger time ms = 198 --> set_head_angle(30, duration=0.264)
Trigger time ms = 627 --> set_head_angle(21, duration=0.099)
Trigger time ms = 726 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1287 --> set_head_angle(21, duration=0.099)
Trigger time ms = 1386 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1914 --> set_head_angle(21, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 2211 --> set_head_angle(-13, duration=0.099)
Trigger time ms = 2475 --> set_head_angle(0, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.264)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=1.914)
Trigger time ms = 2178 --> drive_wheels(l_wheel_speed=-97, r_wheel_speed=-97, duration=0.132)
Trigger time ms = 2310 --> drive_wheels(l_wheel_speed=10, r_wheel_speed=10, duration=0.66)
