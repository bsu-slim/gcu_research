Original file = data/sdk_converted/anim_workout_highenergy_trans_01
Animation anim_workout_highenergy_trans_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(53, duration=0.396)
Trigger time ms = 396 --> set_lift_height(91, duration=0.528)
Trigger time ms = 1122 --> set_lift_height(53, duration=0.396)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(6, duration=0.099)
Trigger time ms = 99 --> set_head_angle(-7, duration=0.165)
Trigger time ms = 264 --> set_head_angle(17, duration=0.33)
Trigger time ms = 594 --> set_head_angle(25, duration=0.231)
Trigger time ms = 957 --> set_head_angle(-15, duration=0.462)
Trigger time ms = 1419 --> set_head_angle(6, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.165)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-74, r_wheel_speed=-74, duration=0.33)
Trigger time ms = 561 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.33)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=73, r_wheel_speed=73, duration=0.297)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.165)
