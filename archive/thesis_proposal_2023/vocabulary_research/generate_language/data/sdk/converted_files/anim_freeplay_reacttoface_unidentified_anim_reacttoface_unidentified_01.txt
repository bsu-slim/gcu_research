Original file = data/sdk_converted/anim_freeplay_reacttoface_unidentified
Animation anim_reacttoface_unidentified_01 clip 2/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 495 --> set_lift_height(42, duration=0.099)
Trigger time ms = 594 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 99 --> set_head_angle(12, duration=0.099)
Trigger time ms = 198 --> set_head_angle(11, duration=0.165)
Trigger time ms = 627 --> set_head_angle(5, duration=0.066)
Trigger time ms = 693 --> set_head_angle(21, duration=0.099)
Trigger time ms = 792 --> set_head_angle(8, duration=0.264)
Trigger time ms = 1056 --> set_head_angle(4, duration=0.099)
Trigger time ms = 1155 --> set_head_angle(5, duration=0.066)
Trigger time ms = 1221 --> set_head_angle(8, duration=0.132)
Trigger time ms = 1353 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=-32, r_wheel_speed=-32, duration=0.066)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=32, r_wheel_speed=32, duration=0.198)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=-28, r_wheel_speed=-28, duration=0.264)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.066)
