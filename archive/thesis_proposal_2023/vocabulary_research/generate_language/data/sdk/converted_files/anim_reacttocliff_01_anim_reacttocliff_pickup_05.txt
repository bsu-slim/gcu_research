Original file = data/sdk_converted/anim_reacttocliff_01
Animation anim_reacttocliff_pickup_05 clip 8/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> set_lift_height(42, duration=0.107)
Trigger time ms = 239 --> set_lift_height(5, duration=0.091)
Trigger time ms = 330 --> set_lift_height(0, duration=0.08)
Trigger time ms = 410 --> set_lift_height(0, duration=0.646)
Trigger time ms = 1056 --> set_lift_height(39, duration=0.099)
Trigger time ms = 1155 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1221 --> set_lift_height(0, duration=0.297)
Trigger time ms = 2178 --> set_lift_height(45, duration=0.066)
Trigger time ms = 2244 --> set_lift_height(0, duration=0.099)
Trigger time ms = 2607 --> set_lift_height(0, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 200 --> set_head_angle(12, duration=0.07)
Trigger time ms = 271 --> set_head_angle(22, duration=0.057)
Trigger time ms = 327 --> set_head_angle(41, duration=0.066)
Trigger time ms = 393 --> set_head_angle(37, duration=0.069)
Trigger time ms = 462 --> set_head_angle(33, duration=0.495)
Trigger time ms = 957 --> set_head_angle(37, duration=0.099)
Trigger time ms = 1056 --> set_head_angle(38, duration=0.099)
Trigger time ms = 1155 --> set_head_angle(34, duration=0.066)
Trigger time ms = 1221 --> set_head_angle(25, duration=0.033)
Trigger time ms = 1254 --> set_head_angle(23, duration=0.363)
Trigger time ms = 1617 --> set_head_angle(30, duration=0.066)
Trigger time ms = 1683 --> set_head_angle(32, duration=0.094)
Trigger time ms = 1777 --> set_head_angle(31, duration=0.053)
Trigger time ms = 1830 --> set_head_angle(31, duration=0.117)
Trigger time ms = 2178 --> set_head_angle(-3, duration=0.132)
Trigger time ms = 2310 --> set_head_angle(-1, duration=0.066)
Trigger time ms = 2376 --> set_head_angle(-1, duration=0.165)
Trigger time ms = 2607 --> set_head_angle(-1, duration=0.033)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=79, r_wheel_speed=79, duration=0.066)
Trigger time ms = 561 --> drive_wheels(l_wheel_speed=-138, r_wheel_speed=-138, duration=0.099)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=132, r_wheel_speed=132, duration=0.132)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=-136, r_wheel_speed=-136, duration=0.066)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=139, r_wheel_speed=139, duration=0.099)
Trigger time ms = 2178 --> drive_wheels(l_wheel_speed=-39, r_wheel_speed=-39, duration=0.132)
