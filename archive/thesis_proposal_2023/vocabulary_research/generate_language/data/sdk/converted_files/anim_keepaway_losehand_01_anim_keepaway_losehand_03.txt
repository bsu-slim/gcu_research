Original file = data/sdk_converted/anim_keepaway_losehand_01
Animation anim_keepaway_losehand_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1947 --> set_lift_height(92, duration=0.33)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> set_head_angle(-19, duration=0.033)
Trigger time ms = 198 --> set_head_angle(-16, duration=0.132)
Trigger time ms = 627 --> set_head_angle(4, duration=0.099)
Trigger time ms = 1221 --> set_head_angle(1, duration=0.033)
Trigger time ms = 1254 --> set_head_angle(4, duration=0.099)
Trigger time ms = 1914 --> set_head_angle(-17, duration=0.33)
Trigger time ms = 2244 --> set_head_angle(-16, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 561 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.132)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.033)
Trigger time ms = 1947 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.264)
