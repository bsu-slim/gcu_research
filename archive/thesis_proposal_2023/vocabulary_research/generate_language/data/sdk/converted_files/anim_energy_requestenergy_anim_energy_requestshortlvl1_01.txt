Original file = data/sdk_converted/anim_energy_requestenergy
Animation anim_energy_requestshortlvl1_01 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.396)
Trigger time ms = 396 --> set_lift_height(54, duration=0.198)
Trigger time ms = 759 --> set_lift_height(47, duration=0.132)
Trigger time ms = 891 --> set_lift_height(55, duration=0.099)
Trigger time ms = 990 --> set_lift_height(57, duration=0.066)
Trigger time ms = 1056 --> set_lift_height(47, duration=0.066)
Trigger time ms = 1122 --> set_lift_height(55, duration=0.099)
Trigger time ms = 1221 --> set_lift_height(57, duration=0.066)
Trigger time ms = 1287 --> set_lift_height(47, duration=0.066)
Trigger time ms = 1353 --> set_lift_height(55, duration=0.099)
Trigger time ms = 1452 --> set_lift_height(57, duration=0.066)
Trigger time ms = 1848 --> set_lift_height(32, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.033)
Trigger time ms = 33 --> set_head_angle(3, duration=0.099)
Trigger time ms = 132 --> set_head_angle(-7, duration=0.132)
Trigger time ms = 264 --> set_head_angle(36, duration=0.396)
Trigger time ms = 660 --> set_head_angle(29, duration=0.099)
Trigger time ms = 1287 --> set_head_angle(32, duration=0.132)
Trigger time ms = 1419 --> set_head_angle(32, duration=0.561)
Trigger time ms = 1980 --> set_head_angle(-9, duration=0.264)
Trigger time ms = 2244 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=-48, r_wheel_speed=-48, duration=0.231)
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=-12, r_wheel_speed=-12, duration=0.165)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=8, r_wheel_speed=8, duration=0.132)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=35, r_wheel_speed=35, duration=0.396)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.099)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 1947 --> drive_wheels(l_wheel_speed=21, r_wheel_speed=21, duration=0.33)
