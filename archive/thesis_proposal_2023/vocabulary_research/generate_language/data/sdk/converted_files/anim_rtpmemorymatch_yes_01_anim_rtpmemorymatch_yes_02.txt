Original file = data/sdk_converted/anim_rtpmemorymatch_yes_01
Animation anim_rtpmemorymatch_yes_02 clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> set_lift_height(40, duration=0.066)
Trigger time ms = 198 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-3, duration=0.132)
Trigger time ms = 132 --> set_head_angle(31, duration=0.132)
Trigger time ms = 264 --> set_head_angle(28, duration=0.099)
Trigger time ms = 363 --> set_head_angle(28, duration=0.165)
Trigger time ms = 528 --> set_head_angle(36, duration=0.033)
Trigger time ms = 594 --> set_head_angle(24, duration=0.033)
Trigger time ms = 693 --> set_head_angle(34, duration=0.033)
Trigger time ms = 759 --> set_head_angle(33, duration=0.165)
Trigger time ms = 924 --> set_head_angle(5, duration=0.066)
Trigger time ms = 990 --> set_head_angle(0, duration=0.099)
Trigger time ms = 1089 --> set_head_angle(5, duration=0.099)
Trigger time ms = 1188 --> set_head_angle(-1, duration=0.165)
Trigger time ms = 1353 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-43, r_wheel_speed=-43, duration=0.099)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=43, r_wheel_speed=43, duration=0.198)
