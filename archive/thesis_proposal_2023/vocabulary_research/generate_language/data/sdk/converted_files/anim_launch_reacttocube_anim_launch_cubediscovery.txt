Original file = data/sdk_converted/anim_launch_reacttocube
Animation anim_launch_cubediscovery clip 1/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> set_lift_height(48, duration=0.099)
Trigger time ms = 165 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-8, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-9, duration=0.099)
Trigger time ms = 132 --> set_head_angle(3, duration=0.099)
Trigger time ms = 231 --> set_head_angle(-13, duration=0.132)
Trigger time ms = 363 --> set_head_angle(-11, duration=0.165)
Trigger time ms = 528 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 627 --> set_head_angle(-10, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-118, r_wheel_speed=-118, duration=0.198)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=31, r_wheel_speed=31, duration=0.165)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=2, r_wheel_speed=2, duration=0.099)
