Original file = data/sdk_converted/anim_guarddog_fakeout
Animation anim_guarddog_fakeout_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.264)
Trigger time ms = 264 --> set_lift_height(56, duration=0.132)
Trigger time ms = 396 --> set_lift_height(32, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(9, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 198 --> set_head_angle(45, duration=0.066)
Trigger time ms = 264 --> set_head_angle(-11, duration=0.198)
Trigger time ms = 462 --> set_head_angle(21, duration=0.165)
Trigger time ms = 627 --> set_head_angle(3, duration=0.132)
Trigger time ms = 759 --> set_head_angle(5, duration=0.231)
Trigger time ms = 990 --> set_head_angle(6, duration=1.056)
Trigger time ms = 3168 --> set_head_angle(-2, duration=0.231)
Trigger time ms = 3399 --> set_head_angle(-6, duration=0.198)
Trigger time ms = 3597 --> set_head_angle(0, duration=0.099)
Trigger time ms = 3696 --> set_head_angle(3, duration=0.231)
Trigger time ms = 3927 --> set_head_angle(3, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=67, r_wheel_speed=67, duration=0.165)
Trigger time ms = 198 --> turn_in_place(degrees(232))
Trigger time ms = 297 --> turn_in_place(degrees(-170))
Trigger time ms = 3366 --> turn_in_place(degrees(30))
Trigger time ms = 3663 --> turn_in_place(degrees(-4))
