Original file = data/sdk_converted/anim_repair_mild_fix_lift_01
Animation anim_repair_mild_fix_lift_02 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.264)
Trigger time ms = 264 --> set_lift_height(57, duration=0.132)
Trigger time ms = 396 --> set_lift_height(48, duration=0.099)
Trigger time ms = 495 --> set_lift_height(47, duration=0.165)
Trigger time ms = 1056 --> set_lift_height(45, duration=0.132)
Trigger time ms = 1188 --> set_lift_height(92, duration=0.165)
Trigger time ms = 1782 --> set_lift_height(63, duration=0.165)
Trigger time ms = 1947 --> set_lift_height(65, duration=0.165)
Trigger time ms = 2640 --> set_lift_height(34, duration=0.231)
Trigger time ms = 2871 --> set_lift_height(32, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.297)
Trigger time ms = 297 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 396 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 495 --> set_head_angle(-13, duration=0.132)
Trigger time ms = 627 --> set_head_angle(-13, duration=0.165)
Trigger time ms = 792 --> set_head_angle(-12, duration=0.198)
Trigger time ms = 990 --> set_head_angle(-15, duration=0.066)
Trigger time ms = 1056 --> set_head_angle(-16, duration=0.066)
Trigger time ms = 1122 --> set_head_angle(3, duration=0.198)
Trigger time ms = 1320 --> set_head_angle(7, duration=0.132)
Trigger time ms = 1881 --> set_head_angle(-10, duration=0.132)
Trigger time ms = 2442 --> set_head_angle(3, duration=0.165)
Trigger time ms = 2607 --> set_head_angle(-17, duration=0.132)
Trigger time ms = 2739 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-29984.5, r_wheel_speed=-34079.5, duration=0.099)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-17077.5, r_wheel_speed=-15862.5, duration=0.198)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=132.5, r_wheel_speed=-92.5, duration=0.066)
Trigger time ms = 1881 --> drive_wheels(l_wheel_speed=58.5, r_wheel_speed=-76.5, duration=0.099)
Trigger time ms = 2541 --> drive_wheels(l_wheel_speed=1332.5, r_wheel_speed=-512.5, duration=0.066)
Trigger time ms = 2607 --> drive_wheels(l_wheel_speed=225.5, r_wheel_speed=-269.5, duration=0.066)
Trigger time ms = 2673 --> drive_wheels(l_wheel_speed=-390.0, r_wheel_speed=510.0, duration=0.066)
Trigger time ms = 2739 --> drive_wheels(l_wheel_speed=-1638.0, r_wheel_speed=702.0, duration=0.066)
Trigger time ms = 2805 --> drive_wheels(l_wheel_speed=-351.0, r_wheel_speed=459.0, duration=0.066)
Trigger time ms = 2871 --> drive_wheels(l_wheel_speed=-2336.0, r_wheel_speed=544.0, duration=0.066)
Trigger time ms = 2937 --> drive_wheels(l_wheel_speed=-390.5, r_wheel_speed=104.5, duration=0.066)
