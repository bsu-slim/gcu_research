Original file = data/sdk_converted/anim_play_03
Animation anim_play_idle_03 clip 6/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 594 --> set_lift_height(38, duration=0.099)
Trigger time ms = 693 --> set_lift_height(32, duration=0.099)
Trigger time ms = 1419 --> set_lift_height(39, duration=0.066)
Trigger time ms = 1485 --> set_lift_height(32, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 429 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 1551 --> set_head_angle(-10, duration=0.066)
Trigger time ms = 1617 --> set_head_angle(0, duration=0.066)
Trigger time ms = 1683 --> set_head_angle(0, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 528 --> turn_in_place(degrees(30))
Trigger time ms = 1716 --> turn_in_place(degrees(-30))
