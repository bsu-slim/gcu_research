Original file = data/sdk_converted/anim_energy_cubereact
Animation anim_energy_cubedone_02 clip 5/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 106 --> set_lift_height(40, duration=0.159)
Trigger time ms = 265 --> set_lift_height(32, duration=0.239)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(27, duration=0.146)
Trigger time ms = 146 --> set_head_angle(35, duration=0.119)
Trigger time ms = 265 --> set_head_angle(37, duration=0.159)
Trigger time ms = 424 --> set_head_angle(20, duration=0.12)
Trigger time ms = 544 --> set_head_angle(36, duration=0.199)
Trigger time ms = 743 --> set_head_angle(31, duration=0.199)
Trigger time ms = 942 --> set_head_angle(30, duration=0.119)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-5, r_wheel_speed=-5, duration=0.146)
Trigger time ms = 146 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.597)
