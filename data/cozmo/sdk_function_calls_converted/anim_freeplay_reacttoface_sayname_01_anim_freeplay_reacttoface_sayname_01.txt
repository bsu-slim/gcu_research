Original file = data/sdk_converted/anim_freeplay_reacttoface_sayname_01
Animation anim_freeplay_reacttoface_sayname_01 clip 1/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.297)
Trigger time ms = 297 --> set_lift_height(48, duration=0.041)
Trigger time ms = 338 --> set_lift_height(5, duration=0.091)
Trigger time ms = 429 --> set_lift_height(0, duration=0.08)
Trigger time ms = 509 --> set_lift_height(0, duration=1.03)
Trigger time ms = 1538 --> set_lift_height(47, duration=0.079)
Trigger time ms = 1617 --> set_lift_height(9, duration=0.047)
Trigger time ms = 1664 --> set_lift_height(0, duration=0.187)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.365)
Trigger time ms = 365 --> set_head_angle(-18, duration=0.07)
Trigger time ms = 436 --> set_head_angle(-8, duration=0.057)
Trigger time ms = 492 --> set_head_angle(11, duration=0.066)
Trigger time ms = 558 --> set_head_angle(7, duration=0.069)
Trigger time ms = 1518 --> set_head_angle(8, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 1716 --> set_head_angle(4, duration=0.165)
Trigger time ms = 1881 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 1980 --> set_head_angle(3, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(1, duration=0.099)
Trigger time ms = 2178 --> set_head_angle(1, duration=0.165)
Trigger time ms = 2541 --> set_head_angle(-1, duration=0.033)
Trigger time ms = 2574 --> set_head_angle(1, duration=0.066)
Trigger time ms = 2640 --> set_head_angle(1, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-25, r_wheel_speed=-25, duration=0.099)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=-24, r_wheel_speed=-24, duration=0.198)
Trigger time ms = 644 --> drive_wheels(l_wheel_speed=56, r_wheel_speed=56, duration=0.033)
Trigger time ms = 677 --> drive_wheels(l_wheel_speed=-86, r_wheel_speed=-86, duration=0.027)
Trigger time ms = 704 --> drive_wheels(l_wheel_speed=80, r_wheel_speed=80, duration=0.033)
Trigger time ms = 737 --> drive_wheels(l_wheel_speed=-80, r_wheel_speed=-80, duration=0.033)
Trigger time ms = 770 --> drive_wheels(l_wheel_speed=75, r_wheel_speed=75, duration=0.041)
Trigger time ms = 811 --> drive_wheels(l_wheel_speed=-69, r_wheel_speed=-69, duration=0.033)
Trigger time ms = 844 --> drive_wheels(l_wheel_speed=63, r_wheel_speed=63, duration=0.037)
Trigger time ms = 881 --> drive_wheels(l_wheel_speed=-58, r_wheel_speed=-58, duration=0.023)
Trigger time ms = 904 --> drive_wheels(l_wheel_speed=47, r_wheel_speed=47, duration=0.046)
Trigger time ms = 950 --> drive_wheels(l_wheel_speed=-27, r_wheel_speed=-27, duration=0.046)
Trigger time ms = 997 --> drive_wheels(l_wheel_speed=-12, r_wheel_speed=-12, duration=0.059)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=134, r_wheel_speed=134, duration=0.132)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=-33, r_wheel_speed=-33, duration=0.132)
