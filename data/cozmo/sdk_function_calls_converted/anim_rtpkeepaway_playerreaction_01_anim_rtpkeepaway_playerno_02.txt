Original file = data/sdk_converted/anim_rtpkeepaway_playerreaction_01
Animation anim_rtpkeepaway_playerno_02 clip 5/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.561)
Trigger time ms = 3531 --> set_lift_height(0, duration=0.495)
Trigger time ms = 4059 --> set_lift_height(0, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(-14, duration=0.099)
Trigger time ms = 132 --> set_head_angle(-5, duration=0.165)
Trigger time ms = 297 --> set_head_angle(13, duration=0.693)
Trigger time ms = 990 --> set_head_angle(9, duration=0.825)
Trigger time ms = 1815 --> set_head_angle(-10, duration=0.759)
Trigger time ms = 2574 --> set_head_angle(-12, duration=0.858)
Trigger time ms = 3432 --> set_head_angle(-6, duration=0.165)
Trigger time ms = 3597 --> set_head_angle(-13, duration=0.066)
Trigger time ms = 3663 --> set_head_angle(0, duration=0.198)
Trigger time ms = 3861 --> set_head_angle(2, duration=0.165)
Trigger time ms = 4026 --> set_head_angle(-8, duration=0.165)
Trigger time ms = 4191 --> set_head_angle(0, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.099)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.099)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.132)
Trigger time ms = 3729 --> drive_wheels(l_wheel_speed=5, r_wheel_speed=5, duration=0.066)
Trigger time ms = 3795 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.198)
Trigger time ms = 3993 --> drive_wheels(l_wheel_speed=7, r_wheel_speed=7, duration=0.099)
Trigger time ms = 4092 --> drive_wheels(l_wheel_speed=-17, r_wheel_speed=-17, duration=0.165)
