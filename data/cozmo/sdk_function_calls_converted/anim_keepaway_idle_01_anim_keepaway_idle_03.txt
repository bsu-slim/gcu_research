Original file = data/sdk_converted/anim_keepaway_idle_01
Animation anim_keepaway_idle_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> set_head_angle(-17, duration=0.165)
Trigger time ms = 297 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 429 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 2310 --> set_head_angle(-11, duration=0.231)
Trigger time ms = 3432 --> set_head_angle(-12, duration=0.132)
Trigger time ms = 3564 --> set_head_angle(-8, duration=0.231)
Trigger time ms = 3795 --> set_head_angle(-17, duration=0.264)
Trigger time ms = 4059 --> set_head_angle(-16, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> turn_in_place(degrees(-30))
Trigger time ms = 594 --> turn_in_place(degrees(-5))
Trigger time ms = 2013 --> turn_in_place(degrees(14))
Trigger time ms = 3498 --> drive_wheels(l_wheel_speed=11, r_wheel_speed=11, duration=0.363)
Trigger time ms = 3861 --> drive_wheels(l_wheel_speed=-7, r_wheel_speed=-7, duration=0.132)
