Original file = data/sdk_converted/anim_memorymatch_failhand
Animation anim_memorymatch_failhand_player_03 clip 6/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 693 --> set_lift_height(36, duration=0.099)
Trigger time ms = 792 --> set_lift_height(56, duration=0.165)
Trigger time ms = 957 --> set_lift_height(42, duration=0.066)
Trigger time ms = 1023 --> set_lift_height(57, duration=0.099)
Trigger time ms = 1122 --> set_lift_height(48, duration=0.099)
Trigger time ms = 1221 --> set_lift_height(55, duration=0.099)
Trigger time ms = 1320 --> set_lift_height(50, duration=0.099)
Trigger time ms = 1551 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-14, duration=0.099)
Trigger time ms = 132 --> set_head_angle(14, duration=0.066)
Trigger time ms = 198 --> set_head_angle(27, duration=0.033)
Trigger time ms = 231 --> set_head_angle(36, duration=0.099)
Trigger time ms = 330 --> set_head_angle(24, duration=0.099)
Trigger time ms = 429 --> set_head_angle(28, duration=0.165)
Trigger time ms = 594 --> set_head_angle(21, duration=0.033)
Trigger time ms = 627 --> set_head_angle(15, duration=0.033)
Trigger time ms = 660 --> set_head_angle(10, duration=0.033)
Trigger time ms = 693 --> set_head_angle(3, duration=0.066)
Trigger time ms = 759 --> set_head_angle(31, duration=0.165)
Trigger time ms = 924 --> set_head_angle(21, duration=0.132)
Trigger time ms = 1056 --> set_head_angle(23, duration=0.462)
Trigger time ms = 1518 --> set_head_angle(4, duration=0.099)
Trigger time ms = 1617 --> set_head_angle(6, duration=0.132)
Trigger time ms = 1749 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=-25, r_wheel_speed=-25, duration=0.066)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.066)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=-13, r_wheel_speed=-13, duration=0.033)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.033)
Trigger time ms = 792 --> turn_in_place(degrees(-74))
Trigger time ms = 1518 --> turn_in_place(degrees(109))
