Original file = data/sdk_converted/anim_meetcozmo_reenrollment_sayname
Animation anim_meetcozmo_reenrollment_sayname_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.066)
Trigger time ms = 132 --> set_lift_height(66, duration=0.099)
Trigger time ms = 231 --> set_lift_height(72, duration=0.066)
Trigger time ms = 297 --> set_lift_height(55, duration=0.099)
Trigger time ms = 396 --> set_lift_height(62, duration=0.066)
Trigger time ms = 462 --> set_lift_height(45, duration=0.099)
Trigger time ms = 561 --> set_lift_height(54, duration=0.066)
Trigger time ms = 627 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(26, duration=0.099)
Trigger time ms = 99 --> set_head_angle(35, duration=0.066)
Trigger time ms = 165 --> set_head_angle(22, duration=0.132)
Trigger time ms = 297 --> set_head_angle(33, duration=0.066)
Trigger time ms = 363 --> set_head_angle(22, duration=0.099)
Trigger time ms = 462 --> set_head_angle(33, duration=0.066)
Trigger time ms = 528 --> set_head_angle(22, duration=0.099)
Trigger time ms = 627 --> set_head_angle(28, duration=0.066)
Trigger time ms = 693 --> set_head_angle(37, duration=0.099)
Trigger time ms = 792 --> set_head_angle(38, duration=0.033)
Trigger time ms = 825 --> set_head_angle(39, duration=0.33)
Trigger time ms = 1155 --> set_head_angle(-10, duration=0.198)
Trigger time ms = 1353 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-69, r_wheel_speed=-69, duration=0.198)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=38, r_wheel_speed=38, duration=0.132)
Trigger time ms = 462 --> turn_in_place(degrees(121))
Trigger time ms = 495 --> turn_in_place(degrees(-273))
Trigger time ms = 528 --> turn_in_place(degrees(273))
Trigger time ms = 561 --> turn_in_place(degrees(-212))
Trigger time ms = 594 --> turn_in_place(degrees(91))
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=62, r_wheel_speed=62, duration=0.198)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=-40, r_wheel_speed=-40, duration=0.132)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=5, r_wheel_speed=5, duration=0.132)
