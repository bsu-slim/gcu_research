Original file = data/sdk_converted/anim_energy_cubereact
Animation anim_energy_cubeshake_02 clip 4/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.528)
Trigger time ms = 528 --> set_lift_height(46, duration=0.33)
Trigger time ms = 858 --> set_lift_height(52, duration=0.33)
Trigger time ms = 1188 --> set_lift_height(32, duration=0.66)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-9, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-18, duration=0.099)
Trigger time ms = 165 --> set_head_angle(-15, duration=0.132)
Trigger time ms = 297 --> set_head_angle(22, duration=0.231)
Trigger time ms = 528 --> set_head_angle(30, duration=0.132)
Trigger time ms = 660 --> set_head_angle(22, duration=0.198)
Trigger time ms = 858 --> set_head_angle(35, duration=0.165)
Trigger time ms = 1023 --> set_head_angle(26, duration=0.231)
Trigger time ms = 1254 --> set_head_angle(35, duration=0.396)
Trigger time ms = 1650 --> set_head_angle(21, duration=0.165)
Trigger time ms = 1815 --> set_head_angle(30, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=45, r_wheel_speed=45, duration=0.099)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=27, r_wheel_speed=27, duration=0.099)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=-85666.0, r_wheel_speed=-86926.0, duration=0.627)
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=-19, r_wheel_speed=-19, duration=0.066)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=-13, r_wheel_speed=-13, duration=0.066)
Trigger time ms = 957 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.066)
Trigger time ms = 1023 --> turn_in_place(degrees(-4))
Trigger time ms = 1089 --> turn_in_place(degrees(0))
