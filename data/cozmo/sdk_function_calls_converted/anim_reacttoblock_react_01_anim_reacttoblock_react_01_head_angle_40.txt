Original file = data/sdk_converted/anim_reacttoblock_react_01
Animation anim_reacttoblock_react_01_head_angle_40 clip 2/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 660 --> set_lift_height(48, duration=0.132)
Trigger time ms = 792 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 726 --> set_head_angle(11, duration=0.132)
Trigger time ms = 858 --> set_head_angle(-11, duration=0.099)
Trigger time ms = 957 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 1056 --> set_head_angle(45, duration=0.198)
Trigger time ms = 1254 --> set_head_angle(43, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(38, duration=0.066)
Trigger time ms = 1716 --> set_head_angle(42, duration=0.066)
Trigger time ms = 1782 --> set_head_angle(39, duration=0.066)
Trigger time ms = 1848 --> set_head_angle(45, duration=0.099)
Trigger time ms = 1947 --> set_head_angle(43, duration=0.066)
Trigger time ms = 2772 --> set_head_angle(35, duration=0.198)
Trigger time ms = 2970 --> set_head_angle(37, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.132)
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=-118, r_wheel_speed=-118, duration=0.198)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=31, r_wheel_speed=31, duration=0.165)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=2, r_wheel_speed=2, duration=0.099)
