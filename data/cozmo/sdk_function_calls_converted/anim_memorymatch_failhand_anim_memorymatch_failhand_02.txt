Original file = data/sdk_converted/anim_memorymatch_failhand
Animation anim_memorymatch_failhand_02 clip 2/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 198 --> set_lift_height(39, duration=0.066)
Trigger time ms = 264 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(3, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-4, duration=0.066)
Trigger time ms = 132 --> set_head_angle(14, duration=0.132)
Trigger time ms = 264 --> set_head_angle(14, duration=0.66)
Trigger time ms = 924 --> set_head_angle(-13, duration=0.066)
Trigger time ms = 990 --> set_head_angle(4, duration=0.099)
Trigger time ms = 1089 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-38, r_wheel_speed=-38, duration=0.099)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.198)
Trigger time ms = 957 --> drive_wheels(l_wheel_speed=29, r_wheel_speed=29, duration=0.198)
