Original file = data/sdk_converted/anim_bored_getout_01
Animation anim_bored_getout_02 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 858 --> set_lift_height(44, duration=0.099)
Trigger time ms = 957 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-14, duration=0.198)
Trigger time ms = 198 --> set_head_angle(-5, duration=0.198)
Trigger time ms = 396 --> set_head_angle(-4, duration=0.198)
Trigger time ms = 594 --> set_head_angle(-4, duration=0.198)
Trigger time ms = 792 --> set_head_angle(-5, duration=0.099)
Trigger time ms = 891 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 990 --> set_head_angle(5, duration=0.132)
Trigger time ms = 1122 --> set_head_angle(5, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=-108, r_wheel_speed=-108, duration=0.165)
