Original file = data/sdk_converted/anim_keepaway_wingame_01
Animation anim_keepaway_wingame_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 627 --> set_lift_height(65, duration=0.132)
Trigger time ms = 759 --> set_lift_height(91, duration=0.111)
Trigger time ms = 870 --> set_lift_height(56, duration=0.112)
Trigger time ms = 982 --> set_lift_height(91, duration=0.111)
Trigger time ms = 1093 --> set_lift_height(56, duration=0.111)
Trigger time ms = 1204 --> set_lift_height(91, duration=0.112)
Trigger time ms = 1315 --> set_lift_height(56, duration=0.112)
Trigger time ms = 1427 --> set_lift_height(91, duration=0.111)
Trigger time ms = 1538 --> set_lift_height(61, duration=0.111)
Trigger time ms = 1649 --> set_lift_height(82, duration=0.112)
Trigger time ms = 1761 --> set_lift_height(74, duration=0.111)
Trigger time ms = 1872 --> set_lift_height(0, duration=0.141)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> set_head_angle(21, duration=0.231)
Trigger time ms = 627 --> set_head_angle(9, duration=0.132)
Trigger time ms = 759 --> set_head_angle(30, duration=0.099)
Trigger time ms = 858 --> set_head_angle(9, duration=0.132)
Trigger time ms = 990 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1089 --> set_head_angle(9, duration=0.099)
Trigger time ms = 1188 --> set_head_angle(30, duration=0.132)
Trigger time ms = 1320 --> set_head_angle(9, duration=0.099)
Trigger time ms = 1419 --> set_head_angle(30, duration=0.132)
Trigger time ms = 1551 --> set_head_angle(9, duration=0.099)
Trigger time ms = 1650 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1749 --> set_head_angle(9, duration=0.132)
Trigger time ms = 1881 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1980 --> set_head_angle(4, duration=0.132)
Trigger time ms = 2112 --> set_head_angle(8, duration=0.099)
Trigger time ms = 2442 --> set_head_angle(-6, duration=0.132)
Trigger time ms = 2574 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=-51, r_wheel_speed=-51, duration=0.099)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=46, r_wheel_speed=46, duration=0.099)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=-41, r_wheel_speed=-41, duration=0.099)
Trigger time ms = 561 --> turn_in_place(degrees(267))
Trigger time ms = 726 --> turn_in_place(degrees(269))
Trigger time ms = 1914 --> turn_in_place(degrees(-621))
