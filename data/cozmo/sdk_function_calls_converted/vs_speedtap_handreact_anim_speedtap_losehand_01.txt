Original file = data/sdk_converted/vs_speedtap_handreact
Animation anim_speedtap_losehand_01 clip 4/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(88, duration=0.066)
Trigger time ms = 66 --> set_lift_height(92, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-20, duration=0.038)
Trigger time ms = 38 --> set_head_angle(-18, duration=0.038)
Trigger time ms = 76 --> set_head_angle(-20, duration=0.039)
Trigger time ms = 114 --> set_head_angle(-18, duration=0.038)
Trigger time ms = 152 --> set_head_angle(-20, duration=0.038)
Trigger time ms = 191 --> set_head_angle(-18, duration=0.038)
Trigger time ms = 229 --> set_head_angle(-20, duration=0.038)
Trigger time ms = 267 --> set_head_angle(-18, duration=0.038)
Trigger time ms = 305 --> set_head_angle(-20, duration=0.038)
Trigger time ms = 343 --> set_head_angle(-16, duration=0.095)
---------------------------------------
Process body motion animations
---------------------------------------
