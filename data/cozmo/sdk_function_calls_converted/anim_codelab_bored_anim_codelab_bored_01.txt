Original file = data/sdk_converted/anim_codelab_bored
Animation anim_codelab_bored_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 5115 --> set_lift_height(40, duration=0.099)
Trigger time ms = 5214 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-14, duration=0.231)
Trigger time ms = 231 --> set_head_angle(-14, duration=0.066)
Trigger time ms = 297 --> set_head_angle(-17, duration=0.099)
Trigger time ms = 396 --> set_head_angle(24, duration=0.363)
Trigger time ms = 759 --> set_head_angle(26, duration=0.099)
Trigger time ms = 858 --> set_head_angle(26, duration=0.099)
Trigger time ms = 957 --> set_head_angle(26, duration=0.132)
Trigger time ms = 2079 --> set_head_angle(25, duration=0.033)
Trigger time ms = 2112 --> set_head_angle(18, duration=0.099)
Trigger time ms = 2211 --> set_head_angle(25, duration=0.132)
Trigger time ms = 2343 --> set_head_angle(26, duration=0.033)
Trigger time ms = 2376 --> set_head_angle(26, duration=0.033)
Trigger time ms = 2409 --> set_head_angle(26, duration=0.033)
Trigger time ms = 3564 --> set_head_angle(19, duration=0.165)
Trigger time ms = 3729 --> set_head_angle(18, duration=0.132)
Trigger time ms = 4686 --> set_head_angle(18, duration=0.066)
Trigger time ms = 4752 --> set_head_angle(19, duration=0.165)
Trigger time ms = 4917 --> set_head_angle(20, duration=0.099)
Trigger time ms = 5016 --> set_head_angle(44, duration=0.33)
Trigger time ms = 5346 --> set_head_angle(43, duration=0.132)
Trigger time ms = 5478 --> set_head_angle(42, duration=0.066)
Trigger time ms = 5544 --> set_head_angle(39, duration=0.033)
Trigger time ms = 5577 --> set_head_angle(-10, duration=0.198)
Trigger time ms = 5775 --> set_head_angle(-13, duration=0.033)
Trigger time ms = 5808 --> set_head_angle(-14, duration=0.066)
Trigger time ms = 5874 --> set_head_angle(-8, duration=0.198)
Trigger time ms = 6072 --> set_head_angle(-13, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 726 --> turn_in_place(degrees(11))
Trigger time ms = 1089 --> turn_in_place(degrees(12))
Trigger time ms = 2178 --> turn_in_place(degrees(-20))
Trigger time ms = 2277 --> turn_in_place(degrees(-18))
Trigger time ms = 4983 --> turn_in_place(degrees(-17))
Trigger time ms = 5214 --> turn_in_place(degrees(55))
Trigger time ms = 5775 --> turn_in_place(degrees(-81))
Trigger time ms = 5874 --> turn_in_place(degrees(71))
Trigger time ms = 5973 --> turn_in_place(degrees(-45))
Trigger time ms = 6105 --> turn_in_place(degrees(30))
Trigger time ms = 6171 --> turn_in_place(degrees(-30))
