Original file = data/sdk_converted/anim_memorymatch_idle_01
Animation anim_memorymatch_idle_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 495 --> set_lift_height(40, duration=0.099)
Trigger time ms = 594 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> set_head_angle(2, duration=0.132)
Trigger time ms = 363 --> set_head_angle(7, duration=0.231)
Trigger time ms = 594 --> set_head_angle(8, duration=0.264)
Trigger time ms = 858 --> set_head_angle(8, duration=0.231)
Trigger time ms = 1089 --> set_head_angle(8, duration=0.693)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.198)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.132)
