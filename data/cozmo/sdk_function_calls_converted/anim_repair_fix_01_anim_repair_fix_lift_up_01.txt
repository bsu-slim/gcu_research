Original file = data/sdk_converted/anim_repair_fix_01
Animation anim_repair_fix_lift_up_01 clip 2/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(57, duration=0.033)
Trigger time ms = 33 --> set_lift_height(76, duration=0.099)
Trigger time ms = 132 --> set_lift_height(57, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(1, duration=0.033)
Trigger time ms = 66 --> set_head_angle(-5, duration=0.132)
Trigger time ms = 198 --> set_head_angle(4, duration=0.165)
Trigger time ms = 363 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-52, r_wheel_speed=-52, duration=0.099)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=31, r_wheel_speed=31, duration=0.165)
