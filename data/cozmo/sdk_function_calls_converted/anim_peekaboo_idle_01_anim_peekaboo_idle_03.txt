Original file = data/sdk_converted/anim_peekaboo_idle_01
Animation anim_peekaboo_idle_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.264)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(31, duration=0.132)
Trigger time ms = 132 --> set_head_angle(29, duration=0.165)
Trigger time ms = 297 --> set_head_angle(38, duration=0.132)
Trigger time ms = 429 --> set_head_angle(38, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(33, duration=0.231)
Trigger time ms = 3234 --> set_head_angle(33, duration=0.132)
Trigger time ms = 3366 --> set_head_angle(34, duration=0.231)
Trigger time ms = 3597 --> set_head_angle(30, duration=0.363)
Trigger time ms = 3960 --> set_head_angle(31, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
