Original file = data/sdk_converted/vs_speedtap_roundreact
Animation anim_speedtap_winround_intensity02_02 clip 3/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 165 --> set_lift_height(31, duration=0.396)
Trigger time ms = 825 --> set_lift_height(23, duration=0.066)
Trigger time ms = 2178 --> set_lift_height(92, duration=0.429)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> set_head_angle(18, duration=0.066)
Trigger time ms = 165 --> set_head_angle(18, duration=0.165)
Trigger time ms = 330 --> set_head_angle(-17, duration=0.165)
Trigger time ms = 594 --> set_head_angle(-17, duration=0.099)
Trigger time ms = 693 --> set_head_angle(-13, duration=0.066)
Trigger time ms = 891 --> set_head_angle(-19, duration=0.099)
Trigger time ms = 990 --> set_head_angle(-16, duration=0.066)
Trigger time ms = 1056 --> set_head_angle(-17, duration=0.066)
Trigger time ms = 1122 --> set_head_angle(-17, duration=0.066)
Trigger time ms = 1782 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 2673 --> set_head_angle(-10, duration=0.363)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=35, r_wheel_speed=35, duration=0.396)
Trigger time ms = 2178 --> drive_wheels(l_wheel_speed=-79, r_wheel_speed=-79, duration=0.66)
