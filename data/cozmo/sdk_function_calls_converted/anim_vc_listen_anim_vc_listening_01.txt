Original file = data/sdk_converted/anim_vc_listen
Animation anim_vc_listening_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.198)
Trigger time ms = 198 --> set_lift_height(58, duration=0.132)
Trigger time ms = 330 --> set_lift_height(32, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-25, duration=0.066)
Trigger time ms = 198 --> set_head_angle(32, duration=0.066)
Trigger time ms = 264 --> set_head_angle(31, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> turn_in_place(degrees(273))
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-2194.5, r_wheel_speed=-11599.5, duration=0.099)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=21.0, r_wheel_speed=111.0, duration=0.561)
