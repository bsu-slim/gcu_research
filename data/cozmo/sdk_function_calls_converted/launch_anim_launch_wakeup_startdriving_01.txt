Original file = data/sdk_converted/launch
Animation anim_launch_wakeup_startdriving_01 clip 7/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1617 --> set_lift_height(0, duration=2.277)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> set_head_angle(-12, duration=0.231)
Trigger time ms = 1155 --> set_head_angle(12, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=-16302.0, r_wheel_speed=-12882.0, duration=3.531)
