Original file = data/sdk_converted/anim_pyramid_reacttocube_happy
Animation anim_pyramid_reacttocube_happy_low_01 clip 1/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.363)
Trigger time ms = 363 --> set_lift_height(41, duration=0.066)
Trigger time ms = 429 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.165)
Trigger time ms = 165 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 264 --> set_head_angle(-11, duration=0.033)
Trigger time ms = 297 --> set_head_angle(6, duration=0.099)
Trigger time ms = 396 --> set_head_angle(-2, duration=0.165)
Trigger time ms = 561 --> set_head_angle(0, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=29, r_wheel_speed=29, duration=0.132)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-29, r_wheel_speed=-29, duration=0.132)
