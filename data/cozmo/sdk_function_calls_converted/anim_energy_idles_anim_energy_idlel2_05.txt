Original file = data/sdk_converted/anim_energy_idles
Animation anim_energy_idlel2_05 clip 8/15
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2178 --> set_lift_height(32, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(30, duration=0.33)
Trigger time ms = 330 --> set_head_angle(25, duration=0.099)
Trigger time ms = 429 --> set_head_angle(37, duration=0.165)
Trigger time ms = 594 --> set_head_angle(38, duration=0.066)
Trigger time ms = 2013 --> set_head_angle(32, duration=0.066)
Trigger time ms = 2079 --> set_head_angle(35, duration=0.165)
Trigger time ms = 2244 --> set_head_angle(35, duration=0.066)
Trigger time ms = 3399 --> set_head_angle(31, duration=0.099)
Trigger time ms = 3498 --> set_head_angle(30, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1056 --> turn_in_place(degrees(36))
Trigger time ms = 2145 --> turn_in_place(degrees(-45))
