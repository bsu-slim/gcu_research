Original file = data/sdk_converted/anim_repair_severe_state_01
Animation anim_repair_severe_idle_01 clip 2/7
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-8, duration=0.528)
Trigger time ms = 990 --> set_head_angle(-12, duration=0.099)
Trigger time ms = 1089 --> set_head_angle(-4, duration=2.211)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> turn_in_place(degrees(-20))
Trigger time ms = 264 --> turn_in_place(degrees(-10))
Trigger time ms = 363 --> turn_in_place(degrees(-4))
Trigger time ms = 1089 --> turn_in_place(degrees(8))
