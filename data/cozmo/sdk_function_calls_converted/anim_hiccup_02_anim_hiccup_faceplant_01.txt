Original file = data/sdk_converted/anim_hiccup_02
Animation anim_hiccup_faceplant_01 clip 7/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=1.056)
Trigger time ms = 2970 --> set_lift_height(92, duration=0.132)
Trigger time ms = 3102 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 462 --> set_head_angle(16, duration=0.099)
Trigger time ms = 1023 --> set_head_angle(1, duration=0.495)
Trigger time ms = 1518 --> set_head_angle(0, duration=1.584)
Trigger time ms = 3102 --> set_head_angle(24, duration=0.198)
Trigger time ms = 3300 --> set_head_angle(27, duration=0.132)
Trigger time ms = 3432 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 3498 --> set_head_angle(10, duration=0.132)
Trigger time ms = 3630 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 3729 --> set_head_angle(7, duration=0.132)
Trigger time ms = 3861 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 3927 --> set_head_angle(3, duration=0.099)
Trigger time ms = 4026 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
