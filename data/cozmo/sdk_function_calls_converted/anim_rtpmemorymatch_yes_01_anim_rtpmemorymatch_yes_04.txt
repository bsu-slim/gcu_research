Original file = data/sdk_converted/anim_rtpmemorymatch_yes_01
Animation anim_rtpmemorymatch_yes_04 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 231 --> set_lift_height(43, duration=0.066)
Trigger time ms = 297 --> set_lift_height(2, duration=0.165)
Trigger time ms = 462 --> set_lift_height(0, duration=0.198)
Trigger time ms = 1111 --> set_lift_height(45, duration=0.073)
Trigger time ms = 1185 --> set_lift_height(1, duration=0.078)
Trigger time ms = 1262 --> set_lift_height(45, duration=0.077)
Trigger time ms = 1339 --> set_lift_height(1, duration=0.079)
Trigger time ms = 1418 --> set_lift_height(45, duration=0.078)
Trigger time ms = 1496 --> set_lift_height(1, duration=0.075)
Trigger time ms = 1571 --> set_lift_height(45, duration=0.067)
Trigger time ms = 1639 --> set_lift_height(1, duration=0.071)
Trigger time ms = 1709 --> set_lift_height(1, duration=0.172)
Trigger time ms = 1881 --> set_lift_height(0, duration=0.429)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 66 --> set_head_angle(18, duration=0.132)
Trigger time ms = 198 --> set_head_angle(27, duration=0.231)
Trigger time ms = 429 --> set_head_angle(21, duration=0.132)
Trigger time ms = 561 --> set_head_angle(20, duration=0.099)
Trigger time ms = 759 --> set_head_angle(32, duration=0.099)
Trigger time ms = 858 --> set_head_angle(27, duration=0.132)
Trigger time ms = 990 --> set_head_angle(27, duration=0.18)
Trigger time ms = 1170 --> set_head_angle(39, duration=0.078)
Trigger time ms = 1248 --> set_head_angle(28, duration=0.076)
Trigger time ms = 1324 --> set_head_angle(39, duration=0.078)
Trigger time ms = 1402 --> set_head_angle(28, duration=0.079)
Trigger time ms = 1481 --> set_head_angle(39, duration=0.078)
Trigger time ms = 1559 --> set_head_angle(28, duration=0.075)
Trigger time ms = 1635 --> set_head_angle(27, duration=0.18)
Trigger time ms = 1815 --> set_head_angle(-2, duration=0.198)
Trigger time ms = 2013 --> set_head_angle(3, duration=0.231)
Trigger time ms = 2244 --> set_head_angle(3, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=-60, r_wheel_speed=-60, duration=0.053)
Trigger time ms = 1076 --> drive_wheels(l_wheel_speed=54, r_wheel_speed=54, duration=0.057)
Trigger time ms = 1134 --> drive_wheels(l_wheel_speed=-54, r_wheel_speed=-54, duration=0.055)
Trigger time ms = 1189 --> drive_wheels(l_wheel_speed=54, r_wheel_speed=54, duration=0.058)
Trigger time ms = 1246 --> drive_wheels(l_wheel_speed=-54, r_wheel_speed=-54, duration=0.057)
Trigger time ms = 1303 --> drive_wheels(l_wheel_speed=54, r_wheel_speed=54, duration=0.055)
Trigger time ms = 1358 --> drive_wheels(l_wheel_speed=-54, r_wheel_speed=-54, duration=0.058)
Trigger time ms = 1416 --> drive_wheels(l_wheel_speed=54, r_wheel_speed=54, duration=0.057)
Trigger time ms = 1473 --> drive_wheels(l_wheel_speed=-54, r_wheel_speed=-54, duration=0.055)
Trigger time ms = 1528 --> drive_wheels(l_wheel_speed=54, r_wheel_speed=54, duration=0.057)
Trigger time ms = 1585 --> drive_wheels(l_wheel_speed=-54, r_wheel_speed=-54, duration=0.058)
Trigger time ms = 1643 --> drive_wheels(l_wheel_speed=54, r_wheel_speed=54, duration=0.057)
Trigger time ms = 1700 --> drive_wheels(l_wheel_speed=6, r_wheel_speed=6, duration=0.045)
