Original file = data/sdk_converted/anim_rtpkeepaway_idle_01
Animation anim_rtpkeepaway_idle_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(3, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-5, duration=0.132)
Trigger time ms = 198 --> set_head_angle(0, duration=0.099)
Trigger time ms = 1155 --> set_head_angle(3, duration=0.132)
Trigger time ms = 2013 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 2112 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-18, r_wheel_speed=-18, duration=0.099)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=16, r_wheel_speed=16, duration=0.396)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.33)
