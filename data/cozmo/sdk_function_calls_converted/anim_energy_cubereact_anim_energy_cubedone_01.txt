Original file = data/sdk_converted/anim_energy_cubereact
Animation anim_energy_cubedone_01 clip 2/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.099)
Trigger time ms = 99 --> set_lift_height(41, duration=0.165)
Trigger time ms = 264 --> set_lift_height(32, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(31, duration=0.132)
Trigger time ms = 132 --> set_head_angle(16, duration=0.099)
Trigger time ms = 231 --> set_head_angle(37, duration=0.165)
Trigger time ms = 396 --> set_head_angle(30, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-33, r_wheel_speed=-33, duration=0.099)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=81, r_wheel_speed=81, duration=0.264)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=-39, r_wheel_speed=-39, duration=0.132)
