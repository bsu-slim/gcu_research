Original file = data/sdk_converted/anim_cozmosays_badword_01
Animation anim_cozmosays_badword_02_head_angle_-20 clip 6/8
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 330 --> set_lift_height(45, duration=0.099)
Trigger time ms = 429 --> set_lift_height(8, duration=0.066)
Trigger time ms = 495 --> set_lift_height(0, duration=0.132)
Trigger time ms = 2409 --> set_lift_height(38, duration=0.033)
Trigger time ms = 2442 --> set_lift_height(0, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> set_head_angle(-14, duration=0.099)
Trigger time ms = 165 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 231 --> set_head_angle(-25, duration=0.132)
Trigger time ms = 363 --> set_head_angle(-22, duration=0.066)
Trigger time ms = 429 --> set_head_angle(19, duration=0.066)
Trigger time ms = 495 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 594 --> set_head_angle(0, duration=0.099)
Trigger time ms = 1122 --> set_head_angle(6, duration=0.099)
Trigger time ms = 1221 --> set_head_angle(-10, duration=0.066)
Trigger time ms = 1287 --> set_head_angle(3, duration=0.066)
Trigger time ms = 1353 --> set_head_angle(2, duration=0.033)
Trigger time ms = 1386 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 1518 --> set_head_angle(7, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(5, duration=0.132)
Trigger time ms = 1716 --> set_head_angle(-1, duration=0.099)
Trigger time ms = 1815 --> set_head_angle(-3, duration=0.132)
Trigger time ms = 2310 --> set_head_angle(-12, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=-119, r_wheel_speed=-119, duration=0.066)
Trigger time ms = 1155 --> drive_wheels(l_wheel_speed=-1593.0, r_wheel_speed=837.0, duration=0.066)
Trigger time ms = 1221 --> drive_wheels(l_wheel_speed=-331.5, r_wheel_speed=1963.5, duration=0.066)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=1095.0, r_wheel_speed=-255.0, duration=0.033)
Trigger time ms = 1320 --> drive_wheels(l_wheel_speed=-27047.5, r_wheel_speed=-11342.5, duration=0.033)
Trigger time ms = 1353 --> drive_wheels(l_wheel_speed=-4255.0, r_wheel_speed=-925.0, duration=0.066)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=770.5, r_wheel_speed=-264.5, duration=0.033)
Trigger time ms = 1452 --> drive_wheels(l_wheel_speed=1098.0, r_wheel_speed=-522.0, duration=0.066)
Trigger time ms = 1518 --> drive_wheels(l_wheel_speed=-346.5, r_wheel_speed=148.5, duration=0.099)
Trigger time ms = 2376 --> drive_wheels(l_wheel_speed=747.5, r_wheel_speed=-287.5, duration=0.099)
