Original file = data/sdk_converted/anim_meetcozmo_reenrollment_sayname
Animation anim_meetcozmo_reenrollment_sayname_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.033)
Trigger time ms = 33 --> set_lift_height(48, duration=0.061)
Trigger time ms = 94 --> set_lift_height(5, duration=0.104)
Trigger time ms = 198 --> set_lift_height(2, duration=0.132)
Trigger time ms = 330 --> set_lift_height(0, duration=0.594)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.108)
Trigger time ms = 108 --> set_head_angle(1, duration=0.065)
Trigger time ms = 173 --> set_head_angle(27, duration=0.124)
Trigger time ms = 297 --> set_head_angle(37, duration=0.066)
Trigger time ms = 363 --> set_head_angle(29, duration=0.132)
Trigger time ms = 495 --> set_head_angle(34, duration=0.105)
Trigger time ms = 600 --> set_head_angle(30, duration=0.111)
Trigger time ms = 711 --> set_head_angle(30, duration=0.213)
Trigger time ms = 924 --> set_head_angle(33, duration=0.066)
Trigger time ms = 990 --> set_head_angle(34, duration=0.297)
Trigger time ms = 1287 --> set_head_angle(41, duration=0.124)
Trigger time ms = 1411 --> set_head_angle(11, duration=0.123)
Trigger time ms = 1534 --> set_head_angle(20, duration=0.053)
Trigger time ms = 1587 --> set_head_angle(43, duration=0.077)
Trigger time ms = 1664 --> set_head_angle(28, duration=0.087)
Trigger time ms = 1751 --> set_head_angle(41, duration=0.13)
Trigger time ms = 1881 --> set_head_angle(31, duration=0.124)
Trigger time ms = 2005 --> set_head_angle(35, duration=0.21)
Trigger time ms = 2215 --> set_head_angle(36, duration=0.029)
Trigger time ms = 2244 --> set_head_angle(12, duration=0.066)
Trigger time ms = 2310 --> set_head_angle(-4, duration=0.132)
Trigger time ms = 2442 --> set_head_angle(0, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-161, r_wheel_speed=-161, duration=0.132)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=19, r_wheel_speed=19, duration=0.132)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.12)
Trigger time ms = 516 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.177)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=2, r_wheel_speed=2, duration=0.18)
Trigger time ms = 873 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.051)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=-2, r_wheel_speed=-2, duration=0.342)
Trigger time ms = 1266 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.021)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=-48, r_wheel_speed=-48, duration=0.111)
Trigger time ms = 1398 --> drive_wheels(l_wheel_speed=17, r_wheel_speed=17, duration=0.021)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=176, r_wheel_speed=176, duration=0.231)
Trigger time ms = 1650 --> drive_wheels(l_wheel_speed=-26, r_wheel_speed=-26, duration=0.289)
Trigger time ms = 1980 --> drive_wheels(l_wheel_speed=-6, r_wheel_speed=-6, duration=0.462)
