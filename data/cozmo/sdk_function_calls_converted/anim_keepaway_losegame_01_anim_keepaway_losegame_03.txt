Original file = data/sdk_converted/anim_keepaway_losegame_01
Animation anim_keepaway_losegame_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2277 --> set_lift_height(65, duration=0.132)
Trigger time ms = 2409 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 297 --> set_head_angle(-20, duration=0.033)
Trigger time ms = 693 --> set_head_angle(-5, duration=0.033)
Trigger time ms = 891 --> set_head_angle(0, duration=0.033)
Trigger time ms = 1650 --> set_head_angle(-3, duration=0.033)
Trigger time ms = 1980 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(-20, duration=0.165)
Trigger time ms = 2244 --> set_head_angle(-19, duration=0.132)
Trigger time ms = 2376 --> set_head_angle(-16, duration=0.066)
Trigger time ms = 3135 --> set_head_angle(-4, duration=0.231)
Trigger time ms = 5214 --> set_head_angle(3, duration=0.264)
Trigger time ms = 5478 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> turn_in_place(degrees(-242))
Trigger time ms = 891 --> turn_in_place(degrees(386))
Trigger time ms = 2112 --> turn_in_place(degrees(197))
Trigger time ms = 2178 --> turn_in_place(degrees(-333))
Trigger time ms = 2244 --> turn_in_place(degrees(258))
Trigger time ms = 2310 --> turn_in_place(degrees(-101))
Trigger time ms = 5313 --> drive_wheels(l_wheel_speed=39, r_wheel_speed=39, duration=0.165)
