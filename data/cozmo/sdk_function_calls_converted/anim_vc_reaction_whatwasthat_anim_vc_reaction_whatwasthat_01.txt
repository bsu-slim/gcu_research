Original file = data/sdk_converted/anim_vc_reaction_whatwasthat
Animation anim_vc_reaction_whatwasthat_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.099)
Trigger time ms = 99 --> set_lift_height(37, duration=0.099)
Trigger time ms = 198 --> set_lift_height(32, duration=0.099)
Trigger time ms = 1749 --> set_lift_height(48, duration=0.264)
Trigger time ms = 2013 --> set_lift_height(48, duration=0.363)
Trigger time ms = 2376 --> set_lift_height(32, duration=0.33)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(16, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-7, duration=0.132)
Trigger time ms = 264 --> set_head_angle(-9, duration=0.231)
Trigger time ms = 594 --> set_head_angle(-16, duration=0.132)
Trigger time ms = 1815 --> set_head_angle(6, duration=0.099)
Trigger time ms = 1914 --> set_head_angle(19, duration=0.429)
Trigger time ms = 2343 --> set_head_angle(0, duration=0.297)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> turn_in_place(degrees(-60))
Trigger time ms = 330 --> turn_in_place(degrees(-40))
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=0.0, duration=0.165)
Trigger time ms = 1749 --> turn_in_place(degrees(23))
Trigger time ms = 2409 --> drive_wheels(l_wheel_speed=58.5, r_wheel_speed=-76.5, duration=0.198)
