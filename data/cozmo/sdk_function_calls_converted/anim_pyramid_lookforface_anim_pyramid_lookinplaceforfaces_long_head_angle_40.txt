Original file = data/sdk_converted/anim_pyramid_lookforface
Animation anim_pyramid_lookinplaceforfaces_long_head_angle_40 clip 6/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.264)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(40, duration=0.132)
Trigger time ms = 132 --> set_head_angle(39, duration=0.165)
Trigger time ms = 297 --> set_head_angle(45, duration=0.132)
Trigger time ms = 429 --> set_head_angle(45, duration=0.099)
Trigger time ms = 1716 --> set_head_angle(39, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
