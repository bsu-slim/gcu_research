Original file = data/sdk_converted/vs_speedtap_gamereact_02
Animation anim_speedtap_losegame_intensity02_02 clip 1/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(92, duration=0.099)
Trigger time ms = 99 --> set_lift_height(81, duration=0.066)
Trigger time ms = 165 --> set_lift_height(92, duration=0.099)
Trigger time ms = 264 --> set_lift_height(32, duration=0.198)
Trigger time ms = 4587 --> set_lift_height(48, duration=0.099)
Trigger time ms = 4686 --> set_lift_height(32, duration=0.132)
Trigger time ms = 6864 --> set_lift_height(58, duration=0.099)
Trigger time ms = 6963 --> set_lift_height(32, duration=0.132)
Trigger time ms = 7458 --> set_lift_height(92, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-13, duration=0.099)
Trigger time ms = 99 --> set_head_angle(-19, duration=0.099)
Trigger time ms = 198 --> set_head_angle(16, duration=0.066)
Trigger time ms = 264 --> set_head_angle(-16, duration=0.066)
Trigger time ms = 330 --> set_head_angle(28, duration=0.231)
Trigger time ms = 561 --> set_head_angle(28, duration=0.66)
Trigger time ms = 1221 --> set_head_angle(23, duration=0.066)
Trigger time ms = 1287 --> set_head_angle(28, duration=0.066)
Trigger time ms = 1353 --> set_head_angle(23, duration=0.066)
Trigger time ms = 1419 --> set_head_angle(30, duration=0.132)
Trigger time ms = 1551 --> set_head_angle(28, duration=0.198)
Trigger time ms = 1749 --> set_head_angle(-15, duration=0.231)
Trigger time ms = 1980 --> set_head_angle(-15, duration=0.528)
Trigger time ms = 2508 --> set_head_angle(30, duration=0.198)
Trigger time ms = 2706 --> set_head_angle(30, duration=0.693)
Trigger time ms = 3399 --> set_head_angle(14, duration=0.297)
Trigger time ms = 3696 --> set_head_angle(32, duration=0.462)
Trigger time ms = 4158 --> set_head_angle(11, duration=0.429)
Trigger time ms = 4587 --> set_head_angle(26, duration=0.429)
Trigger time ms = 5016 --> set_head_angle(11, duration=0.297)
Trigger time ms = 5313 --> set_head_angle(25, duration=0.396)
Trigger time ms = 5709 --> set_head_angle(10, duration=0.33)
Trigger time ms = 6303 --> set_head_angle(5, duration=0.066)
Trigger time ms = 6369 --> set_head_angle(13, duration=0.132)
Trigger time ms = 6501 --> set_head_angle(10, duration=0.231)
Trigger time ms = 6732 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 6831 --> set_head_angle(18, duration=0.099)
Trigger time ms = 6930 --> set_head_angle(-2, duration=0.132)
Trigger time ms = 7062 --> set_head_angle(0, duration=0.132)
Trigger time ms = 7458 --> set_head_angle(-13, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-169, r_wheel_speed=-169, duration=0.264)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.495)
Trigger time ms = 1782 --> drive_wheels(l_wheel_speed=-32, r_wheel_speed=-32, duration=0.231)
Trigger time ms = 2508 --> drive_wheels(l_wheel_speed=26, r_wheel_speed=26, duration=0.198)
Trigger time ms = 3135 --> drive_wheels(l_wheel_speed=284.0, r_wheel_speed=-76.0, duration=3.003)
Trigger time ms = 6732 --> drive_wheels(l_wheel_speed=3657.5, r_wheel_speed=192.5, duration=0.297)
Trigger time ms = 7029 --> drive_wheels(l_wheel_speed=-1161.5, r_wheel_speed=-126.5, duration=0.165)
