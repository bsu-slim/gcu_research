Original file = data/sdk_converted/anim_freeplay_reacttoface_sayname_02
Animation anim_freeplay_reacttoface_sayname_02_head_angle_20 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.165)
Trigger time ms = 165 --> set_lift_height(48, duration=0.061)
Trigger time ms = 226 --> set_lift_height(5, duration=0.104)
Trigger time ms = 330 --> set_lift_height(2, duration=0.132)
Trigger time ms = 462 --> set_lift_height(0, duration=0.594)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(20, duration=0.24)
Trigger time ms = 240 --> set_head_angle(9, duration=0.065)
Trigger time ms = 305 --> set_head_angle(3, duration=0.091)
Trigger time ms = 396 --> set_head_angle(-5, duration=0.033)
Trigger time ms = 429 --> set_head_angle(22, duration=0.066)
Trigger time ms = 495 --> set_head_angle(14, duration=0.132)
Trigger time ms = 627 --> set_head_angle(19, duration=0.105)
Trigger time ms = 732 --> set_head_angle(15, duration=0.111)
Trigger time ms = 843 --> set_head_angle(15, duration=0.213)
Trigger time ms = 1056 --> set_head_angle(18, duration=0.066)
Trigger time ms = 1122 --> set_head_angle(18, duration=0.297)
Trigger time ms = 1419 --> set_head_angle(26, duration=0.124)
Trigger time ms = 1543 --> set_head_angle(-5, duration=0.123)
Trigger time ms = 1666 --> set_head_angle(5, duration=0.053)
Trigger time ms = 1719 --> set_head_angle(28, duration=0.077)
Trigger time ms = 1796 --> set_head_angle(12, duration=0.087)
Trigger time ms = 1883 --> set_head_angle(25, duration=0.13)
Trigger time ms = 2013 --> set_head_angle(16, duration=0.124)
Trigger time ms = 2137 --> set_head_angle(20, duration=0.21)
Trigger time ms = 2347 --> set_head_angle(21, duration=0.062)
Trigger time ms = 2409 --> set_head_angle(19, duration=0.033)
Trigger time ms = 2442 --> set_head_angle(21, duration=0.066)
Trigger time ms = 2508 --> set_head_angle(20, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-161, r_wheel_speed=-161, duration=0.132)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=19, r_wheel_speed=19, duration=0.132)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.12)
Trigger time ms = 648 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.177)
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=2, r_wheel_speed=2, duration=0.18)
Trigger time ms = 1005 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.051)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=-2, r_wheel_speed=-2, duration=0.342)
Trigger time ms = 1398 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.021)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=-48, r_wheel_speed=-48, duration=0.111)
Trigger time ms = 1530 --> drive_wheels(l_wheel_speed=17, r_wheel_speed=17, duration=0.021)
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=176, r_wheel_speed=176, duration=0.231)
Trigger time ms = 1782 --> drive_wheels(l_wheel_speed=-26, r_wheel_speed=-26, duration=0.289)
Trigger time ms = 2112 --> drive_wheels(l_wheel_speed=-6, r_wheel_speed=-6, duration=0.462)
