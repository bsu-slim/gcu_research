Original file = data/sdk_converted/anim_pyramid_lookforface
Animation anim_pyramid_lookinplaceforfaces_medium_head_angle_-20 clip 7/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-20, duration=0.033)
Trigger time ms = 99 --> set_head_angle(-24, duration=0.132)
Trigger time ms = 231 --> set_head_angle(-15, duration=0.165)
Trigger time ms = 396 --> set_head_angle(-14, duration=0.297)
Trigger time ms = 693 --> set_head_angle(-22, duration=0.099)
Trigger time ms = 792 --> set_head_angle(-20, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
