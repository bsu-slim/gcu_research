Original file = data/sdk_converted/anim_repair_fix_fail_01
Animation anim_repair_severe_fix_fail_01 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(57, duration=0.099)
Trigger time ms = 99 --> set_lift_height(54, duration=0.198)
Trigger time ms = 297 --> set_lift_height(51, duration=0.033)
Trigger time ms = 330 --> set_lift_height(70, duration=0.066)
Trigger time ms = 396 --> set_lift_height(71, duration=0.066)
Trigger time ms = 594 --> set_lift_height(67, duration=0.066)
Trigger time ms = 660 --> set_lift_height(65, duration=0.033)
Trigger time ms = 693 --> set_lift_height(66, duration=0.066)
Trigger time ms = 759 --> set_lift_height(62, duration=0.066)
Trigger time ms = 825 --> set_lift_height(59, duration=0.099)
Trigger time ms = 924 --> set_lift_height(59, duration=0.198)
Trigger time ms = 2244 --> set_lift_height(57, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-14, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-25, duration=0.033)
Trigger time ms = 99 --> set_head_angle(15, duration=0.033)
Trigger time ms = 363 --> set_head_angle(-25, duration=0.033)
Trigger time ms = 462 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 528 --> set_head_angle(14, duration=0.033)
Trigger time ms = 561 --> set_head_angle(-6, duration=0.033)
Trigger time ms = 594 --> set_head_angle(-11, duration=0.033)
Trigger time ms = 627 --> set_head_angle(3, duration=0.033)
Trigger time ms = 660 --> set_head_angle(-15, duration=0.033)
Trigger time ms = 693 --> set_head_angle(-13, duration=0.033)
Trigger time ms = 726 --> set_head_angle(-18, duration=0.033)
Trigger time ms = 759 --> set_head_angle(-17, duration=0.033)
Trigger time ms = 792 --> set_head_angle(-22, duration=0.033)
Trigger time ms = 825 --> set_head_angle(-23, duration=0.099)
Trigger time ms = 1089 --> set_head_angle(-23, duration=0.132)
Trigger time ms = 1716 --> set_head_angle(-24, duration=0.066)
Trigger time ms = 1782 --> set_head_angle(-14, duration=0.561)
Trigger time ms = 2343 --> set_head_angle(-14, duration=0.495)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-23, r_wheel_speed=-23, duration=0.066)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=220, r_wheel_speed=220, duration=0.066)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-91, r_wheel_speed=-91, duration=0.165)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=71, r_wheel_speed=71, duration=0.066)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=11855.5, r_wheel_speed=3710.5, duration=0.066)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-1504.5, r_wheel_speed=790.5, duration=0.066)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-442.0, r_wheel_speed=1898.0, duration=0.066)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=-226811.0, r_wheel_speed=-233381.0, duration=0.033)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=-5418.0, r_wheel_speed=-1638.0, duration=0.066)
