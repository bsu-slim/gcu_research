Original file = data/sdk_converted/anim_memorymatch_failgame_v01
Animation anim_memorymatch_failgame_player_01 clip 4/6
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 65 --> set_head_angle(1, duration=0.066)
Trigger time ms = 131 --> set_head_angle(12, duration=0.1)
Trigger time ms = 428 --> set_head_angle(18, duration=0.132)
Trigger time ms = 560 --> set_head_angle(17, duration=0.33)
Trigger time ms = 890 --> set_head_angle(16, duration=0.1)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=160, r_wheel_speed=160, duration=0.066)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-11, r_wheel_speed=-11, duration=0.033)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=-297, r_wheel_speed=-297, duration=0.033)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.033)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=276, r_wheel_speed=276, duration=0.033)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=11, r_wheel_speed=11, duration=0.033)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=-240, r_wheel_speed=-240, duration=0.033)
