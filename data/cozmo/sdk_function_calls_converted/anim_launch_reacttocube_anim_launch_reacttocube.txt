Original file = data/sdk_converted/anim_launch_reacttocube
Animation anim_launch_reacttocube clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 561 --> set_lift_height(10, duration=0.132)
Trigger time ms = 693 --> set_lift_height(39, duration=0.033)
Trigger time ms = 792 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-4, duration=0.297)
Trigger time ms = 297 --> set_head_angle(-4, duration=0.165)
Trigger time ms = 462 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 594 --> set_head_angle(-17, duration=0.066)
Trigger time ms = 660 --> set_head_angle(-19, duration=0.132)
Trigger time ms = 792 --> set_head_angle(-9, duration=0.165)
Trigger time ms = 957 --> set_head_angle(-10, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=23, r_wheel_speed=23, duration=0.363)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=5, r_wheel_speed=5, duration=0.165)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-7, r_wheel_speed=-7, duration=0.198)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=-17, r_wheel_speed=-17, duration=0.132)
