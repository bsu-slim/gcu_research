Original file = data/sdk_converted/anim_keepaway_pounce_01
Animation anim_keepaway_pounce_01 clip 1/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 330 --> set_lift_height(48, duration=0.099)
Trigger time ms = 429 --> set_lift_height(92, duration=0.066)
Trigger time ms = 1155 --> set_lift_height(0, duration=0.33)
---------------------------------------
Process Head angle animations
---------------------------------------
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-22, r_wheel_speed=-22, duration=0.033)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=3704, r_wheel_speed=3704, duration=0.198)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=1013, r_wheel_speed=1013, duration=0.099)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-214, r_wheel_speed=-214, duration=0.198)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=-36, r_wheel_speed=-36, duration=0.528)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=27, r_wheel_speed=27, duration=0.165)
