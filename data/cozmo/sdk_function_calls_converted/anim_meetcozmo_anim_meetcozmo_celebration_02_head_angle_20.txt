Original file = data/sdk_converted/anim_meetcozmo
Animation anim_meetcozmo_celebration_02_head_angle_20 clip 20/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(4, duration=0.099)
Trigger time ms = 99 --> set_lift_height(82, duration=0.264)
Trigger time ms = 363 --> set_lift_height(92, duration=0.231)
Trigger time ms = 594 --> set_lift_height(89, duration=0.033)
Trigger time ms = 627 --> set_lift_height(80, duration=0.066)
Trigger time ms = 693 --> set_lift_height(92, duration=0.066)
Trigger time ms = 759 --> set_lift_height(80, duration=0.132)
Trigger time ms = 891 --> set_lift_height(92, duration=0.165)
Trigger time ms = 1056 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(22, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-3, duration=0.132)
Trigger time ms = 198 --> set_head_angle(-10, duration=0.165)
Trigger time ms = 363 --> set_head_angle(25, duration=0.132)
Trigger time ms = 495 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1320 --> set_head_angle(14, duration=0.066)
Trigger time ms = 1386 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1485 --> set_head_angle(14, duration=0.099)
Trigger time ms = 1584 --> set_head_angle(30, duration=0.099)
Trigger time ms = 1683 --> set_head_angle(26, duration=0.231)
Trigger time ms = 1914 --> set_head_angle(26, duration=0.33)
Trigger time ms = 2244 --> set_head_angle(30, duration=0.165)
Trigger time ms = 2409 --> set_head_angle(-4, duration=0.198)
Trigger time ms = 2607 --> set_head_angle(0, duration=0.297)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-55, r_wheel_speed=-55, duration=0.099)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-5, r_wheel_speed=-5, duration=0.165)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=95, r_wheel_speed=95, duration=0.198)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-14, r_wheel_speed=-14, duration=0.066)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.099)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=40, r_wheel_speed=40, duration=0.066)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=-40, r_wheel_speed=-40, duration=0.066)
Trigger time ms = 759 --> drive_wheels(l_wheel_speed=38, r_wheel_speed=38, duration=0.099)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=-38, r_wheel_speed=-38, duration=0.099)
Trigger time ms = 957 --> drive_wheels(l_wheel_speed=35, r_wheel_speed=35, duration=0.165)
Trigger time ms = 1353 --> drive_wheels(l_wheel_speed=-16, r_wheel_speed=-16, duration=0.066)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=16, r_wheel_speed=16, duration=0.099)
Trigger time ms = 1518 --> drive_wheels(l_wheel_speed=-31, r_wheel_speed=-31, duration=0.099)
Trigger time ms = 1617 --> drive_wheels(l_wheel_speed=31, r_wheel_speed=31, duration=0.099)
Trigger time ms = 1716 --> drive_wheels(l_wheel_speed=-33, r_wheel_speed=-33, duration=0.198)
Trigger time ms = 2343 --> drive_wheels(l_wheel_speed=-21, r_wheel_speed=-21, duration=0.231)
