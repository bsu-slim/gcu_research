Original file = data/sdk_converted/anim_energy_reacttocliff_lv2
Animation anim_energy_reacttocliff_lv2_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-15, duration=0.132)
Trigger time ms = 132 --> set_head_angle(3, duration=0.231)
Trigger time ms = 363 --> set_head_angle(-5, duration=0.132)
Trigger time ms = 495 --> set_head_angle(-5, duration=0.165)
Trigger time ms = 660 --> set_head_angle(-21, duration=0.33)
Trigger time ms = 2706 --> set_head_angle(-25, duration=0.099)
Trigger time ms = 2805 --> set_head_angle(-11, duration=0.231)
Trigger time ms = 3036 --> set_head_angle(-8, duration=0.165)
Trigger time ms = 3201 --> set_head_angle(-13, duration=0.132)
Trigger time ms = 3333 --> set_head_angle(-9, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-43, r_wheel_speed=-43, duration=0.231)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.033)
Trigger time ms = 2805 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.429)
Trigger time ms = 3234 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.198)
