Original file = data/sdk_converted/anim_memorymatch_point_fast
Animation anim_memorymatch_pointsmallright_fast_02 clip 10/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 33 --> set_lift_height(51, duration=0.099)
Trigger time ms = 132 --> set_lift_height(0, duration=0.198)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-19, duration=0.066)
Trigger time ms = 66 --> set_head_angle(9, duration=0.099)
Trigger time ms = 165 --> set_head_angle(-13, duration=0.099)
Trigger time ms = 264 --> set_head_angle(-9, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> turn_in_place(degrees(-280))
Trigger time ms = 165 --> turn_in_place(degrees(26))
