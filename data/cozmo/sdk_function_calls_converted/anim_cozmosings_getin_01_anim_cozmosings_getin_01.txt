Original file = data/sdk_converted/anim_cozmosings_getin_01
Animation anim_cozmosings_getin_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.462)
Trigger time ms = 495 --> set_head_angle(-11, duration=0.165)
Trigger time ms = 726 --> set_head_angle(14, duration=0.198)
Trigger time ms = 924 --> set_head_angle(8, duration=0.132)
Trigger time ms = 1683 --> set_head_angle(15, duration=0.132)
Trigger time ms = 1815 --> set_head_angle(-2, duration=0.231)
Trigger time ms = 2409 --> set_head_angle(7, duration=0.198)
Trigger time ms = 2607 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 3036 --> set_head_angle(2, duration=0.132)
Trigger time ms = 3168 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 3564 --> set_head_angle(9, duration=0.099)
Trigger time ms = 3663 --> set_head_angle(-17, duration=0.165)
Trigger time ms = 3828 --> set_head_angle(-25, duration=0.165)
Trigger time ms = 4356 --> set_head_angle(5, duration=0.198)
Trigger time ms = 4554 --> set_head_angle(-10, duration=0.165)
Trigger time ms = 4884 --> set_head_angle(4, duration=0.297)
Trigger time ms = 5181 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 5280 --> set_head_angle(-21, duration=0.231)
Trigger time ms = 5742 --> set_head_angle(-1, duration=0.132)
Trigger time ms = 5874 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 6270 --> set_head_angle(-14, duration=0.066)
Trigger time ms = 6435 --> set_head_angle(3, duration=0.198)
Trigger time ms = 6633 --> set_head_angle(5, duration=0.363)
Trigger time ms = 6996 --> set_head_angle(-12, duration=0.132)
Trigger time ms = 7128 --> set_head_angle(-19, duration=0.297)
Trigger time ms = 7425 --> set_head_angle(17, duration=0.363)
Trigger time ms = 7788 --> set_head_angle(22, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-45, r_wheel_speed=-45, duration=0.132)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.099)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=8, r_wheel_speed=8, duration=0.132)
Trigger time ms = 1650 --> turn_in_place(degrees(51))
Trigger time ms = 1749 --> turn_in_place(degrees(-136))
Trigger time ms = 1881 --> turn_in_place(degrees(-30))
Trigger time ms = 3564 --> drive_wheels(l_wheel_speed=-25, r_wheel_speed=-25, duration=0.594)
Trigger time ms = 4356 --> drive_wheels(l_wheel_speed=25, r_wheel_speed=25, duration=0.198)
Trigger time ms = 4950 --> turn_in_place(degrees(53))
Trigger time ms = 5082 --> turn_in_place(degrees(20))
Trigger time ms = 6336 --> turn_in_place(degrees(101))
Trigger time ms = 6534 --> turn_in_place(degrees(40))
Trigger time ms = 6996 --> drive_wheels(l_wheel_speed=-25, r_wheel_speed=-25, duration=0.198)
Trigger time ms = 7194 --> drive_wheels(l_wheel_speed=-10, r_wheel_speed=-10, duration=0.099)
Trigger time ms = 7293 --> drive_wheels(l_wheel_speed=103, r_wheel_speed=103, duration=0.165)
Trigger time ms = 7458 --> drive_wheels(l_wheel_speed=73, r_wheel_speed=73, duration=0.165)
Trigger time ms = 7623 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.033)
Trigger time ms = 7656 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 7722 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 7788 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
