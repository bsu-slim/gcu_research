Original file = data/sdk_converted/anim_pounce_fail_01
Animation anim_pounce_fail_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> set_lift_height(62, duration=0.198)
Trigger time ms = 330 --> set_lift_height(74, duration=0.132)
Trigger time ms = 825 --> set_lift_height(64, duration=0.099)
Trigger time ms = 924 --> set_lift_height(75, duration=0.132)
Trigger time ms = 1353 --> set_lift_height(71, duration=0.099)
Trigger time ms = 1452 --> set_lift_height(75, duration=0.198)
Trigger time ms = 1980 --> set_lift_height(75, duration=0.066)
Trigger time ms = 2046 --> set_lift_height(69, duration=0.066)
Trigger time ms = 2112 --> set_lift_height(8, duration=0.231)
Trigger time ms = 2343 --> set_lift_height(1, duration=0.132)
Trigger time ms = 2475 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-16, duration=0.132)
Trigger time ms = 198 --> set_head_angle(-17, duration=0.462)
Trigger time ms = 660 --> set_head_angle(-15, duration=0.066)
Trigger time ms = 726 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 792 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 891 --> set_head_angle(-12, duration=0.198)
Trigger time ms = 1089 --> set_head_angle(-12, duration=0.198)
Trigger time ms = 1287 --> set_head_angle(-11, duration=0.132)
Trigger time ms = 1749 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 1881 --> set_head_angle(-7, duration=0.132)
Trigger time ms = 2211 --> set_head_angle(-11, duration=0.099)
Trigger time ms = 2310 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 2376 --> set_head_angle(15, duration=0.132)
Trigger time ms = 2508 --> set_head_angle(15, duration=0.132)
Trigger time ms = 2640 --> set_head_angle(14, duration=0.132)
Trigger time ms = 3102 --> set_head_angle(-15, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.066)
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.066)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-17, r_wheel_speed=-17, duration=0.396)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.099)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.066)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=-160, r_wheel_speed=-160, duration=0.132)
Trigger time ms = 1386 --> turn_in_place(degrees(106))
Trigger time ms = 2277 --> turn_in_place(degrees(-141))
Trigger time ms = 2376 --> drive_wheels(l_wheel_speed=-27, r_wheel_speed=-27, duration=0.165)
Trigger time ms = 2541 --> drive_wheels(l_wheel_speed=37, r_wheel_speed=37, duration=0.231)
Trigger time ms = 2772 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.165)
Trigger time ms = 2937 --> drive_wheels(l_wheel_speed=6, r_wheel_speed=6, duration=0.165)
Trigger time ms = 3102 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.132)
