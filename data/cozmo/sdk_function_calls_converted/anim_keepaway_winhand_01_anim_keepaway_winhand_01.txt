Original file = data/sdk_converted/anim_keepaway_winhand_01
Animation anim_keepaway_winhand_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 726 --> set_lift_height(40, duration=0.198)
Trigger time ms = 924 --> set_lift_height(53, duration=0.099)
Trigger time ms = 1023 --> set_lift_height(40, duration=0.099)
Trigger time ms = 1122 --> set_lift_height(53, duration=0.165)
Trigger time ms = 1716 --> set_lift_height(42, duration=0.132)
Trigger time ms = 1848 --> set_lift_height(92, duration=0.264)
Trigger time ms = 2112 --> set_lift_height(91, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> set_head_angle(-18, duration=0.066)
Trigger time ms = 495 --> set_head_angle(13, duration=0.264)
Trigger time ms = 924 --> set_head_angle(12, duration=0.066)
Trigger time ms = 990 --> set_head_angle(13, duration=0.066)
Trigger time ms = 1056 --> set_head_angle(10, duration=0.066)
Trigger time ms = 1122 --> set_head_angle(15, duration=0.066)
Trigger time ms = 1188 --> set_head_angle(9, duration=0.066)
Trigger time ms = 1254 --> set_head_angle(15, duration=0.066)
Trigger time ms = 1320 --> set_head_angle(9, duration=0.066)
Trigger time ms = 1386 --> set_head_angle(15, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(9, duration=0.066)
Trigger time ms = 1518 --> set_head_angle(15, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(10, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(12, duration=0.066)
Trigger time ms = 1782 --> set_head_angle(-25, duration=0.165)
Trigger time ms = 1947 --> set_head_angle(-16, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=8, r_wheel_speed=8, duration=0.066)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=-19, r_wheel_speed=-19, duration=0.066)
Trigger time ms = 990 --> drive_wheels(l_wheel_speed=33, r_wheel_speed=33, duration=0.066)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=-55, r_wheel_speed=-55, duration=0.066)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=66, r_wheel_speed=66, duration=0.066)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=-66, r_wheel_speed=-66, duration=0.066)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=66, r_wheel_speed=66, duration=0.066)
Trigger time ms = 1320 --> drive_wheels(l_wheel_speed=-66, r_wheel_speed=-66, duration=0.066)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=66, r_wheel_speed=66, duration=0.066)
Trigger time ms = 1452 --> drive_wheels(l_wheel_speed=-58, r_wheel_speed=-58, duration=0.066)
Trigger time ms = 1518 --> drive_wheels(l_wheel_speed=32, r_wheel_speed=32, duration=0.066)
Trigger time ms = 1584 --> drive_wheels(l_wheel_speed=-16, r_wheel_speed=-16, duration=0.066)
Trigger time ms = 1782 --> drive_wheels(l_wheel_speed=22, r_wheel_speed=22, duration=0.231)
