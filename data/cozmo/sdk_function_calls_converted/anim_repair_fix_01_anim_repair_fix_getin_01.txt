Original file = data/sdk_converted/anim_repair_fix_01
Animation anim_repair_fix_getin_01 clip 7/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.132)
Trigger time ms = 132 --> set_lift_height(39, duration=0.132)
Trigger time ms = 264 --> set_lift_height(32, duration=0.099)
Trigger time ms = 693 --> set_lift_height(40, duration=0.066)
Trigger time ms = 759 --> set_lift_height(32, duration=0.066)
Trigger time ms = 825 --> set_lift_height(40, duration=0.066)
Trigger time ms = 891 --> set_lift_height(32, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(0, duration=0.033)
Trigger time ms = 66 --> set_head_angle(-11, duration=0.066)
Trigger time ms = 132 --> set_head_angle(0, duration=0.132)
Trigger time ms = 561 --> set_head_angle(16, duration=0.132)
Trigger time ms = 693 --> set_head_angle(16, duration=0.231)
Trigger time ms = 924 --> set_head_angle(0, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-111, r_wheel_speed=-111, duration=0.066)
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-48, r_wheel_speed=-48, duration=0.099)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=28, r_wheel_speed=28, duration=0.033)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=57, r_wheel_speed=57, duration=0.099)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.429)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=-46, r_wheel_speed=-46, duration=0.066)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=84, r_wheel_speed=84, duration=0.066)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=-84, r_wheel_speed=-84, duration=0.066)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=46, r_wheel_speed=46, duration=0.066)
