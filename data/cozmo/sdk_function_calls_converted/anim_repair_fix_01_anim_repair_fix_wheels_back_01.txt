Original file = data/sdk_converted/anim_repair_fix_01
Animation anim_repair_fix_wheels_back_01 clip 5/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 99 --> set_head_angle(4, duration=0.099)
Trigger time ms = 198 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> drive_wheels(l_wheel_speed=-212, r_wheel_speed=-212, duration=0.066)
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-121, r_wheel_speed=-121, duration=0.033)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=91, r_wheel_speed=91, duration=0.066)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.033)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=76, r_wheel_speed=76, duration=0.066)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=45, r_wheel_speed=45, duration=0.066)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.099)
