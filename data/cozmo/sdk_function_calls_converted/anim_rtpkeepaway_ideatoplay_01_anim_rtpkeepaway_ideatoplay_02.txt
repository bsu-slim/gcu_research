Original file = data/sdk_converted/anim_rtpkeepaway_ideatoplay_01
Animation anim_rtpkeepaway_ideatoplay_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1947 --> set_lift_height(50, duration=0.099)
Trigger time ms = 2046 --> set_lift_height(55, duration=0.066)
Trigger time ms = 2112 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> set_head_angle(-12, duration=0.264)
Trigger time ms = 429 --> set_head_angle(-16, duration=0.198)
Trigger time ms = 627 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 726 --> set_head_angle(-10, duration=0.165)
Trigger time ms = 891 --> set_head_angle(-9, duration=0.891)
Trigger time ms = 1782 --> set_head_angle(-9, duration=0.033)
Trigger time ms = 1815 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 1881 --> set_head_angle(13, duration=0.165)
Trigger time ms = 2343 --> set_head_angle(17, duration=0.033)
Trigger time ms = 2376 --> set_head_angle(9, duration=0.066)
Trigger time ms = 2475 --> set_head_angle(13, duration=0.033)
Trigger time ms = 2508 --> set_head_angle(5, duration=0.066)
Trigger time ms = 2607 --> set_head_angle(10, duration=0.033)
Trigger time ms = 2640 --> set_head_angle(1, duration=0.066)
Trigger time ms = 2739 --> set_head_angle(9, duration=0.099)
Trigger time ms = 2838 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 759 --> turn_in_place(degrees(38))
Trigger time ms = 1914 --> drive_wheels(l_wheel_speed=-76, r_wheel_speed=-76, duration=0.132)
Trigger time ms = 2046 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.099)
Trigger time ms = 2145 --> drive_wheels(l_wheel_speed=60, r_wheel_speed=60, duration=0.066)
Trigger time ms = 2211 --> drive_wheels(l_wheel_speed=-38, r_wheel_speed=-38, duration=0.033)
Trigger time ms = 2739 --> drive_wheels(l_wheel_speed=-25, r_wheel_speed=-25, duration=0.066)
Trigger time ms = 2805 --> drive_wheels(l_wheel_speed=25, r_wheel_speed=25, duration=0.132)
