Original file = data/sdk_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getin_medium_01 clip 2/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1518 --> set_lift_height(0, duration=0.132)
Trigger time ms = 1650 --> set_lift_height(0, duration=0.165)
Trigger time ms = 1815 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1881 --> set_lift_height(0, duration=0.099)
Trigger time ms = 2244 --> set_lift_height(53, duration=0.165)
Trigger time ms = 2409 --> set_lift_height(55, duration=0.033)
Trigger time ms = 2442 --> set_lift_height(40, duration=0.165)
Trigger time ms = 2607 --> set_lift_height(42, duration=0.165)
Trigger time ms = 2772 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> set_head_angle(-9, duration=0.066)
Trigger time ms = 132 --> set_head_angle(7, duration=0.066)
Trigger time ms = 198 --> set_head_angle(9, duration=0.066)
Trigger time ms = 264 --> set_head_angle(9, duration=0.132)
Trigger time ms = 462 --> set_head_angle(9, duration=0.363)
Trigger time ms = 825 --> set_head_angle(2, duration=0.132)
Trigger time ms = 957 --> set_head_angle(2, duration=0.198)
Trigger time ms = 1155 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 1254 --> set_head_angle(-2, duration=0.099)
Trigger time ms = 1848 --> set_head_angle(7, duration=0.33)
Trigger time ms = 2178 --> set_head_angle(10, duration=0.363)
Trigger time ms = 2541 --> set_head_angle(-20, duration=0.099)
Trigger time ms = 2640 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=-96, r_wheel_speed=-96, duration=0.132)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=-9, r_wheel_speed=-9, duration=0.066)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.066)
Trigger time ms = 2508 --> drive_wheels(l_wheel_speed=-35, r_wheel_speed=-35, duration=0.099)
Trigger time ms = 2607 --> drive_wheels(l_wheel_speed=186, r_wheel_speed=186, duration=0.198)
