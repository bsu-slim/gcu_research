Original file = data/sdk_converted/launch
Animation anim_launch_wakeup_04 clip 5/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.231)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-16, duration=0.363)
Trigger time ms = 363 --> set_head_angle(-9, duration=0.792)
Trigger time ms = 1155 --> set_head_angle(-8, duration=0.264)
Trigger time ms = 1419 --> set_head_angle(4, duration=0.792)
Trigger time ms = 2211 --> set_head_angle(-5, duration=0.132)
Trigger time ms = 2343 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 2409 --> set_head_angle(5, duration=0.198)
Trigger time ms = 2607 --> set_head_angle(6, duration=0.132)
Trigger time ms = 2739 --> set_head_angle(11, duration=0.066)
Trigger time ms = 2805 --> set_head_angle(12, duration=0.264)
Trigger time ms = 3069 --> set_head_angle(0, duration=0.132)
Trigger time ms = 3267 --> set_head_angle(-4, duration=0.033)
Trigger time ms = 3300 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=1.32)
