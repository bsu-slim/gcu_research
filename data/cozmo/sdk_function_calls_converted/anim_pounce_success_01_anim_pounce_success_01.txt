Original file = data/sdk_converted/anim_pounce_success_01
Animation anim_pounce_success_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.495)
Trigger time ms = 495 --> set_lift_height(47, duration=0.099)
Trigger time ms = 594 --> set_lift_height(8, duration=0.099)
Trigger time ms = 693 --> set_lift_height(0, duration=0.033)
Trigger time ms = 726 --> set_lift_height(41, duration=0.066)
Trigger time ms = 792 --> set_lift_height(8, duration=0.099)
Trigger time ms = 891 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-10, duration=0.198)
Trigger time ms = 198 --> set_head_angle(-18, duration=0.099)
Trigger time ms = 297 --> set_head_angle(19, duration=0.132)
Trigger time ms = 429 --> set_head_angle(24, duration=0.099)
Trigger time ms = 528 --> set_head_angle(19, duration=0.231)
Trigger time ms = 759 --> set_head_angle(20, duration=0.165)
Trigger time ms = 924 --> set_head_angle(21, duration=0.264)
Trigger time ms = 1188 --> set_head_angle(25, duration=0.198)
Trigger time ms = 1386 --> set_head_angle(-15, duration=0.165)
Trigger time ms = 1551 --> set_head_angle(-17, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=4, r_wheel_speed=4, duration=0.066)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=29, r_wheel_speed=29, duration=0.099)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=19, r_wheel_speed=19, duration=0.066)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=9, r_wheel_speed=9, duration=0.033)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-1236, r_wheel_speed=-1236, duration=0.297)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=3, r_wheel_speed=3, duration=0.66)
