Original file = data/sdk_converted/anim_launch_idle_01
Animation anim_launch_idle_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> set_head_angle(-17, duration=0.132)
Trigger time ms = 363 --> set_head_angle(-14, duration=0.231)
Trigger time ms = 594 --> set_head_angle(-14, duration=0.264)
Trigger time ms = 858 --> set_head_angle(-14, duration=0.231)
Trigger time ms = 1089 --> set_head_angle(-14, duration=0.693)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-16, r_wheel_speed=-16, duration=0.198)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.132)
