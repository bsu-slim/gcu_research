Original file = data/sdk_converted/anim_energy_driving
Animation anim_energy_driveloop_03 clip 5/5
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-9, duration=0.33)
Trigger time ms = 330 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 396 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 1716 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 1815 --> set_head_angle(-9, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
