Original file = data/sdk_converted/anim_memorymatch_point_v02
Animation anim_memorymatch_pointcenter_03 clip 3/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 231 --> set_lift_height(51, duration=0.132)
Trigger time ms = 363 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-2, duration=0.066)
Trigger time ms = 198 --> set_head_angle(-14, duration=0.066)
Trigger time ms = 264 --> set_head_angle(1, duration=0.099)
Trigger time ms = 363 --> set_head_angle(-11, duration=0.165)
Trigger time ms = 528 --> set_head_angle(-9, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=63, r_wheel_speed=63, duration=0.165)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-68, r_wheel_speed=-68, duration=0.198)
