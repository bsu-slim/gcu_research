Original file = data/sdk_converted/anim_keepaway_idle_01
Animation anim_keepaway_idle_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1221 --> set_lift_height(90, duration=0.165)
Trigger time ms = 1386 --> set_lift_height(92, duration=0.264)
Trigger time ms = 2541 --> set_lift_height(90, duration=0.165)
Trigger time ms = 2838 --> set_lift_height(92, duration=0.231)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> set_head_angle(-17, duration=0.165)
Trigger time ms = 990 --> set_head_angle(-18, duration=0.165)
Trigger time ms = 2574 --> set_head_angle(-20, duration=0.132)
Trigger time ms = 2706 --> set_head_angle(-11, duration=0.165)
Trigger time ms = 2871 --> set_head_angle(-9, duration=0.099)
Trigger time ms = 2970 --> set_head_angle(-16, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1155 --> turn_in_place(degrees(-14))
Trigger time ms = 2904 --> turn_in_place(degrees(22))
