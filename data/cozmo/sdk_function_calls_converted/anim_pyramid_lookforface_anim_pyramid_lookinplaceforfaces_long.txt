Original file = data/sdk_converted/anim_pyramid_lookforface
Animation anim_pyramid_lookinplaceforfaces_long clip 1/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.264)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-1, duration=0.165)
Trigger time ms = 297 --> set_head_angle(5, duration=0.132)
Trigger time ms = 429 --> set_head_angle(6, duration=0.099)
Trigger time ms = 1716 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
