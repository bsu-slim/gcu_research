Original file = data/sdk_converted/vs_speedtap_handreact
Animation anim_speedtap_losehand_02 clip 5/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> set_lift_height(89, duration=0.132)
Trigger time ms = 231 --> set_lift_height(92, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> set_head_angle(-20, duration=0.033)
Trigger time ms = 165 --> set_head_angle(-7, duration=0.132)
Trigger time ms = 462 --> set_head_angle(-20, duration=0.099)
Trigger time ms = 561 --> set_head_angle(-16, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
