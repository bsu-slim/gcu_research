Original file = data/sdk_converted/anim_dancing_mambo_02
Animation anim_dancing_mambo_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1419 --> set_lift_height(41, duration=0.099)
Trigger time ms = 1518 --> set_lift_height(32, duration=0.099)
Trigger time ms = 2475 --> set_lift_height(51, duration=0.099)
Trigger time ms = 2574 --> set_lift_height(34, duration=0.099)
Trigger time ms = 2673 --> set_lift_height(32, duration=0.165)
Trigger time ms = 4521 --> set_lift_height(56, duration=0.132)
Trigger time ms = 4653 --> set_lift_height(54, duration=0.099)
Trigger time ms = 5775 --> set_lift_height(59, duration=0.066)
Trigger time ms = 5841 --> set_lift_height(32, duration=0.132)
Trigger time ms = 9537 --> set_lift_height(66, duration=0.198)
Trigger time ms = 9735 --> set_lift_height(38, duration=0.165)
Trigger time ms = 9900 --> set_lift_height(32, duration=0.231)
Trigger time ms = 10428 --> set_lift_height(53, duration=0.099)
Trigger time ms = 10527 --> set_lift_height(32, duration=0.132)
Trigger time ms = 12078 --> set_lift_height(51, duration=0.099)
Trigger time ms = 12177 --> set_lift_height(32, duration=0.132)
Trigger time ms = 12705 --> set_lift_height(61, duration=0.132)
Trigger time ms = 12837 --> set_lift_height(32, duration=0.231)
Trigger time ms = 13431 --> set_lift_height(61, duration=0.132)
Trigger time ms = 13563 --> set_lift_height(32, duration=0.231)
Trigger time ms = 13926 --> set_lift_height(76, duration=0.264)
Trigger time ms = 14190 --> set_lift_height(32, duration=0.528)
Trigger time ms = 14718 --> set_lift_height(34, duration=0.264)
Trigger time ms = 14982 --> set_lift_height(46, duration=0.132)
Trigger time ms = 15114 --> set_lift_height(33, duration=0.099)
Trigger time ms = 15213 --> set_lift_height(32, duration=0.231)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.66)
Trigger time ms = 660 --> set_head_angle(-7, duration=0.132)
Trigger time ms = 792 --> set_head_angle(0, duration=0.132)
Trigger time ms = 1221 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 1353 --> set_head_angle(1, duration=0.132)
Trigger time ms = 1881 --> set_head_angle(-14, duration=0.132)
Trigger time ms = 2013 --> set_head_angle(1, duration=0.132)
Trigger time ms = 2409 --> set_head_angle(14, duration=0.132)
Trigger time ms = 2541 --> set_head_angle(-18, duration=0.132)
Trigger time ms = 2673 --> set_head_angle(36, duration=0.132)
Trigger time ms = 2805 --> set_head_angle(36, duration=0.132)
Trigger time ms = 2937 --> set_head_angle(30, duration=0.066)
Trigger time ms = 3498 --> set_head_angle(-25, duration=0.099)
Trigger time ms = 3597 --> set_head_angle(20, duration=0.165)
Trigger time ms = 3762 --> set_head_angle(-19, duration=0.132)
Trigger time ms = 4290 --> set_head_angle(14, duration=0.099)
Trigger time ms = 4389 --> set_head_angle(9, duration=0.132)
Trigger time ms = 4521 --> set_head_angle(1, duration=0.066)
Trigger time ms = 4587 --> set_head_angle(21, duration=0.066)
Trigger time ms = 4653 --> set_head_angle(9, duration=0.132)
Trigger time ms = 4785 --> set_head_angle(21, duration=0.132)
Trigger time ms = 4917 --> set_head_angle(9, duration=0.132)
Trigger time ms = 5049 --> set_head_angle(33, duration=0.198)
Trigger time ms = 5247 --> set_head_angle(28, duration=0.165)
Trigger time ms = 5643 --> set_head_angle(-10, duration=0.165)
Trigger time ms = 5808 --> set_head_angle(42, duration=0.132)
Trigger time ms = 5940 --> set_head_angle(-9, duration=0.231)
Trigger time ms = 6171 --> set_head_angle(17, duration=0.33)
Trigger time ms = 6501 --> set_head_angle(8, duration=0.33)
Trigger time ms = 6831 --> set_head_angle(17, duration=0.33)
Trigger time ms = 7161 --> set_head_angle(8, duration=0.33)
Trigger time ms = 7491 --> set_head_angle(31, duration=0.264)
Trigger time ms = 7755 --> set_head_angle(40, duration=0.198)
Trigger time ms = 7953 --> set_head_angle(18, duration=0.264)
Trigger time ms = 8217 --> set_head_angle(38, duration=0.165)
Trigger time ms = 8382 --> set_head_angle(1, duration=0.132)
Trigger time ms = 8514 --> set_head_angle(18, duration=0.165)
Trigger time ms = 8679 --> set_head_angle(1, duration=0.132)
Trigger time ms = 8811 --> set_head_angle(34, duration=0.165)
Trigger time ms = 8976 --> set_head_angle(16, duration=0.132)
Trigger time ms = 9108 --> set_head_angle(34, duration=0.165)
Trigger time ms = 9273 --> set_head_angle(16, duration=0.132)
Trigger time ms = 9405 --> set_head_angle(34, duration=0.099)
Trigger time ms = 9504 --> set_head_angle(-25, duration=0.165)
Trigger time ms = 9669 --> set_head_angle(38, duration=0.198)
Trigger time ms = 9867 --> set_head_angle(18, duration=0.165)
Trigger time ms = 10461 --> set_head_angle(30, duration=0.099)
Trigger time ms = 10560 --> set_head_angle(-25, duration=0.198)
Trigger time ms = 10758 --> set_head_angle(-17, duration=0.198)
Trigger time ms = 12177 --> set_head_angle(-19, duration=0.132)
Trigger time ms = 12309 --> set_head_angle(0, duration=0.132)
Trigger time ms = 12441 --> set_head_angle(-19, duration=0.132)
Trigger time ms = 12573 --> set_head_angle(19, duration=0.132)
Trigger time ms = 12705 --> set_head_angle(-11, duration=0.132)
Trigger time ms = 12837 --> set_head_angle(19, duration=0.231)
Trigger time ms = 13068 --> set_head_angle(-1, duration=0.132)
Trigger time ms = 13200 --> set_head_angle(28, duration=0.132)
Trigger time ms = 13332 --> set_head_angle(38, duration=0.132)
Trigger time ms = 13464 --> set_head_angle(41, duration=0.33)
Trigger time ms = 13794 --> set_head_angle(19, duration=0.297)
Trigger time ms = 14091 --> set_head_angle(42, duration=0.264)
Trigger time ms = 14355 --> set_head_angle(31, duration=0.099)
Trigger time ms = 14454 --> set_head_angle(42, duration=0.099)
Trigger time ms = 14553 --> set_head_angle(31, duration=0.099)
Trigger time ms = 14652 --> set_head_angle(41, duration=0.066)
Trigger time ms = 14718 --> set_head_angle(19, duration=0.165)
Trigger time ms = 14883 --> set_head_angle(2, duration=0.066)
Trigger time ms = 14949 --> set_head_angle(6, duration=0.066)
Trigger time ms = 15015 --> set_head_angle(12, duration=0.132)
Trigger time ms = 15147 --> set_head_angle(-1, duration=0.132)
Trigger time ms = 15279 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1815 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 1947 --> drive_wheels(l_wheel_speed=51, r_wheel_speed=51, duration=0.099)
Trigger time ms = 2046 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 2079 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.099)
Trigger time ms = 2178 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 2442 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.132)
Trigger time ms = 2574 --> drive_wheels(l_wheel_speed=-162, r_wheel_speed=-162, duration=0.099)
Trigger time ms = 2673 --> drive_wheels(l_wheel_speed=-152, r_wheel_speed=-152, duration=0.033)
Trigger time ms = 2706 --> drive_wheels(l_wheel_speed=121, r_wheel_speed=121, duration=0.165)
Trigger time ms = 2871 --> drive_wheels(l_wheel_speed=-43, r_wheel_speed=-43, duration=0.231)
Trigger time ms = 3102 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.297)
Trigger time ms = 3399 --> drive_wheels(l_wheel_speed=45, r_wheel_speed=45, duration=0.066)
Trigger time ms = 3465 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.066)
Trigger time ms = 3531 --> drive_wheels(l_wheel_speed=-202, r_wheel_speed=-202, duration=0.099)
Trigger time ms = 3630 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 3729 --> turn_in_place(degrees(300))
Trigger time ms = 3828 --> turn_in_place(degrees(-300))
Trigger time ms = 3927 --> turn_in_place(degrees(-300))
Trigger time ms = 4092 --> turn_in_place(degrees(300))
Trigger time ms = 4158 --> turn_in_place(degrees(300))
Trigger time ms = 4257 --> drive_wheels(l_wheel_speed=5390.0, r_wheel_speed=-4510.0, duration=0.066)
Trigger time ms = 4323 --> drive_wheels(l_wheel_speed=-106.0, r_wheel_speed=74.0, duration=0.033)
Trigger time ms = 4356 --> drive_wheels(l_wheel_speed=-137.5, r_wheel_speed=87.5, duration=0.033)
Trigger time ms = 4389 --> drive_wheels(l_wheel_speed=-159.5, r_wheel_speed=335.5, duration=0.099)
Trigger time ms = 4488 --> drive_wheels(l_wheel_speed=-383.5, r_wheel_speed=201.5, duration=0.066)
Trigger time ms = 4554 --> drive_wheels(l_wheel_speed=-1200.0, r_wheel_speed=240.0, duration=0.033)
Trigger time ms = 4587 --> drive_wheels(l_wheel_speed=153.0, r_wheel_speed=-117.0, duration=0.033)
Trigger time ms = 4620 --> drive_wheels(l_wheel_speed=-304.5, r_wheel_speed=640.5, duration=0.033)
Trigger time ms = 4653 --> drive_wheels(l_wheel_speed=-105.0, r_wheel_speed=1785.0, duration=0.033)
Trigger time ms = 4686 --> drive_wheels(l_wheel_speed=-2075.0, r_wheel_speed=175.0, duration=0.099)
Trigger time ms = 4785 --> drive_wheels(l_wheel_speed=-247.5, r_wheel_speed=1777.5, duration=0.099)
Trigger time ms = 4884 --> drive_wheels(l_wheel_speed=-966.0, r_wheel_speed=294.0, duration=0.099)
Trigger time ms = 4983 --> drive_wheels(l_wheel_speed=-364.0, r_wheel_speed=2156.0, duration=0.099)
Trigger time ms = 5082 --> drive_wheels(l_wheel_speed=637.5, r_wheel_speed=-1657.5, duration=0.099)
Trigger time ms = 5181 --> drive_wheels(l_wheel_speed=826.5, r_wheel_speed=-478.5, duration=0.099)
Trigger time ms = 5280 --> drive_wheels(l_wheel_speed=487.5, r_wheel_speed=-1267.5, duration=0.099)
Trigger time ms = 5379 --> drive_wheels(l_wheel_speed=627.0, r_wheel_speed=-363.0, duration=0.099)
Trigger time ms = 5478 --> drive_wheels(l_wheel_speed=232.5, r_wheel_speed=-442.5, duration=0.099)
Trigger time ms = 5577 --> drive_wheels(l_wheel_speed=23.5, r_wheel_speed=-21.5, duration=0.066)
Trigger time ms = 5643 --> drive_wheels(l_wheel_speed=82.5, r_wheel_speed=-52.5, duration=0.033)
Trigger time ms = 5676 --> drive_wheels(l_wheel_speed=21.5, r_wheel_speed=-23.5, duration=0.066)
Trigger time ms = 5742 --> drive_wheels(l_wheel_speed=74.0, r_wheel_speed=-106.0, duration=0.033)
Trigger time ms = 5775 --> drive_wheels(l_wheel_speed=-122.5, r_wheel_speed=102.5, duration=0.066)
Trigger time ms = 5841 --> drive_wheels(l_wheel_speed=-82.5, r_wheel_speed=52.5, duration=0.033)
Trigger time ms = 5907 --> drive_wheels(l_wheel_speed=745.5, r_wheel_speed=-199.5, duration=0.099)
Trigger time ms = 6006 --> turn_in_place(degrees(97))
Trigger time ms = 6171 --> turn_in_place(degrees(98))
Trigger time ms = 6303 --> turn_in_place(degrees(-40))
Trigger time ms = 6402 --> turn_in_place(degrees(-42))
Trigger time ms = 6666 --> turn_in_place(degrees(47))
Trigger time ms = 7029 --> turn_in_place(degrees(-50))
Trigger time ms = 7392 --> turn_in_place(degrees(52))
Trigger time ms = 7755 --> turn_in_place(degrees(-48))
Trigger time ms = 7953 --> drive_wheels(l_wheel_speed=68, r_wheel_speed=68, duration=0.132)
Trigger time ms = 8085 --> drive_wheels(l_wheel_speed=-121, r_wheel_speed=-121, duration=0.198)
Trigger time ms = 8316 --> turn_in_place(degrees(-72))
Trigger time ms = 8580 --> turn_in_place(degrees(58))
Trigger time ms = 8910 --> turn_in_place(degrees(-58))
Trigger time ms = 9240 --> turn_in_place(degrees(58))
Trigger time ms = 9570 --> turn_in_place(degrees(-58))
Trigger time ms = 10164 --> drive_wheels(l_wheel_speed=-5312.5, r_wheel_speed=-1487.5, duration=0.033)
Trigger time ms = 10197 --> drive_wheels(l_wheel_speed=13750.0, r_wheel_speed=3850.0, duration=0.066)
Trigger time ms = 10263 --> drive_wheels(l_wheel_speed=-13970.0, r_wheel_speed=-4070.0, duration=0.099)
Trigger time ms = 10362 --> drive_wheels(l_wheel_speed=8953.5, r_wheel_speed=2608.5, duration=0.066)
Trigger time ms = 10428 --> drive_wheels(l_wheel_speed=-8953.5, r_wheel_speed=-2608.5, duration=0.066)
Trigger time ms = 10494 --> drive_wheels(l_wheel_speed=3429.0, r_wheel_speed=999.0, duration=0.132)
Trigger time ms = 10626 --> drive_wheels(l_wheel_speed=3289.5, r_wheel_speed=994.5, duration=0.33)
Trigger time ms = 10956 --> drive_wheels(l_wheel_speed=-2489.0, r_wheel_speed=-779.0, duration=0.66)
Trigger time ms = 11616 --> drive_wheels(l_wheel_speed=2460.5, r_wheel_speed=795.5, duration=0.66)
Trigger time ms = 12276 --> drive_wheels(l_wheel_speed=-2700.0, r_wheel_speed=-900.0, duration=0.33)
Trigger time ms = 12606 --> drive_wheels(l_wheel_speed=-14445.0, r_wheel_speed=-4815.0, duration=0.066)
Trigger time ms = 12672 --> drive_wheels(l_wheel_speed=12825.0, r_wheel_speed=4275.0, duration=0.033)
Trigger time ms = 12705 --> drive_wheels(l_wheel_speed=12825.0, r_wheel_speed=4275.0, duration=0.033)
Trigger time ms = 12738 --> drive_wheels(l_wheel_speed=-14850.0, r_wheel_speed=-4950.0, duration=0.066)
Trigger time ms = 12804 --> drive_wheels(l_wheel_speed=3982.5, r_wheel_speed=1327.5, duration=0.132)
Trigger time ms = 12936 --> drive_wheels(l_wheel_speed=-6644.5, r_wheel_speed=-2279.5, duration=0.099)
Trigger time ms = 13035 --> drive_wheels(l_wheel_speed=-6644.5, r_wheel_speed=-2279.5, duration=0.033)
Trigger time ms = 13068 --> drive_wheels(l_wheel_speed=8905.0, r_wheel_speed=3055.0, duration=0.099)
Trigger time ms = 13167 --> drive_wheels(l_wheel_speed=8631.0, r_wheel_speed=2961.0, duration=0.198)
Trigger time ms = 13365 --> drive_wheels(l_wheel_speed=-8973.5, r_wheel_speed=-3078.5, duration=0.264)
Trigger time ms = 13629 --> drive_wheels(l_wheel_speed=4795.0, r_wheel_speed=1645.0, duration=0.264)
Trigger time ms = 13893 --> drive_wheels(l_wheel_speed=5000.5, r_wheel_speed=1715.5, duration=0.033)
Trigger time ms = 13926 --> drive_wheels(l_wheel_speed=-6507.5, r_wheel_speed=-2232.5, duration=0.363)
Trigger time ms = 14289 --> drive_wheels(l_wheel_speed=2945.5, r_wheel_speed=1010.5, duration=0.297)
Trigger time ms = 14586 --> drive_wheels(l_wheel_speed=3082.5, r_wheel_speed=1057.5, duration=0.198)
Trigger time ms = 14784 --> drive_wheels(l_wheel_speed=1644.0, r_wheel_speed=564.0, duration=0.099)
Trigger time ms = 14916 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.099)
Trigger time ms = 15015 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.066)
Trigger time ms = 15081 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.033)
Trigger time ms = 15114 --> drive_wheels(l_wheel_speed=11, r_wheel_speed=11, duration=0.264)
