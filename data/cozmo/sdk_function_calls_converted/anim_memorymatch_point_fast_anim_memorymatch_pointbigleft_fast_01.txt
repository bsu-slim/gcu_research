Original file = data/sdk_converted/anim_memorymatch_point_fast
Animation anim_memorymatch_pointbigleft_fast_01 clip 5/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(62, duration=0.132)
Trigger time ms = 132 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-25, duration=0.099)
Trigger time ms = 99 --> set_head_angle(-1, duration=0.033)
Trigger time ms = 132 --> set_head_angle(-11, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> turn_in_place(degrees(720))
