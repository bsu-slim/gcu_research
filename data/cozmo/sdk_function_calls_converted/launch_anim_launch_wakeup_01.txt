Original file = data/sdk_converted/launch
Animation anim_launch_wakeup_01 clip 2/11
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-20, duration=1.881)
Trigger time ms = 1881 --> set_head_angle(-17, duration=0.561)
Trigger time ms = 2970 --> set_head_angle(-7, duration=0.231)
Trigger time ms = 4125 --> set_head_angle(-2, duration=0.231)
Trigger time ms = 4620 --> set_head_angle(2, duration=0.132)
Trigger time ms = 5115 --> set_head_angle(23, duration=0.198)
Trigger time ms = 5676 --> set_head_angle(5, duration=0.297)
Trigger time ms = 5973 --> set_head_angle(1, duration=0.495)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 4125 --> turn_in_place(degrees(43))
Trigger time ms = 4620 --> turn_in_place(degrees(8))
Trigger time ms = 5115 --> turn_in_place(degrees(-35))
Trigger time ms = 5676 --> turn_in_place(degrees(-8))
Trigger time ms = 7425 --> turn_in_place(degrees(15))
