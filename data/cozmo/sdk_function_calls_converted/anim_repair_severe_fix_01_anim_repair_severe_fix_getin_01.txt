Original file = data/sdk_converted/anim_repair_severe_fix_01
Animation anim_repair_severe_fix_getin_01 clip 7/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(57, duration=0.66)
Trigger time ms = 660 --> set_lift_height(48, duration=0.726)
Trigger time ms = 3729 --> set_lift_height(57, duration=0.726)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=1.155)
Trigger time ms = 1155 --> set_head_angle(9, duration=0.66)
Trigger time ms = 1815 --> set_head_angle(9, duration=0.198)
Trigger time ms = 3465 --> set_head_angle(-14, duration=0.693)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=-25, r_wheel_speed=-25, duration=1.386)
Trigger time ms = 1815 --> drive_wheels(l_wheel_speed=-15, r_wheel_speed=-15, duration=0.132)
Trigger time ms = 1947 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 2079 --> drive_wheels(l_wheel_speed=-15, r_wheel_speed=-15, duration=0.132)
Trigger time ms = 2211 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 2343 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 2475 --> drive_wheels(l_wheel_speed=-15, r_wheel_speed=-15, duration=0.132)
Trigger time ms = 2739 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 2871 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 3003 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 3267 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 3927 --> drive_wheels(l_wheel_speed=23, r_wheel_speed=23, duration=0.66)
Trigger time ms = 4587 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.066)
Trigger time ms = 4653 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 4719 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 4785 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 4851 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 4917 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 4983 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 5115 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 5181 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 5247 --> drive_wheels(l_wheel_speed=10, r_wheel_speed=10, duration=0.099)
Trigger time ms = 5412 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 5610 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
