Original file = data/sdk_converted/anim_reacttoblock_putdown2nd_01
Animation anim_reacttoblock_putdown2nd_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(77, duration=0.132)
Trigger time ms = 132 --> set_lift_height(68, duration=0.132)
Trigger time ms = 264 --> set_lift_height(77, duration=0.132)
Trigger time ms = 528 --> set_lift_height(71, duration=0.132)
Trigger time ms = 660 --> set_lift_height(0, duration=0.297)
Trigger time ms = 957 --> set_lift_height(36, duration=0.132)
Trigger time ms = 1089 --> set_lift_height(44, duration=0.066)
Trigger time ms = 1155 --> set_lift_height(5, duration=0.132)
Trigger time ms = 1287 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 264 --> set_head_angle(8, duration=0.198)
Trigger time ms = 462 --> set_head_angle(4, duration=0.099)
Trigger time ms = 561 --> set_head_angle(14, duration=0.165)
Trigger time ms = 726 --> set_head_angle(17, duration=0.132)
Trigger time ms = 1056 --> set_head_angle(-8, duration=0.132)
Trigger time ms = 1188 --> set_head_angle(5, duration=0.132)
Trigger time ms = 1320 --> set_head_angle(0, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-2, r_wheel_speed=-2, duration=0.066)
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-11, r_wheel_speed=-11, duration=0.066)
Trigger time ms = 165 --> turn_in_place(degrees(207))
Trigger time ms = 363 --> turn_in_place(degrees(121))
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=52, r_wheel_speed=52, duration=0.165)
Trigger time ms = 891 --> drive_wheels(l_wheel_speed=-43, r_wheel_speed=-43, duration=0.066)
Trigger time ms = 957 --> drive_wheels(l_wheel_speed=-66, r_wheel_speed=-66, duration=0.099)
Trigger time ms = 1056 --> drive_wheels(l_wheel_speed=-33, r_wheel_speed=-33, duration=0.132)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.099)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=-1, r_wheel_speed=-1, duration=0.165)
