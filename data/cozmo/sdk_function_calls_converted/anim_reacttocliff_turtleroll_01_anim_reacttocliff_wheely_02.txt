Original file = data/sdk_converted/anim_reacttocliff_turtleroll_01
Animation anim_reacttocliff_wheely_02 clip 8/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 792 --> set_lift_height(40, duration=0.066)
Trigger time ms = 858 --> set_lift_height(0, duration=0.066)
Trigger time ms = 924 --> set_lift_height(88, duration=0.264)
Trigger time ms = 1188 --> set_lift_height(79, duration=0.066)
Trigger time ms = 1254 --> set_lift_height(87, duration=0.066)
Trigger time ms = 1320 --> set_lift_height(79, duration=0.066)
Trigger time ms = 1386 --> set_lift_height(88, duration=0.066)
Trigger time ms = 1452 --> set_lift_height(80, duration=0.066)
Trigger time ms = 1518 --> set_lift_height(88, duration=0.066)
Trigger time ms = 1584 --> set_lift_height(83, duration=0.099)
Trigger time ms = 1683 --> set_lift_height(85, duration=0.066)
Trigger time ms = 1749 --> set_lift_height(92, duration=0.049)
Trigger time ms = 1807 --> set_lift_height(85, duration=0.041)
Trigger time ms = 1848 --> set_lift_height(92, duration=0.066)
Trigger time ms = 1914 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 198 --> set_head_angle(-8, duration=0.165)
Trigger time ms = 363 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 495 --> set_head_angle(-9, duration=0.149)
Trigger time ms = 743 --> set_head_angle(-19, duration=0.083)
Trigger time ms = 825 --> set_head_angle(-8, duration=0.083)
Trigger time ms = 908 --> set_head_angle(-7, duration=0.198)
Trigger time ms = 1106 --> set_head_angle(3, duration=0.132)
Trigger time ms = 1238 --> set_head_angle(24, duration=0.111)
Trigger time ms = 1348 --> set_head_angle(3, duration=0.112)
Trigger time ms = 1461 --> set_head_angle(24, duration=0.111)
Trigger time ms = 1571 --> set_head_angle(3, duration=0.045)
Trigger time ms = 1616 --> set_head_angle(24, duration=0.07)
Trigger time ms = 1686 --> set_head_angle(13, duration=0.063)
Trigger time ms = 1749 --> set_head_angle(4, duration=0.028)
Trigger time ms = 1777 --> set_head_angle(22, duration=0.005)
Trigger time ms = 1782 --> set_head_angle(26, duration=0.041)
Trigger time ms = 1823 --> set_head_angle(29, duration=0.025)
Trigger time ms = 1848 --> set_head_angle(-21, duration=0.211)
Trigger time ms = 2059 --> set_head_angle(16, duration=0.165)
Trigger time ms = 2290 --> set_head_angle(-22, duration=0.066)
Trigger time ms = 2356 --> set_head_angle(21, duration=0.132)
Trigger time ms = 2488 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 2587 --> set_head_angle(7, duration=0.132)
Trigger time ms = 2719 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 2785 --> set_head_angle(3, duration=0.198)
Trigger time ms = 2983 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 866 --> drive_wheels(l_wheel_speed=-23, r_wheel_speed=-23, duration=0.14)
Trigger time ms = 1040 --> drive_wheels(l_wheel_speed=-925.0, r_wheel_speed=2405.0, duration=0.776)
Trigger time ms = 1914 --> drive_wheels(l_wheel_speed=144, r_wheel_speed=144, duration=0.066)
Trigger time ms = 1980 --> drive_wheels(l_wheel_speed=-845, r_wheel_speed=-845, duration=0.432)
