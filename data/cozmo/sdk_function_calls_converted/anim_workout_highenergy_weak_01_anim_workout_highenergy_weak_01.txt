Original file = data/sdk_converted/anim_workout_highenergy_weak_01
Animation anim_workout_highenergy_weak_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 198 --> set_lift_height(53, duration=0.363)
Trigger time ms = 561 --> set_lift_height(67, duration=0.33)
Trigger time ms = 1122 --> set_lift_height(53, duration=0.264)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-11, duration=0.264)
Trigger time ms = 396 --> set_head_angle(-15, duration=0.231)
Trigger time ms = 627 --> set_head_angle(5, duration=0.165)
Trigger time ms = 792 --> set_head_angle(12, duration=0.33)
Trigger time ms = 1122 --> set_head_angle(-5, duration=0.264)
Trigger time ms = 1386 --> set_head_angle(8, duration=0.165)
Trigger time ms = 1551 --> set_head_angle(6, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.264)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-25, r_wheel_speed=-25, duration=0.231)
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=28, r_wheel_speed=28, duration=0.297)
