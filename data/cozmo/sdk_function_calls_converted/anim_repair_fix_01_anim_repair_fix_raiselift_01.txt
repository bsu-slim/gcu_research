Original file = data/sdk_converted/anim_repair_fix_01
Animation anim_repair_fix_raiselift_01 clip 8/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.132)
Trigger time ms = 462 --> set_lift_height(63, duration=0.132)
Trigger time ms = 594 --> set_lift_height(53, duration=0.099)
Trigger time ms = 693 --> set_lift_height(57, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> set_head_angle(-19, duration=0.066)
Trigger time ms = 726 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=-105, r_wheel_speed=-105, duration=0.066)
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.165)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-135, r_wheel_speed=-135, duration=0.099)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=46, r_wheel_speed=46, duration=0.165)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=23, r_wheel_speed=23, duration=0.066)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.066)
