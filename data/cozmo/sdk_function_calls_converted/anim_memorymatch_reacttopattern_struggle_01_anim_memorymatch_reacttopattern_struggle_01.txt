Original file = data/sdk_converted/anim_memorymatch_reacttopattern_struggle_01
Animation anim_memorymatch_reacttopattern_struggle_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2805 --> set_lift_height(56, duration=0.165)
Trigger time ms = 2970 --> set_lift_height(0, duration=0.198)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> set_head_angle(-1, duration=0.033)
Trigger time ms = 165 --> set_head_angle(-3, duration=0.033)
Trigger time ms = 198 --> set_head_angle(0, duration=0.099)
Trigger time ms = 297 --> set_head_angle(-2, duration=0.198)
Trigger time ms = 495 --> set_head_angle(-1, duration=0.264)
Trigger time ms = 759 --> set_head_angle(-4, duration=0.066)
Trigger time ms = 1089 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 1155 --> set_head_angle(0, duration=0.132)
Trigger time ms = 2673 --> set_head_angle(13, duration=0.198)
Trigger time ms = 2871 --> set_head_angle(12, duration=0.231)
Trigger time ms = 3102 --> set_head_angle(3, duration=0.066)
Trigger time ms = 3168 --> set_head_angle(11, duration=0.111)
Trigger time ms = 3279 --> set_head_angle(11, duration=0.12)
Trigger time ms = 3399 --> set_head_angle(11, duration=0.33)
Trigger time ms = 3729 --> set_head_angle(2, duration=0.066)
Trigger time ms = 3795 --> set_head_angle(11, duration=0.132)
Trigger time ms = 3927 --> set_head_angle(10, duration=0.264)
Trigger time ms = 4191 --> set_head_angle(8, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 297 --> turn_in_place(degrees(-500))
Trigger time ms = 363 --> turn_in_place(degrees(61))
Trigger time ms = 396 --> turn_in_place(degrees(61))
Trigger time ms = 1188 --> turn_in_place(degrees(81))
Trigger time ms = 2640 --> drive_wheels(l_wheel_speed=-24, r_wheel_speed=-24, duration=0.165)
Trigger time ms = 2805 --> drive_wheels(l_wheel_speed=-5, r_wheel_speed=-5, duration=0.066)
Trigger time ms = 2871 --> drive_wheels(l_wheel_speed=29, r_wheel_speed=29, duration=0.33)
