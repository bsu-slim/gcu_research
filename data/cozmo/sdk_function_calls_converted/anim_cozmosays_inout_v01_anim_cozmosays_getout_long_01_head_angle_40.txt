Original file = data/sdk_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getout_long_01_head_angle_40 clip 24/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 396 --> set_lift_height(38, duration=0.066)
Trigger time ms = 462 --> set_lift_height(0, duration=0.066)
Trigger time ms = 627 --> set_lift_height(42, duration=0.099)
Trigger time ms = 726 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(31, duration=0.066)
Trigger time ms = 99 --> set_head_angle(45, duration=0.099)
Trigger time ms = 462 --> set_head_angle(29, duration=0.066)
Trigger time ms = 528 --> set_head_angle(43, duration=0.066)
Trigger time ms = 594 --> set_head_angle(31, duration=0.066)
Trigger time ms = 660 --> set_head_angle(27, duration=0.033)
Trigger time ms = 693 --> set_head_angle(28, duration=0.033)
Trigger time ms = 726 --> set_head_angle(39, duration=0.099)
Trigger time ms = 825 --> set_head_angle(37, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=280.5, r_wheel_speed=-214.5, duration=0.066)
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=1210.5, r_wheel_speed=-35104.5, duration=0.099)
Trigger time ms = 561 --> drive_wheels(l_wheel_speed=265.0, r_wheel_speed=5035.0, duration=0.033)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-1102.5, r_wheel_speed=3622.5, duration=0.066)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=-493.0, r_wheel_speed=1037.0, duration=0.033)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=-100.0, r_wheel_speed=-1900.0, duration=0.099)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=-98.0, r_wheel_speed=-1358.0, duration=0.033)
