Original file = data/sdk_converted/anim_codelab
Animation anim_codelab_getout_01 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=1.584)
Trigger time ms = 1584 --> set_lift_height(44, duration=0.066)
Trigger time ms = 1650 --> set_lift_height(32, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-11, duration=0.099)
Trigger time ms = 99 --> set_head_angle(7, duration=0.132)
Trigger time ms = 231 --> set_head_angle(-2, duration=0.231)
Trigger time ms = 462 --> set_head_angle(0, duration=0.198)
Trigger time ms = 1551 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 1650 --> set_head_angle(4, duration=0.132)
Trigger time ms = 1782 --> set_head_angle(0, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> turn_in_place(degrees(136))
Trigger time ms = 66 --> turn_in_place(degrees(-192))
Trigger time ms = 165 --> turn_in_place(degrees(68))
Trigger time ms = 297 --> turn_in_place(degrees(-13))
Trigger time ms = 1617 --> turn_in_place(degrees(117))
