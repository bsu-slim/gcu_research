Original file = data/sdk_converted/anim_explorer_inout
Animation anim_explorer_getin_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 264 --> set_lift_height(45, duration=0.132)
Trigger time ms = 396 --> set_lift_height(8, duration=0.099)
Trigger time ms = 495 --> set_lift_height(0, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 297 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 396 --> set_head_angle(23, duration=0.099)
Trigger time ms = 495 --> set_head_angle(24, duration=0.132)
Trigger time ms = 627 --> set_head_angle(23, duration=0.099)
Trigger time ms = 726 --> set_head_angle(25, duration=0.066)
Trigger time ms = 792 --> set_head_angle(-7, duration=0.099)
Trigger time ms = 891 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-92, r_wheel_speed=-92, duration=0.099)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=43, r_wheel_speed=43, duration=0.165)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=-26, r_wheel_speed=-26, duration=0.231)
