Original file = data/sdk_converted/anim_petdetection_04
Animation anim_petdetection_shortreaction_02_head_angle_20 clip 7/8
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> set_lift_height(42, duration=0.099)
Trigger time ms = 231 --> set_lift_height(0, duration=0.099)
Trigger time ms = 1683 --> set_lift_height(40, duration=0.099)
Trigger time ms = 1782 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> set_head_angle(45, duration=0.231)
Trigger time ms = 825 --> set_head_angle(22, duration=0.165)
Trigger time ms = 990 --> set_head_angle(26, duration=0.066)
Trigger time ms = 1518 --> set_head_angle(9, duration=0.066)
Trigger time ms = 1584 --> set_head_angle(45, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(45, duration=0.132)
Trigger time ms = 1782 --> set_head_angle(45, duration=0.231)
Trigger time ms = 2013 --> set_head_angle(-7, duration=0.231)
Trigger time ms = 2244 --> set_head_angle(1, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=29, r_wheel_speed=29, duration=0.396)
Trigger time ms = 1419 --> drive_wheels(l_wheel_speed=-27, r_wheel_speed=-27, duration=0.099)
Trigger time ms = 1518 --> drive_wheels(l_wheel_speed=12, r_wheel_speed=12, duration=0.066)
Trigger time ms = 1584 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.165)
Trigger time ms = 1749 --> drive_wheels(l_wheel_speed=-21, r_wheel_speed=-21, duration=0.165)
