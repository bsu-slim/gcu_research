Original file = data/sdk_converted/anim_energy_getin
Animation anim_energy_getin_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.132)
Trigger time ms = 132 --> set_lift_height(65, duration=0.264)
Trigger time ms = 1353 --> set_lift_height(35, duration=0.198)
Trigger time ms = 1551 --> set_lift_height(32, duration=0.198)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.132)
Trigger time ms = 132 --> set_head_angle(-11, duration=0.132)
Trigger time ms = 264 --> set_head_angle(44, duration=0.264)
Trigger time ms = 528 --> set_head_angle(35, duration=0.099)
Trigger time ms = 1221 --> set_head_angle(32, duration=0.231)
Trigger time ms = 1452 --> set_head_angle(-20, duration=0.231)
Trigger time ms = 1683 --> set_head_angle(-9, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=77, r_wheel_speed=77, duration=0.165)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=-45, r_wheel_speed=-45, duration=0.33)
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=-11, r_wheel_speed=-11, duration=0.33)
