Original file = data/sdk_converted/anim_memorymatch_solo_failhand_player
Animation anim_memorymatch_solo_failhand_player_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 924 --> set_lift_height(0, duration=0.561)
Trigger time ms = 1485 --> set_lift_height(46, duration=0.132)
Trigger time ms = 1617 --> set_lift_height(0, duration=0.165)
Trigger time ms = 1782 --> set_lift_height(0, duration=0.165)
Trigger time ms = 1947 --> set_lift_height(0, duration=0.363)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> set_head_angle(-9, duration=0.066)
Trigger time ms = 132 --> set_head_angle(16, duration=0.165)
Trigger time ms = 297 --> set_head_angle(15, duration=0.429)
Trigger time ms = 924 --> set_head_angle(4, duration=0.066)
Trigger time ms = 990 --> set_head_angle(21, duration=0.099)
Trigger time ms = 1089 --> set_head_angle(8, duration=0.132)
Trigger time ms = 1221 --> set_head_angle(16, duration=0.099)
Trigger time ms = 1320 --> set_head_angle(11, duration=0.066)
Trigger time ms = 1386 --> set_head_angle(6, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(13, duration=0.132)
Trigger time ms = 1584 --> set_head_angle(-8, duration=0.231)
Trigger time ms = 1815 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 1881 --> set_head_angle(0, duration=0.066)
Trigger time ms = 1947 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-58, r_wheel_speed=-58, duration=0.132)
Trigger time ms = 198 --> drive_wheels(l_wheel_speed=45, r_wheel_speed=45, duration=0.198)
Trigger time ms = 924 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.099)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=-18, r_wheel_speed=-18, duration=0.099)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.132)
Trigger time ms = 1254 --> drive_wheels(l_wheel_speed=-18, r_wheel_speed=-18, duration=0.132)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=46, r_wheel_speed=46, duration=0.231)
Trigger time ms = 1617 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.33)
Trigger time ms = 1947 --> drive_wheels(l_wheel_speed=-31, r_wheel_speed=-31, duration=0.231)
