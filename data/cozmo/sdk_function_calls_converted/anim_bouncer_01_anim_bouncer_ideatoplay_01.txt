Original file = data/sdk_converted/anim_bouncer_01
Animation anim_bouncer_ideatoplay_01 clip 1/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 330 --> set_lift_height(46, duration=0.099)
Trigger time ms = 429 --> set_lift_height(32, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 396 --> set_head_angle(-8, duration=0.099)
Trigger time ms = 495 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> turn_in_place(degrees(71))
