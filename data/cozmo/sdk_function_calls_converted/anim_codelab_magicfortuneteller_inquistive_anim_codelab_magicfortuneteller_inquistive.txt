Original file = data/sdk_converted/anim_codelab_magicfortuneteller_inquistive
Animation anim_codelab_magicfortuneteller_inquistive clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.066)
Trigger time ms = 594 --> set_lift_height(54, duration=0.198)
Trigger time ms = 792 --> set_lift_height(50, duration=0.132)
Trigger time ms = 1485 --> set_lift_height(47, duration=0.066)
Trigger time ms = 1551 --> set_lift_height(55, duration=0.099)
Trigger time ms = 1650 --> set_lift_height(57, duration=0.066)
Trigger time ms = 1716 --> set_lift_height(47, duration=0.066)
Trigger time ms = 1782 --> set_lift_height(55, duration=0.099)
Trigger time ms = 1881 --> set_lift_height(57, duration=0.066)
Trigger time ms = 2871 --> set_lift_height(32, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.066)
Trigger time ms = 66 --> set_head_angle(3, duration=0.099)
Trigger time ms = 165 --> set_head_angle(-7, duration=0.165)
Trigger time ms = 330 --> set_head_angle(29, duration=0.528)
Trigger time ms = 858 --> set_head_angle(21, duration=0.132)
Trigger time ms = 1353 --> set_head_angle(17, duration=0.033)
Trigger time ms = 1386 --> set_head_angle(21, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(17, duration=0.033)
Trigger time ms = 2112 --> set_head_angle(20, duration=0.132)
Trigger time ms = 2508 --> set_head_angle(17, duration=0.033)
Trigger time ms = 2541 --> set_head_angle(21, duration=0.198)
Trigger time ms = 2937 --> set_head_angle(9, duration=0.066)
Trigger time ms = 3003 --> set_head_angle(44, duration=0.165)
Trigger time ms = 3168 --> set_head_angle(39, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.165)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-8, r_wheel_speed=-8, duration=0.132)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=24, r_wheel_speed=24, duration=0.165)
Trigger time ms = 2937 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.396)
