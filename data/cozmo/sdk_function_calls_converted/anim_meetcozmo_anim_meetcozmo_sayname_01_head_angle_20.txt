Original file = data/sdk_converted/anim_meetcozmo
Animation anim_meetcozmo_sayname_01_head_angle_20 clip 14/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.099)
Trigger time ms = 330 --> set_lift_height(39, duration=0.066)
Trigger time ms = 396 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(19, duration=0.099)
Trigger time ms = 132 --> set_head_angle(-16, duration=0.165)
Trigger time ms = 297 --> set_head_angle(-9, duration=0.231)
Trigger time ms = 528 --> set_head_angle(-9, duration=0.396)
Trigger time ms = 1881 --> set_head_angle(-15, duration=0.099)
Trigger time ms = 1980 --> set_head_angle(-13, duration=0.132)
Trigger time ms = 2112 --> set_head_angle(-4, duration=0.099)
Trigger time ms = 2211 --> set_head_angle(31, duration=0.231)
Trigger time ms = 3102 --> set_head_angle(26, duration=0.099)
Trigger time ms = 3201 --> set_head_angle(30, duration=0.099)
Trigger time ms = 3300 --> set_head_angle(30, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> turn_in_place(degrees(-87))
Trigger time ms = 1947 --> turn_in_place(degrees(30))
