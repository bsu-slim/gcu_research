Original file = data/sdk_converted/anim_codelab_kitchen
Animation anim_codelab_kitchen_eating_01 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=1.353)
Trigger time ms = 1353 --> set_lift_height(54, duration=1.32)
Trigger time ms = 2673 --> set_lift_height(32, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.165)
Trigger time ms = 165 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 231 --> set_head_angle(0, duration=1.056)
Trigger time ms = 1287 --> set_head_angle(23, duration=1.452)
Trigger time ms = 2739 --> set_head_angle(-7, duration=0.066)
Trigger time ms = 2805 --> set_head_angle(14, duration=0.099)
Trigger time ms = 2904 --> set_head_angle(-3, duration=0.099)
Trigger time ms = 3003 --> set_head_angle(15, duration=0.066)
Trigger time ms = 3069 --> set_head_angle(0, duration=0.099)
Trigger time ms = 3168 --> set_head_angle(10, duration=0.132)
Trigger time ms = 3300 --> set_head_angle(-5, duration=0.132)
Trigger time ms = 3432 --> set_head_angle(5, duration=0.198)
Trigger time ms = 3630 --> set_head_angle(-4, duration=0.165)
Trigger time ms = 3795 --> set_head_angle(3, duration=0.053)
Trigger time ms = 3848 --> set_head_angle(-4, duration=0.056)
Trigger time ms = 3903 --> set_head_angle(3, duration=0.099)
Trigger time ms = 4002 --> set_head_angle(4, duration=0.165)
Trigger time ms = 4167 --> set_head_angle(0, duration=0.123)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=-114.5, r_wheel_speed=-159.5, duration=0.825)
Trigger time ms = 825 --> drive_wheels(l_wheel_speed=-1057.5, r_wheel_speed=-1732.5, duration=1.782)
Trigger time ms = 2739 --> turn_in_place(degrees(182))
Trigger time ms = 2772 --> turn_in_place(degrees(111))
Trigger time ms = 2799 --> turn_in_place(degrees(-128))
Trigger time ms = 2838 --> drive_wheels(l_wheel_speed=-70.5, r_wheel_speed=64.5, duration=0.045)
Trigger time ms = 2882 --> drive_wheels(l_wheel_speed=23.5, r_wheel_speed=-21.5, duration=0.062)
Trigger time ms = 2944 --> turn_in_place(degrees(-10))
Trigger time ms = 3047 --> turn_in_place(degrees(48))
Trigger time ms = 3153 --> turn_in_place(degrees(0))
Trigger time ms = 3311 --> drive_wheels(l_wheel_speed=-0.0, r_wheel_speed=0.0, duration=0.451)
Trigger time ms = 3762 --> turn_in_place(degrees(4))
