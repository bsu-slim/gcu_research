Original file = data/sdk_converted/anim_repair_fix_01
Animation anim_repair_fix_getready_01 clip 12/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.066)
Trigger time ms = 66 --> set_head_angle(0, duration=0.033)
Trigger time ms = 99 --> set_head_angle(7, duration=0.099)
Trigger time ms = 396 --> set_head_angle(0, duration=0.066)
Trigger time ms = 462 --> set_head_angle(15, duration=0.264)
Trigger time ms = 726 --> set_head_angle(0, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=-350.0, r_wheel_speed=550.0, duration=0.132)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.297)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=287237.5, r_wheel_speed=284762.5, duration=0.132)
Trigger time ms = 726 --> turn_in_place(degrees(-287))
