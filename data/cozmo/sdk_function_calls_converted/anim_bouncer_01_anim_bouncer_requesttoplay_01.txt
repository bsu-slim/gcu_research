Original file = data/sdk_converted/anim_bouncer_01
Animation anim_bouncer_requesttoplay_01 clip 2/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=2.145)
Trigger time ms = 2145 --> set_lift_height(46, duration=0.066)
Trigger time ms = 2607 --> set_lift_height(32, duration=0.165)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(15, duration=1.419)
Trigger time ms = 1419 --> set_head_angle(5, duration=0.165)
Trigger time ms = 1584 --> set_head_angle(18, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(14, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> turn_in_place(degrees(-56))
Trigger time ms = 231 --> turn_in_place(degrees(65))
Trigger time ms = 2310 --> drive_wheels(l_wheel_speed=53, r_wheel_speed=53, duration=0.132)
Trigger time ms = 2442 --> drive_wheels(l_wheel_speed=-38, r_wheel_speed=-38, duration=0.132)
Trigger time ms = 2574 --> drive_wheels(l_wheel_speed=-12, r_wheel_speed=-12, duration=0.165)
