Original file = data/sdk_converted/anim_keepaway_losehand_01
Animation anim_keepaway_losehand_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1221 --> set_lift_height(65, duration=0.165)
Trigger time ms = 1419 --> set_lift_height(0, duration=0.033)
Trigger time ms = 2541 --> set_lift_height(92, duration=0.33)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.165)
Trigger time ms = 165 --> set_head_angle(0, duration=0.033)
Trigger time ms = 198 --> set_head_angle(-16, duration=0.132)
Trigger time ms = 396 --> set_head_angle(0, duration=0.099)
Trigger time ms = 891 --> set_head_angle(3, duration=0.198)
Trigger time ms = 1089 --> set_head_angle(0, duration=0.033)
Trigger time ms = 1122 --> set_head_angle(2, duration=0.033)
Trigger time ms = 1155 --> set_head_angle(12, duration=0.132)
Trigger time ms = 1320 --> set_head_angle(0, duration=0.099)
Trigger time ms = 1848 --> set_head_angle(-5, duration=0.066)
Trigger time ms = 1914 --> set_head_angle(0, duration=0.198)
Trigger time ms = 2508 --> set_head_angle(-11, duration=0.066)
Trigger time ms = 2574 --> set_head_angle(-9, duration=0.198)
Trigger time ms = 2772 --> set_head_angle(-16, duration=0.264)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-115, r_wheel_speed=-115, duration=0.132)
Trigger time ms = 1881 --> drive_wheels(l_wheel_speed=-23, r_wheel_speed=-23, duration=0.099)
Trigger time ms = 1980 --> drive_wheels(l_wheel_speed=53, r_wheel_speed=53, duration=0.33)
