Original file = data/sdk_converted/anim_pyramid_reactions_01
Animation anim_pyramid_success_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.066)
Trigger time ms = 66 --> set_lift_height(0, duration=0.33)
Trigger time ms = 396 --> set_lift_height(0, duration=0.033)
Trigger time ms = 990 --> set_lift_height(91, duration=0.363)
Trigger time ms = 1353 --> set_lift_height(83, duration=0.165)
Trigger time ms = 1518 --> set_lift_height(92, duration=0.231)
Trigger time ms = 1749 --> set_lift_height(90, duration=0.132)
Trigger time ms = 1881 --> set_lift_height(46, duration=0.165)
Trigger time ms = 2046 --> set_lift_height(90, duration=0.231)
Trigger time ms = 2277 --> set_lift_height(92, duration=0.264)
Trigger time ms = 2541 --> set_lift_height(0, duration=0.264)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.396)
Trigger time ms = 396 --> set_head_angle(1, duration=0.066)
Trigger time ms = 462 --> set_head_angle(14, duration=0.099)
Trigger time ms = 561 --> set_head_angle(11, duration=0.495)
Trigger time ms = 1056 --> set_head_angle(-1, duration=0.099)
Trigger time ms = 1155 --> set_head_angle(14, duration=0.33)
Trigger time ms = 1485 --> set_head_angle(5, duration=0.099)
Trigger time ms = 1584 --> set_head_angle(20, duration=0.198)
Trigger time ms = 1782 --> set_head_angle(6, duration=0.099)
Trigger time ms = 1881 --> set_head_angle(3, duration=0.132)
Trigger time ms = 2013 --> set_head_angle(19, duration=0.33)
Trigger time ms = 2343 --> set_head_angle(10, duration=0.066)
Trigger time ms = 2409 --> set_head_angle(19, duration=0.066)
Trigger time ms = 2475 --> set_head_angle(24, duration=0.066)
Trigger time ms = 2541 --> set_head_angle(0, duration=0.066)
Trigger time ms = 2607 --> set_head_angle(14, duration=0.198)
Trigger time ms = 3234 --> set_head_angle(5, duration=0.066)
Trigger time ms = 3300 --> set_head_angle(0, duration=0.198)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.198)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=2, r_wheel_speed=2, duration=0.363)
Trigger time ms = 1023 --> drive_wheels(l_wheel_speed=-69, r_wheel_speed=-69, duration=0.165)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=100, r_wheel_speed=100, duration=0.198)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.099)
Trigger time ms = 1485 --> drive_wheels(l_wheel_speed=-21, r_wheel_speed=-21, duration=0.198)
Trigger time ms = 1848 --> turn_in_place(degrees(45))
Trigger time ms = 1914 --> turn_in_place(degrees(-76))
Trigger time ms = 1980 --> turn_in_place(degrees(121))
Trigger time ms = 2046 --> turn_in_place(degrees(-167))
Trigger time ms = 2112 --> turn_in_place(degrees(121))
Trigger time ms = 2211 --> turn_in_place(degrees(-91))
Trigger time ms = 2541 --> drive_wheels(l_wheel_speed=52, r_wheel_speed=52, duration=0.165)
Trigger time ms = 3234 --> drive_wheels(l_wheel_speed=-43, r_wheel_speed=-43, duration=0.066)
Trigger time ms = 3300 --> drive_wheels(l_wheel_speed=8, r_wheel_speed=8, duration=0.165)
Trigger time ms = 3465 --> drive_wheels(l_wheel_speed=33, r_wheel_speed=33, duration=0.165)
