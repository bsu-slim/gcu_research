Original file = data/sdk_converted/anim_workout_mediumenergy_trans_01
Animation anim_workout_mediumenergy_trans_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(53, duration=0.165)
Trigger time ms = 660 --> set_lift_height(50, duration=0.198)
Trigger time ms = 858 --> set_lift_height(63, duration=0.33)
Trigger time ms = 1287 --> set_lift_height(77, duration=0.396)
Trigger time ms = 1683 --> set_lift_height(53, duration=0.363)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.165)
Trigger time ms = 363 --> set_head_angle(5, duration=0.066)
Trigger time ms = 429 --> set_head_angle(-9, duration=0.132)
Trigger time ms = 561 --> set_head_angle(-17, duration=0.297)
Trigger time ms = 990 --> set_head_angle(6, duration=0.165)
Trigger time ms = 1155 --> set_head_angle(16, duration=0.33)
Trigger time ms = 1749 --> set_head_angle(-11, duration=0.132)
Trigger time ms = 1881 --> set_head_angle(9, duration=0.132)
Trigger time ms = 2013 --> set_head_angle(6, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> drive_wheels(l_wheel_speed=-44, r_wheel_speed=-44, duration=0.165)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=-16, r_wheel_speed=-16, duration=0.132)
Trigger time ms = 759 --> drive_wheels(l_wheel_speed=36, r_wheel_speed=36, duration=0.363)
Trigger time ms = 1122 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.594)
Trigger time ms = 1716 --> drive_wheels(l_wheel_speed=-49, r_wheel_speed=-49, duration=0.231)
