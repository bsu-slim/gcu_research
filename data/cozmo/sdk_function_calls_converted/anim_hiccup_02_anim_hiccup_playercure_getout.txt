Original file = data/sdk_converted/anim_hiccup_02
Animation anim_hiccup_playercure_getout clip 10/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 33 --> set_lift_height(0, duration=0.594)
Trigger time ms = 3069 --> set_lift_height(42, duration=0.099)
Trigger time ms = 3168 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(-2, duration=0.594)
Trigger time ms = 627 --> set_head_angle(-2, duration=0.099)
Trigger time ms = 726 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 792 --> set_head_angle(-2, duration=0.099)
Trigger time ms = 891 --> set_head_angle(-1, duration=0.495)
Trigger time ms = 1386 --> set_head_angle(-6, duration=0.066)
Trigger time ms = 1452 --> set_head_angle(1, duration=0.132)
Trigger time ms = 1584 --> set_head_angle(7, duration=0.099)
Trigger time ms = 1683 --> set_head_angle(15, duration=0.231)
Trigger time ms = 1914 --> set_head_angle(25, duration=1.056)
Trigger time ms = 2970 --> set_head_angle(9, duration=0.132)
Trigger time ms = 3102 --> set_head_angle(21, duration=0.132)
Trigger time ms = 4257 --> set_head_angle(16, duration=0.132)
Trigger time ms = 4950 --> set_head_angle(-4, duration=0.132)
Trigger time ms = 5082 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 390 --> turn_in_place(degrees(-282))
Trigger time ms = 825 --> turn_in_place(degrees(333))
Trigger time ms = 1650 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.264)
Trigger time ms = 1914 --> drive_wheels(l_wheel_speed=19, r_wheel_speed=19, duration=0.33)
Trigger time ms = 2541 --> drive_wheels(l_wheel_speed=-20, r_wheel_speed=-20, duration=0.264)
Trigger time ms = 2937 --> drive_wheels(l_wheel_speed=-56, r_wheel_speed=-56, duration=0.066)
Trigger time ms = 3003 --> drive_wheels(l_wheel_speed=-56, r_wheel_speed=-56, duration=0.066)
Trigger time ms = 3069 --> drive_wheels(l_wheel_speed=138, r_wheel_speed=138, duration=0.231)
Trigger time ms = 3300 --> drive_wheels(l_wheel_speed=-71, r_wheel_speed=-71, duration=0.066)
Trigger time ms = 4389 --> turn_in_place(degrees(40))
Trigger time ms = 4983 --> turn_in_place(degrees(97))
