Original file = data/sdk_converted/anim_gamesetup_01
Animation anim_gamesetup_getin_01_head_angle_-20 clip 5/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.099)
Trigger time ms = 99 --> set_lift_height(41, duration=0.066)
Trigger time ms = 165 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(-20, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-25, duration=0.099)
Trigger time ms = 165 --> set_head_angle(-17, duration=0.099)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> drive_wheels(l_wheel_speed=14, r_wheel_speed=14, duration=0.066)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=13, r_wheel_speed=13, duration=0.066)
