Original file = data/sdk_converted/anim_reacttoblock_react_short_01
Animation anim_reacttoblock_react_short_02_head_angle_40 clip 6/8
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 297 --> set_head_angle(45, duration=0.099)
Trigger time ms = 396 --> set_head_angle(41, duration=0.099)
Trigger time ms = 495 --> set_head_angle(38, duration=0.132)
Trigger time ms = 627 --> set_head_angle(40, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-44, r_wheel_speed=-44, duration=0.099)
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=14, r_wheel_speed=14, duration=0.165)
