Original file = data/sdk_converted/anim_hiccup_02
Animation anim_hiccup_playercure_pickupreact clip 6/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.033)
Trigger time ms = 99 --> set_lift_height(44, duration=0.066)
Trigger time ms = 165 --> set_lift_height(3, duration=0.099)
Trigger time ms = 264 --> set_lift_height(0, duration=0.792)
Trigger time ms = 1056 --> set_lift_height(0, duration=0.594)
Trigger time ms = 1650 --> set_lift_height(38, duration=0.099)
Trigger time ms = 1749 --> set_lift_height(0, duration=0.132)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=0.033)
Trigger time ms = 33 --> set_head_angle(-2, duration=0.033)
Trigger time ms = 66 --> set_head_angle(-5, duration=0.033)
Trigger time ms = 99 --> set_head_angle(9, duration=0.132)
Trigger time ms = 231 --> set_head_angle(9, duration=0.099)
Trigger time ms = 330 --> set_head_angle(8, duration=0.099)
Trigger time ms = 429 --> set_head_angle(-5, duration=0.099)
Trigger time ms = 528 --> set_head_angle(-9, duration=0.528)
Trigger time ms = 1056 --> set_head_angle(-9, duration=0.231)
Trigger time ms = 1287 --> set_head_angle(3, duration=0.231)
Trigger time ms = 1518 --> set_head_angle(-9, duration=0.198)
Trigger time ms = 1716 --> set_head_angle(-19, duration=0.132)
Trigger time ms = 1848 --> set_head_angle(-12, duration=0.132)
Trigger time ms = 1980 --> set_head_angle(-16, duration=0.099)
Trigger time ms = 2079 --> set_head_angle(-16, duration=0.033)
Trigger time ms = 2112 --> set_head_angle(-16, duration=0.099)
Trigger time ms = 2211 --> set_head_angle(-15, duration=0.066)
Trigger time ms = 2607 --> set_head_angle(3, duration=0.165)
Trigger time ms = 2772 --> set_head_angle(1, duration=0.231)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=-75, r_wheel_speed=-75, duration=0.264)
