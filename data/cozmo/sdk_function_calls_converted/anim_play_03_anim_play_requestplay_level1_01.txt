Original file = data/sdk_converted/anim_play_03
Animation anim_play_requestplay_level1_01 clip 1/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> set_lift_height(32, duration=0.033)
Trigger time ms = 462 --> set_lift_height(47, duration=0.066)
Trigger time ms = 528 --> set_lift_height(32, duration=0.066)
Trigger time ms = 627 --> set_lift_height(44, duration=0.066)
Trigger time ms = 693 --> set_lift_height(32, duration=0.066)
Trigger time ms = 792 --> set_lift_height(48, duration=0.066)
Trigger time ms = 858 --> set_lift_height(32, duration=0.066)
Trigger time ms = 1452 --> set_lift_height(45, duration=0.066)
Trigger time ms = 1518 --> set_lift_height(32, duration=0.066)
Trigger time ms = 1617 --> set_lift_height(46, duration=0.066)
Trigger time ms = 1683 --> set_lift_height(32, duration=0.066)
Trigger time ms = 1782 --> set_lift_height(49, duration=0.066)
Trigger time ms = 1848 --> set_lift_height(32, duration=0.066)
Trigger time ms = 4158 --> set_lift_height(41, duration=0.066)
Trigger time ms = 4224 --> set_lift_height(32, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> set_head_angle(0, duration=0.033)
Trigger time ms = 132 --> set_head_angle(-3, duration=0.033)
Trigger time ms = 165 --> set_head_angle(-11, duration=0.066)
Trigger time ms = 231 --> set_head_angle(-11, duration=0.033)
Trigger time ms = 264 --> set_head_angle(-12, duration=0.033)
Trigger time ms = 297 --> set_head_angle(-12, duration=0.033)
Trigger time ms = 330 --> set_head_angle(-12, duration=0.033)
Trigger time ms = 363 --> set_head_angle(1, duration=0.132)
Trigger time ms = 495 --> set_head_angle(45, duration=0.132)
Trigger time ms = 627 --> set_head_angle(38, duration=0.066)
Trigger time ms = 693 --> set_head_angle(38, duration=0.594)
Trigger time ms = 1287 --> set_head_angle(32, duration=0.066)
Trigger time ms = 1353 --> set_head_angle(38, duration=0.066)
Trigger time ms = 1419 --> set_head_angle(38, duration=0.33)
Trigger time ms = 1749 --> set_head_angle(19, duration=0.066)
Trigger time ms = 1815 --> set_head_angle(38, duration=0.132)
Trigger time ms = 1947 --> set_head_angle(38, duration=0.693)
Trigger time ms = 3300 --> set_head_angle(16, duration=0.099)
Trigger time ms = 3399 --> set_head_angle(-8, duration=0.066)
Trigger time ms = 3465 --> set_head_angle(-3, duration=0.066)
Trigger time ms = 4092 --> set_head_angle(0, duration=0.066)
Trigger time ms = 4158 --> set_head_angle(4, duration=0.033)
Trigger time ms = 4191 --> set_head_angle(0, duration=0.033)
Trigger time ms = 4224 --> set_head_angle(0, duration=0.033)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.033)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=-91, r_wheel_speed=-91, duration=0.066)
Trigger time ms = 231 --> drive_wheels(l_wheel_speed=-121, r_wheel_speed=-121, duration=0.033)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.033)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=-91, r_wheel_speed=-91, duration=0.033)
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.033)
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=192, r_wheel_speed=192, duration=0.099)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.033)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.033)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.033)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=91, r_wheel_speed=91, duration=0.033)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.033)
Trigger time ms = 759 --> drive_wheels(l_wheel_speed=-91, r_wheel_speed=-91, duration=0.033)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=76, r_wheel_speed=76, duration=0.066)
Trigger time ms = 858 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 1287 --> drive_wheels(l_wheel_speed=45, r_wheel_speed=45, duration=0.066)
Trigger time ms = 1353 --> drive_wheels(l_wheel_speed=-45, r_wheel_speed=-45, duration=0.066)
Trigger time ms = 1485 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 1551 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.066)
Trigger time ms = 1617 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 1683 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.066)
Trigger time ms = 1782 --> drive_wheels(l_wheel_speed=-91, r_wheel_speed=-91, duration=0.033)
Trigger time ms = 1815 --> drive_wheels(l_wheel_speed=-121, r_wheel_speed=-121, duration=0.033)
Trigger time ms = 1848 --> drive_wheels(l_wheel_speed=76, r_wheel_speed=76, duration=0.066)
Trigger time ms = 1914 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.033)
Trigger time ms = 2145 --> turn_in_place(degrees(30))
Trigger time ms = 2178 --> turn_in_place(degrees(30))
Trigger time ms = 2211 --> turn_in_place(degrees(-121))
Trigger time ms = 2244 --> turn_in_place(degrees(91))
Trigger time ms = 2310 --> turn_in_place(degrees(-106))
Trigger time ms = 2376 --> turn_in_place(degrees(76))
Trigger time ms = 2442 --> turn_in_place(degrees(-61))
Trigger time ms = 2508 --> turn_in_place(degrees(91))
Trigger time ms = 2574 --> turn_in_place(degrees(-30))
Trigger time ms = 2640 --> turn_in_place(degrees(30))
Trigger time ms = 2706 --> turn_in_place(degrees(-30))
Trigger time ms = 3300 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.066)
Trigger time ms = 3366 --> drive_wheels(l_wheel_speed=-71, r_wheel_speed=-71, duration=0.099)
Trigger time ms = 3465 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.033)
Trigger time ms = 3498 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 3531 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
Trigger time ms = 4191 --> turn_in_place(degrees(91))
