Original file = data/sdk_converted/anim_gotosleep_getout_01
Animation anim_gotosleep_getout_02 clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2211 --> set_lift_height(32, duration=0.231)
Trigger time ms = 2442 --> set_lift_height(45, duration=1.353)
Trigger time ms = 3795 --> set_lift_height(0, duration=0.462)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> set_head_angle(-11, duration=0.462)
Trigger time ms = 627 --> set_head_angle(-10, duration=0.099)
Trigger time ms = 726 --> set_head_angle(-7, duration=0.528)
Trigger time ms = 1254 --> set_head_angle(-4, duration=0.396)
Trigger time ms = 1650 --> set_head_angle(-5, duration=0.264)
Trigger time ms = 1914 --> set_head_angle(2, duration=0.726)
Trigger time ms = 2640 --> set_head_angle(15, duration=0.33)
Trigger time ms = 2970 --> set_head_angle(20, duration=0.099)
Trigger time ms = 3069 --> set_head_angle(16, duration=0.099)
Trigger time ms = 3168 --> set_head_angle(22, duration=0.066)
Trigger time ms = 3234 --> set_head_angle(19, duration=0.099)
Trigger time ms = 3333 --> set_head_angle(24, duration=0.066)
Trigger time ms = 3399 --> set_head_angle(21, duration=0.066)
Trigger time ms = 3465 --> set_head_angle(24, duration=0.033)
Trigger time ms = 3498 --> set_head_angle(21, duration=0.099)
Trigger time ms = 3597 --> set_head_angle(25, duration=0.066)
Trigger time ms = 3663 --> set_head_angle(0, duration=0.627)
Trigger time ms = 4290 --> set_head_angle(-1, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1815 --> drive_wheels(l_wheel_speed=18, r_wheel_speed=18, duration=0.495)
Trigger time ms = 2310 --> drive_wheels(l_wheel_speed=-67, r_wheel_speed=-67, duration=0.429)
Trigger time ms = 2739 --> drive_wheels(l_wheel_speed=-24, r_wheel_speed=-24, duration=0.33)
Trigger time ms = 3069 --> drive_wheels(l_wheel_speed=21, r_wheel_speed=21, duration=0.099)
Trigger time ms = 3168 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 3234 --> drive_wheels(l_wheel_speed=20, r_wheel_speed=20, duration=0.066)
Trigger time ms = 3300 --> drive_wheels(l_wheel_speed=-44, r_wheel_speed=-44, duration=0.099)
Trigger time ms = 3399 --> drive_wheels(l_wheel_speed=35, r_wheel_speed=35, duration=0.066)
Trigger time ms = 3465 --> drive_wheels(l_wheel_speed=-26, r_wheel_speed=-26, duration=0.066)
Trigger time ms = 3531 --> drive_wheels(l_wheel_speed=35, r_wheel_speed=35, duration=0.066)
Trigger time ms = 3597 --> drive_wheels(l_wheel_speed=-44, r_wheel_speed=-44, duration=0.066)
Trigger time ms = 3663 --> drive_wheels(l_wheel_speed=58, r_wheel_speed=58, duration=0.066)
Trigger time ms = 3729 --> drive_wheels(l_wheel_speed=-28, r_wheel_speed=-28, duration=0.099)
Trigger time ms = 3828 --> drive_wheels(l_wheel_speed=6, r_wheel_speed=6, duration=0.264)
Trigger time ms = 4092 --> drive_wheels(l_wheel_speed=34, r_wheel_speed=34, duration=0.33)
