Original file = data/sdk_converted/anim_cozmosays_badword_01
Animation anim_cozmosays_badword_01_head_angle_20 clip 4/8
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 165 --> set_lift_height(48, duration=0.066)
Trigger time ms = 231 --> set_lift_height(0, duration=0.066)
Trigger time ms = 1881 --> set_lift_height(7, duration=0.033)
Trigger time ms = 1914 --> set_lift_height(22, duration=0.033)
Trigger time ms = 1947 --> set_lift_height(45, duration=0.066)
Trigger time ms = 2013 --> set_lift_height(0, duration=0.066)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(9, duration=0.066)
Trigger time ms = 99 --> set_head_angle(28, duration=0.099)
Trigger time ms = 198 --> set_head_angle(29, duration=0.066)
Trigger time ms = 264 --> set_head_angle(29, duration=0.132)
Trigger time ms = 462 --> set_head_angle(29, duration=0.627)
Trigger time ms = 1089 --> set_head_angle(28, duration=0.033)
Trigger time ms = 1122 --> set_head_angle(26, duration=0.033)
Trigger time ms = 1155 --> set_head_angle(25, duration=0.033)
Trigger time ms = 1452 --> set_head_angle(24, duration=0.033)
Trigger time ms = 1485 --> set_head_angle(21, duration=0.066)
Trigger time ms = 1551 --> set_head_angle(19, duration=0.033)
Trigger time ms = 1584 --> set_head_angle(16, duration=0.066)
Trigger time ms = 1650 --> set_head_angle(13, duration=0.099)
Trigger time ms = 1749 --> set_head_angle(11, duration=0.099)
Trigger time ms = 1848 --> set_head_angle(28, duration=0.066)
Trigger time ms = 1914 --> set_head_angle(30, duration=0.033)
Trigger time ms = 1947 --> set_head_angle(31, duration=0.033)
Trigger time ms = 1980 --> set_head_angle(20, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=70, r_wheel_speed=70, duration=0.132)
Trigger time ms = 297 --> drive_wheels(l_wheel_speed=7, r_wheel_speed=7, duration=0.066)
Trigger time ms = 363 --> drive_wheels(l_wheel_speed=1, r_wheel_speed=1, duration=0.066)
Trigger time ms = 1056 --> turn_in_place(degrees(-61))
Trigger time ms = 1122 --> turn_in_place(degrees(-61))
Trigger time ms = 1155 --> turn_in_place(degrees(-30))
Trigger time ms = 1188 --> turn_in_place(degrees(1030))
Trigger time ms = 1221 --> turn_in_place(degrees(1000))
Trigger time ms = 1254 --> turn_in_place(degrees(-970))
Trigger time ms = 1287 --> turn_in_place(degrees(-273))
Trigger time ms = 1353 --> turn_in_place(degrees(1000))
Trigger time ms = 1386 --> turn_in_place(degrees(379))
Trigger time ms = 1452 --> turn_in_place(degrees(121))
Trigger time ms = 1485 --> turn_in_place(degrees(-879))
Trigger time ms = 1551 --> turn_in_place(degrees(-91))
Trigger time ms = 1584 --> turn_in_place(degrees(76))
Trigger time ms = 1650 --> turn_in_place(degrees(283))
Trigger time ms = 1749 --> turn_in_place(degrees(172))
Trigger time ms = 1848 --> turn_in_place(degrees(-530))
Trigger time ms = 1980 --> drive_wheels(l_wheel_speed=-74, r_wheel_speed=-74, duration=0.066)
