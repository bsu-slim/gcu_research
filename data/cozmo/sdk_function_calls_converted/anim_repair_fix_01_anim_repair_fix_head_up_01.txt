Original file = data/sdk_converted/anim_repair_fix_01
Animation anim_repair_fix_head_up_01 clip 4/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> set_head_angle(0, duration=0.033)
Trigger time ms = 66 --> set_head_angle(28, duration=0.132)
Trigger time ms = 198 --> set_head_angle(0, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> drive_wheels(l_wheel_speed=15, r_wheel_speed=15, duration=0.066)
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.066)
Trigger time ms = 132 --> drive_wheels(l_wheel_speed=-53, r_wheel_speed=-53, duration=0.132)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=23, r_wheel_speed=23, duration=0.132)
Trigger time ms = 396 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.033)
