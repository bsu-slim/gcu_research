Original file = data/sdk_converted/anim_meetcozmo
Animation anim_meetcozmo_lookface_getout_head_angle_20 clip 29/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(0, duration=0.066)
Trigger time ms = 132 --> set_lift_height(41, duration=0.099)
Trigger time ms = 231 --> set_lift_height(0, duration=0.099)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(21, duration=0.033)
Trigger time ms = 33 --> set_head_angle(17, duration=0.066)
Trigger time ms = 99 --> set_head_angle(23, duration=0.066)
Trigger time ms = 165 --> set_head_angle(16, duration=0.165)
Trigger time ms = 330 --> set_head_angle(19, duration=0.066)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> drive_wheels(l_wheel_speed=-52, r_wheel_speed=-52, duration=0.099)
Trigger time ms = 165 --> drive_wheels(l_wheel_speed=55, r_wheel_speed=55, duration=0.099)
Trigger time ms = 264 --> drive_wheels(l_wheel_speed=-3, r_wheel_speed=-3, duration=0.132)
