Original file = data/sdk_converted/anim_vc_alrighty
Animation anim_vc_alrighty_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> set_lift_height(32, duration=0.033)
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(8, duration=0.066)
Trigger time ms = 66 --> set_head_angle(-3, duration=0.033)
Trigger time ms = 99 --> set_head_angle(1, duration=0.033)
Trigger time ms = 132 --> set_head_angle(28, duration=0.132)
Trigger time ms = 396 --> set_head_angle(45, duration=0.033)
Trigger time ms = 429 --> set_head_angle(-11, duration=0.099)
Trigger time ms = 528 --> set_head_angle(18, duration=0.165)
Trigger time ms = 693 --> set_head_angle(18, duration=0.462)
Trigger time ms = 1155 --> set_head_angle(-7, duration=0.165)
Trigger time ms = 1320 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> drive_wheels(l_wheel_speed=-40, r_wheel_speed=-40, duration=0.099)
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=111, r_wheel_speed=111, duration=0.099)
Trigger time ms = 528 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.066)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=-61, r_wheel_speed=-61, duration=0.066)
Trigger time ms = 1188 --> drive_wheels(l_wheel_speed=-38, r_wheel_speed=-38, duration=0.132)
