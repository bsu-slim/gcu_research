Original file = data/sdk_converted/anim_keepaway_pounce_01
Animation anim_keepaway_pounce_02 clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 495 --> set_lift_height(90, duration=0.066)
Trigger time ms = 561 --> set_lift_height(90, duration=0.033)
Trigger time ms = 594 --> set_lift_height(92, duration=0.033)
Trigger time ms = 627 --> set_lift_height(90, duration=0.033)
Trigger time ms = 660 --> set_lift_height(91, duration=0.033)
Trigger time ms = 693 --> set_lift_height(90, duration=0.033)
Trigger time ms = 726 --> set_lift_height(91, duration=0.033)
Trigger time ms = 759 --> set_lift_height(88, duration=0.033)
Trigger time ms = 792 --> set_lift_height(86, duration=0.033)
Trigger time ms = 825 --> set_lift_height(88, duration=0.033)
Trigger time ms = 858 --> set_lift_height(86, duration=0.033)
Trigger time ms = 891 --> set_lift_height(88, duration=0.033)
Trigger time ms = 924 --> set_lift_height(85, duration=0.033)
Trigger time ms = 957 --> set_lift_height(88, duration=0.033)
Trigger time ms = 990 --> set_lift_height(92, duration=0.066)
Trigger time ms = 1056 --> set_lift_height(57, duration=0.066)
Trigger time ms = 1155 --> set_lift_height(79, duration=0.066)
Trigger time ms = 1815 --> set_lift_height(88, duration=0.165)
Trigger time ms = 1980 --> set_lift_height(0, duration=0.198)
---------------------------------------
Process Head angle animations
---------------------------------------
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> drive_wheels(l_wheel_speed=-30, r_wheel_speed=-30, duration=0.066)
Trigger time ms = 495 --> drive_wheels(l_wheel_speed=30, r_wheel_speed=30, duration=0.066)
Trigger time ms = 561 --> drive_wheels(l_wheel_speed=-34, r_wheel_speed=-34, duration=0.033)
Trigger time ms = 594 --> drive_wheels(l_wheel_speed=32, r_wheel_speed=32, duration=0.033)
Trigger time ms = 627 --> drive_wheels(l_wheel_speed=-31, r_wheel_speed=-31, duration=0.033)
Trigger time ms = 660 --> drive_wheels(l_wheel_speed=29, r_wheel_speed=29, duration=0.033)
Trigger time ms = 693 --> drive_wheels(l_wheel_speed=-66, r_wheel_speed=-66, duration=0.033)
Trigger time ms = 726 --> drive_wheels(l_wheel_speed=61, r_wheel_speed=61, duration=0.033)
Trigger time ms = 759 --> drive_wheels(l_wheel_speed=-120, r_wheel_speed=-120, duration=0.033)
Trigger time ms = 792 --> drive_wheels(l_wheel_speed=10211, r_wheel_speed=10211, duration=0.231)
Trigger time ms = 1089 --> drive_wheels(l_wheel_speed=-693, r_wheel_speed=-693, duration=0.297)
Trigger time ms = 1386 --> drive_wheels(l_wheel_speed=78, r_wheel_speed=78, duration=0.198)
Trigger time ms = 1584 --> drive_wheels(l_wheel_speed=-13, r_wheel_speed=-13, duration=0.198)
