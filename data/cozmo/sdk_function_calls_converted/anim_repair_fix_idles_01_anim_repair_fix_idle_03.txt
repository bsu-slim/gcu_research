Original file = data/sdk_converted/anim_repair_fix_idles_01
Animation anim_repair_fix_idle_03 clip 3/6
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> set_head_angle(0, duration=1.221)
Trigger time ms = 1221 --> set_head_angle(9, duration=0.132)
Trigger time ms = 3465 --> set_head_angle(0, duration=0.099)
Trigger time ms = 3564 --> set_head_angle(9, duration=0.099)
Trigger time ms = 4653 --> set_head_angle(0, duration=0.132)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1287 --> turn_in_place(degrees(-55))
Trigger time ms = 3531 --> turn_in_place(degrees(34))
