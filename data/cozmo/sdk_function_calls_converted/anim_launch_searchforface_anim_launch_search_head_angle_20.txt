Original file = data/sdk_converted/anim_launch_searchforface
Animation anim_launch_search_head_angle_20 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> set_head_angle(18, duration=0.132)
Trigger time ms = 495 --> set_head_angle(26, duration=0.165)
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> turn_in_place(degrees(23))
Trigger time ms = 495 --> turn_in_place(degrees(-40))
