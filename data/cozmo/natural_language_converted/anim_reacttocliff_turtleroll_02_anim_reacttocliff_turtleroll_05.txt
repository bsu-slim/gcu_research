Original file = ../../data/cozmo/natural_language_converted/anim_reacttocliff_turtleroll_02
Animation anim_reacttocliff_turtleroll_05 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 462 --> raise arms fully
Trigger time ms = 1188 --> raise arms fully
Trigger time ms = 3465 --> raise arms fully
Trigger time ms = 4455 --> raise arms fully
Trigger time ms = 4686 --> raise arms fully
Trigger time ms = 4884 --> raise arms fully
Trigger time ms = 5082 --> raise arms fully
Trigger time ms = 5280 --> raise arms fully
Trigger time ms = 5478 --> raise arms fully
Trigger time ms = 5676 --> raise arms fully
Trigger time ms = 5841 --> raise arms fully
Trigger time ms = 6006 --> raise arms fully
Trigger time ms = 6171 --> raise arms fully
Trigger time ms = 6336 --> raise arms fully
Trigger time ms = 6534 --> raise arms fully
Trigger time ms = 6699 --> raise arms fully
Trigger time ms = 6831 --> raise arms fully
Trigger time ms = 7062 --> raise arms fully
Trigger time ms = 7260 --> raise arms fully
Trigger time ms = 7524 --> lower arms fully
Trigger time ms = 7854 --> lower arms a bit
Trigger time ms = 8019 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 330 --> Look all the way down
Trigger time ms = 396 --> Look all the way up
Trigger time ms = 1089 --> Look all the way down
Trigger time ms = 1782 --> Look all the way down
Trigger time ms = 1815 --> Look all the way down
Trigger time ms = 3069 --> Look to the front
Trigger time ms = 3300 --> Look all the way down
Trigger time ms = 3366 --> Look down a little bit
Trigger time ms = 3762 --> Look all the way up
Trigger time ms = 4125 --> Look all the way up
Trigger time ms = 4389 --> Look down a little bit
Trigger time ms = 4719 --> Look to the front
Trigger time ms = 4917 --> Look down a little bit
Trigger time ms = 5115 --> Look to the front
Trigger time ms = 5313 --> Look down a little bit
Trigger time ms = 5511 --> Look to the front
Trigger time ms = 5709 --> Look down a little bit
Trigger time ms = 5874 --> Look to the front
Trigger time ms = 6039 --> Look down a little bit
Trigger time ms = 6204 --> Look to the front
Trigger time ms = 6369 --> Look down a little bit
Trigger time ms = 6567 --> Look to the front
Trigger time ms = 6732 --> Look down a little bit
Trigger time ms = 6864 --> Look to the front
Trigger time ms = 7095 --> Look down a little bit
Trigger time ms = 7293 --> Look to the front
Trigger time ms = 7524 --> Look down a little bit
Trigger time ms = 7590 --> Look all the way up
Trigger time ms = 7755 --> Look all the way up
Trigger time ms = 7821 --> Look up a little bit
Trigger time ms = 8052 --> Look to the front
Trigger time ms = 8547 --> Look all the way up
Trigger time ms = 8778 --> Look down a little bit
Trigger time ms = 8844 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 4554 --> walk forward turning fast
Trigger time ms = 4950 --> walk forward turning fast
Trigger time ms = 5346 --> walk forward turning fast
Trigger time ms = 5742 --> walk forward turning fast
Trigger time ms = 6072 --> walk forward turning fast
Trigger time ms = 6402 --> walk forward turning fast
Trigger time ms = 6765 --> walk forward turning fast
Trigger time ms = 7095 --> walk forward turning fast
Trigger time ms = 7557 --> walk forward  fast
Trigger time ms = 7656 --> walk back  fast
Trigger time ms = 7953 --> walk forward  medium fast
Trigger time ms = 8085 --> walk back  medium slow
Trigger time ms = 8184 --> walk forward  medium slow
Trigger time ms = 8316 --> walk back  slow
