Original file = ../../data/cozmo/natural_language_converted/anim_bouncer_01
Animation anim_bouncer_intoscore_01 clip 6/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 132 --> Look all the way down
Trigger time ms = 1353 --> Look down a little bit
Trigger time ms = 2046 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> walk forward  medium fast
Trigger time ms = 132 --> walk back  fast
Trigger time ms = 297 --> walk forward  slow
