Original file = ../../data/cozmo/natural_language_converted/vs_speedtap_gamereact_02
Animation anim_speedtap_losegame_intensity02_02 clip 1/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 99 --> raise arms fully
Trigger time ms = 165 --> raise arms fully
Trigger time ms = 264 --> lower arms a bit
Trigger time ms = 4587 --> arms to front
Trigger time ms = 4686 --> lower arms a bit
Trigger time ms = 6864 --> raise arms fully
Trigger time ms = 6963 --> lower arms a bit
Trigger time ms = 7458 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 99 --> Look all the way down
Trigger time ms = 198 --> Look up a little bit
Trigger time ms = 264 --> Look all the way down
Trigger time ms = 330 --> Look all the way up
Trigger time ms = 561 --> Look all the way up
Trigger time ms = 1221 --> Look all the way up
Trigger time ms = 1287 --> Look all the way up
Trigger time ms = 1353 --> Look all the way up
Trigger time ms = 1419 --> Look all the way up
Trigger time ms = 1551 --> Look all the way up
Trigger time ms = 1749 --> Look down a little bit
Trigger time ms = 1980 --> Look down a little bit
Trigger time ms = 2508 --> Look all the way up
Trigger time ms = 2706 --> Look all the way up
Trigger time ms = 3399 --> Look up a little bit
Trigger time ms = 3696 --> Look all the way up
Trigger time ms = 4158 --> Look up a little bit
Trigger time ms = 4587 --> Look all the way up
Trigger time ms = 5016 --> Look up a little bit
Trigger time ms = 5313 --> Look all the way up
Trigger time ms = 5709 --> Look up a little bit
Trigger time ms = 6303 --> Look up a little bit
Trigger time ms = 6369 --> Look up a little bit
Trigger time ms = 6501 --> Look up a little bit
Trigger time ms = 6732 --> Look to the front
Trigger time ms = 6831 --> Look up a little bit
Trigger time ms = 6930 --> Look to the front
Trigger time ms = 7062 --> Look to the front
Trigger time ms = 7458 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> walk back  fast
Trigger time ms = 330 --> walk forward  slow
Trigger time ms = 1782 --> walk back  medium slow
Trigger time ms = 2508 --> walk forward  medium slow
Trigger time ms = 3135 --> walk back turning slow
Trigger time ms = 6732 --> walk back turning medium fast
Trigger time ms = 7029 --> walk forward turning medium slow
