Original file = ../../data/cozmo/natural_language_converted/anim_energy_cubereact_02
Animation anim_energy_cubeshakelvl1_02 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 132 --> lower arms a bit
Trigger time ms = 198 --> lower arms a bit
Trigger time ms = 2904 --> arms to front
Trigger time ms = 3003 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 99 --> Look all the way up
Trigger time ms = 297 --> Look all the way up
Trigger time ms = 495 --> Look all the way up
Trigger time ms = 693 --> Look all the way up
Trigger time ms = 858 --> Look all the way up
Trigger time ms = 1056 --> Look all the way up
Trigger time ms = 1287 --> Look all the way up
Trigger time ms = 1485 --> Look all the way up
Trigger time ms = 1650 --> Look all the way up
Trigger time ms = 1848 --> Look all the way up
Trigger time ms = 2013 --> Look all the way up
Trigger time ms = 2145 --> Look all the way up
Trigger time ms = 2277 --> Look all the way up
Trigger time ms = 2376 --> Look all the way up
Trigger time ms = 2475 --> Look all the way up
Trigger time ms = 2574 --> Look all the way up
Trigger time ms = 2673 --> Look up a little bit
Trigger time ms = 2772 --> Look all the way down
Trigger time ms = 2838 --> Look all the way up
Trigger time ms = 2970 --> Look down a little bit
Trigger time ms = 3102 --> Look up a little bit
Trigger time ms = 3267 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk back turning slow
Trigger time ms = 264 --> walk forward turning slow
Trigger time ms = 396 --> walk forward turning slow
Trigger time ms = 528 --> turn forward in place medium slow
Trigger time ms = 693 --> turn back in place medium slow
Trigger time ms = 858 --> turn forward in place medium slow
Trigger time ms = 1023 --> turn back in place medium slow
Trigger time ms = 1188 --> turn forward in place medium slow
Trigger time ms = 1386 --> turn back in place medium slow
Trigger time ms = 1551 --> turn forward in place medium slow
Trigger time ms = 1716 --> turn back in place medium slow
Trigger time ms = 1914 --> turn forward in place medium slow
Trigger time ms = 2145 --> turn back in place medium slow
Trigger time ms = 2376 --> turn forward in place medium slow
Trigger time ms = 2574 --> turn back in place medium fast
Trigger time ms = 2673 --> walk back turning fast
Trigger time ms = 2739 --> walk forward  medium fast
Trigger time ms = 2838 --> walk forward turning medium slow
Trigger time ms = 2937 --> walk forward turning medium slow
Trigger time ms = 3069 --> walk back turning medium slow
