Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_solo_failhand_player
Animation anim_memorymatch_solo_failhand_player_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 924 --> lower arms fully
Trigger time ms = 1485 --> arms to front
Trigger time ms = 1617 --> lower arms fully
Trigger time ms = 1782 --> lower arms fully
Trigger time ms = 1947 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look down a little bit
Trigger time ms = 132 --> Look up a little bit
Trigger time ms = 297 --> Look up a little bit
Trigger time ms = 924 --> Look to the front
Trigger time ms = 990 --> Look all the way up
Trigger time ms = 1089 --> Look up a little bit
Trigger time ms = 1221 --> Look up a little bit
Trigger time ms = 1320 --> Look up a little bit
Trigger time ms = 1386 --> Look up a little bit
Trigger time ms = 1452 --> Look up a little bit
Trigger time ms = 1584 --> Look down a little bit
Trigger time ms = 1815 --> Look down a little bit
Trigger time ms = 1881 --> Look to the front
Trigger time ms = 1947 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> walk back  medium slow
Trigger time ms = 198 --> walk forward  medium slow
Trigger time ms = 924 --> walk forward  slow
Trigger time ms = 1023 --> walk back  slow
Trigger time ms = 1122 --> walk forward  slow
Trigger time ms = 1254 --> walk back  slow
Trigger time ms = 1386 --> walk forward  medium slow
Trigger time ms = 1617 --> walk back  slow
Trigger time ms = 1947 --> walk back  medium slow
