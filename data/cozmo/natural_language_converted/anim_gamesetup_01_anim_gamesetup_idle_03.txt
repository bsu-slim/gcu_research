Original file = ../../data/cozmo/natural_language_converted/anim_gamesetup_01
Animation anim_gamesetup_idle_03 clip 3/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 660 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1155 --> walk forward  medium slow
Trigger time ms = 1221 --> walk back  medium slow
Trigger time ms = 1287 --> walk back  slow
