Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_point_v02
Animation anim_memorymatch_pointcenter_01 clip 1/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> raise arms fully
Trigger time ms = 165 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 99 --> Look up a little bit
Trigger time ms = 165 --> Look down a little bit
Trigger time ms = 231 --> Look down a little bit
Trigger time ms = 330 --> Look down a little bit
Trigger time ms = 429 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk forward  fast
Trigger time ms = 198 --> walk back  medium fast
