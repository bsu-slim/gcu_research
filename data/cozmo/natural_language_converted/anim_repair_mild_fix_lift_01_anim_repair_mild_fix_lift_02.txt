Original file = ../../data/cozmo/natural_language_converted/anim_repair_mild_fix_lift_01
Animation anim_repair_mild_fix_lift_02 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 264 --> raise arms fully
Trigger time ms = 396 --> arms to front
Trigger time ms = 495 --> arms to front
Trigger time ms = 1056 --> arms to front
Trigger time ms = 1188 --> raise arms fully
Trigger time ms = 1782 --> raise arms fully
Trigger time ms = 1947 --> raise arms fully
Trigger time ms = 2640 --> lower arms a bit
Trigger time ms = 2871 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 297 --> Look down a little bit
Trigger time ms = 396 --> Look to the front
Trigger time ms = 495 --> Look down a little bit
Trigger time ms = 627 --> Look down a little bit
Trigger time ms = 792 --> Look down a little bit
Trigger time ms = 990 --> Look down a little bit
Trigger time ms = 1056 --> Look all the way down
Trigger time ms = 1122 --> Look to the front
Trigger time ms = 1320 --> Look up a little bit
Trigger time ms = 1881 --> Look down a little bit
Trigger time ms = 2442 --> Look to the front
Trigger time ms = 2607 --> Look all the way down
Trigger time ms = 2739 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> walk back turning medium fast
Trigger time ms = 462 --> walk forward turning medium slow
Trigger time ms = 1254 --> walk back turning slow
Trigger time ms = 1881 --> walk back turning slow
Trigger time ms = 2541 --> walk back turning medium slow
Trigger time ms = 2607 --> walk back turning slow
Trigger time ms = 2673 --> walk forward turning slow
Trigger time ms = 2739 --> walk forward turning medium slow
Trigger time ms = 2805 --> walk forward turning slow
Trigger time ms = 2871 --> walk forward turning medium fast
Trigger time ms = 2937 --> walk forward turning slow
