Original file = ../../data/cozmo/natural_language_converted/anim_cozmosings_getin_01
Animation anim_cozmosings_getin_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 495 --> Look down a little bit
Trigger time ms = 726 --> Look up a little bit
Trigger time ms = 924 --> Look up a little bit
Trigger time ms = 1683 --> Look up a little bit
Trigger time ms = 1815 --> Look to the front
Trigger time ms = 2409 --> Look up a little bit
Trigger time ms = 2607 --> Look down a little bit
Trigger time ms = 3036 --> Look to the front
Trigger time ms = 3168 --> Look to the front
Trigger time ms = 3564 --> Look up a little bit
Trigger time ms = 3663 --> Look all the way down
Trigger time ms = 3828 --> Look all the way down
Trigger time ms = 4356 --> Look up a little bit
Trigger time ms = 4554 --> Look down a little bit
Trigger time ms = 4884 --> Look to the front
Trigger time ms = 5181 --> Look down a little bit
Trigger time ms = 5280 --> Look all the way down
Trigger time ms = 5742 --> Look to the front
Trigger time ms = 5874 --> Look down a little bit
Trigger time ms = 6270 --> Look down a little bit
Trigger time ms = 6435 --> Look to the front
Trigger time ms = 6633 --> Look up a little bit
Trigger time ms = 6996 --> Look down a little bit
Trigger time ms = 7128 --> Look all the way down
Trigger time ms = 7425 --> Look up a little bit
Trigger time ms = 7788 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 528 --> walk back  medium slow
Trigger time ms = 693 --> walk forward  medium fast
Trigger time ms = 792 --> walk forward  slow
Trigger time ms = 1650 --> turn forward in place medium slow
Trigger time ms = 1749 --> turn back in place medium fast
Trigger time ms = 1881 --> turn back in place medium slow
Trigger time ms = 3564 --> walk back  medium slow
Trigger time ms = 4356 --> walk forward  medium slow
Trigger time ms = 4950 --> turn forward in place medium slow
Trigger time ms = 5082 --> turn forward in place slow
Trigger time ms = 6336 --> turn forward in place medium fast
Trigger time ms = 6534 --> turn forward in place medium slow
Trigger time ms = 6996 --> walk back  medium slow
Trigger time ms = 7194 --> walk back  slow
Trigger time ms = 7293 --> walk forward  medium fast
Trigger time ms = 7458 --> walk forward  medium fast
Trigger time ms = 7623 --> walk forward  medium fast
Trigger time ms = 7656 --> walk forward  medium slow
Trigger time ms = 7722 --> walk forward  medium slow
Trigger time ms = 7788 --> walk forward  medium slow
