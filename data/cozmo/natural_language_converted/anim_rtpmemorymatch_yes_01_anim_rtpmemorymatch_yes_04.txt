Original file = ../../data/cozmo/natural_language_converted/anim_rtpmemorymatch_yes_01
Animation anim_rtpmemorymatch_yes_04 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 231 --> arms to front
Trigger time ms = 297 --> lower arms fully
Trigger time ms = 462 --> lower arms fully
Trigger time ms = 1111 --> arms to front
Trigger time ms = 1185 --> lower arms fully
Trigger time ms = 1262 --> arms to front
Trigger time ms = 1339 --> lower arms fully
Trigger time ms = 1418 --> arms to front
Trigger time ms = 1496 --> lower arms fully
Trigger time ms = 1571 --> arms to front
Trigger time ms = 1639 --> lower arms fully
Trigger time ms = 1709 --> lower arms fully
Trigger time ms = 1881 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look up a little bit
Trigger time ms = 198 --> Look all the way up
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 561 --> Look all the way up
Trigger time ms = 759 --> Look all the way up
Trigger time ms = 858 --> Look all the way up
Trigger time ms = 990 --> Look all the way up
Trigger time ms = 1170 --> Look all the way up
Trigger time ms = 1248 --> Look all the way up
Trigger time ms = 1324 --> Look all the way up
Trigger time ms = 1402 --> Look all the way up
Trigger time ms = 1481 --> Look all the way up
Trigger time ms = 1559 --> Look all the way up
Trigger time ms = 1635 --> Look all the way up
Trigger time ms = 1815 --> Look to the front
Trigger time ms = 2013 --> Look to the front
Trigger time ms = 2244 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1023 --> walk back  medium slow
Trigger time ms = 1076 --> walk forward  medium slow
Trigger time ms = 1134 --> walk back  medium slow
Trigger time ms = 1189 --> walk forward  medium slow
Trigger time ms = 1246 --> walk back  medium slow
Trigger time ms = 1303 --> walk forward  medium slow
Trigger time ms = 1358 --> walk back  medium slow
Trigger time ms = 1416 --> walk forward  medium slow
Trigger time ms = 1473 --> walk back  medium slow
Trigger time ms = 1528 --> walk forward  medium slow
Trigger time ms = 1585 --> walk back  medium slow
Trigger time ms = 1643 --> walk forward  medium slow
Trigger time ms = 1700 --> walk forward  slow
