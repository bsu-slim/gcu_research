Original file = ../../data/cozmo/natural_language_converted/anim_hiking_driving_02
Animation anim_hiking_driving_start_03 clip 1/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1221 --> lower arms fully
Trigger time ms = 2277 --> arms to front
Trigger time ms = 2343 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 165 --> Look down a little bit
Trigger time ms = 1320 --> Look all the way down
Trigger time ms = 1353 --> Look up a little bit
Trigger time ms = 1485 --> Look up a little bit
Trigger time ms = 2178 --> Look up a little bit
Trigger time ms = 2211 --> Look to the front
Trigger time ms = 2277 --> Look down a little bit
Trigger time ms = 2343 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
