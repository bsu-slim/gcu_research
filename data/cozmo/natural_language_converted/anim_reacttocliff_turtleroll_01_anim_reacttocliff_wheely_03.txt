Original file = ../../data/cozmo/natural_language_converted/anim_reacttocliff_turtleroll_01
Animation anim_reacttocliff_wheely_03 clip 9/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1015 --> lower arms fully
Trigger time ms = 1452 --> raise arms fully
Trigger time ms = 3135 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 479 --> Look all the way up
Trigger time ms = 798 --> Look all the way down
Trigger time ms = 957 --> Look all the way down
Trigger time ms = 1097 --> Look all the way down
Trigger time ms = 1188 --> Look all the way down
Trigger time ms = 1295 --> Look down a little bit
Trigger time ms = 1452 --> Look all the way down
Trigger time ms = 1518 --> Look down a little bit
Trigger time ms = 1551 --> Look all the way down
Trigger time ms = 1584 --> Look down a little bit
Trigger time ms = 1617 --> Look all the way down
Trigger time ms = 1650 --> Look all the way down
Trigger time ms = 1770 --> Look down a little bit
Trigger time ms = 1836 --> Look all the way down
Trigger time ms = 1902 --> Look down a little bit
Trigger time ms = 1968 --> Look all the way down
Trigger time ms = 2034 --> Look down a little bit
Trigger time ms = 2100 --> Look all the way down
Trigger time ms = 2166 --> Look down a little bit
Trigger time ms = 2232 --> Look down a little bit
Trigger time ms = 2257 --> Look all the way down
Trigger time ms = 2277 --> Look all the way down
Trigger time ms = 2310 --> Look up a little bit
Trigger time ms = 3003 --> Look all the way up
Trigger time ms = 3135 --> Look up a little bit
Trigger time ms = 3329 --> Look all the way down
Trigger time ms = 3395 --> Look all the way up
Trigger time ms = 3527 --> Look to the front
Trigger time ms = 3626 --> Look up a little bit
Trigger time ms = 3758 --> Look to the front
Trigger time ms = 3824 --> Look to the front
Trigger time ms = 4022 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1519 --> walk forward turning slow
Trigger time ms = 1597 --> walk forward turning medium slow
Trigger time ms = 1674 --> walk forward turning fast
Trigger time ms = 1751 --> walk forward turning fast
Trigger time ms = 1829 --> walk forward turning fast
Trigger time ms = 1906 --> walk forward turning fast
Trigger time ms = 1983 --> walk forward turning fast
Trigger time ms = 2061 --> walk forward turning fast
Trigger time ms = 2138 --> walk forward turning fast
Trigger time ms = 2166 --> walk forward turning fast
Trigger time ms = 2212 --> walk back turning fast
Trigger time ms = 2232 --> walk back turning medium fast
Trigger time ms = 2277 --> walk back turning medium slow
Trigger time ms = 2310 --> walk back turning medium fast
Trigger time ms = 3069 --> walk forward  fast
Trigger time ms = 3201 --> walk back  fast
