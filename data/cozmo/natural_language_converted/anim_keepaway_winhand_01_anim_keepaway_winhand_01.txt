Original file = ../../data/cozmo/natural_language_converted/anim_keepaway_winhand_01
Animation anim_keepaway_winhand_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 726 --> arms to front
Trigger time ms = 924 --> raise arms fully
Trigger time ms = 1023 --> arms to front
Trigger time ms = 1122 --> raise arms fully
Trigger time ms = 1716 --> arms to front
Trigger time ms = 1848 --> raise arms fully
Trigger time ms = 2112 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> Look all the way down
Trigger time ms = 495 --> Look up a little bit
Trigger time ms = 924 --> Look up a little bit
Trigger time ms = 990 --> Look up a little bit
Trigger time ms = 1056 --> Look up a little bit
Trigger time ms = 1122 --> Look up a little bit
Trigger time ms = 1188 --> Look up a little bit
Trigger time ms = 1254 --> Look up a little bit
Trigger time ms = 1320 --> Look up a little bit
Trigger time ms = 1386 --> Look up a little bit
Trigger time ms = 1452 --> Look up a little bit
Trigger time ms = 1518 --> Look up a little bit
Trigger time ms = 1584 --> Look up a little bit
Trigger time ms = 1650 --> Look up a little bit
Trigger time ms = 1782 --> Look all the way down
Trigger time ms = 1947 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 858 --> walk forward  slow
Trigger time ms = 924 --> walk back  slow
Trigger time ms = 990 --> walk forward  medium slow
Trigger time ms = 1056 --> walk back  medium slow
Trigger time ms = 1122 --> walk forward  medium fast
Trigger time ms = 1188 --> walk back  medium fast
Trigger time ms = 1254 --> walk forward  medium fast
Trigger time ms = 1320 --> walk back  medium fast
Trigger time ms = 1386 --> walk forward  medium fast
Trigger time ms = 1452 --> walk back  medium slow
Trigger time ms = 1518 --> walk forward  medium slow
Trigger time ms = 1584 --> walk back  slow
Trigger time ms = 1782 --> walk forward  medium slow
