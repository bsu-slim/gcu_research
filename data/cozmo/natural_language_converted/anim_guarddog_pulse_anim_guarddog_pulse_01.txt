Original file = ../../data/cozmo/natural_language_converted/anim_guarddog_pulse
Animation anim_guarddog_pulse_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> arms to front
Trigger time ms = 99 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look down a little bit
Trigger time ms = 132 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> walk back  medium fast
Trigger time ms = 132 --> walk forward  medium fast
