Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_successhand_v01
Animation anim_memorymatch_successhand_cozmo_03 clip 3/7
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 396 --> raise arms fully
Trigger time ms = 627 --> raise arms fully
Trigger time ms = 759 --> raise arms fully
Trigger time ms = 870 --> arms to front
Trigger time ms = 982 --> raise arms fully
Trigger time ms = 1093 --> arms to front
Trigger time ms = 1204 --> raise arms fully
Trigger time ms = 1315 --> arms to front
Trigger time ms = 1427 --> raise arms fully
Trigger time ms = 1538 --> raise arms fully
Trigger time ms = 1649 --> raise arms fully
Trigger time ms = 1782 --> raise arms fully
Trigger time ms = 1893 --> raise arms fully
Trigger time ms = 2004 --> lower arms a bit
Trigger time ms = 2112 --> raise arms fully
Trigger time ms = 2211 --> lower arms a bit
Trigger time ms = 2310 --> raise arms fully
Trigger time ms = 2409 --> arms to front
Trigger time ms = 2508 --> raise arms fully
Trigger time ms = 2612 --> lower arms fully
Trigger time ms = 2707 --> lower arms a bit
Trigger time ms = 2805 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> Look up a little bit
Trigger time ms = 264 --> Look up a little bit
Trigger time ms = 627 --> Look to the front
Trigger time ms = 759 --> Look all the way up
Trigger time ms = 870 --> Look to the front
Trigger time ms = 982 --> Look all the way up
Trigger time ms = 1093 --> Look to the front
Trigger time ms = 1204 --> Look all the way up
Trigger time ms = 1316 --> Look to the front
Trigger time ms = 1427 --> Look all the way up
Trigger time ms = 1538 --> Look to the front
Trigger time ms = 1650 --> Look all the way up
Trigger time ms = 1761 --> Look to the front
Trigger time ms = 1872 --> Look all the way up
Trigger time ms = 2039 --> Look up a little bit
Trigger time ms = 2607 --> Look to the front
Trigger time ms = 2838 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 231 --> walk back  medium slow
Trigger time ms = 330 --> walk forward  medium slow
Trigger time ms = 429 --> walk back  medium slow
Trigger time ms = 561 --> walk forward turning medium fast
Trigger time ms = 2772 --> turn back in place fast
