Original file = ../../data/cozmo/natural_language_converted/anim_repair_severe_fix_01
Animation anim_repair_severe_fix_head_up_01 clip 4/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> Look to the front
Trigger time ms = 66 --> Look all the way up
Trigger time ms = 198 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk forward  slow
Trigger time ms = 66 --> walk forward  medium slow
Trigger time ms = 132 --> walk back  medium slow
Trigger time ms = 264 --> walk forward  medium slow
Trigger time ms = 396 --> walk forward  medium slow
