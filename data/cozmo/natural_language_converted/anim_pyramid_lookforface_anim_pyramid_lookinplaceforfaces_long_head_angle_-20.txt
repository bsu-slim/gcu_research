Original file = ../../data/cozmo/natural_language_converted/anim_pyramid_lookforface
Animation anim_pyramid_lookinplaceforfaces_long_head_angle_-20 clip 4/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 132 --> Look all the way down
Trigger time ms = 297 --> Look down a little bit
Trigger time ms = 429 --> Look down a little bit
Trigger time ms = 1716 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
