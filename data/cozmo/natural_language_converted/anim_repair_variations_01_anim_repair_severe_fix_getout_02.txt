Original file = ../../data/cozmo/natural_language_converted/anim_repair_variations_01
Animation anim_repair_severe_fix_getout_02 clip 1/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> arms to front
Trigger time ms = 5016 --> arms to front
Trigger time ms = 5115 --> arms to front
Trigger time ms = 5181 --> arms to front
Trigger time ms = 5610 --> lower arms a bit
Trigger time ms = 5775 --> lower arms a bit
Trigger time ms = 5973 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 198 --> Look down a little bit
Trigger time ms = 297 --> Look all the way down
Trigger time ms = 429 --> Look down a little bit
Trigger time ms = 528 --> Look all the way up
Trigger time ms = 627 --> Look all the way up
Trigger time ms = 693 --> Look up a little bit
Trigger time ms = 792 --> Look up a little bit
Trigger time ms = 1584 --> Look to the front
Trigger time ms = 1683 --> Look up a little bit
Trigger time ms = 1782 --> Look up a little bit
Trigger time ms = 1881 --> Look up a little bit
Trigger time ms = 1947 --> Look up a little bit
Trigger time ms = 2442 --> Look up a little bit
Trigger time ms = 2541 --> Look up a little bit
Trigger time ms = 2640 --> Look up a little bit
Trigger time ms = 3399 --> Look to the front
Trigger time ms = 3564 --> Look all the way up
Trigger time ms = 3663 --> Look all the way up
Trigger time ms = 3762 --> Look up a little bit
Trigger time ms = 3993 --> Look up a little bit
Trigger time ms = 4059 --> Look up a little bit
Trigger time ms = 4191 --> Look up a little bit
Trigger time ms = 4290 --> Look up a little bit
Trigger time ms = 4389 --> Look up a little bit
Trigger time ms = 4818 --> Look all the way down
Trigger time ms = 4950 --> Look all the way up
Trigger time ms = 5082 --> Look up a little bit
Trigger time ms = 5214 --> Look up a little bit
Trigger time ms = 5313 --> Look up a little bit
Trigger time ms = 5643 --> Look down a little bit
Trigger time ms = 5742 --> Look all the way up
Trigger time ms = 5907 --> Look up a little bit
Trigger time ms = 6237 --> Look all the way up
Trigger time ms = 6336 --> Look down a little bit
Trigger time ms = 6468 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 231 --> walk back  medium slow
Trigger time ms = 330 --> walk back turning slow
Trigger time ms = 429 --> walk forward turning medium slow
Trigger time ms = 1518 --> walk forward turning slow
Trigger time ms = 1584 --> walk forward turning slow
Trigger time ms = 1716 --> stop walking
Trigger time ms = 3432 --> walk back turning medium slow
Trigger time ms = 3498 --> walk forward  medium fast
Trigger time ms = 3597 --> walk back  medium fast
Trigger time ms = 3729 --> stop walking
Trigger time ms = 3762 --> walk back turning slow
Trigger time ms = 4191 --> walk forward  medium slow
Trigger time ms = 4290 --> walk back  medium slow
Trigger time ms = 4422 --> stop walking
Trigger time ms = 4785 --> walk back  medium slow
Trigger time ms = 4851 --> walk forward turning medium fast
Trigger time ms = 4983 --> walk back turning fast
Trigger time ms = 5082 --> walk forward  medium slow
Trigger time ms = 5181 --> stop walking
Trigger time ms = 5610 --> walk back  medium slow
Trigger time ms = 5709 --> walk forward  medium fast
Trigger time ms = 5940 --> walk back  medium fast
Trigger time ms = 6039 --> walk forward  medium slow
Trigger time ms = 6171 --> walk back  medium fast
Trigger time ms = 6369 --> walk forward  slow
