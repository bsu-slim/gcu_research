Original file = ../../data/cozmo/natural_language_converted/anim_sparking_idle
Animation anim_sparking_idle_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 660 --> Look to the front
Trigger time ms = 726 --> Look to the front
Trigger time ms = 1089 --> Look to the front
Trigger time ms = 1188 --> Look to the front
Trigger time ms = 1485 --> Look to the front
Trigger time ms = 1584 --> Look to the front
Trigger time ms = 2244 --> Look down a little bit
Trigger time ms = 2838 --> Look to the front
Trigger time ms = 3531 --> Look up a little bit
Trigger time ms = 4653 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 693 --> turn back in place medium fast
Trigger time ms = 1122 --> turn forward in place medium slow
Trigger time ms = 1485 --> turn back in place slow
Trigger time ms = 2343 --> walk back  medium slow
Trigger time ms = 3498 --> walk forward  medium slow
Trigger time ms = 3663 --> turn forward in place fast
Trigger time ms = 3696 --> turn back in place fast
Trigger time ms = 3795 --> turn back in place fast
