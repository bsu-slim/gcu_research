Original file = ../../data/cozmo/natural_language_converted/anim_workout_lowenergy_strong_01
Animation anim_workout_lowenergy_strong_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 3729 --> raise arms fully
Trigger time ms = 4290 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 627 --> Look up a little bit
Trigger time ms = 693 --> Look down a little bit
Trigger time ms = 2277 --> Look all the way down
Trigger time ms = 3135 --> Look down a little bit
Trigger time ms = 3234 --> Look down a little bit
Trigger time ms = 3366 --> Look all the way up
Trigger time ms = 4059 --> Look all the way up
Trigger time ms = 4323 --> Look down a little bit
Trigger time ms = 4521 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1584 --> turn forward in place slow
Trigger time ms = 1716 --> turn back in place medium slow
Trigger time ms = 1782 --> turn forward in place medium slow
Trigger time ms = 1848 --> turn back in place medium slow
Trigger time ms = 1914 --> turn forward in place medium slow
Trigger time ms = 1980 --> turn back in place medium slow
Trigger time ms = 2046 --> turn forward in place medium slow
Trigger time ms = 2112 --> turn back in place medium slow
Trigger time ms = 2178 --> turn forward in place medium slow
Trigger time ms = 2244 --> turn back in place medium slow
Trigger time ms = 2310 --> turn forward in place medium slow
Trigger time ms = 2376 --> turn back in place medium slow
Trigger time ms = 2442 --> turn forward in place medium slow
Trigger time ms = 2508 --> turn back in place medium slow
Trigger time ms = 2574 --> turn forward in place medium slow
Trigger time ms = 2640 --> turn back in place medium slow
Trigger time ms = 2706 --> turn forward in place medium slow
Trigger time ms = 2772 --> turn back in place medium slow
Trigger time ms = 2838 --> turn forward in place medium slow
Trigger time ms = 2904 --> turn back in place medium slow
Trigger time ms = 2970 --> turn forward in place medium slow
Trigger time ms = 3036 --> turn back in place medium slow
Trigger time ms = 3102 --> turn forward in place medium slow
Trigger time ms = 3168 --> turn back in place medium slow
Trigger time ms = 3234 --> turn forward in place medium slow
Trigger time ms = 3300 --> turn back in place medium slow
Trigger time ms = 3366 --> turn forward in place medium slow
Trigger time ms = 3432 --> turn back in place medium slow
Trigger time ms = 3498 --> turn forward in place medium slow
Trigger time ms = 3564 --> turn back in place medium slow
Trigger time ms = 3630 --> turn forward in place medium slow
Trigger time ms = 3696 --> turn back in place medium slow
Trigger time ms = 3762 --> turn forward in place medium slow
Trigger time ms = 3828 --> turn back in place medium slow
Trigger time ms = 3894 --> turn forward in place medium slow
Trigger time ms = 3960 --> turn back in place medium slow
Trigger time ms = 4026 --> turn forward in place medium slow
Trigger time ms = 4092 --> turn back in place medium slow
Trigger time ms = 4224 --> walk back  medium fast
Trigger time ms = 4422 --> walk forward  medium slow
