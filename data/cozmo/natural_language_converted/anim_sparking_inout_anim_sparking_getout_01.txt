Original file = ../../data/cozmo/natural_language_converted/anim_sparking_inout
Animation anim_sparking_getout_01 clip 2/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 99 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 99 --> Look to the front
Trigger time ms = 264 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> turn forward in place fast
