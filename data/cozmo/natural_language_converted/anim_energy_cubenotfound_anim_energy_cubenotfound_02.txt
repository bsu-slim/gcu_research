Original file = ../../data/cozmo/natural_language_converted/anim_energy_cubenotfound
Animation anim_energy_cubenotfound_02 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 33 --> arms to front
Trigger time ms = 165 --> lower arms a bit
Trigger time ms = 2145 --> lower arms a bit
Trigger time ms = 2277 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 132 --> Look up a little bit
Trigger time ms = 231 --> Look to the front
Trigger time ms = 1122 --> Look all the way down
Trigger time ms = 1287 --> Look down a little bit
Trigger time ms = 1881 --> Look all the way down
Trigger time ms = 1947 --> Look up a little bit
Trigger time ms = 2145 --> Look up a little bit
Trigger time ms = 2277 --> Look up a little bit
Trigger time ms = 3366 --> Look down a little bit
Trigger time ms = 3564 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk forward  slow
Trigger time ms = 99 --> walk back  medium fast
Trigger time ms = 396 --> walk back  medium fast
Trigger time ms = 429 --> walk back  medium fast
Trigger time ms = 462 --> walk back  medium slow
Trigger time ms = 495 --> walk back  medium slow
Trigger time ms = 528 --> walk back  medium slow
Trigger time ms = 561 --> walk back  medium slow
Trigger time ms = 627 --> walk back  medium slow
Trigger time ms = 1188 --> turn forward in place medium slow
Trigger time ms = 1287 --> turn forward in place medium slow
Trigger time ms = 1353 --> turn forward in place medium fast
