Original file = ../../data/cozmo/natural_language_converted/anim_play_03
Animation anim_play_idle_02 clip 5/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 264 --> lower arms a bit
Trigger time ms = 363 --> arms to front
Trigger time ms = 462 --> lower arms a bit
Trigger time ms = 891 --> arms to front
Trigger time ms = 957 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> Look to the front
Trigger time ms = 132 --> Look to the front
Trigger time ms = 231 --> Look down a little bit
Trigger time ms = 330 --> Look up a little bit
Trigger time ms = 891 --> Look down a little bit
Trigger time ms = 1023 --> Look down a little bit
Trigger time ms = 1584 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> walk back  slow
Trigger time ms = 264 --> walk forward  medium slow
Trigger time ms = 429 --> walk back  slow
Trigger time ms = 957 --> turn back in place medium fast
Trigger time ms = 1551 --> turn forward in place medium fast
