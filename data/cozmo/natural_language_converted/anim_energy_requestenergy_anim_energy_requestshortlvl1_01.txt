Original file = ../../data/cozmo/natural_language_converted/anim_energy_requestenergy
Animation anim_energy_requestshortlvl1_01 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 396 --> raise arms fully
Trigger time ms = 759 --> arms to front
Trigger time ms = 891 --> raise arms fully
Trigger time ms = 990 --> raise arms fully
Trigger time ms = 1056 --> arms to front
Trigger time ms = 1122 --> raise arms fully
Trigger time ms = 1221 --> raise arms fully
Trigger time ms = 1287 --> arms to front
Trigger time ms = 1353 --> raise arms fully
Trigger time ms = 1452 --> raise arms fully
Trigger time ms = 1848 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 33 --> Look to the front
Trigger time ms = 132 --> Look down a little bit
Trigger time ms = 264 --> Look all the way up
Trigger time ms = 660 --> Look all the way up
Trigger time ms = 1287 --> Look all the way up
Trigger time ms = 1419 --> Look all the way up
Trigger time ms = 1980 --> Look down a little bit
Trigger time ms = 2244 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk back  medium slow
Trigger time ms = 429 --> walk back  slow
Trigger time ms = 594 --> walk forward  slow
Trigger time ms = 726 --> walk forward  medium slow
Trigger time ms = 1122 --> walk forward  slow
Trigger time ms = 1221 --> walk forward  slow
Trigger time ms = 1947 --> walk forward  medium slow
