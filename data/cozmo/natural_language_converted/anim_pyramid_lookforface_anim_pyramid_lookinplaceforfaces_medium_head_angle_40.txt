Original file = ../../data/cozmo/natural_language_converted/anim_pyramid_lookforface
Animation anim_pyramid_lookinplaceforfaces_medium_head_angle_40 clip 9/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 99 --> Look all the way up
Trigger time ms = 231 --> Look all the way up
Trigger time ms = 396 --> Look all the way up
Trigger time ms = 693 --> Look all the way up
Trigger time ms = 792 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
