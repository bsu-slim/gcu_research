Original file = ../../data/cozmo/natural_language_converted/anim_launch_idle_01
Animation anim_launch_idle_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> Look all the way down
Trigger time ms = 264 --> Look all the way down
Trigger time ms = 363 --> Look down a little bit
Trigger time ms = 561 --> Look down a little bit
Trigger time ms = 891 --> Look down a little bit
Trigger time ms = 1023 --> Look down a little bit
Trigger time ms = 1188 --> Look down a little bit
Trigger time ms = 1254 --> Look down a little bit
Trigger time ms = 1749 --> Look down a little bit
Trigger time ms = 1782 --> Look down a little bit
Trigger time ms = 1815 --> Look down a little bit
Trigger time ms = 1848 --> Look down a little bit
Trigger time ms = 1881 --> Look down a little bit
Trigger time ms = 1914 --> Look down a little bit
Trigger time ms = 1947 --> Look down a little bit
Trigger time ms = 1980 --> Look down a little bit
Trigger time ms = 2013 --> Look down a little bit
Trigger time ms = 2475 --> Look all the way down
Trigger time ms = 2541 --> Look all the way down
Trigger time ms = 2574 --> Look all the way down
Trigger time ms = 2607 --> Look all the way down
Trigger time ms = 2640 --> Look all the way down
Trigger time ms = 2673 --> Look all the way down
Trigger time ms = 2706 --> Look all the way down
Trigger time ms = 2871 --> Look all the way down
Trigger time ms = 2904 --> Look all the way down
Trigger time ms = 2970 --> Look all the way down
Trigger time ms = 3003 --> Look all the way down
Trigger time ms = 3333 --> Look all the way down
Trigger time ms = 3432 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk forward  slow
Trigger time ms = 330 --> walk back  medium slow
Trigger time ms = 528 --> walk back  slow
Trigger time ms = 1551 --> turn back in place medium slow
Trigger time ms = 2838 --> turn forward in place medium slow
