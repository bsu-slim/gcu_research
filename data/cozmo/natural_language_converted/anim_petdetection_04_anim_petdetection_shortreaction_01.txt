Original file = ../../data/cozmo/natural_language_converted/anim_petdetection_04
Animation anim_petdetection_shortreaction_01 clip 1/8
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> arms to front
Trigger time ms = 231 --> lower arms fully
Trigger time ms = 1683 --> arms to front
Trigger time ms = 1782 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> Look all the way up
Trigger time ms = 825 --> Look up a little bit
Trigger time ms = 990 --> Look up a little bit
Trigger time ms = 1518 --> Look down a little bit
Trigger time ms = 1584 --> Look all the way up
Trigger time ms = 1683 --> Look all the way up
Trigger time ms = 1815 --> Look all the way up
Trigger time ms = 2013 --> Look down a little bit
Trigger time ms = 2244 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> walk forward  medium slow
Trigger time ms = 1419 --> walk back  medium slow
Trigger time ms = 1518 --> walk forward  slow
Trigger time ms = 1584 --> walk forward  medium slow
Trigger time ms = 1749 --> walk back  medium slow
Trigger time ms = 1914 --> walk back  slow
