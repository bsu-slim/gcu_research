Original file = ../../data/cozmo/natural_language_converted/anim_repair_fix_01
Animation anim_repair_fix_getready_01 clip 12/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look to the front
Trigger time ms = 99 --> Look up a little bit
Trigger time ms = 396 --> Look to the front
Trigger time ms = 462 --> Look up a little bit
Trigger time ms = 726 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> walk forward turning slow
Trigger time ms = 297 --> walk forward  slow
Trigger time ms = 594 --> walk back turning medium slow
Trigger time ms = 726 --> turn back in place fast
