Original file = ../../data/cozmo/natural_language_converted/anim_keepaway_getout_01
Animation anim_keepaway_getout_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 297 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> Look all the way down
Trigger time ms = 297 --> Look to the front
Trigger time ms = 462 --> Look to the front
Trigger time ms = 693 --> Look to the front
Trigger time ms = 759 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> walk forward  medium slow
Trigger time ms = 198 --> walk back  medium slow
