Original file = ../../data/cozmo/natural_language_converted/anim_reacttocliff_faceplant_01
Animation anim_reacttocliff_faceplantroll_armup_01 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 594 --> raise arms fully
Trigger time ms = 858 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 495 --> Look up a little bit
Trigger time ms = 825 --> Look all the way up
Trigger time ms = 957 --> Look all the way up
Trigger time ms = 1089 --> Look to the front
Trigger time ms = 1155 --> Look up a little bit
Trigger time ms = 1287 --> Look to the front
Trigger time ms = 1353 --> Look up a little bit
Trigger time ms = 1485 --> Look to the front
Trigger time ms = 1551 --> Look to the front
Trigger time ms = 1650 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 759 --> walk back  fast
Trigger time ms = 957 --> walk forward  slow
