Original file = ../../data/cozmo/natural_language_converted/anim_meetcozmo
Animation anim_meetcozmo_celebration clip 4/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 2376 --> raise arms fully
Trigger time ms = 2508 --> raise arms fully
Trigger time ms = 2673 --> raise arms fully
Trigger time ms = 3003 --> raise arms fully
Trigger time ms = 3168 --> raise arms fully
Trigger time ms = 3498 --> raise arms fully
Trigger time ms = 3729 --> raise arms fully
Trigger time ms = 4092 --> raise arms fully
Trigger time ms = 4356 --> raise arms fully
Trigger time ms = 4653 --> raise arms fully
Trigger time ms = 4884 --> raise arms fully
Trigger time ms = 5775 --> arms to front
Trigger time ms = 5940 --> lower arms a bit
Trigger time ms = 6204 --> raise arms fully
Trigger time ms = 6402 --> lower arms fully
Trigger time ms = 6666 --> raise arms fully
Trigger time ms = 7986 --> raise arms fully
Trigger time ms = 8217 --> raise arms fully
Trigger time ms = 8415 --> raise arms fully
Trigger time ms = 8514 --> arms to front
Trigger time ms = 8712 --> raise arms fully
Trigger time ms = 9009 --> lower arms fully
Trigger time ms = 9141 --> lower arms a bit
Trigger time ms = 9306 --> raise arms fully
Trigger time ms = 9438 --> raise arms fully
Trigger time ms = 9735 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 231 --> Look up a little bit
Trigger time ms = 297 --> Look up a little bit
Trigger time ms = 594 --> Look up a little bit
Trigger time ms = 792 --> Look to the front
Trigger time ms = 1320 --> Look to the front
Trigger time ms = 1584 --> Look all the way up
Trigger time ms = 1881 --> Look all the way up
Trigger time ms = 2112 --> Look up a little bit
Trigger time ms = 2277 --> Look up a little bit
Trigger time ms = 2409 --> Look up a little bit
Trigger time ms = 5973 --> Look to the front
Trigger time ms = 6237 --> Look up a little bit
Trigger time ms = 6435 --> Look down a little bit
Trigger time ms = 6699 --> Look down a little bit
Trigger time ms = 6963 --> Look up a little bit
Trigger time ms = 7986 --> Look to the front
Trigger time ms = 8217 --> Look up a little bit
Trigger time ms = 8415 --> Look all the way down
Trigger time ms = 9273 --> Look up a little bit
Trigger time ms = 9702 --> Look to the front
Trigger time ms = 9834 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 594 --> walk back  medium slow
Trigger time ms = 759 --> walk forward  medium slow
Trigger time ms = 1056 --> walk forward  slow
Trigger time ms = 3036 --> turn back in place fast
Trigger time ms = 3498 --> walk forward  medium fast
Trigger time ms = 4059 --> walk forward  medium fast
Trigger time ms = 4290 --> walk back  medium fast
Trigger time ms = 4851 --> turn back in place fast
Trigger time ms = 6171 --> turn forward in place fast
Trigger time ms = 6699 --> walk back  medium slow
Trigger time ms = 8151 --> turn forward in place fast
Trigger time ms = 9339 --> walk back  medium slow
Trigger time ms = 9537 --> walk back  medium slow
Trigger time ms = 9867 --> walk back  medium fast
