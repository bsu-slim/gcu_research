Original file = ../../data/cozmo/natural_language_converted/vs_speedtap_gamereact
Animation anim_speedtap_losegame_intensity02_01 clip 5/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> raise arms fully
Trigger time ms = 1254 --> lower arms fully
Trigger time ms = 3927 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 33 --> Look down a little bit
Trigger time ms = 891 --> Look to the front
Trigger time ms = 3531 --> Look to the front
Trigger time ms = 3894 --> Look all the way down
Trigger time ms = 4686 --> Look all the way down
Trigger time ms = 5313 --> Look all the way down
Trigger time ms = 5676 --> Look all the way down
Trigger time ms = 6237 --> Look all the way down
Trigger time ms = 6534 --> Look all the way down
Trigger time ms = 6930 --> Look down a little bit
Trigger time ms = 7161 --> Look all the way down
Trigger time ms = 7656 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> walk back  medium slow
Trigger time ms = 268 --> walk back  medium slow
Trigger time ms = 2812 --> walk back  slow
Trigger time ms = 3378 --> walk back turning slow
Trigger time ms = 4528 --> turn forward in place medium slow
Trigger time ms = 5334 --> turn forward in place slow
