Original file = ../../data/cozmo/natural_language_converted/anim_repair_variations_01
Animation anim_repair_severe_fix_getout_03 clip 2/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> arms to front
Trigger time ms = 66 --> arms to front
Trigger time ms = 132 --> lower arms a bit
Trigger time ms = 2046 --> arms to front
Trigger time ms = 2112 --> lower arms a bit
Trigger time ms = 2178 --> arms to front
Trigger time ms = 2244 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 66 --> Look all the way down
Trigger time ms = 132 --> Look to the front
Trigger time ms = 792 --> Look all the way down
Trigger time ms = 1980 --> Look all the way down
Trigger time ms = 2079 --> Look up a little bit
Trigger time ms = 2211 --> Look up a little bit
Trigger time ms = 2607 --> Look down a little bit
Trigger time ms = 2739 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> walk back  medium slow
Trigger time ms = 792 --> walk back turning medium slow
Trigger time ms = 1617 --> walk back turning medium slow
Trigger time ms = 2046 --> walk forward  medium fast
Trigger time ms = 2277 --> walk back turning medium fast
Trigger time ms = 2640 --> walk back  medium slow
Trigger time ms = 2739 --> walk forward  medium slow
