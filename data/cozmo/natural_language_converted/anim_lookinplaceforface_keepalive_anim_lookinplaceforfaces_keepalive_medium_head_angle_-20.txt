Original file = ../../data/cozmo/natural_language_converted/anim_lookinplaceforface_keepalive
Animation anim_lookinplaceforfaces_keepalive_medium_head_angle_-20 clip 7/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> Look all the way down
Trigger time ms = 231 --> Look down a little bit
Trigger time ms = 396 --> Look down a little bit
Trigger time ms = 693 --> Look all the way down
Trigger time ms = 792 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> turn back in place medium slow
Trigger time ms = 264 --> turn back in place slow
