Original file = ../../data/cozmo/natural_language_converted/anim_freeplay_reacttoface_like_01
Animation anim_freeplay_reacttoface_like_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2442 --> arms to front
Trigger time ms = 2541 --> lower arms a bit
Trigger time ms = 2673 --> raise arms fully
Trigger time ms = 2805 --> lower arms a bit
Trigger time ms = 2904 --> arms to front
Trigger time ms = 3069 --> lower arms fully
Trigger time ms = 3135 --> lower arms fully
Trigger time ms = 3795 --> arms to front
Trigger time ms = 3834 --> lower arms fully
Trigger time ms = 3866 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look down a little bit
Trigger time ms = 132 --> Look all the way up
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 528 --> Look all the way up
Trigger time ms = 792 --> Look all the way up
Trigger time ms = 858 --> Look all the way up
Trigger time ms = 1188 --> Look all the way up
Trigger time ms = 1254 --> Look all the way up
Trigger time ms = 1419 --> Look all the way up
Trigger time ms = 1518 --> Look all the way up
Trigger time ms = 2046 --> Look all the way up
Trigger time ms = 2112 --> Look up a little bit
Trigger time ms = 2178 --> Look all the way up
Trigger time ms = 2277 --> Look all the way up
Trigger time ms = 2508 --> Look all the way up
Trigger time ms = 2640 --> Look up a little bit
Trigger time ms = 2818 --> Look up a little bit
Trigger time ms = 2883 --> Look up a little bit
Trigger time ms = 2970 --> Look up a little bit
Trigger time ms = 3102 --> Look up a little bit
Trigger time ms = 3201 --> Look up a little bit
Trigger time ms = 3345 --> Look all the way up
Trigger time ms = 3465 --> Look all the way up
Trigger time ms = 4224 --> Look all the way up
Trigger time ms = 4257 --> Look up a little bit
Trigger time ms = 4422 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk back  medium fast
Trigger time ms = 858 --> walk back  slow
Trigger time ms = 891 --> walk back  slow
Trigger time ms = 924 --> walk forward  slow
Trigger time ms = 990 --> walk forward  medium slow
Trigger time ms = 1056 --> walk forward  slow
Trigger time ms = 1269 --> turn forward in place medium slow
Trigger time ms = 1383 --> turn forward in place medium slow
Trigger time ms = 1634 --> turn back in place medium fast
Trigger time ms = 1683 --> turn back in place medium slow
Trigger time ms = 2442 --> walk back  medium fast
Trigger time ms = 2706 --> walk forward  medium slow
Trigger time ms = 3102 --> walk back  medium slow
