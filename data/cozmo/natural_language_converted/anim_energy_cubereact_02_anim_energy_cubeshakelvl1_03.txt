Original file = ../../data/cozmo/natural_language_converted/anim_energy_cubereact_02
Animation anim_energy_cubeshakelvl1_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look down a little bit
Trigger time ms = 165 --> Look all the way up
Trigger time ms = 264 --> Look all the way up
Trigger time ms = 330 --> Look all the way up
Trigger time ms = 396 --> Look all the way up
Trigger time ms = 1023 --> Look all the way up
Trigger time ms = 2079 --> Look all the way up
Trigger time ms = 2442 --> Look all the way up
Trigger time ms = 2607 --> Look all the way up
Trigger time ms = 2772 --> Look all the way up
Trigger time ms = 3993 --> Look all the way up
Trigger time ms = 4125 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk back  medium slow
Trigger time ms = 231 --> walk forward  medium slow
Trigger time ms = 396 --> walk forward  slow
Trigger time ms = 594 --> walk forward  slow
Trigger time ms = 759 --> walk forward  slow
Trigger time ms = 891 --> walk forward  slow
Trigger time ms = 1023 --> walk forward  slow
Trigger time ms = 1155 --> walk forward  slow
Trigger time ms = 1287 --> walk forward  slow
Trigger time ms = 1419 --> walk forward  slow
Trigger time ms = 1551 --> walk forward  slow
Trigger time ms = 1683 --> walk forward  slow
Trigger time ms = 1815 --> walk forward  slow
Trigger time ms = 1947 --> walk forward  slow
Trigger time ms = 2079 --> walk forward  slow
Trigger time ms = 2211 --> walk forward  slow
Trigger time ms = 2343 --> walk forward  slow
Trigger time ms = 2475 --> walk forward  slow
Trigger time ms = 2607 --> walk forward  slow
Trigger time ms = 2739 --> walk forward  slow
Trigger time ms = 2871 --> walk forward  slow
Trigger time ms = 3003 --> walk forward  slow
Trigger time ms = 3135 --> walk forward  slow
Trigger time ms = 3267 --> walk forward  slow
Trigger time ms = 3399 --> walk forward  slow
Trigger time ms = 3531 --> walk forward  slow
Trigger time ms = 3663 --> walk forward  slow
Trigger time ms = 3795 --> walk forward  slow
Trigger time ms = 3960 --> walk back  medium slow
Trigger time ms = 4158 --> walk back  slow
