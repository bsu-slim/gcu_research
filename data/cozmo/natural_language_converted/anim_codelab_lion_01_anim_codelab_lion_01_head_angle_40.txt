Original file = ../../data/cozmo/natural_language_converted/anim_codelab_lion_01
Animation anim_codelab_lion_01_head_angle_40 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2277 --> raise arms fully
Trigger time ms = 2376 --> arms to front
Trigger time ms = 2475 --> raise arms fully
Trigger time ms = 2574 --> lower arms fully
Trigger time ms = 2805 --> lower arms a bit
Trigger time ms = 2904 --> lower arms fully
Trigger time ms = 6105 --> arms to front
Trigger time ms = 6237 --> lower arms a bit
Trigger time ms = 6369 --> arms to front
Trigger time ms = 6567 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> Look all the way up
Trigger time ms = 396 --> Look all the way up
Trigger time ms = 561 --> Look all the way up
Trigger time ms = 924 --> Look all the way up
Trigger time ms = 990 --> Look all the way up
Trigger time ms = 1089 --> Look all the way up
Trigger time ms = 1650 --> Look all the way up
Trigger time ms = 1815 --> Look all the way up
Trigger time ms = 2310 --> Look all the way up
Trigger time ms = 2409 --> Look up a little bit
Trigger time ms = 2574 --> Look to the front
Trigger time ms = 2739 --> Look all the way up
Trigger time ms = 2871 --> Look to the front
Trigger time ms = 3135 --> Look up a little bit
Trigger time ms = 3498 --> Look up a little bit
Trigger time ms = 3663 --> Look all the way up
Trigger time ms = 3861 --> Look all the way up
Trigger time ms = 4158 --> Look all the way up
Trigger time ms = 4290 --> Look all the way up
Trigger time ms = 4488 --> Look to the front
Trigger time ms = 4653 --> Look down a little bit
Trigger time ms = 4884 --> Look to the front
Trigger time ms = 5049 --> Look all the way up
Trigger time ms = 5214 --> Look all the way up
Trigger time ms = 5709 --> Look all the way up
Trigger time ms = 5907 --> Look up a little bit
Trigger time ms = 6072 --> Look down a little bit
Trigger time ms = 6237 --> Look all the way up
Trigger time ms = 6468 --> Look all the way up
Trigger time ms = 7194 --> Look up a little bit
Trigger time ms = 7260 --> Look all the way up
Trigger time ms = 7359 --> Look up a little bit
Trigger time ms = 7425 --> Look all the way up
Trigger time ms = 7524 --> Look to the front
Trigger time ms = 7623 --> Look to the front
Trigger time ms = 7755 --> Look to the front
Trigger time ms = 7821 --> Look up a little bit
Trigger time ms = 7887 --> Look to the front
Trigger time ms = 7953 --> Look up a little bit
Trigger time ms = 8019 --> Look to the front
Trigger time ms = 8118 --> Look up a little bit
Trigger time ms = 8184 --> Look to the front
Trigger time ms = 8250 --> Look up a little bit
Trigger time ms = 8316 --> Look up a little bit
Trigger time ms = 8382 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> walk back  slow
Trigger time ms = 264 --> walk back  medium slow
Trigger time ms = 2310 --> walk forward  medium slow
Trigger time ms = 2409 --> walk forward  medium slow
Trigger time ms = 2508 --> walk back  fast
Trigger time ms = 2739 --> walk forward  medium slow
Trigger time ms = 2838 --> walk forward  medium slow
Trigger time ms = 3729 --> walk back  medium fast
Trigger time ms = 3993 --> walk back  medium slow
Trigger time ms = 4158 --> walk back  slow
Trigger time ms = 4224 --> walk back  slow
Trigger time ms = 4389 --> walk back  slow
Trigger time ms = 4488 --> walk forward  medium slow
Trigger time ms = 4587 --> walk forward  medium fast
Trigger time ms = 4686 --> walk forward  medium slow
Trigger time ms = 4719 --> walk forward  medium fast
Trigger time ms = 4983 --> walk back  medium slow
Trigger time ms = 5412 --> walk back  slow
Trigger time ms = 6006 --> turn back in place medium fast
Trigger time ms = 6171 --> turn forward in place medium fast
Trigger time ms = 6369 --> turn back in place medium slow
Trigger time ms = 6600 --> turn back in place slow
Trigger time ms = 7260 --> walk back turning slow
Trigger time ms = 7326 --> walk back turning slow
Trigger time ms = 7392 --> walk back turning medium slow
Trigger time ms = 7458 --> walk back turning medium slow
Trigger time ms = 7524 --> walk back turning slow
Trigger time ms = 7590 --> walk forward turning medium slow
Trigger time ms = 7623 --> walk back turning medium slow
Trigger time ms = 7656 --> walk back turning medium fast
Trigger time ms = 7689 --> walk back turning medium fast
Trigger time ms = 7722 --> walk forward turning medium slow
Trigger time ms = 7755 --> walk forward turning medium slow
Trigger time ms = 7788 --> walk back turning slow
