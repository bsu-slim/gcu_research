Original file = ../../data/cozmo/natural_language_converted/anim_play_03
Animation anim_play_requestplay_level2_01 clip 3/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> lower arms a bit
Trigger time ms = 396 --> arms to front
Trigger time ms = 462 --> lower arms a bit
Trigger time ms = 594 --> arms to front
Trigger time ms = 660 --> lower arms a bit
Trigger time ms = 825 --> arms to front
Trigger time ms = 891 --> lower arms a bit
Trigger time ms = 1815 --> arms to front
Trigger time ms = 1881 --> lower arms a bit
Trigger time ms = 1980 --> arms to front
Trigger time ms = 2046 --> lower arms a bit
Trigger time ms = 2079 --> arms to front
Trigger time ms = 2244 --> lower arms a bit
Trigger time ms = 3003 --> arms to front
Trigger time ms = 3135 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> Look to the front
Trigger time ms = 132 --> Look to the front
Trigger time ms = 198 --> Look to the front
Trigger time ms = 297 --> Look down a little bit
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 495 --> Look all the way up
Trigger time ms = 627 --> Look all the way up
Trigger time ms = 1485 --> Look all the way up
Trigger time ms = 1815 --> Look all the way up
Trigger time ms = 2079 --> Look all the way up
Trigger time ms = 2145 --> Look all the way up
Trigger time ms = 2343 --> Look all the way up
Trigger time ms = 2409 --> Look all the way up
Trigger time ms = 2706 --> Look all the way up
Trigger time ms = 2838 --> Look all the way up
Trigger time ms = 2904 --> Look all the way up
Trigger time ms = 2970 --> Look all the way up
Trigger time ms = 3069 --> Look to the front
Trigger time ms = 3168 --> Look all the way down
Trigger time ms = 3234 --> Look to the front
Trigger time ms = 3333 --> Look down a little bit
Trigger time ms = 3399 --> Look down a little bit
Trigger time ms = 4620 --> Look down a little bit
Trigger time ms = 4686 --> Look to the front
Trigger time ms = 5346 --> Look to the front
Trigger time ms = 5445 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> walk back  medium fast
Trigger time ms = 396 --> walk forward  fast
Trigger time ms = 495 --> walk forward  slow
Trigger time ms = 2112 --> walk back  medium slow
Trigger time ms = 2178 --> walk forward  fast
Trigger time ms = 2244 --> walk back  medium fast
Trigger time ms = 3003 --> walk forward  medium fast
Trigger time ms = 3135 --> walk back  medium slow
Trigger time ms = 3267 --> turn back in place medium slow
Trigger time ms = 3696 --> turn forward in place slow
Trigger time ms = 5346 --> turn forward in place medium fast
