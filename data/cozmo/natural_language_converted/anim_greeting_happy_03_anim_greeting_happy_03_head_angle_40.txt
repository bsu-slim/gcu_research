Original file = ../../data/cozmo/natural_language_converted/anim_greeting_happy_03
Animation anim_greeting_happy_03_head_angle_40 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 33 --> arms to front
Trigger time ms = 165 --> lower arms fully
Trigger time ms = 264 --> lower arms fully
Trigger time ms = 561 --> arms to front
Trigger time ms = 693 --> lower arms fully
Trigger time ms = 792 --> arms to front
Trigger time ms = 891 --> lower arms fully
Trigger time ms = 990 --> raise arms fully
Trigger time ms = 1089 --> lower arms fully
Trigger time ms = 1155 --> arms to front
Trigger time ms = 1254 --> lower arms fully
Trigger time ms = 2013 --> raise arms fully
Trigger time ms = 2079 --> lower arms fully
Trigger time ms = 2178 --> lower arms fully
Trigger time ms = 2739 --> lower arms fully
Trigger time ms = 2772 --> raise arms fully
Trigger time ms = 2871 --> lower arms fully
Trigger time ms = 2970 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look all the way up
Trigger time ms = 165 --> Look all the way up
Trigger time ms = 330 --> Look all the way up
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 528 --> Look all the way up
Trigger time ms = 627 --> Look all the way up
Trigger time ms = 759 --> Look all the way up
Trigger time ms = 858 --> Look all the way up
Trigger time ms = 957 --> Look all the way up
Trigger time ms = 1056 --> Look all the way up
Trigger time ms = 1155 --> Look all the way up
Trigger time ms = 1221 --> Look all the way up
Trigger time ms = 1320 --> Look all the way up
Trigger time ms = 1551 --> Look all the way up
Trigger time ms = 1617 --> Look all the way up
Trigger time ms = 1683 --> Look all the way up
Trigger time ms = 1749 --> Look all the way up
Trigger time ms = 1815 --> Look all the way up
Trigger time ms = 1881 --> Look all the way up
Trigger time ms = 1947 --> Look all the way up
Trigger time ms = 2013 --> Look all the way up
Trigger time ms = 2079 --> Look all the way up
Trigger time ms = 2178 --> Look all the way up
Trigger time ms = 2376 --> Look all the way up
Trigger time ms = 2508 --> Look all the way up
Trigger time ms = 2541 --> Look all the way up
Trigger time ms = 2607 --> Look all the way up
Trigger time ms = 2739 --> Look all the way up
Trigger time ms = 2805 --> Look to the front
Trigger time ms = 2937 --> Look to the front
Trigger time ms = 2970 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> walk back  medium fast
Trigger time ms = 198 --> walk forward  slow
Trigger time ms = 462 --> walk forward turning medium slow
Trigger time ms = 528 --> walk forward turning medium fast
Trigger time ms = 660 --> walk forward turning medium slow
Trigger time ms = 759 --> walk forward turning medium slow
Trigger time ms = 858 --> walk forward turning medium fast
Trigger time ms = 957 --> walk forward turning medium fast
Trigger time ms = 1056 --> walk back turning medium fast
Trigger time ms = 1122 --> walk back turning medium fast
Trigger time ms = 1221 --> walk back turning slow
Trigger time ms = 1551 --> walk back  fast
Trigger time ms = 1617 --> walk forward  fast
Trigger time ms = 1683 --> walk back  fast
Trigger time ms = 1749 --> walk forward  fast
Trigger time ms = 1815 --> walk back  fast
Trigger time ms = 2013 --> walk forward  fast
Trigger time ms = 2145 --> walk back  slow
Trigger time ms = 2178 --> walk back  slow
Trigger time ms = 2376 --> walk back  slow
Trigger time ms = 2772 --> turn forward in place fast
