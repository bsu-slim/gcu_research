Original file = ../../data/cozmo/natural_language_converted/anim_energy_eating_lv1_02
Animation anim_energy_eat_lvl1_03 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 4158 --> raise arms fully
Trigger time ms = 4224 --> raise arms fully
Trigger time ms = 4323 --> raise arms fully
Trigger time ms = 4389 --> raise arms fully
Trigger time ms = 4455 --> raise arms fully
Trigger time ms = 4521 --> raise arms fully
Trigger time ms = 4587 --> raise arms fully
Trigger time ms = 4620 --> raise arms fully
Trigger time ms = 4719 --> arms to front
Trigger time ms = 4851 --> raise arms fully
Trigger time ms = 4983 --> lower arms a bit
Trigger time ms = 5280 --> arms to front
Trigger time ms = 5346 --> lower arms a bit
Trigger time ms = 5412 --> arms to front
Trigger time ms = 5478 --> lower arms a bit
Trigger time ms = 5544 --> arms to front
Trigger time ms = 5610 --> lower arms a bit
Trigger time ms = 5676 --> arms to front
Trigger time ms = 5742 --> lower arms a bit
Trigger time ms = 6237 --> arms to front
Trigger time ms = 6369 --> lower arms a bit
Trigger time ms = 6501 --> arms to front
Trigger time ms = 6633 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 198 --> Look up a little bit
Trigger time ms = 330 --> Look down a little bit
Trigger time ms = 594 --> Look down a little bit
Trigger time ms = 990 --> Look to the front
Trigger time ms = 1089 --> Look down a little bit
Trigger time ms = 1155 --> Look to the front
Trigger time ms = 1254 --> Look down a little bit
Trigger time ms = 1683 --> Look down a little bit
Trigger time ms = 1980 --> Look up a little bit
Trigger time ms = 2574 --> Look all the way up
Trigger time ms = 2739 --> Look up a little bit
Trigger time ms = 2838 --> Look up a little bit
Trigger time ms = 3663 --> Look all the way up
Trigger time ms = 3762 --> Look all the way up
Trigger time ms = 3861 --> Look all the way up
Trigger time ms = 4257 --> Look all the way up
Trigger time ms = 4323 --> Look all the way up
Trigger time ms = 4422 --> Look all the way up
Trigger time ms = 4488 --> Look all the way up
Trigger time ms = 4554 --> Look all the way up
Trigger time ms = 4620 --> Look all the way up
Trigger time ms = 4653 --> Look all the way up
Trigger time ms = 4719 --> Look all the way up
Trigger time ms = 4950 --> Look to the front
Trigger time ms = 5049 --> Look all the way down
Trigger time ms = 5115 --> Look to the front
Trigger time ms = 5181 --> Look to the front
Trigger time ms = 5379 --> Look all the way down
Trigger time ms = 5445 --> Look up a little bit
Trigger time ms = 5742 --> Look up a little bit
Trigger time ms = 5808 --> Look down a little bit
Trigger time ms = 5940 --> Look down a little bit
Trigger time ms = 5973 --> Look down a little bit
Trigger time ms = 6105 --> Look all the way down
Trigger time ms = 6204 --> Look up a little bit
Trigger time ms = 6336 --> Look down a little bit
Trigger time ms = 6435 --> Look to the front
Trigger time ms = 6567 --> Look down a little bit
Trigger time ms = 6633 --> Look to the front
Trigger time ms = 6699 --> Look down a little bit
Trigger time ms = 6831 --> Look to the front
Trigger time ms = 6930 --> Look down a little bit
Trigger time ms = 6996 --> Look to the front
Trigger time ms = 7062 --> Look down a little bit
Trigger time ms = 7194 --> Look to the front
Trigger time ms = 7293 --> Look up a little bit
Trigger time ms = 7491 --> Look down a little bit
Trigger time ms = 7656 --> Look to the front
Trigger time ms = 7821 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 4752 --> walk back turning medium slow
Trigger time ms = 4851 --> walk forward turning slow
Trigger time ms = 5181 --> walk forward turning medium slow
Trigger time ms = 5379 --> walk back turning fast
Trigger time ms = 5841 --> walk forward turning medium slow
Trigger time ms = 6171 --> walk forward turning fast
Trigger time ms = 6237 --> walk back turning fast
Trigger time ms = 6303 --> walk forward turning fast
Trigger time ms = 6369 --> walk back turning fast
Trigger time ms = 6435 --> walk back turning fast
Trigger time ms = 6501 --> walk forward turning fast
Trigger time ms = 6567 --> walk back turning medium fast
Trigger time ms = 6633 --> walk forward turning fast
Trigger time ms = 6699 --> walk back turning medium fast
Trigger time ms = 6765 --> walk forward turning fast
Trigger time ms = 6831 --> walk back turning medium fast
Trigger time ms = 6897 --> walk forward turning fast
Trigger time ms = 7491 --> walk back  medium slow
Trigger time ms = 7656 --> walk forward turning slow
