Original file = ../../data/cozmo/natural_language_converted/anim_pounce_pounce_02
Animation anim_pounce_03 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 66 --> lower arms a bit
Trigger time ms = 132 --> lower arms a bit
Trigger time ms = 231 --> lower arms a bit
Trigger time ms = 429 --> arms to front
Trigger time ms = 561 --> arms to front
Trigger time ms = 660 --> arms to front
Trigger time ms = 759 --> raise arms fully
Trigger time ms = 858 --> raise arms fully
Trigger time ms = 1023 --> raise arms fully
Trigger time ms = 1155 --> raise arms fully
Trigger time ms = 1353 --> arms to front
Trigger time ms = 1419 --> raise arms fully
Trigger time ms = 1485 --> raise arms fully
Trigger time ms = 1518 --> raise arms fully
Trigger time ms = 1584 --> lower arms fully
Trigger time ms = 1650 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 165 --> Look down a little bit
Trigger time ms = 231 --> Look down a little bit
Trigger time ms = 396 --> Look all the way down
Trigger time ms = 726 --> Look all the way down
Trigger time ms = 858 --> Look all the way down
Trigger time ms = 957 --> Look all the way down
Trigger time ms = 1056 --> Look all the way down
Trigger time ms = 1254 --> Look all the way down
Trigger time ms = 1353 --> Look all the way up
Trigger time ms = 1485 --> Look all the way up
Trigger time ms = 1518 --> Look all the way up
Trigger time ms = 1551 --> Look all the way down
Trigger time ms = 1749 --> Look up a little bit
Trigger time ms = 1881 --> Look down a little bit
Trigger time ms = 1980 --> Look to the front
Trigger time ms = 2079 --> Look to the front
Trigger time ms = 2904 --> Look down a little bit
Trigger time ms = 3003 --> Look all the way down
Trigger time ms = 3399 --> Look all the way down
Trigger time ms = 3465 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1188 --> walk back  slow
Trigger time ms = 1254 --> walk back  fast
Trigger time ms = 1287 --> walk forward turning fast
Trigger time ms = 1353 --> walk forward turning fast
Trigger time ms = 1386 --> walk forward turning fast
Trigger time ms = 1419 --> walk forward turning fast
Trigger time ms = 1485 --> walk forward turning fast
Trigger time ms = 1551 --> walk back  fast
Trigger time ms = 1617 --> walk forward  fast
