Original file = ../../data/cozmo/natural_language_converted/anim_energy_liftoncube
Animation anim_energy_liftoncube_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 66 --> lower arms a bit
Trigger time ms = 198 --> raise arms fully
Trigger time ms = 495 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 198 --> Look all the way down
Trigger time ms = 363 --> Look to the front
Trigger time ms = 495 --> Look up a little bit
Trigger time ms = 594 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk back  medium slow
Trigger time ms = 132 --> walk forward  medium fast
