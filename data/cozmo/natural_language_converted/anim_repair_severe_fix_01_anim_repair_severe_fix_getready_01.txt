Original file = ../../data/cozmo/natural_language_converted/anim_repair_severe_fix_01
Animation anim_repair_severe_fix_getready_01 clip 12/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1320 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> Look up a little bit
Trigger time ms = 990 --> Look to the front
Trigger time ms = 1320 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> walk forward  slow
Trigger time ms = 198 --> walk forward  slow
Trigger time ms = 264 --> walk forward  medium slow
Trigger time ms = 330 --> walk forward  medium slow
Trigger time ms = 396 --> walk forward  medium slow
Trigger time ms = 627 --> walk forward  medium slow
Trigger time ms = 726 --> walk forward  slow
Trigger time ms = 825 --> walk forward  slow
Trigger time ms = 924 --> walk forward  slow
Trigger time ms = 1023 --> walk forward  slow
Trigger time ms = 1122 --> walk back  slow
Trigger time ms = 1221 --> walk back  medium slow
Trigger time ms = 1320 --> walk back  medium slow
Trigger time ms = 1386 --> walk back  medium fast
Trigger time ms = 1452 --> walk back  medium slow
Trigger time ms = 1518 --> walk back  medium slow
Trigger time ms = 1584 --> walk back  slow
