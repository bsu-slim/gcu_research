Original file = ../../data/cozmo/natural_language_converted/anim_freeplay_reacttoface_identified
Animation anim_freeplay_reacttoface_identified_03_head_angle_-20 clip 12/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 165 --> arms to front
Trigger time ms = 297 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> Look down a little bit
Trigger time ms = 363 --> Look all the way down
Trigger time ms = 462 --> Look up a little bit
Trigger time ms = 561 --> Look to the front
Trigger time ms = 792 --> Look all the way down
Trigger time ms = 957 --> Look all the way down
Trigger time ms = 1221 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk forward  slow
Trigger time ms = 330 --> walk back  medium fast
Trigger time ms = 528 --> walk forward  medium slow
Trigger time ms = 1221 --> walk forward  medium fast
