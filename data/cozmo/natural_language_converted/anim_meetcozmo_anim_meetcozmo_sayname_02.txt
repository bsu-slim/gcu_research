Original file = ../../data/cozmo/natural_language_converted/anim_meetcozmo
Animation anim_meetcozmo_sayname_02 clip 5/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 33 --> lower arms a bit
Trigger time ms = 99 --> lower arms a bit
Trigger time ms = 132 --> lower arms fully
Trigger time ms = 924 --> arms to front
Trigger time ms = 990 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 132 --> Look up a little bit
Trigger time ms = 264 --> Look up a little bit
Trigger time ms = 462 --> Look up a little bit
Trigger time ms = 693 --> Look up a little bit
Trigger time ms = 759 --> Look up a little bit
Trigger time ms = 891 --> Look all the way up
Trigger time ms = 1122 --> Look all the way up
Trigger time ms = 1254 --> Look all the way up
Trigger time ms = 1452 --> Look all the way up
Trigger time ms = 1551 --> Look all the way up
Trigger time ms = 1683 --> Look all the way up
Trigger time ms = 1848 --> Look all the way up
Trigger time ms = 1947 --> Look all the way up
Trigger time ms = 2013 --> Look all the way up
Trigger time ms = 2244 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1353 --> walk back  medium fast
Trigger time ms = 1584 --> walk forward  slow
Trigger time ms = 1683 --> walk forward  slow
Trigger time ms = 1881 --> walk back  medium slow
Trigger time ms = 1980 --> walk forward  medium slow
Trigger time ms = 2079 --> walk back  medium slow
Trigger time ms = 2277 --> walk back  slow
