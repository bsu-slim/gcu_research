Original file = ../../data/cozmo/natural_language_converted/anim_repair_severe_fix_01
Animation anim_repair_severe_fix_lowerlift_01 clip 9/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 264 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 264 --> Look down a little bit
Trigger time ms = 396 --> Look down a little bit
Trigger time ms = 429 --> Look down a little bit
Trigger time ms = 528 --> Look down a little bit
Trigger time ms = 594 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> walk back  medium slow
Trigger time ms = 462 --> walk forward  medium slow
Trigger time ms = 594 --> walk forward  medium slow
Trigger time ms = 660 --> walk forward  medium slow
Trigger time ms = 726 --> walk forward  medium slow
Trigger time ms = 792 --> walk forward  medium slow
Trigger time ms = 858 --> walk forward  medium slow
Trigger time ms = 924 --> walk forward  slow
