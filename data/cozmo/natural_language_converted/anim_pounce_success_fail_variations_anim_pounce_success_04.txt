Original file = ../../data/cozmo/natural_language_converted/anim_pounce_success_fail_variations
Animation anim_pounce_success_04 clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 231 --> raise arms fully
Trigger time ms = 297 --> raise arms fully
Trigger time ms = 363 --> raise arms fully
Trigger time ms = 429 --> raise arms fully
Trigger time ms = 495 --> raise arms fully
Trigger time ms = 561 --> raise arms fully
Trigger time ms = 693 --> raise arms fully
Trigger time ms = 759 --> raise arms fully
Trigger time ms = 858 --> raise arms fully
Trigger time ms = 990 --> raise arms fully
Trigger time ms = 1122 --> raise arms fully
Trigger time ms = 1188 --> raise arms fully
Trigger time ms = 1320 --> lower arms a bit
Trigger time ms = 1419 --> lower arms a bit
Trigger time ms = 1815 --> lower arms a bit
Trigger time ms = 1881 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 99 --> Look all the way up
Trigger time ms = 198 --> Look up a little bit
Trigger time ms = 297 --> Look all the way up
Trigger time ms = 396 --> Look up a little bit
Trigger time ms = 495 --> Look all the way up
Trigger time ms = 594 --> Look up a little bit
Trigger time ms = 693 --> Look all the way up
Trigger time ms = 792 --> Look down a little bit
Trigger time ms = 924 --> Look all the way up
Trigger time ms = 1122 --> Look to the front
Trigger time ms = 1287 --> Look all the way up
Trigger time ms = 1452 --> Look all the way up
Trigger time ms = 1551 --> Look all the way up
Trigger time ms = 1749 --> Look up a little bit
Trigger time ms = 1815 --> Look all the way up
Trigger time ms = 2244 --> Look all the way up
Trigger time ms = 2343 --> Look down a little bit
Trigger time ms = 2442 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk back  medium slow
Trigger time ms = 66 --> walk forward  fast
Trigger time ms = 231 --> walk back  medium fast
Trigger time ms = 726 --> walk back turning medium slow
Trigger time ms = 759 --> turn back in place fast
Trigger time ms = 825 --> walk back turning medium fast
Trigger time ms = 858 --> turn forward in place fast
Trigger time ms = 990 --> walk forward turning medium fast
Trigger time ms = 1023 --> walk forward turning slow
Trigger time ms = 1089 --> walk forward turning slow
Trigger time ms = 1122 --> walk back turning slow
Trigger time ms = 1221 --> walk back turning slow
