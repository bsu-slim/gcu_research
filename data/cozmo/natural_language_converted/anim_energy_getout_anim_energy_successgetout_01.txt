Original file = ../../data/cozmo/natural_language_converted/anim_energy_getout
Animation anim_energy_successgetout_01 clip 3/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 264 --> raise arms fully
Trigger time ms = 363 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 132 --> Look down a little bit
Trigger time ms = 198 --> Look all the way up
Trigger time ms = 297 --> Look all the way up
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 594 --> Look down a little bit
Trigger time ms = 825 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk forward  slow
Trigger time ms = 165 --> walk forward  slow
Trigger time ms = 297 --> walk forward  medium slow
Trigger time ms = 528 --> walk back  medium slow
