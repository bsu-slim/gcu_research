Original file = ../../data/cozmo/natural_language_converted/anim_freeplay_reacttoface_sayname_02
Animation anim_freeplay_reacttoface_sayname_02 clip 1/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 165 --> arms to front
Trigger time ms = 226 --> lower arms fully
Trigger time ms = 330 --> lower arms fully
Trigger time ms = 462 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 240 --> Look down a little bit
Trigger time ms = 305 --> Look all the way down
Trigger time ms = 396 --> Look all the way down
Trigger time ms = 429 --> Look to the front
Trigger time ms = 495 --> Look down a little bit
Trigger time ms = 627 --> Look to the front
Trigger time ms = 732 --> Look to the front
Trigger time ms = 843 --> Look to the front
Trigger time ms = 1056 --> Look to the front
Trigger time ms = 1122 --> Look to the front
Trigger time ms = 1419 --> Look up a little bit
Trigger time ms = 1543 --> Look all the way down
Trigger time ms = 1666 --> Look down a little bit
Trigger time ms = 1719 --> Look up a little bit
Trigger time ms = 1796 --> Look down a little bit
Trigger time ms = 1883 --> Look up a little bit
Trigger time ms = 2013 --> Look to the front
Trigger time ms = 2137 --> Look to the front
Trigger time ms = 2347 --> Look to the front
Trigger time ms = 2409 --> Look to the front
Trigger time ms = 2442 --> Look to the front
Trigger time ms = 2508 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> walk back  fast
Trigger time ms = 396 --> walk forward  slow
Trigger time ms = 528 --> walk forward  slow
Trigger time ms = 648 --> walk forward  slow
Trigger time ms = 825 --> walk forward  slow
Trigger time ms = 1005 --> walk back  slow
Trigger time ms = 1056 --> walk back  slow
Trigger time ms = 1398 --> walk back  slow
Trigger time ms = 1419 --> walk back  medium slow
Trigger time ms = 1530 --> walk forward  slow
Trigger time ms = 1551 --> walk forward  fast
Trigger time ms = 1782 --> walk back  medium slow
Trigger time ms = 2112 --> walk back  slow
