Original file = ../../data/cozmo/natural_language_converted/anim_workout_mediumenergy_getin_01
Animation anim_workout_mediumenergy_getin_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 495 --> arms to front
Trigger time ms = 561 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 33 --> Look down a little bit
Trigger time ms = 99 --> Look to the front
Trigger time ms = 132 --> Look up a little bit
Trigger time ms = 165 --> Look up a little bit
Trigger time ms = 264 --> Look up a little bit
Trigger time ms = 330 --> Look up a little bit
Trigger time ms = 396 --> Look up a little bit
Trigger time ms = 429 --> Look up a little bit
Trigger time ms = 495 --> Look up a little bit
Trigger time ms = 561 --> Look up a little bit
Trigger time ms = 594 --> Look up a little bit
Trigger time ms = 627 --> Look to the front
Trigger time ms = 759 --> Look to the front
Trigger time ms = 792 --> Look to the front
Trigger time ms = 825 --> Look to the front
Trigger time ms = 858 --> Look to the front
Trigger time ms = 891 --> Look to the front
Trigger time ms = 924 --> Look to the front
Trigger time ms = 957 --> Look to the front
Trigger time ms = 990 --> Look to the front
Trigger time ms = 1089 --> Look to the front
Trigger time ms = 1122 --> Look to the front
Trigger time ms = 1155 --> Look to the front
Trigger time ms = 1188 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> walk back  medium slow
Trigger time ms = 264 --> walk back  medium slow
Trigger time ms = 330 --> walk back  medium slow
Trigger time ms = 396 --> walk back  slow
Trigger time ms = 429 --> walk back  slow
Trigger time ms = 495 --> walk forward  medium slow
Trigger time ms = 561 --> walk forward  medium slow
Trigger time ms = 594 --> walk forward  slow
Trigger time ms = 627 --> walk forward  slow
