Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_successhand_v01
Animation anim_memorymatch_successhand_cozmo_04 clip 4/7
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1485 --> raise arms fully
Trigger time ms = 1656 --> raise arms fully
Trigger time ms = 1763 --> raise arms fully
Trigger time ms = 1868 --> raise arms fully
Trigger time ms = 1975 --> raise arms fully
Trigger time ms = 2079 --> raise arms fully
Trigger time ms = 2188 --> raise arms fully
Trigger time ms = 2295 --> raise arms fully
Trigger time ms = 2399 --> raise arms fully
Trigger time ms = 2507 --> raise arms fully
Trigger time ms = 2607 --> raise arms fully
Trigger time ms = 2713 --> raise arms fully
Trigger time ms = 2822 --> raise arms fully
Trigger time ms = 2927 --> raise arms fully
Trigger time ms = 3034 --> raise arms fully
Trigger time ms = 3135 --> raise arms fully
Trigger time ms = 3223 --> raise arms fully
Trigger time ms = 3306 --> raise arms fully
Trigger time ms = 3332 --> raise arms fully
Trigger time ms = 3337 --> raise arms fully
Trigger time ms = 3484 --> raise arms fully
Trigger time ms = 3572 --> raise arms fully
Trigger time ms = 3662 --> raise arms fully
Trigger time ms = 3749 --> raise arms fully
Trigger time ms = 3881 --> raise arms fully
Trigger time ms = 3967 --> raise arms fully
Trigger time ms = 4099 --> raise arms fully
Trigger time ms = 4187 --> raise arms fully
Trigger time ms = 4276 --> raise arms fully
Trigger time ms = 4405 --> raise arms fully
Trigger time ms = 4492 --> raise arms fully
Trigger time ms = 4625 --> raise arms fully
Trigger time ms = 4713 --> raise arms fully
Trigger time ms = 4842 --> raise arms fully
Trigger time ms = 4931 --> raise arms fully
Trigger time ms = 5019 --> raise arms fully
Trigger time ms = 5181 --> lower arms a bit
Trigger time ms = 5280 --> raise arms fully
Trigger time ms = 5368 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 264 --> Look down a little bit
Trigger time ms = 320 --> Look all the way up
Trigger time ms = 528 --> Look up a little bit
Trigger time ms = 990 --> Look up a little bit
Trigger time ms = 1320 --> Look up a little bit
Trigger time ms = 1386 --> Look up a little bit
Trigger time ms = 5108 --> Look all the way up
Trigger time ms = 5207 --> Look up a little bit
Trigger time ms = 5273 --> Look all the way up
Trigger time ms = 5354 --> Look up a little bit
Trigger time ms = 5415 --> Look all the way up
Trigger time ms = 5490 --> Look all the way up
Trigger time ms = 5554 --> Look all the way up
Trigger time ms = 6006 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 297 --> turn forward in place slow
Trigger time ms = 391 --> turn back in place medium slow
Trigger time ms = 470 --> turn forward in place medium slow
Trigger time ms = 550 --> turn back in place medium slow
Trigger time ms = 629 --> turn forward in place medium slow
Trigger time ms = 709 --> turn back in place medium slow
Trigger time ms = 788 --> turn forward in place medium slow
Trigger time ms = 868 --> turn back in place medium slow
Trigger time ms = 947 --> turn forward in place medium slow
Trigger time ms = 1026 --> turn back in place medium slow
Trigger time ms = 1106 --> turn forward in place medium slow
Trigger time ms = 1185 --> turn back in place medium slow
Trigger time ms = 1264 --> turn forward in place medium slow
Trigger time ms = 1344 --> turn back in place slow
Trigger time ms = 1463 --> walk back  medium fast
Trigger time ms = 1650 --> walk forward turning medium fast
Trigger time ms = 3346 --> walk forward turning medium fast
