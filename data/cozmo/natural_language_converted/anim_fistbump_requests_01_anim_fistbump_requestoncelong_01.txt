Original file = ../../data/cozmo/natural_language_converted/anim_fistbump_requests_01
Animation anim_fistbump_requestoncelong_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 330 --> raise arms fully
Trigger time ms = 2178 --> arms to front
Trigger time ms = 2640 --> arms to front
Trigger time ms = 2739 --> lower arms fully
Trigger time ms = 2904 --> arms to front
Trigger time ms = 2970 --> lower arms a bit
Trigger time ms = 3036 --> raise arms fully
Trigger time ms = 3168 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 198 --> Look down a little bit
Trigger time ms = 297 --> Look down a little bit
Trigger time ms = 1518 --> Look all the way down
Trigger time ms = 1584 --> Look down a little bit
Trigger time ms = 2277 --> Look all the way up
Trigger time ms = 2607 --> Look all the way up
Trigger time ms = 2673 --> Look all the way down
Trigger time ms = 2904 --> Look all the way down
Trigger time ms = 3036 --> Look all the way up
Trigger time ms = 3234 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> turn forward in place medium slow
Trigger time ms = 1485 --> turn back in place medium slow
Trigger time ms = 2244 --> turn back in place medium fast
Trigger time ms = 2310 --> turn back in place medium fast
Trigger time ms = 2607 --> walk back  medium slow
Trigger time ms = 2871 --> walk forward  medium slow
Trigger time ms = 3168 --> walk back  slow
