Original file = ../../data/cozmo/natural_language_converted/anim_peekaboo_getout_01
Animation anim_peekaboo_successgetout_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 2079 --> raise arms fully
Trigger time ms = 2244 --> raise arms fully
Trigger time ms = 2310 --> arms to front
Trigger time ms = 2409 --> raise arms fully
Trigger time ms = 2508 --> arms to front
Trigger time ms = 2607 --> raise arms fully
Trigger time ms = 2706 --> arms to front
Trigger time ms = 2805 --> raise arms fully
Trigger time ms = 2904 --> arms to front
Trigger time ms = 3036 --> raise arms fully
Trigger time ms = 3135 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 165 --> Look all the way up
Trigger time ms = 231 --> Look all the way down
Trigger time ms = 1254 --> Look all the way down
Trigger time ms = 1485 --> Look all the way up
Trigger time ms = 1749 --> Look all the way up
Trigger time ms = 1980 --> Look all the way up
Trigger time ms = 2343 --> Look all the way up
Trigger time ms = 2475 --> Look all the way up
Trigger time ms = 2607 --> Look all the way up
Trigger time ms = 2706 --> Look all the way up
Trigger time ms = 2805 --> Look up a little bit
Trigger time ms = 2904 --> Look all the way up
Trigger time ms = 3003 --> Look all the way up
Trigger time ms = 3102 --> Look all the way up
Trigger time ms = 3333 --> Look all the way up
Trigger time ms = 3795 --> Look all the way up
Trigger time ms = 3861 --> Look all the way up
Trigger time ms = 4125 --> Look all the way up
Trigger time ms = 4653 --> Look up a little bit
Trigger time ms = 4851 --> Look to the front
Trigger time ms = 4983 --> Look to the front
Trigger time ms = 5148 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk back turning slow
Trigger time ms = 231 --> walk back turning slow
Trigger time ms = 330 --> walk forward turning medium slow
Trigger time ms = 396 --> walk back turning medium slow
Trigger time ms = 429 --> walk forward turning slow
Trigger time ms = 495 --> walk back turning medium slow
Trigger time ms = 528 --> walk forward turning slow
Trigger time ms = 627 --> walk back turning slow
Trigger time ms = 693 --> walk forward turning slow
Trigger time ms = 792 --> walk back turning slow
Trigger time ms = 891 --> walk forward turning slow
Trigger time ms = 957 --> walk forward turning slow
Trigger time ms = 990 --> walk back turning slow
Trigger time ms = 1056 --> walk forward turning slow
Trigger time ms = 1122 --> walk back turning slow
Trigger time ms = 1287 --> walk forward  medium slow
Trigger time ms = 1419 --> walk forward  slow
Trigger time ms = 1485 --> walk back  medium fast
Trigger time ms = 1848 --> walk back  slow
Trigger time ms = 2211 --> walk forward  slow
Trigger time ms = 2310 --> walk forward  medium fast
Trigger time ms = 2409 --> walk back  medium fast
Trigger time ms = 2475 --> walk forward  medium fast
Trigger time ms = 2508 --> walk back  medium fast
Trigger time ms = 2574 --> walk forward  medium fast
Trigger time ms = 2607 --> walk back  medium fast
Trigger time ms = 2706 --> walk forward  medium fast
Trigger time ms = 2772 --> walk back  medium fast
Trigger time ms = 2871 --> walk forward  medium fast
Trigger time ms = 2970 --> walk back  medium fast
Trigger time ms = 3069 --> walk forward  medium fast
Trigger time ms = 3135 --> walk back  medium slow
Trigger time ms = 4686 --> walk back  slow
Trigger time ms = 4818 --> walk forward  slow
