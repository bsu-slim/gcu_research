Original file = ../../data/cozmo/natural_language_converted/anim_repair_fix_01
Animation anim_repair_fix_getout_01 clip 10/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 858 --> raise arms fully
Trigger time ms = 1782 --> raise arms fully
Trigger time ms = 3300 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 198 --> Look down a little bit
Trigger time ms = 891 --> Look all the way down
Trigger time ms = 2013 --> Look up a little bit
Trigger time ms = 2145 --> Look to the front
Trigger time ms = 3234 --> Look down a little bit
Trigger time ms = 3300 --> Look up a little bit
Trigger time ms = 3894 --> Look to the front
Trigger time ms = 4059 --> Look to the front
Trigger time ms = 4290 --> Look to the front
Trigger time ms = 4587 --> Look down a little bit
Trigger time ms = 4686 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 858 --> turn back in place fast
Trigger time ms = 1881 --> turn forward in place medium fast
Trigger time ms = 2211 --> walk back turning slow
Trigger time ms = 2640 --> walk forward turning medium slow
Trigger time ms = 3300 --> walk forward  fast
Trigger time ms = 3366 --> walk back  fast
Trigger time ms = 3432 --> walk forward  fast
Trigger time ms = 3498 --> walk back  fast
Trigger time ms = 3564 --> walk forward  fast
Trigger time ms = 3630 --> walk back  fast
Trigger time ms = 3696 --> walk forward  fast
Trigger time ms = 3762 --> walk back  fast
Trigger time ms = 4587 --> turn forward in place fast
