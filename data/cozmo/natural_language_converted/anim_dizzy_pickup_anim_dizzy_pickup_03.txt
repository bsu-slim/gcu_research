Original file = ../../data/cozmo/natural_language_converted/anim_dizzy_pickup
Animation anim_dizzy_pickup_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 66 --> arms to front
Trigger time ms = 132 --> lower arms fully
Trigger time ms = 231 --> lower arms fully
Trigger time ms = 1023 --> lower arms fully
Trigger time ms = 1617 --> lower arms a bit
Trigger time ms = 1716 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look up a little bit
Trigger time ms = 198 --> Look up a little bit
Trigger time ms = 297 --> Look up a little bit
Trigger time ms = 396 --> Look to the front
Trigger time ms = 495 --> Look down a little bit
Trigger time ms = 1023 --> Look down a little bit
Trigger time ms = 1254 --> Look to the front
Trigger time ms = 1485 --> Look down a little bit
Trigger time ms = 1683 --> Look all the way down
Trigger time ms = 1815 --> Look down a little bit
Trigger time ms = 1947 --> Look all the way down
Trigger time ms = 2046 --> Look all the way down
Trigger time ms = 2079 --> Look all the way down
Trigger time ms = 2178 --> Look down a little bit
Trigger time ms = 2574 --> Look to the front
Trigger time ms = 2739 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1518 --> walk back  medium fast
