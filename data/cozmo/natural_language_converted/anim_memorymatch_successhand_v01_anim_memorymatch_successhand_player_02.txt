Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_successhand_v01
Animation anim_memorymatch_successhand_player_02 clip 6/7
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> arms to front
Trigger time ms = 173 --> lower arms fully
Trigger time ms = 264 --> lower arms fully
Trigger time ms = 344 --> lower arms fully
Trigger time ms = 1373 --> lower arms fully
Trigger time ms = 2673 --> arms to front
Trigger time ms = 2739 --> lower arms fully
Trigger time ms = 3102 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 200 --> Look up a little bit
Trigger time ms = 271 --> Look all the way up
Trigger time ms = 327 --> Look all the way up
Trigger time ms = 393 --> Look all the way up
Trigger time ms = 1122 --> Look all the way up
Trigger time ms = 1452 --> Look all the way up
Trigger time ms = 1551 --> Look all the way up
Trigger time ms = 1650 --> Look all the way up
Trigger time ms = 1716 --> Look all the way up
Trigger time ms = 1749 --> Look all the way up
Trigger time ms = 2112 --> Look all the way up
Trigger time ms = 2178 --> Look all the way up
Trigger time ms = 2272 --> Look all the way up
Trigger time ms = 2325 --> Look all the way up
Trigger time ms = 2673 --> Look to the front
Trigger time ms = 2805 --> Look to the front
Trigger time ms = 2871 --> Look to the front
Trigger time ms = 3102 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 488 --> walk forward  medium fast
Trigger time ms = 577 --> walk back  medium fast
Trigger time ms = 671 --> walk forward  medium fast
Trigger time ms = 783 --> walk back  medium fast
Trigger time ms = 990 --> walk forward  medium fast
Trigger time ms = 1188 --> walk back  slow
Trigger time ms = 1584 --> walk back  slow
Trigger time ms = 1617 --> turn forward in place medium fast
Trigger time ms = 1749 --> turn back in place fast
Trigger time ms = 2112 --> turn back in place medium fast
Trigger time ms = 2244 --> turn forward in place fast
Trigger time ms = 2673 --> walk back  medium slow
