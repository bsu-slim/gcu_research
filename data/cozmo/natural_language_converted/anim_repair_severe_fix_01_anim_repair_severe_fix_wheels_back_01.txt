Original file = ../../data/cozmo/natural_language_converted/anim_repair_severe_fix_01
Animation anim_repair_severe_fix_wheels_back_01 clip 5/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> Look down a little bit
Trigger time ms = 99 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> walk back  fast
Trigger time ms = 99 --> walk back  medium fast
Trigger time ms = 132 --> walk forward  medium fast
Trigger time ms = 198 --> walk forward  medium fast
Trigger time ms = 231 --> walk forward  medium fast
Trigger time ms = 297 --> walk forward  medium slow
