Original file = ../../data/cozmo/natural_language_converted/anim_meetcozmo
Animation anim_meetcozmo_lookface_getin_head_angle_40 clip 22/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 462 --> arms to front
Trigger time ms = 561 --> lower arms fully
Trigger time ms = 693 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 132 --> Look all the way up
Trigger time ms = 198 --> Look all the way up
Trigger time ms = 264 --> Look all the way up
Trigger time ms = 330 --> Look all the way up
Trigger time ms = 627 --> Look all the way up
Trigger time ms = 792 --> Look all the way up
Trigger time ms = 957 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk back  medium slow
Trigger time ms = 198 --> walk forward  medium slow
