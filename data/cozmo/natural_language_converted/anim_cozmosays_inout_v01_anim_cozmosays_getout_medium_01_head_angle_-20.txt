Original file = ../../data/cozmo/natural_language_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getout_medium_01_head_angle_-20 clip 19/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 231 --> arms to front
Trigger time ms = 330 --> lower arms fully
Trigger time ms = 1155 --> arms to front
Trigger time ms = 1254 --> lower arms fully
Trigger time ms = 1320 --> arms to front
Trigger time ms = 1386 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> Look down a little bit
Trigger time ms = 231 --> Look to the front
Trigger time ms = 330 --> Look all the way down
Trigger time ms = 561 --> Look all the way down
Trigger time ms = 660 --> Look up a little bit
Trigger time ms = 1023 --> Look to the front
Trigger time ms = 1287 --> Look all the way down
Trigger time ms = 1353 --> Look to the front
Trigger time ms = 1419 --> Look all the way down
Trigger time ms = 1485 --> Look to the front
Trigger time ms = 1551 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> turn forward in place medium fast
Trigger time ms = 429 --> turn forward in place medium fast
Trigger time ms = 528 --> turn forward in place slow
Trigger time ms = 627 --> turn back in place fast
Trigger time ms = 759 --> turn back in place medium slow
Trigger time ms = 1221 --> walk back  medium fast
Trigger time ms = 1287 --> walk forward  fast
Trigger time ms = 1419 --> walk back  slow
