Original file = ../../data/cozmo/natural_language_converted/anim_vc_reaction_yesfaceheardyou_01
Animation anim_vc_reaction_yesfaceheardyou_01_head_angle_40 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 429 --> arms to front
Trigger time ms = 561 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 165 --> Look all the way up
Trigger time ms = 264 --> Look all the way up
Trigger time ms = 330 --> Look all the way up
Trigger time ms = 462 --> Look all the way up
Trigger time ms = 1584 --> Look all the way up
Trigger time ms = 1881 --> Look all the way up
Trigger time ms = 1980 --> Look all the way up
Trigger time ms = 2409 --> Look up a little bit
Trigger time ms = 3861 --> Look all the way up
Trigger time ms = 4059 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk back  medium fast
Trigger time ms = 330 --> walk forward  medium fast
Trigger time ms = 462 --> walk forward  medium slow
Trigger time ms = 2442 --> walk back  medium fast
Trigger time ms = 2607 --> turn back in place medium fast
Trigger time ms = 3762 --> walk back turning medium slow
Trigger time ms = 3828 --> walk back turning medium fast
Trigger time ms = 3894 --> walk back turning fast
Trigger time ms = 3960 --> walk back turning medium slow
Trigger time ms = 4026 --> walk back turning slow
