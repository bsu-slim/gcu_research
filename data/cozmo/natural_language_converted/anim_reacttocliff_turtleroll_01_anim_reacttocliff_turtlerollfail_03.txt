Original file = ../../data/cozmo/natural_language_converted/anim_reacttocliff_turtleroll_01
Animation anim_reacttocliff_turtlerollfail_03 clip 6/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 462 --> raise arms fully
Trigger time ms = 594 --> raise arms fully
Trigger time ms = 1089 --> raise arms fully
Trigger time ms = 1188 --> lower arms a bit
Trigger time ms = 1363 --> lower arms a bit
Trigger time ms = 1386 --> raise arms fully
Trigger time ms = 1452 --> lower arms fully
Trigger time ms = 1551 --> raise arms fully
Trigger time ms = 1683 --> lower arms fully
Trigger time ms = 1782 --> raise arms fully
Trigger time ms = 1848 --> lower arms a bit
Trigger time ms = 1914 --> raise arms fully
Trigger time ms = 1980 --> lower arms a bit
Trigger time ms = 2079 --> raise arms fully
Trigger time ms = 2211 --> lower arms fully
Trigger time ms = 2409 --> lower arms fully
Trigger time ms = 3135 --> raise arms fully
Trigger time ms = 3432 --> raise arms fully
Trigger time ms = 5346 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> Look down a little bit
Trigger time ms = 264 --> Look down a little bit
Trigger time ms = 492 --> Look up a little bit
Trigger time ms = 591 --> Look down a little bit
Trigger time ms = 657 --> Look down a little bit
Trigger time ms = 759 --> Look down a little bit
Trigger time ms = 1089 --> Look to the front
Trigger time ms = 1188 --> Look down a little bit
Trigger time ms = 1363 --> Look down a little bit
Trigger time ms = 1568 --> Look up a little bit
Trigger time ms = 1727 --> Look down a little bit
Trigger time ms = 1832 --> Look all the way up
Trigger time ms = 1939 --> Look down a little bit
Trigger time ms = 2045 --> Look up a little bit
Trigger time ms = 2098 --> Look all the way down
Trigger time ms = 2150 --> Look down a little bit
Trigger time ms = 2203 --> Look all the way down
Trigger time ms = 2257 --> Look down a little bit
Trigger time ms = 2309 --> Look all the way down
Trigger time ms = 2362 --> Look all the way down
Trigger time ms = 2415 --> Look all the way down
Trigger time ms = 2574 --> Look all the way down
Trigger time ms = 2871 --> Look to the front
Trigger time ms = 3003 --> Look all the way up
Trigger time ms = 3300 --> Look all the way up
Trigger time ms = 3564 --> Look all the way up
Trigger time ms = 3597 --> Look all the way up
Trigger time ms = 3729 --> Look all the way up
Trigger time ms = 3828 --> Look all the way up
Trigger time ms = 4752 --> Look all the way up
Trigger time ms = 4851 --> Look all the way up
Trigger time ms = 5049 --> Look all the way up
Trigger time ms = 5346 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1386 --> turn back in place fast
Trigger time ms = 1452 --> turn forward in place fast
Trigger time ms = 1518 --> turn back in place fast
Trigger time ms = 1584 --> turn forward in place fast
Trigger time ms = 1650 --> turn back in place fast
Trigger time ms = 1716 --> turn forward in place fast
Trigger time ms = 1749 --> turn back in place fast
Trigger time ms = 1815 --> turn forward in place fast
Trigger time ms = 1848 --> turn back in place fast
Trigger time ms = 1881 --> turn forward in place medium fast
Trigger time ms = 1947 --> turn back in place fast
Trigger time ms = 1980 --> turn forward in place fast
Trigger time ms = 2013 --> turn back in place fast
Trigger time ms = 2046 --> turn forward in place medium fast
Trigger time ms = 2079 --> turn back in place medium fast
Trigger time ms = 2112 --> turn forward in place medium slow
Trigger time ms = 2970 --> walk back  medium slow
Trigger time ms = 3300 --> walk forward  slow
Trigger time ms = 3564 --> walk forward  slow
Trigger time ms = 5346 --> walk forward  medium slow
