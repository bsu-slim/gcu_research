Original file = ../../data/cozmo/natural_language_converted/anim_meetcozmo_reenrollment_sayname
Animation anim_meetcozmo_reenrollment_sayname_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 132 --> raise arms fully
Trigger time ms = 231 --> raise arms fully
Trigger time ms = 297 --> raise arms fully
Trigger time ms = 396 --> raise arms fully
Trigger time ms = 462 --> arms to front
Trigger time ms = 561 --> raise arms fully
Trigger time ms = 627 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 99 --> Look all the way up
Trigger time ms = 165 --> Look all the way up
Trigger time ms = 297 --> Look all the way up
Trigger time ms = 363 --> Look all the way up
Trigger time ms = 462 --> Look all the way up
Trigger time ms = 528 --> Look all the way up
Trigger time ms = 627 --> Look all the way up
Trigger time ms = 693 --> Look all the way up
Trigger time ms = 792 --> Look all the way up
Trigger time ms = 825 --> Look all the way up
Trigger time ms = 1155 --> Look down a little bit
Trigger time ms = 1353 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk back  medium fast
Trigger time ms = 297 --> walk forward  medium slow
Trigger time ms = 462 --> turn forward in place medium fast
Trigger time ms = 495 --> turn back in place fast
Trigger time ms = 528 --> turn forward in place fast
Trigger time ms = 561 --> turn back in place fast
Trigger time ms = 594 --> turn forward in place medium fast
Trigger time ms = 1089 --> walk forward  medium fast
Trigger time ms = 1287 --> walk back  medium slow
Trigger time ms = 1419 --> walk forward  slow
