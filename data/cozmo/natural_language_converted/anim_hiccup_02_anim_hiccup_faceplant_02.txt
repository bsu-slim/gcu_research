Original file = ../../data/cozmo/natural_language_converted/anim_hiccup_02
Animation anim_hiccup_faceplant_02 clip 8/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 759 --> raise arms fully
Trigger time ms = 990 --> raise arms fully
Trigger time ms = 1254 --> lower arms fully
Trigger time ms = 1386 --> arms to front
Trigger time ms = 1485 --> lower arms fully
Trigger time ms = 1584 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 495 --> Look up a little bit
Trigger time ms = 660 --> Look up a little bit
Trigger time ms = 924 --> Look up a little bit
Trigger time ms = 1122 --> Look all the way up
Trigger time ms = 1320 --> Look all the way up
Trigger time ms = 1452 --> Look down a little bit
Trigger time ms = 1518 --> Look to the front
Trigger time ms = 1584 --> Look up a little bit
Trigger time ms = 1650 --> Look to the front
Trigger time ms = 1749 --> Look up a little bit
Trigger time ms = 1881 --> Look to the front
Trigger time ms = 1947 --> Look to the front
Trigger time ms = 2046 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
