Original file = ../../data/cozmo/natural_language_converted/anim_freeplay_reacttoface_sayname_01
Animation anim_freeplay_reacttoface_sayname_01_head_angle_40 clip 4/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 297 --> arms to front
Trigger time ms = 338 --> lower arms fully
Trigger time ms = 429 --> lower arms fully
Trigger time ms = 509 --> lower arms fully
Trigger time ms = 1538 --> arms to front
Trigger time ms = 1617 --> lower arms fully
Trigger time ms = 1664 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 365 --> Look all the way up
Trigger time ms = 436 --> Look all the way up
Trigger time ms = 492 --> Look all the way up
Trigger time ms = 558 --> Look all the way up
Trigger time ms = 1518 --> Look all the way up
Trigger time ms = 1584 --> Look all the way up
Trigger time ms = 1650 --> Look up a little bit
Trigger time ms = 1716 --> Look all the way up
Trigger time ms = 1881 --> Look all the way up
Trigger time ms = 1980 --> Look all the way up
Trigger time ms = 2079 --> Look all the way up
Trigger time ms = 2178 --> Look all the way up
Trigger time ms = 2541 --> Look all the way up
Trigger time ms = 2574 --> Look all the way up
Trigger time ms = 2640 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 264 --> walk back  medium slow
Trigger time ms = 363 --> walk back  medium slow
Trigger time ms = 644 --> walk forward  medium slow
Trigger time ms = 677 --> walk back  medium fast
Trigger time ms = 704 --> walk forward  medium fast
Trigger time ms = 737 --> walk back  medium fast
Trigger time ms = 770 --> walk forward  medium fast
Trigger time ms = 811 --> walk back  medium fast
Trigger time ms = 844 --> walk forward  medium fast
Trigger time ms = 881 --> walk back  medium slow
Trigger time ms = 904 --> walk forward  medium slow
Trigger time ms = 950 --> walk back  medium slow
Trigger time ms = 997 --> walk back  slow
Trigger time ms = 1056 --> walk forward  medium fast
Trigger time ms = 1188 --> walk back  medium slow
