Original file = ../../data/cozmo/natural_language_converted/anim_meetcozmo_reenrollment_sayname
Animation anim_meetcozmo_reenrollment_sayname_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 33 --> arms to front
Trigger time ms = 74 --> lower arms fully
Trigger time ms = 165 --> lower arms fully
Trigger time ms = 245 --> lower arms fully
Trigger time ms = 1274 --> arms to front
Trigger time ms = 1353 --> lower arms fully
Trigger time ms = 1400 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 99 --> Look up a little bit
Trigger time ms = 132 --> Look all the way up
Trigger time ms = 172 --> Look all the way up
Trigger time ms = 228 --> Look all the way up
Trigger time ms = 294 --> Look all the way up
Trigger time ms = 363 --> Look all the way up
Trigger time ms = 825 --> Look all the way up
Trigger time ms = 891 --> Look all the way up
Trigger time ms = 990 --> Look all the way up
Trigger time ms = 1188 --> Look all the way up
Trigger time ms = 1254 --> Look all the way up
Trigger time ms = 1386 --> Look all the way up
Trigger time ms = 1452 --> Look all the way up
Trigger time ms = 1617 --> Look all the way up
Trigger time ms = 1716 --> Look all the way up
Trigger time ms = 1815 --> Look all the way up
Trigger time ms = 1914 --> Look all the way up
Trigger time ms = 2079 --> Look all the way up
Trigger time ms = 2277 --> Look all the way up
Trigger time ms = 2310 --> Look to the front
Trigger time ms = 2475 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk back  medium slow
Trigger time ms = 99 --> walk back  medium slow
Trigger time ms = 380 --> walk forward  medium slow
Trigger time ms = 413 --> walk back  medium fast
Trigger time ms = 440 --> walk forward  medium fast
Trigger time ms = 473 --> walk back  medium fast
Trigger time ms = 506 --> walk forward  medium fast
Trigger time ms = 547 --> walk back  medium fast
Trigger time ms = 580 --> walk forward  medium fast
Trigger time ms = 617 --> walk back  medium slow
Trigger time ms = 640 --> walk forward  medium slow
Trigger time ms = 686 --> walk back  medium slow
Trigger time ms = 733 --> walk back  slow
Trigger time ms = 792 --> walk forward  medium fast
Trigger time ms = 924 --> walk back  medium slow
