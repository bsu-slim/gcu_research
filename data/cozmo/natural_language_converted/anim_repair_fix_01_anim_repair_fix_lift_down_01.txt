Original file = ../../data/cozmo/natural_language_converted/anim_repair_fix_01
Animation anim_repair_fix_lift_down_01 clip 1/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 33 --> arms to front
Trigger time ms = 132 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look to the front
Trigger time ms = 99 --> Look up a little bit
Trigger time ms = 231 --> Look to the front
Trigger time ms = 396 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk forward  medium slow
Trigger time ms = 198 --> walk back  medium slow
