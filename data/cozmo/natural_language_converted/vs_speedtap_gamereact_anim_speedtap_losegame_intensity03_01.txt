Original file = ../../data/cozmo/natural_language_converted/vs_speedtap_gamereact
Animation anim_speedtap_losegame_intensity03_01 clip 4/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> raise arms fully
Trigger time ms = 99 --> lower arms fully
Trigger time ms = 2937 --> raise arms fully
Trigger time ms = 3069 --> lower arms fully
Trigger time ms = 4026 --> raise arms fully
Trigger time ms = 4125 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 33 --> Look up a little bit
Trigger time ms = 363 --> Look to the front
Trigger time ms = 561 --> Look up a little bit
Trigger time ms = 1155 --> Look up a little bit
Trigger time ms = 1353 --> Look up a little bit
Trigger time ms = 1386 --> Look up a little bit
Trigger time ms = 1452 --> Look up a little bit
Trigger time ms = 1485 --> Look up a little bit
Trigger time ms = 1518 --> Look up a little bit
Trigger time ms = 1584 --> Look up a little bit
Trigger time ms = 1617 --> Look up a little bit
Trigger time ms = 1650 --> Look up a little bit
Trigger time ms = 1716 --> Look up a little bit
Trigger time ms = 1749 --> Look up a little bit
Trigger time ms = 1815 --> Look to the front
Trigger time ms = 2112 --> Look down a little bit
Trigger time ms = 2376 --> Look down a little bit
Trigger time ms = 2475 --> Look to the front
Trigger time ms = 3102 --> Look down a little bit
Trigger time ms = 4256 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> walk back  medium fast
Trigger time ms = 2508 --> walk forward  medium fast
Trigger time ms = 2742 --> turn forward in place fast
Trigger time ms = 3828 --> walk forward turning fast
Trigger time ms = 4124 --> walk back turning medium slow
