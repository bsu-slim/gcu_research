Original file = ../../data/cozmo/natural_language_converted/anim_pounce_pounce_02
Animation anim_pounce_02 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 297 --> lower arms a bit
Trigger time ms = 363 --> lower arms fully
Trigger time ms = 429 --> arms to front
Trigger time ms = 495 --> lower arms fully
Trigger time ms = 528 --> lower arms fully
Trigger time ms = 627 --> arms to front
Trigger time ms = 660 --> lower arms fully
Trigger time ms = 792 --> raise arms fully
Trigger time ms = 858 --> raise arms fully
Trigger time ms = 924 --> lower arms fully
Trigger time ms = 990 --> lower arms fully
Trigger time ms = 1947 --> arms to front
Trigger time ms = 2013 --> arms to front
Trigger time ms = 2079 --> lower arms a bit
Trigger time ms = 2178 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 132 --> Look all the way down
Trigger time ms = 231 --> Look down a little bit
Trigger time ms = 264 --> Look all the way down
Trigger time ms = 297 --> Look down a little bit
Trigger time ms = 330 --> Look all the way down
Trigger time ms = 363 --> Look down a little bit
Trigger time ms = 396 --> Look all the way down
Trigger time ms = 429 --> Look to the front
Trigger time ms = 462 --> Look all the way down
Trigger time ms = 495 --> Look down a little bit
Trigger time ms = 627 --> Look to the front
Trigger time ms = 726 --> Look all the way up
Trigger time ms = 825 --> Look all the way down
Trigger time ms = 990 --> Look down a little bit
Trigger time ms = 1122 --> Look down a little bit
Trigger time ms = 1155 --> Look down a little bit
Trigger time ms = 1353 --> Look all the way down
Trigger time ms = 1947 --> Look all the way down
Trigger time ms = 2013 --> Look to the front
Trigger time ms = 2178 --> Look to the front
Trigger time ms = 2442 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 693 --> walk forward  fast
Trigger time ms = 924 --> walk back  slow
Trigger time ms = 1155 --> walk back  medium slow
Trigger time ms = 2013 --> walk forward  slow
Trigger time ms = 2211 --> walk back  slow
