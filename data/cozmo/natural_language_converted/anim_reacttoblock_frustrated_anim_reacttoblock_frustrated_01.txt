Original file = ../../data/cozmo/natural_language_converted/anim_reacttoblock_frustrated
Animation anim_reacttoblock_frustrated_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 165 --> lower arms a bit
Trigger time ms = 198 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 231 --> Look to the front
Trigger time ms = 264 --> Look to the front
Trigger time ms = 330 --> Look to the front
Trigger time ms = 363 --> Look to the front
Trigger time ms = 396 --> Look to the front
Trigger time ms = 429 --> Look to the front
Trigger time ms = 462 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> turn back in place medium slow
Trigger time ms = 165 --> turn forward in place fast
Trigger time ms = 198 --> turn back in place slow
