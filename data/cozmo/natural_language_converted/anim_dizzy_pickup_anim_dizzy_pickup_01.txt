Original file = ../../data/cozmo/natural_language_converted/anim_dizzy_pickup
Animation anim_dizzy_pickup_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 66 --> arms to front
Trigger time ms = 132 --> lower arms fully
Trigger time ms = 231 --> lower arms fully
Trigger time ms = 858 --> lower arms fully
Trigger time ms = 924 --> lower arms fully
Trigger time ms = 1155 --> lower arms a bit
Trigger time ms = 1254 --> lower arms fully
Trigger time ms = 1716 --> lower arms fully
Trigger time ms = 1782 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look up a little bit
Trigger time ms = 198 --> Look up a little bit
Trigger time ms = 297 --> Look up a little bit
Trigger time ms = 396 --> Look down a little bit
Trigger time ms = 495 --> Look all the way down
Trigger time ms = 924 --> Look down a little bit
Trigger time ms = 1122 --> Look all the way down
Trigger time ms = 1188 --> Look all the way up
Trigger time ms = 1320 --> Look up a little bit
Trigger time ms = 1716 --> Look up a little bit
Trigger time ms = 1782 --> Look up a little bit
Trigger time ms = 1815 --> Look to the front
Trigger time ms = 1848 --> Look up a little bit
Trigger time ms = 1980 --> Look up a little bit
Trigger time ms = 2277 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
