Original file = ../../data/cozmo/natural_language_converted/anim_reacttoblock_ask_01
Animation anim_reacttoblock_ask_01_head_angle_20 clip 5/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 198 --> lower arms fully
Trigger time ms = 264 --> raise arms fully
Trigger time ms = 462 --> lower arms fully
Trigger time ms = 561 --> raise arms fully
Trigger time ms = 660 --> lower arms fully
Trigger time ms = 759 --> raise arms fully
Trigger time ms = 825 --> lower arms fully
Trigger time ms = 924 --> lower arms fully
Trigger time ms = 1320 --> raise arms fully
Trigger time ms = 1584 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> Look to the front
Trigger time ms = 264 --> Look up a little bit
Trigger time ms = 396 --> Look up a little bit
Trigger time ms = 891 --> Look up a little bit
Trigger time ms = 957 --> Look to the front
Trigger time ms = 1089 --> Look to the front
Trigger time ms = 1320 --> Look all the way up
Trigger time ms = 1584 --> Look all the way up
Trigger time ms = 1749 --> Look up a little bit
Trigger time ms = 1881 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> walk back  medium slow
Trigger time ms = 198 --> walk forward  medium fast
Trigger time ms = 330 --> walk back  medium slow
Trigger time ms = 858 --> walk back  medium slow
Trigger time ms = 1089 --> walk forward  fast
Trigger time ms = 1386 --> walk forward  medium slow
Trigger time ms = 1584 --> walk back  medium fast
