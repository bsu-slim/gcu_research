Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_failgame_v01
Animation anim_memorymatch_failgame_player_03 clip 6/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> arms to front
Trigger time ms = 132 --> lower arms fully
Trigger time ms = 2046 --> arms to front
Trigger time ms = 2145 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 33 --> Look down a little bit
Trigger time ms = 165 --> Look to the front
Trigger time ms = 231 --> Look up a little bit
Trigger time ms = 429 --> Look up a little bit
Trigger time ms = 594 --> Look all the way up
Trigger time ms = 1320 --> Look up a little bit
Trigger time ms = 1386 --> Look all the way up
Trigger time ms = 1452 --> Look up a little bit
Trigger time ms = 1518 --> Look all the way up
Trigger time ms = 1584 --> Look up a little bit
Trigger time ms = 1650 --> Look all the way up
Trigger time ms = 1716 --> Look all the way up
Trigger time ms = 1914 --> Look to the front
Trigger time ms = 2178 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> walk back  fast
Trigger time ms = 165 --> walk forward  medium fast
Trigger time ms = 2013 --> walk forward  medium fast
