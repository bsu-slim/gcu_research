Original file = ../../data/cozmo/natural_language_converted/anim_keepaway_losehand_01
Animation anim_keepaway_losehand_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1419 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> Look down a little bit
Trigger time ms = 297 --> Look down a little bit
Trigger time ms = 429 --> Look down a little bit
Trigger time ms = 726 --> Look all the way down
Trigger time ms = 1287 --> Look all the way down
Trigger time ms = 1353 --> Look all the way up
Trigger time ms = 1749 --> Look to the front
Trigger time ms = 1848 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 231 --> walk back  medium fast
Trigger time ms = 396 --> walk forward  medium fast
Trigger time ms = 1518 --> turn forward in place medium fast
Trigger time ms = 1584 --> turn back in place fast
Trigger time ms = 1650 --> turn forward in place fast
Trigger time ms = 1716 --> turn back in place medium fast
Trigger time ms = 1782 --> turn forward in place medium slow
Trigger time ms = 1881 --> walk forward  medium fast
