Original file = ../../data/cozmo/natural_language_converted/anim_energy_cubereact
Animation anim_energy_cubedone_02 clip 5/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 106 --> arms to front
Trigger time ms = 265 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 146 --> Look all the way up
Trigger time ms = 265 --> Look all the way up
Trigger time ms = 424 --> Look all the way up
Trigger time ms = 544 --> Look all the way up
Trigger time ms = 743 --> Look all the way up
Trigger time ms = 942 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk back  slow
Trigger time ms = 146 --> walk forward  slow
