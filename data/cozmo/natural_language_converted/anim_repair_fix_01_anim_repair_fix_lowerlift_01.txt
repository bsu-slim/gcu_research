Original file = ../../data/cozmo/natural_language_converted/anim_repair_fix_01
Animation anim_repair_fix_lowerlift_01 clip 11/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 594 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look all the way down
Trigger time ms = 726 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk back  medium fast
Trigger time ms = 429 --> walk forward  slow
Trigger time ms = 594 --> walk back  medium fast
Trigger time ms = 693 --> walk forward  medium slow
Trigger time ms = 858 --> walk forward  medium slow
Trigger time ms = 924 --> walk forward  slow
