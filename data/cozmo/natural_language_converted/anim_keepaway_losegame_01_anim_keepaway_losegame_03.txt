Original file = ../../data/cozmo/natural_language_converted/anim_keepaway_losegame_01
Animation anim_keepaway_losegame_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2277 --> raise arms fully
Trigger time ms = 2409 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 297 --> Look all the way down
Trigger time ms = 693 --> Look to the front
Trigger time ms = 891 --> Look to the front
Trigger time ms = 1650 --> Look to the front
Trigger time ms = 1980 --> Look down a little bit
Trigger time ms = 2079 --> Look all the way down
Trigger time ms = 2244 --> Look all the way down
Trigger time ms = 2376 --> Look all the way down
Trigger time ms = 3135 --> Look to the front
Trigger time ms = 5214 --> Look to the front
Trigger time ms = 5478 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 462 --> turn back in place fast
Trigger time ms = 891 --> turn forward in place fast
Trigger time ms = 2112 --> turn forward in place fast
Trigger time ms = 2178 --> turn back in place fast
Trigger time ms = 2244 --> turn forward in place fast
Trigger time ms = 2310 --> turn back in place medium fast
Trigger time ms = 5313 --> walk forward  medium slow
