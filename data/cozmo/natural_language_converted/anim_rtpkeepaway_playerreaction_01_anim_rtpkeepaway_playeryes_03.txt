Original file = ../../data/cozmo/natural_language_converted/anim_rtpkeepaway_playerreaction_01
Animation anim_rtpkeepaway_playeryes_03 clip 3/6
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 561 --> lower arms a bit
Trigger time ms = 759 --> lower arms fully
Trigger time ms = 1089 --> arms to front
Trigger time ms = 1221 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 165 --> Look to the front
Trigger time ms = 198 --> Look to the front
Trigger time ms = 528 --> Look down a little bit
Trigger time ms = 660 --> Look to the front
Trigger time ms = 924 --> Look all the way up
Trigger time ms = 1089 --> Look all the way up
Trigger time ms = 1155 --> Look up a little bit
Trigger time ms = 1221 --> Look up a little bit
Trigger time ms = 1452 --> Look up a little bit
Trigger time ms = 1518 --> Look up a little bit
Trigger time ms = 1584 --> Look to the front
Trigger time ms = 1650 --> Look up a little bit
Trigger time ms = 1716 --> Look to the front
Trigger time ms = 2310 --> Look to the front
Trigger time ms = 2376 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 528 --> walk forward  medium slow
Trigger time ms = 693 --> walk forward  slow
Trigger time ms = 792 --> walk back  medium fast
Trigger time ms = 990 --> walk back  slow
Trigger time ms = 1419 --> walk forward  medium slow
Trigger time ms = 1485 --> walk back  medium fast
Trigger time ms = 1551 --> walk forward  medium fast
Trigger time ms = 1617 --> walk back  medium slow
Trigger time ms = 1815 --> walk back  slow
Trigger time ms = 1881 --> walk forward  medium slow
