Original file = ../../data/cozmo/natural_language_converted/anim_hiking_getin_01
Animation anim_hiking_getin_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 330 --> lower arms a bit
Trigger time ms = 429 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 198 --> Look to the front
Trigger time ms = 330 --> Look down a little bit
Trigger time ms = 429 --> Look down a little bit
Trigger time ms = 462 --> Look down a little bit
Trigger time ms = 495 --> Look down a little bit
Trigger time ms = 528 --> Look up a little bit
Trigger time ms = 627 --> Look up a little bit
Trigger time ms = 660 --> Look to the front
Trigger time ms = 825 --> Look to the front
Trigger time ms = 891 --> Look to the front
Trigger time ms = 957 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 363 --> walk forward  medium slow
Trigger time ms = 990 --> walk back  slow
