Original file = ../../data/cozmo/natural_language_converted/anim_keepaway_winhand_01
Animation anim_keepaway_winhand_02 clip 2/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1254 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 198 --> Look all the way down
Trigger time ms = 264 --> Look all the way down
Trigger time ms = 858 --> Look down a little bit
Trigger time ms = 1188 --> Look down a little bit
Trigger time ms = 1221 --> Look to the front
Trigger time ms = 1419 --> Look to the front
Trigger time ms = 1452 --> Look all the way down
Trigger time ms = 1683 --> Look down a little bit
Trigger time ms = 1914 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1155 --> walk back  medium slow
Trigger time ms = 1452 --> walk forward  medium fast
Trigger time ms = 1683 --> walk back  medium slow
