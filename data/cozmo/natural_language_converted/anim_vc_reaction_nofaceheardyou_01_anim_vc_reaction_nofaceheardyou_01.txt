Original file = ../../data/cozmo/natural_language_converted/anim_vc_reaction_nofaceheardyou_01
Animation anim_vc_reaction_nofaceheardyou_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 99 --> raise arms fully
Trigger time ms = 198 --> lower arms a bit
Trigger time ms = 528 --> raise arms fully
Trigger time ms = 627 --> lower arms a bit
Trigger time ms = 1320 --> arms to front
Trigger time ms = 1452 --> lower arms a bit
Trigger time ms = 3069 --> arms to front
Trigger time ms = 3894 --> raise arms fully
Trigger time ms = 3993 --> lower arms a bit
Trigger time ms = 4092 --> lower arms a bit
Trigger time ms = 4158 --> lower arms a bit
Trigger time ms = 4224 --> lower arms a bit
Trigger time ms = 4290 --> lower arms a bit
Trigger time ms = 4752 --> lower arms a bit
Trigger time ms = 4917 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 132 --> Look all the way up
Trigger time ms = 330 --> Look all the way up
Trigger time ms = 561 --> Look all the way up
Trigger time ms = 627 --> Look all the way up
Trigger time ms = 726 --> Look all the way up
Trigger time ms = 1320 --> Look all the way up
Trigger time ms = 1386 --> Look all the way up
Trigger time ms = 1518 --> Look all the way up
Trigger time ms = 2277 --> Look all the way up
Trigger time ms = 2970 --> Look to the front
Trigger time ms = 3498 --> Look to the front
Trigger time ms = 3531 --> Look to the front
Trigger time ms = 3564 --> Look to the front
Trigger time ms = 3597 --> Look to the front
Trigger time ms = 3630 --> Look to the front
Trigger time ms = 3663 --> Look to the front
Trigger time ms = 3696 --> Look to the front
Trigger time ms = 3729 --> Look to the front
Trigger time ms = 3762 --> Look down a little bit
Trigger time ms = 3828 --> Look all the way up
Trigger time ms = 3960 --> Look all the way up
Trigger time ms = 4092 --> Look all the way up
Trigger time ms = 4191 --> Look all the way up
Trigger time ms = 4290 --> Look all the way up
Trigger time ms = 4389 --> Look all the way up
Trigger time ms = 4686 --> Look all the way up
Trigger time ms = 4851 --> Look to the front
Trigger time ms = 4983 --> Look up a little bit
Trigger time ms = 5676 --> Look up a little bit
Trigger time ms = 5775 --> Look all the way up
Trigger time ms = 5907 --> Look all the way up
Trigger time ms = 6369 --> Look up a little bit
Trigger time ms = 6468 --> Look all the way up
Trigger time ms = 6600 --> Look all the way up
Trigger time ms = 7458 --> Look all the way up
Trigger time ms = 8910 --> Look all the way up
Trigger time ms = 9009 --> Look to the front
Trigger time ms = 9207 --> Look to the front
Trigger time ms = 9339 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> turn forward in place fast
Trigger time ms = 561 --> walk back turning medium fast
Trigger time ms = 1320 --> walk forward turning fast
Trigger time ms = 2673 --> turn back in place slow
Trigger time ms = 3894 --> turn forward in place fast
Trigger time ms = 3960 --> turn back in place medium slow
Trigger time ms = 4026 --> walk forward turning fast
Trigger time ms = 4752 --> walk forward turning fast
Trigger time ms = 4884 --> walk forward turning slow
Trigger time ms = 5016 --> walk back turning slow
Trigger time ms = 5709 --> turn back in place medium fast
Trigger time ms = 6402 --> turn forward in place medium slow
Trigger time ms = 7392 --> turn forward in place medium fast
Trigger time ms = 9141 --> walk forward turning medium slow
