Original file = ../../data/cozmo/natural_language_converted/anim_reacttoblock_happydetermined_01
Animation anim_reacttoblock_happydetermined_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> lower arms a bit
Trigger time ms = 198 --> arms to front
Trigger time ms = 264 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look down a little bit
Trigger time ms = 198 --> Look up a little bit
Trigger time ms = 363 --> Look to the front
Trigger time ms = 726 --> Look up a little bit
Trigger time ms = 858 --> Look to the front
Trigger time ms = 990 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 66 --> walk back  medium fast
Trigger time ms = 264 --> walk forward  medium slow
Trigger time ms = 429 --> turn forward in place medium slow
Trigger time ms = 462 --> turn back in place medium fast
Trigger time ms = 495 --> turn forward in place medium fast
Trigger time ms = 528 --> turn back in place medium fast
Trigger time ms = 825 --> walk forward  medium slow
Trigger time ms = 990 --> walk back  medium slow
