Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_successhand_v01
Animation anim_memorymatch_successhand_cozmo_01 clip 2/7
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 66 --> lower arms a bit
Trigger time ms = 165 --> arms to front
Trigger time ms = 231 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 33 --> Look all the way down
Trigger time ms = 165 --> Look to the front
Trigger time ms = 330 --> Look to the front
Trigger time ms = 693 --> Look to the front
Trigger time ms = 825 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> walk back  medium fast
Trigger time ms = 231 --> walk forward  medium slow
Trigger time ms = 396 --> turn forward in place medium slow
Trigger time ms = 429 --> turn back in place medium fast
Trigger time ms = 462 --> turn forward in place medium fast
Trigger time ms = 495 --> turn back in place medium fast
Trigger time ms = 792 --> walk forward  medium slow
Trigger time ms = 957 --> walk back  medium slow
