Original file = ../../data/cozmo/natural_language_converted/anim_meetcozmo
Animation anim_meetcozmo_lookface clip 8/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 1353 --> lower arms a bit
Trigger time ms = 1452 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 264 --> Look up a little bit
Trigger time ms = 660 --> Look up a little bit
Trigger time ms = 1419 --> Look up a little bit
Trigger time ms = 1980 --> Look up a little bit
Trigger time ms = 2343 --> Look up a little bit
Trigger time ms = 2739 --> Look up a little bit
Trigger time ms = 3300 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
