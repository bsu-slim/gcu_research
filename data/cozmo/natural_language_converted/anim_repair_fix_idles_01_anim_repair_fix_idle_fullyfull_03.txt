Original file = ../../data/cozmo/natural_language_converted/anim_repair_fix_idles_01
Animation anim_repair_fix_idle_fullyfull_03 clip 6/6
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 627 --> Look up a little bit
Trigger time ms = 3762 --> Look to the front
Trigger time ms = 4521 --> Look to the front
Trigger time ms = 4587 --> Look to the front
Trigger time ms = 4653 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> walk forward turning slow
Trigger time ms = 264 --> walk forward turning slow
Trigger time ms = 363 --> walk forward turning slow
Trigger time ms = 462 --> walk forward turning slow
Trigger time ms = 561 --> walk forward turning slow
Trigger time ms = 660 --> walk forward turning slow
Trigger time ms = 957 --> walk forward turning slow
Trigger time ms = 1023 --> walk forward turning slow
Trigger time ms = 1089 --> walk forward turning slow
Trigger time ms = 1155 --> walk forward turning slow
Trigger time ms = 1221 --> walk forward turning slow
Trigger time ms = 1287 --> walk forward turning slow
Trigger time ms = 1386 --> walk forward turning slow
Trigger time ms = 1485 --> walk forward turning slow
Trigger time ms = 1584 --> walk forward turning slow
Trigger time ms = 1683 --> walk forward turning slow
Trigger time ms = 1782 --> walk forward turning slow
Trigger time ms = 1881 --> walk forward turning slow
Trigger time ms = 1980 --> walk forward turning slow
Trigger time ms = 2079 --> walk forward turning slow
Trigger time ms = 2178 --> stop walking
Trigger time ms = 3729 --> turn back in place slow
Trigger time ms = 3795 --> turn back in place slow
Trigger time ms = 3861 --> turn back in place slow
Trigger time ms = 3927 --> turn back in place medium slow
Trigger time ms = 4191 --> turn back in place medium slow
Trigger time ms = 4257 --> turn back in place medium slow
Trigger time ms = 4323 --> turn back in place slow
Trigger time ms = 4389 --> turn back in place slow
Trigger time ms = 4455 --> turn back in place slow
Trigger time ms = 4521 --> turn back in place slow
Trigger time ms = 4587 --> turn back in place slow
