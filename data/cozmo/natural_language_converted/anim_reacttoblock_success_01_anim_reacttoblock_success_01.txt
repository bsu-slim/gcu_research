Original file = ../../data/cozmo/natural_language_converted/anim_reacttoblock_success_01
Animation anim_reacttoblock_success_01 clip 1/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 132 --> arms to front
Trigger time ms = 231 --> lower arms fully
Trigger time ms = 330 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 99 --> Look all the way down
Trigger time ms = 198 --> Look all the way up
Trigger time ms = 297 --> Look all the way up
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 561 --> Look all the way up
Trigger time ms = 726 --> Look all the way up
Trigger time ms = 825 --> Look to the front
Trigger time ms = 990 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk back  medium fast
Trigger time ms = 132 --> walk back  slow
Trigger time ms = 165 --> walk forward  medium fast
Trigger time ms = 297 --> walk forward  medium fast
Trigger time ms = 363 --> walk forward  slow
Trigger time ms = 462 --> walk back  medium slow
Trigger time ms = 660 --> walk forward  slow
