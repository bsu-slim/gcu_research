Original file = ../../data/cozmo/natural_language_converted/anim_repair_severe_fix_01
Animation anim_repair_severe_fix_getin_01 clip 7/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 660 --> arms to front
Trigger time ms = 3729 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 1155 --> Look up a little bit
Trigger time ms = 1815 --> Look up a little bit
Trigger time ms = 3465 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 429 --> walk back  medium slow
Trigger time ms = 1815 --> walk back  slow
Trigger time ms = 1947 --> walk back  slow
Trigger time ms = 2079 --> walk back  slow
Trigger time ms = 2211 --> walk back  slow
Trigger time ms = 2343 --> walk back  slow
Trigger time ms = 2475 --> walk back  slow
Trigger time ms = 2739 --> walk back  slow
Trigger time ms = 2871 --> walk back  slow
Trigger time ms = 3003 --> walk back  slow
Trigger time ms = 3267 --> walk back  slow
Trigger time ms = 3927 --> walk forward  medium slow
Trigger time ms = 4587 --> walk forward  medium slow
Trigger time ms = 4653 --> walk forward  slow
Trigger time ms = 4719 --> walk forward  slow
Trigger time ms = 4785 --> walk forward  slow
Trigger time ms = 4851 --> walk forward  slow
Trigger time ms = 4917 --> walk forward  slow
Trigger time ms = 4983 --> walk forward  slow
Trigger time ms = 5115 --> walk forward  slow
Trigger time ms = 5181 --> walk forward  slow
Trigger time ms = 5247 --> walk forward  slow
Trigger time ms = 5412 --> walk forward  slow
Trigger time ms = 5610 --> walk forward  slow
