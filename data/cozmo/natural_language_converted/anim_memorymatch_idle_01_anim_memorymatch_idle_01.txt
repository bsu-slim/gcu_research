Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_idle_01
Animation anim_memorymatch_idle_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 99 --> Look to the front
Trigger time ms = 264 --> Look to the front
Trigger time ms = 363 --> Look up a little bit
Trigger time ms = 561 --> Look up a little bit
Trigger time ms = 891 --> Look up a little bit
Trigger time ms = 1023 --> Look up a little bit
Trigger time ms = 1188 --> Look up a little bit
Trigger time ms = 1254 --> Look up a little bit
Trigger time ms = 1749 --> Look up a little bit
Trigger time ms = 1782 --> Look up a little bit
Trigger time ms = 1815 --> Look up a little bit
Trigger time ms = 1848 --> Look up a little bit
Trigger time ms = 1881 --> Look up a little bit
Trigger time ms = 1914 --> Look up a little bit
Trigger time ms = 1947 --> Look up a little bit
Trigger time ms = 1980 --> Look up a little bit
Trigger time ms = 2013 --> Look up a little bit
Trigger time ms = 2475 --> Look to the front
Trigger time ms = 2541 --> Look to the front
Trigger time ms = 2574 --> Look to the front
Trigger time ms = 2607 --> Look to the front
Trigger time ms = 2640 --> Look to the front
Trigger time ms = 2673 --> Look to the front
Trigger time ms = 2706 --> Look to the front
Trigger time ms = 2871 --> Look to the front
Trigger time ms = 2904 --> Look to the front
Trigger time ms = 2970 --> Look to the front
Trigger time ms = 3003 --> Look to the front
Trigger time ms = 3333 --> Look to the front
Trigger time ms = 3432 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk forward  slow
Trigger time ms = 330 --> walk back  medium slow
Trigger time ms = 528 --> walk back  slow
Trigger time ms = 1551 --> turn back in place medium slow
Trigger time ms = 2838 --> turn forward in place medium slow
