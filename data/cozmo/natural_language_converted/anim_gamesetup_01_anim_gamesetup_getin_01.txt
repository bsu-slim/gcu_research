Original file = ../../data/cozmo/natural_language_converted/anim_gamesetup_01
Animation anim_gamesetup_getin_01 clip 4/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 99 --> arms to front
Trigger time ms = 165 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look all the way down
Trigger time ms = 165 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk forward  medium slow
