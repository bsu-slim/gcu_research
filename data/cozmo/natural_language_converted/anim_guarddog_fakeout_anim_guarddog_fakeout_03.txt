Original file = ../../data/cozmo/natural_language_converted/anim_guarddog_fakeout
Animation anim_guarddog_fakeout_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 264 --> raise arms fully
Trigger time ms = 396 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 132 --> Look all the way down
Trigger time ms = 198 --> Look all the way up
Trigger time ms = 264 --> Look down a little bit
Trigger time ms = 462 --> Look all the way up
Trigger time ms = 627 --> Look to the front
Trigger time ms = 759 --> Look up a little bit
Trigger time ms = 990 --> Look up a little bit
Trigger time ms = 3168 --> Look to the front
Trigger time ms = 3399 --> Look down a little bit
Trigger time ms = 3597 --> Look to the front
Trigger time ms = 3696 --> Look to the front
Trigger time ms = 3927 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk forward  medium fast
Trigger time ms = 198 --> turn forward in place fast
Trigger time ms = 297 --> turn back in place fast
Trigger time ms = 3366 --> turn forward in place medium slow
Trigger time ms = 3663 --> turn back in place slow
