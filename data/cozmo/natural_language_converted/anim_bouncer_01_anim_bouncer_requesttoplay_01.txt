Original file = ../../data/cozmo/natural_language_converted/anim_bouncer_01
Animation anim_bouncer_requesttoplay_01 clip 2/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 2145 --> arms to front
Trigger time ms = 2607 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 1419 --> Look up a little bit
Trigger time ms = 1584 --> Look up a little bit
Trigger time ms = 1650 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 33 --> turn back in place medium slow
Trigger time ms = 231 --> turn forward in place medium fast
Trigger time ms = 2310 --> walk forward  medium slow
Trigger time ms = 2442 --> walk back  medium slow
Trigger time ms = 2574 --> walk back  slow
