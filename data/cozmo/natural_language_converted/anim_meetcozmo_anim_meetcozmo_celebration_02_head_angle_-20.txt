Original file = ../../data/cozmo/natural_language_converted/anim_meetcozmo
Animation anim_meetcozmo_celebration_02_head_angle_-20 clip 21/30
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 99 --> raise arms fully
Trigger time ms = 363 --> raise arms fully
Trigger time ms = 594 --> raise arms fully
Trigger time ms = 627 --> raise arms fully
Trigger time ms = 693 --> raise arms fully
Trigger time ms = 759 --> raise arms fully
Trigger time ms = 891 --> raise arms fully
Trigger time ms = 1056 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look all the way down
Trigger time ms = 198 --> Look all the way down
Trigger time ms = 363 --> Look up a little bit
Trigger time ms = 495 --> Look up a little bit
Trigger time ms = 1320 --> Look to the front
Trigger time ms = 1386 --> Look up a little bit
Trigger time ms = 1485 --> Look to the front
Trigger time ms = 1584 --> Look up a little bit
Trigger time ms = 1683 --> Look up a little bit
Trigger time ms = 1914 --> Look up a little bit
Trigger time ms = 2244 --> Look up a little bit
Trigger time ms = 2409 --> Look to the front
Trigger time ms = 2607 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 0 --> walk back  medium slow
Trigger time ms = 99 --> walk back  slow
Trigger time ms = 264 --> walk forward  medium fast
Trigger time ms = 462 --> walk back  slow
Trigger time ms = 528 --> walk back  slow
Trigger time ms = 627 --> walk forward  medium slow
Trigger time ms = 693 --> walk back  medium slow
Trigger time ms = 759 --> walk forward  medium slow
Trigger time ms = 858 --> walk back  medium slow
Trigger time ms = 957 --> walk forward  medium slow
Trigger time ms = 1353 --> walk back  slow
Trigger time ms = 1419 --> walk forward  slow
Trigger time ms = 1518 --> walk back  medium slow
Trigger time ms = 1617 --> walk forward  medium slow
Trigger time ms = 1716 --> walk back  medium slow
Trigger time ms = 2343 --> walk back  medium slow
