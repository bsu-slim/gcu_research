Original file = ../../data/cozmo/natural_language_converted/anim_freeplay_reacttoface_identified
Animation anim_freeplay_reacttoface_identified_02 clip 2/12
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 165 --> Look all the way up
Trigger time ms = 297 --> Look all the way up
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 627 --> Look up a little bit
Trigger time ms = 726 --> Look all the way up
Trigger time ms = 1056 --> Look all the way up
Trigger time ms = 1155 --> Look to the front
Trigger time ms = 1221 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 363 --> walk back  slow
Trigger time ms = 462 --> walk back  slow
Trigger time ms = 627 --> walk forward  medium slow
Trigger time ms = 825 --> walk back  slow
Trigger time ms = 891 --> walk back  slow
Trigger time ms = 1122 --> walk back  medium slow
