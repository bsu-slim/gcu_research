Original file = ../../data/cozmo/natural_language_converted/anim_repair_fix_idles_01
Animation anim_repair_fix_idle_02 clip 2/6
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 198 --> Look down a little bit
Trigger time ms = 462 --> Look down a little bit
Trigger time ms = 1056 --> Look down a little bit
Trigger time ms = 1155 --> Look down a little bit
Trigger time ms = 2343 --> Look up a little bit
Trigger time ms = 3102 --> Look up a little bit
Trigger time ms = 4785 --> Look to the front
Trigger time ms = 4950 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk back turning medium fast
Trigger time ms = 1056 --> walk back turning medium slow
Trigger time ms = 4785 --> walk forward turning medium fast
