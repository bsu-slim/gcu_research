Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_reacttopattern_struggle_01
Animation anim_memorymatch_reacttopattern_struggle_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 2805 --> raise arms fully
Trigger time ms = 2970 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> Look to the front
Trigger time ms = 165 --> Look to the front
Trigger time ms = 198 --> Look to the front
Trigger time ms = 297 --> Look to the front
Trigger time ms = 495 --> Look to the front
Trigger time ms = 759 --> Look to the front
Trigger time ms = 1089 --> Look to the front
Trigger time ms = 1155 --> Look to the front
Trigger time ms = 2673 --> Look up a little bit
Trigger time ms = 2871 --> Look up a little bit
Trigger time ms = 3102 --> Look to the front
Trigger time ms = 3168 --> Look up a little bit
Trigger time ms = 3279 --> Look up a little bit
Trigger time ms = 3399 --> Look up a little bit
Trigger time ms = 3729 --> Look to the front
Trigger time ms = 3795 --> Look up a little bit
Trigger time ms = 3927 --> Look up a little bit
Trigger time ms = 4191 --> Look up a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 297 --> turn back in place fast
Trigger time ms = 363 --> turn forward in place medium fast
Trigger time ms = 396 --> turn forward in place medium fast
Trigger time ms = 1188 --> turn forward in place medium fast
Trigger time ms = 2640 --> walk back  medium slow
Trigger time ms = 2805 --> walk back  slow
Trigger time ms = 2871 --> walk forward  medium slow
