Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_point_v02
Animation anim_memorymatch_pointbigleft_01 clip 5/11
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 264 --> raise arms fully
Trigger time ms = 396 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 132 --> Look all the way down
Trigger time ms = 264 --> Look to the front
Trigger time ms = 396 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> turn forward in place fast
Trigger time ms = 363 --> turn back in place slow
