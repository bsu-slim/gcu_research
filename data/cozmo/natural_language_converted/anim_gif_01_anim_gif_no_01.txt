Original file = ../../data/cozmo/natural_language_converted/anim_gif_01
Animation anim_gif_no_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 594 --> Look to the front
Trigger time ms = 726 --> Look down a little bit
Trigger time ms = 891 --> Look down a little bit
Trigger time ms = 1848 --> Look to the front
Trigger time ms = 2013 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 594 --> walk back  slow
Trigger time ms = 759 --> turn back in place medium fast
Trigger time ms = 891 --> turn back in place medium slow
Trigger time ms = 990 --> turn forward in place medium slow
Trigger time ms = 1089 --> turn forward in place medium fast
Trigger time ms = 1254 --> turn forward in place medium slow
Trigger time ms = 1353 --> turn back in place medium slow
Trigger time ms = 1452 --> turn back in place fast
Trigger time ms = 1617 --> turn back in place medium slow
Trigger time ms = 1716 --> turn forward in place medium slow
Trigger time ms = 1815 --> turn forward in place medium fast
Trigger time ms = 1914 --> turn forward in place medium slow
Trigger time ms = 1947 --> turn forward in place slow
Trigger time ms = 1980 --> turn forward in place slow
