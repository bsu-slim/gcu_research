Original file = ../../data/cozmo/natural_language_converted/anim_workout_mediumenergy_getout_01
Animation anim_workout_mediumenergy_getout_01 clip 1/1
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> raise arms fully
Trigger time ms = 990 --> raise arms fully
Trigger time ms = 1518 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 297 --> Look up a little bit
Trigger time ms = 429 --> Look up a little bit
Trigger time ms = 858 --> Look all the way up
Trigger time ms = 1419 --> Look all the way up
Trigger time ms = 1815 --> Look up a little bit
Trigger time ms = 2409 --> Look to the front
Trigger time ms = 2475 --> Look up a little bit
Trigger time ms = 2739 --> Look up a little bit
Trigger time ms = 3069 --> Look all the way up
Trigger time ms = 3201 --> Look up a little bit
Trigger time ms = 3267 --> Look up a little bit
Trigger time ms = 3366 --> Look up a little bit
Trigger time ms = 3795 --> Look down a little bit
Trigger time ms = 3960 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 396 --> walk forward  medium slow
Trigger time ms = 594 --> walk forward  slow
Trigger time ms = 891 --> walk back  medium fast
Trigger time ms = 1221 --> walk back  slow
Trigger time ms = 1353 --> turn forward in place fast
Trigger time ms = 1782 --> walk back  fast
Trigger time ms = 2178 --> walk back  slow
Trigger time ms = 2607 --> walk forward  medium fast
Trigger time ms = 2805 --> walk forward  slow
Trigger time ms = 3498 --> walk back  medium slow
