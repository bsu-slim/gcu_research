Original file = ../../data/cozmo/natural_language_converted/anim_freeplay_reacttoface_unidentified
Animation anim_reacttoface_unidentified_01_head_angle_-20 clip 9/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 495 --> arms to front
Trigger time ms = 594 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 33 --> Look all the way down
Trigger time ms = 99 --> Look down a little bit
Trigger time ms = 198 --> Look down a little bit
Trigger time ms = 627 --> Look down a little bit
Trigger time ms = 693 --> Look to the front
Trigger time ms = 792 --> Look down a little bit
Trigger time ms = 1056 --> Look all the way down
Trigger time ms = 1155 --> Look down a little bit
Trigger time ms = 1221 --> Look down a little bit
Trigger time ms = 1353 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 627 --> walk back  medium slow
Trigger time ms = 693 --> walk forward  medium slow
Trigger time ms = 891 --> walk back  medium slow
Trigger time ms = 1155 --> walk back  slow
