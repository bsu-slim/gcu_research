Original file = ../../data/cozmo/natural_language_converted/anim_energy_cubenotfound
Animation anim_energy_cubenotfound_01 clip 1/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 528 --> arms to front
Trigger time ms = 627 --> lower arms a bit
Trigger time ms = 2046 --> lower arms a bit
Trigger time ms = 2145 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 66 --> Look down a little bit
Trigger time ms = 165 --> Look to the front
Trigger time ms = 396 --> Look to the front
Trigger time ms = 594 --> Look all the way down
Trigger time ms = 693 --> Look down a little bit
Trigger time ms = 825 --> Look down a little bit
Trigger time ms = 858 --> Look down a little bit
Trigger time ms = 957 --> Look all the way down
Trigger time ms = 1023 --> Look down a little bit
Trigger time ms = 1188 --> Look down a little bit
Trigger time ms = 1353 --> Look down a little bit
Trigger time ms = 2046 --> Look all the way down
Trigger time ms = 2145 --> Look up a little bit
Trigger time ms = 2310 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 561 --> walk back  medium fast
Trigger time ms = 891 --> walk forward  slow
Trigger time ms = 2178 --> turn forward in place medium fast
Trigger time ms = 2442 --> walk back  slow
