Original file = ../../data/cozmo/natural_language_converted/anim_codelab_rooster_01
Animation anim_codelab_rooster_01_head_angle_-20 clip 2/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 132 --> arms to front
Trigger time ms = 231 --> lower arms fully
Trigger time ms = 1683 --> arms to front
Trigger time ms = 1782 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 99 --> Look up a little bit
Trigger time ms = 825 --> Look down a little bit
Trigger time ms = 990 --> Look down a little bit
Trigger time ms = 1518 --> Look all the way down
Trigger time ms = 1584 --> Look up a little bit
Trigger time ms = 1650 --> Look all the way up
Trigger time ms = 1782 --> Look all the way up
Trigger time ms = 2013 --> Look all the way down
Trigger time ms = 2244 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 132 --> walk forward  medium slow
Trigger time ms = 1419 --> walk back  medium slow
Trigger time ms = 1518 --> walk forward  slow
Trigger time ms = 1584 --> walk forward  medium slow
Trigger time ms = 1749 --> walk back  medium slow
