Original file = ../../data/cozmo/natural_language_converted/anim_energy_getout
Animation anim_energy_failgetout_02 clip 2/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 330 --> raise arms fully
Trigger time ms = 429 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 264 --> Look to the front
Trigger time ms = 396 --> Look all the way down
Trigger time ms = 495 --> Look to the front
Trigger time ms = 627 --> Look up a little bit
Trigger time ms = 924 --> Look to the front
Trigger time ms = 1023 --> Look to the front
Trigger time ms = 1089 --> Look down a little bit
Trigger time ms = 1254 --> Look down a little bit
Trigger time ms = 1683 --> Look all the way down
Trigger time ms = 1749 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 363 --> walk back  medium fast
