Original file = ../../data/cozmo/natural_language_converted/anim_pyramid_lookforface
Animation anim_pyramid_lookinplaceforfaces_long_head_angle_20 clip 5/12
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way up
Trigger time ms = 132 --> Look up a little bit
Trigger time ms = 297 --> Look all the way up
Trigger time ms = 429 --> Look all the way up
Trigger time ms = 1716 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
