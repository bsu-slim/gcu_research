Original file = ../../data/cozmo/natural_language_converted/anim_gamesetup_01
Animation anim_gamesetup_idle_02 clip 2/9
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look all the way down
Trigger time ms = 198 --> Look all the way down
Trigger time ms = 363 --> Look down a little bit
Trigger time ms = 495 --> Look down a little bit
Trigger time ms = 1419 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 330 --> walk back  medium slow
Trigger time ms = 495 --> walk back  slow
Trigger time ms = 2475 --> walk forward  medium slow
