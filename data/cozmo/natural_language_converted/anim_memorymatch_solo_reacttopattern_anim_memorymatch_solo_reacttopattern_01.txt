Original file = ../../data/cozmo/natural_language_converted/anim_memorymatch_solo_reacttopattern
Animation anim_memorymatch_solo_reacttopattern_01 clip 1/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 99 --> arms to front
Trigger time ms = 198 --> lower arms fully
Trigger time ms = 2871 --> lower arms a bit
Trigger time ms = 2970 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look down a little bit
Trigger time ms = 165 --> Look up a little bit
Trigger time ms = 330 --> Look to the front
Trigger time ms = 792 --> Look to the front
Trigger time ms = 858 --> Look to the front
Trigger time ms = 1353 --> Look to the front
Trigger time ms = 1386 --> Look to the front
Trigger time ms = 1782 --> Look to the front
Trigger time ms = 1815 --> Look to the front
Trigger time ms = 1848 --> Look to the front
Trigger time ms = 2046 --> Look to the front
Trigger time ms = 2079 --> Look to the front
Trigger time ms = 2112 --> Look to the front
Trigger time ms = 2376 --> Look down a little bit
Trigger time ms = 2442 --> Look to the front
Trigger time ms = 2550 --> Look to the front
Trigger time ms = 2748 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 99 --> walk forward  medium fast
