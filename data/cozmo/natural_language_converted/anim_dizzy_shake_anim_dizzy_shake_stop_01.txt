Original file = ../../data/cozmo/natural_language_converted/anim_dizzy_shake
Animation anim_dizzy_shake_stop_01 clip 2/2
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 3861 --> arms to front
Trigger time ms = 3927 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 33 --> Look up a little bit
Trigger time ms = 99 --> Look to the front
Trigger time ms = 165 --> Look up a little bit
Trigger time ms = 231 --> Look to the front
Trigger time ms = 297 --> Look up a little bit
Trigger time ms = 363 --> Look to the front
Trigger time ms = 429 --> Look up a little bit
Trigger time ms = 495 --> Look to the front
Trigger time ms = 561 --> Look up a little bit
Trigger time ms = 627 --> Look to the front
Trigger time ms = 693 --> Look up a little bit
Trigger time ms = 792 --> Look to the front
Trigger time ms = 891 --> Look up a little bit
Trigger time ms = 990 --> Look to the front
Trigger time ms = 1089 --> Look up a little bit
Trigger time ms = 1188 --> Look to the front
Trigger time ms = 1287 --> Look up a little bit
Trigger time ms = 1386 --> Look to the front
Trigger time ms = 1518 --> Look up a little bit
Trigger time ms = 1650 --> Look to the front
Trigger time ms = 1815 --> Look up a little bit
Trigger time ms = 1980 --> Look to the front
Trigger time ms = 2178 --> Look up a little bit
Trigger time ms = 2376 --> Look to the front
Trigger time ms = 2574 --> Look up a little bit
Trigger time ms = 2772 --> Look to the front
Trigger time ms = 2970 --> Look up a little bit
Trigger time ms = 3201 --> Look to the front
Trigger time ms = 3498 --> Look up a little bit
Trigger time ms = 3729 --> Look up a little bit
Trigger time ms = 3795 --> Look up a little bit
Trigger time ms = 3828 --> Look to the front
Trigger time ms = 3894 --> Look to the front
Trigger time ms = 3993 --> Look to the front
Trigger time ms = 4092 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
