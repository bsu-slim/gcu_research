Original file = ../../data/cozmo/natural_language_converted/anim_hiccup_02
Animation anim_hiccup_playercure_pickupreact clip 6/10
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms fully
Trigger time ms = 99 --> arms to front
Trigger time ms = 165 --> lower arms fully
Trigger time ms = 264 --> lower arms fully
Trigger time ms = 1056 --> lower arms fully
Trigger time ms = 1650 --> lower arms a bit
Trigger time ms = 1749 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look to the front
Trigger time ms = 33 --> Look to the front
Trigger time ms = 66 --> Look to the front
Trigger time ms = 99 --> Look up a little bit
Trigger time ms = 231 --> Look up a little bit
Trigger time ms = 330 --> Look up a little bit
Trigger time ms = 429 --> Look to the front
Trigger time ms = 528 --> Look down a little bit
Trigger time ms = 1056 --> Look down a little bit
Trigger time ms = 1287 --> Look to the front
Trigger time ms = 1518 --> Look down a little bit
Trigger time ms = 1716 --> Look all the way down
Trigger time ms = 1848 --> Look down a little bit
Trigger time ms = 1980 --> Look all the way down
Trigger time ms = 2079 --> Look all the way down
Trigger time ms = 2112 --> Look all the way down
Trigger time ms = 2211 --> Look down a little bit
Trigger time ms = 2607 --> Look to the front
Trigger time ms = 2772 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 1551 --> walk back  medium fast
