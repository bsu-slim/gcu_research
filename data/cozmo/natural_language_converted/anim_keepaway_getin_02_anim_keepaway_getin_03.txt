Original file = ../../data/cozmo/natural_language_converted/anim_keepaway_getin_02
Animation anim_keepaway_getin_03 clip 3/3
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 594 --> arms to front
Trigger time ms = 660 --> lower arms fully
Trigger time ms = 726 --> arms to front
Trigger time ms = 792 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 264 --> Look all the way down
Trigger time ms = 1221 --> Look down a little bit
Trigger time ms = 1386 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 198 --> walk back  medium slow
Trigger time ms = 330 --> walk forward  medium slow
