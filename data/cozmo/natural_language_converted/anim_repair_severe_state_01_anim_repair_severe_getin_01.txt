Original file = ../../data/cozmo/natural_language_converted/anim_repair_severe_state_01
Animation anim_repair_severe_getin_01 clip 1/7
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 165 --> arms to front
Trigger time ms = 264 --> lower arms a bit
Trigger time ms = 4389 --> arms to front
Trigger time ms = 5181 --> arms to front
Trigger time ms = 5214 --> arms to front
Trigger time ms = 5280 --> raise arms fully
Trigger time ms = 5379 --> raise arms fully
Trigger time ms = 6303 --> raise arms fully
Trigger time ms = 6336 --> raise arms fully
Trigger time ms = 6435 --> raise arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look up a little bit
Trigger time ms = 165 --> Look to the front
Trigger time ms = 363 --> Look to the front
Trigger time ms = 396 --> Look to the front
Trigger time ms = 429 --> Look to the front
Trigger time ms = 462 --> Look to the front
Trigger time ms = 495 --> Look to the front
Trigger time ms = 528 --> Look to the front
Trigger time ms = 561 --> Look to the front
Trigger time ms = 594 --> Look to the front
Trigger time ms = 627 --> Look to the front
Trigger time ms = 660 --> Look to the front
Trigger time ms = 693 --> Look to the front
Trigger time ms = 726 --> Look to the front
Trigger time ms = 759 --> Look down a little bit
Trigger time ms = 792 --> Look to the front
Trigger time ms = 825 --> Look down a little bit
Trigger time ms = 858 --> Look to the front
Trigger time ms = 891 --> Look down a little bit
Trigger time ms = 924 --> Look up a little bit
Trigger time ms = 1023 --> Look to the front
Trigger time ms = 1353 --> Look down a little bit
Trigger time ms = 1749 --> Look all the way down
Trigger time ms = 2013 --> Look all the way down
Trigger time ms = 2079 --> Look down a little bit
Trigger time ms = 2178 --> Look down a little bit
Trigger time ms = 3465 --> Look all the way down
Trigger time ms = 4191 --> Look all the way down
Trigger time ms = 4851 --> Look all the way down
Trigger time ms = 5214 --> Look down a little bit
Trigger time ms = 5346 --> Look all the way down
Trigger time ms = 5478 --> Look down a little bit
Trigger time ms = 5610 --> Look all the way down
Trigger time ms = 6765 --> Look all the way down
Trigger time ms = 6864 --> Look all the way down
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> walk back  medium fast
Trigger time ms = 198 --> walk forward  fast
Trigger time ms = 264 --> walk back  medium fast
Trigger time ms = 429 --> walk back  medium slow
Trigger time ms = 462 --> walk forward  medium fast
Trigger time ms = 528 --> walk back  fast
Trigger time ms = 561 --> walk forward  medium fast
Trigger time ms = 594 --> walk back  medium fast
Trigger time ms = 627 --> walk forward  medium fast
Trigger time ms = 660 --> walk back  medium fast
Trigger time ms = 693 --> walk forward  medium fast
Trigger time ms = 726 --> walk back  medium slow
Trigger time ms = 759 --> walk forward  medium slow
Trigger time ms = 792 --> walk back  medium slow
Trigger time ms = 825 --> walk forward  slow
Trigger time ms = 990 --> walk forward  medium fast
Trigger time ms = 1023 --> walk back  fast
Trigger time ms = 1353 --> walk back  medium fast
Trigger time ms = 1386 --> walk forward  medium slow
Trigger time ms = 1419 --> walk forward  slow
Trigger time ms = 1749 --> walk forward  medium fast
Trigger time ms = 1782 --> walk back  medium fast
Trigger time ms = 1815 --> walk back  slow
Trigger time ms = 2178 --> walk back  medium fast
Trigger time ms = 2211 --> walk back turning slow
Trigger time ms = 5049 --> walk back turning medium slow
Trigger time ms = 5115 --> walk back turning medium slow
Trigger time ms = 5181 --> walk forward turning fast
Trigger time ms = 5214 --> walk back turning fast
Trigger time ms = 5280 --> walk forward turning medium slow
Trigger time ms = 5379 --> walk back turning medium slow
Trigger time ms = 5445 --> walk back turning medium slow
Trigger time ms = 5511 --> walk back turning medium slow
Trigger time ms = 6171 --> walk back turning slow
Trigger time ms = 6237 --> walk back turning slow
Trigger time ms = 6303 --> walk back turning fast
Trigger time ms = 6336 --> walk forward turning medium slow
Trigger time ms = 6435 --> walk back turning medium slow
Trigger time ms = 6501 --> walk back turning medium slow
