Original file = ../../data/cozmo/natural_language_converted/anim_cozmosays_inout_v01
Animation anim_cozmosays_getin_medium_01_head_angle_20 clip 11/24
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 1518 --> lower arms fully
Trigger time ms = 1650 --> lower arms fully
Trigger time ms = 1815 --> lower arms fully
Trigger time ms = 1881 --> lower arms fully
Trigger time ms = 2244 --> raise arms fully
Trigger time ms = 2409 --> raise arms fully
Trigger time ms = 2442 --> arms to front
Trigger time ms = 2607 --> arms to front
Trigger time ms = 2772 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 66 --> Look up a little bit
Trigger time ms = 132 --> Look all the way up
Trigger time ms = 198 --> Look all the way up
Trigger time ms = 264 --> Look all the way up
Trigger time ms = 462 --> Look all the way up
Trigger time ms = 825 --> Look all the way up
Trigger time ms = 957 --> Look all the way up
Trigger time ms = 1155 --> Look up a little bit
Trigger time ms = 1254 --> Look up a little bit
Trigger time ms = 1848 --> Look all the way up
Trigger time ms = 2178 --> Look all the way up
Trigger time ms = 2541 --> Look to the front
Trigger time ms = 2640 --> Look all the way up
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> walk back  medium fast
Trigger time ms = 297 --> walk back  slow
Trigger time ms = 363 --> walk back  slow
Trigger time ms = 2508 --> walk back  medium slow
Trigger time ms = 2607 --> walk forward  fast
