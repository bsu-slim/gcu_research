Original file = ../../data/cozmo/natural_language_converted/anim_pounce_success_fail_variations
Animation anim_pounce_fail_03 clip 3/4
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 0 --> lower arms a bit
Trigger time ms = 198 --> raise arms fully
Trigger time ms = 396 --> raise arms fully
Trigger time ms = 1320 --> raise arms fully
Trigger time ms = 1419 --> raise arms fully
Trigger time ms = 1914 --> raise arms fully
Trigger time ms = 1980 --> raise arms fully
Trigger time ms = 2046 --> raise arms fully
Trigger time ms = 2112 --> lower arms a bit
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 0 --> Look down a little bit
Trigger time ms = 165 --> Look to the front
Trigger time ms = 198 --> Look to the front
Trigger time ms = 231 --> Look up a little bit
Trigger time ms = 297 --> Look down a little bit
Trigger time ms = 627 --> Look all the way down
Trigger time ms = 726 --> Look down a little bit
Trigger time ms = 1320 --> Look all the way down
Trigger time ms = 1386 --> Look all the way down
Trigger time ms = 1683 --> Look all the way down
Trigger time ms = 1716 --> Look all the way down
Trigger time ms = 1749 --> Look all the way down
Trigger time ms = 1782 --> Look all the way down
Trigger time ms = 1881 --> Look all the way up
Trigger time ms = 2013 --> Look down a little bit
Trigger time ms = 2046 --> Look all the way up
Trigger time ms = 2079 --> Look to the front
Trigger time ms = 2112 --> Look all the way up
Trigger time ms = 3366 --> Look all the way up
Trigger time ms = 3498 --> Look all the way down
Trigger time ms = 3663 --> Look down a little bit
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 165 --> walk forward  medium fast
Trigger time ms = 198 --> walk back  fast
Trigger time ms = 264 --> walk forward turning medium fast
Trigger time ms = 363 --> walk forward turning slow
Trigger time ms = 495 --> walk forward turning slow
Trigger time ms = 561 --> walk forward turning slow
Trigger time ms = 627 --> walk back  medium fast
Trigger time ms = 660 --> walk forward turning slow
Trigger time ms = 726 --> stop walking
Trigger time ms = 1584 --> walk forward turning slow
Trigger time ms = 1848 --> walk back turning medium slow
Trigger time ms = 1881 --> walk back turning medium fast
Trigger time ms = 1914 --> walk forward  medium fast
Trigger time ms = 1947 --> walk forward turning medium fast
Trigger time ms = 1980 --> walk back turning slow
Trigger time ms = 2046 --> walk forward turning slow
Trigger time ms = 2112 --> walk back turning slow
Trigger time ms = 2178 --> turn back in place medium slow
Trigger time ms = 3465 --> turn forward in place fast
Trigger time ms = 3498 --> turn back in place fast
Trigger time ms = 3564 --> turn forward in place fast
