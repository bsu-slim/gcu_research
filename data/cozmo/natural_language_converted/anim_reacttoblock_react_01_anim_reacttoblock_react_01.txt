Original file = ../../data/cozmo/natural_language_converted/anim_reacttoblock_react_01
Animation anim_reacttoblock_react_01 clip 1/5
---------------------------------------
Process lift height
---------------------------------------
Trigger time ms = 660 --> arms to front
Trigger time ms = 792 --> lower arms fully
---------------------------------------
Process Head angle animations
---------------------------------------
Trigger time ms = 726 --> Look up a little bit
Trigger time ms = 858 --> Look down a little bit
Trigger time ms = 957 --> Look down a little bit
Trigger time ms = 1056 --> Look up a little bit
Trigger time ms = 1254 --> Look up a little bit
Trigger time ms = 1650 --> Look to the front
Trigger time ms = 1716 --> Look up a little bit
Trigger time ms = 1782 --> Look to the front
Trigger time ms = 1848 --> Look up a little bit
Trigger time ms = 1947 --> Look up a little bit
Trigger time ms = 2772 --> Look to the front
Trigger time ms = 2970 --> Look to the front
---------------------------------------
Process body motion animations
---------------------------------------
Trigger time ms = 693 --> walk forward  slow
Trigger time ms = 825 --> walk back  medium fast
Trigger time ms = 1023 --> walk forward  medium slow
Trigger time ms = 1188 --> walk forward  slow
